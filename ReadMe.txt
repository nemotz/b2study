■目標
・共通部分を最新状態に保てるよう管理していきます
・縦/横 切り替えができるように維持します

■開発
・android-ndk-r8b
・cocos2dx-2.1.5


■ベースのプロジェクト
・2013/08/22時点のドリルマンを基準に新規作成しています。


■JoyStick
・SneakyInput-Cocos2dx-2.0.x を追加しました。
  ... HelloWorld に addChild し、入力値を GameLayer にDelegateで通知します。

      PrivateConfig.h で以下のフラグのコメントを外してください。
        //JoyStickをつかうかどうか
        //#define USE_JOYSTICK

      画像はファイルで用意しました。
      画像をテクスチャーに含める場合は JoyStickLayer の記述を修正してください。


■縦横対応
・(iOS)アプリ.xcodeproj
  ... PROJECT の TARGET を選択し、[Supported Interface Orientations] を設定
      縦: Portrait を選択
      横: Landscape Left, Landscape Right を選択
      (Info.plist にArrayで優先順に登録されるみたいで, Right, Left の順で設定するとスプラッシュの向きと一致します。)

・(iOS)RootViewController
  ... 縦横用の定義を用意しました。(横のときはコメントにしてください。)
      縦: #define APP_ORIENTATION_IS_PORTRAIT
      横: //#define APP_ORIENTATION_IS_PORTRAIT

・(iOS)HouseAdConfig.h
  ... 以下の定義を設定
      縦: #define APP_ORIENTATION    @"PORTRAIT"
      横: #define APP_ORIENTATION  @"LANDSCAPE"

・(iOS)スプラッシュ用画像
  ... 縦横用の画像に差し替え
      ・Default-568h@2x.png
      ・Default.png
      ・Default@2x.png

・(Android)AndroidManifest.xml
  ... アクティビティの設定
      縦: android:screenOrientation="portrait"
      横: android:screenOrientation="landscape"
