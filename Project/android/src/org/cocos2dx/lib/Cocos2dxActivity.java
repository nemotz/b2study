/****************************************************************************
Copyright (c) 2010-2013 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package org.cocos2dx.lib;

import java.util.List;

import jp.basicinc.gamefeat.android.sdk.controller.GameFeatAppController;
import jp.beyond.sdk.Bead;
import jp.co.goodia.TemplateProject215.Asta;
import jp.co.goodia.TemplateProject215.CheckHouseadJson;
import jp.co.goodia.TemplateProject215.rankplat.RankPlatDataManager;
import jp.co.goodia.TemplateProject215.rankplat.RankPlatService;
import jp.co.goodia.TemplateProject215.rankplat.RankingDialogFragment;
import jp.co.imobile.android.AdIconViewParams;
import jp.live_aid.aid.AdController;
import jp.maru.mrd.IconLoader;

import org.cocos2dx.lib.Cocos2dxHelper.Cocos2dxHelperListener;

import com.amoad.amoadsdk.AMoAdSdk;
import com.amoad.amoadsdk.AMoAdSdkWallActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.Gravity;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public abstract class Cocos2dxActivity extends FragmentActivity implements Cocos2dxHelperListener{

	//(me == this)
	protected static FragmentActivity me = null;

	// ===========================================================
	// Constants
	// ===========================================================
	protected FrameLayout mFramelayout;

	private static final String TAG = Cocos2dxActivity.class.getSimpleName();

	// ===========================================================
	// Fields
	// ===========================================================
	
	private Cocos2dxGLSurfaceView mGLSurfaceView;
	private Cocos2dxHandler mHandler;
	private static Context sContext = null;
	
	public static Context getContext(){
		return sContext;
	}
	
	// ===========================================================
	// Orientation
	// ===========================================================
	protected static Boolean isPortrait = true;

	// ===========================================================
	// Life cycle
	// ===========================================================
	
	@Override
	protected void onCreate(final Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		sContext = this;

		//縦横取得
		WindowManager windowManager = getWindowManager();
		Display display = windowManager.getDefaultDisplay();
		DisplayMetrics displayMetrics = new DisplayMetrics();
		display.getMetrics(displayMetrics);

		Log.d(TAG, "Rotation:" + display.getRotation());

		if(display.getRotation() == Surface.ROTATION_0){
		 	Log.d(TAG, "Rotation constant: 0 degree rotation (natural orientation)");
		 	isPortrait = true;
		 }else if(display.getRotation() == Surface.ROTATION_90){
		 	Log.d(TAG, "Rotation constant: 90 degree rotation");
		 	isPortrait = false;
		 }else if(display.getRotation() == Surface.ROTATION_180){
		 	Log.d(TAG, "Rotation constant: 180 degree rotation");
		 	isPortrait = true;
		 }else if(display.getRotation() == Surface.ROTATION_270){
		 	Log.d(TAG, "Rotation constant: 270 degree rotation");
		 	isPortrait = false;
		 }
		
		this.mHandler = new Cocos2dxHandler(this);

		this.init();

		Cocos2dxHelper.init(this, this);
		
		//インスタンス変数 -> static変数
		me = this;
	}

	@Override
	protected void onStart(){
		super.onStart();
	}

	@Override
	protected void onStop(){
		super.onStop();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
	}

	@Override
	protected void onResume(){
		super.onResume();

		Cocos2dxHelper.onResume();
		this.mGLSurfaceView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();

		Cocos2dxHelper.onPause();
		this.mGLSurfaceView.onPause();
	}

	@Override
	public void showDialog(final String pTitle, final String pMessage) {
		Message msg = new Message();
		msg.what = Cocos2dxHandler.HANDLER_SHOW_DIALOG;
		msg.obj = new Cocos2dxHandler.DialogMessage(pTitle, pMessage);
		this.mHandler.sendMessage(msg);
	}

	@Override
	public void showEditTextDialog(final String pTitle, final String pContent, final int pInputMode, final int pInputFlag, final int pReturnType, final int pMaxLength) { 
		Message msg = new Message();
		msg.what = Cocos2dxHandler.HANDLER_SHOW_EDITBOX_DIALOG;
		msg.obj = new Cocos2dxHandler.EditBoxMessage(pTitle, pContent, pInputMode, pInputFlag, pReturnType, pMaxLength);
		this.mHandler.sendMessage(msg);
	}
	
	@Override
	public void runOnGLThread(final Runnable pRunnable) {
		this.mGLSurfaceView.queueEvent(pRunnable);
	}

	// ===========================================================
	// Methods
	// ===========================================================
	public void init(){
		
		// FrameLayout
		ViewGroup.LayoutParams framelayout_params =
				new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.FILL_PARENT);
		mFramelayout = new FrameLayout(this);
		mFramelayout.setLayoutParams(framelayout_params);

		// Cocos2dxEditText layout
		ViewGroup.LayoutParams edittext_layout_params =
				new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
		Cocos2dxEditText edittext = new Cocos2dxEditText(this);
		edittext.setLayoutParams(edittext_layout_params);

		// ...add to FrameLayout
		mFramelayout.addView(edittext);

		// Cocos2dxGLSurfaceView
		this.mGLSurfaceView = this.onCreateView();

		// ...add to FrameLayout
		mFramelayout.addView(this.mGLSurfaceView);

		// Switch to supported OpenGL (ARGB888) mode on emulator
		if(isAndroidEmulator())
			mGLSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);

		mGLSurfaceView.setCocos2dxRenderer(new Cocos2dxRenderer());
		mGLSurfaceView.setCocos2dxEditText(edittext);

		// Set framelayout as the content view
		setContentView(mFramelayout);
	}
	
	public Cocos2dxGLSurfaceView onCreateView() {
		return new Cocos2dxGLSurfaceView(this);
	}

	private final static boolean isAndroidEmulator(){
		String model = Build.MODEL;
		Log.d(TAG, "model=" + model);
		String product = Build.PRODUCT;
		Log.d(TAG, "product=" + product);
		boolean isEmulator = false;
		if (product != null) {
			isEmulator = product.equals("sdk") || product.contains("_sdk") || product.contains("sdk_");
		}
		Log.d(TAG, "isEmulator=" + isEmulator);
		return isEmulator;
	}
}
