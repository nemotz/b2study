package jp.co.goodia.TemplateProject215;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.cocos2dx.lib.Cocos2dxActivity;

import com.ad_stir.AdstirTerminate;
import com.ad_stir.AdstirView;
import jp.maru.mrd.IconLoader;

public class AstaActivity extends AdstirActivity{

	private static final String TAG = "AstaActivity";

	// ===========================================================
	// アイコン広告 astrisk
	// ===========================================================
	/** Asta */
	// ジャストのサイズにする
	private final int FP = LinearLayout.LayoutParams.MATCH_PARENT;

	// 広告レイアウト
	protected static LinearLayout iconAdView;			//タイトル画面用
	protected static LinearLayout iconAdViewRight;	//エンディング画面用
	protected static LinearLayout iconAdViewLeft;		//エンディング画面用

	// アイコン型広告
	private IconLoader<Integer> iconAdTitle;	//タイトル画面用
	private IconLoader<Integer> iconAdEnding;	//エンディング画面用
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		// Asta
		// アイコン型広告処理
		RelativeLayout adMain = new  RelativeLayout(this);
		this.initIconAd();

		// 広告用RelativeLayoutに追加←RelativeLayoutがキーポイント！
		adMain.addView(iconAdView);
		adMain.addView(iconAdViewRight);
		adMain.addView(iconAdViewLeft);

		// ...add to FrameLayout
		mFramelayout.addView(adMain);
	}

	@Override
	protected void onStart(){
		super.onStart();
	}

	@Override
	protected void onStop(){
		super.onStop();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
	}

	@Override
	protected void onResume(){
		super.onResume();

		//Asta
		{
			iconAdTitle.startLoading();
			iconAdEnding.startLoading();
		}
	}
	
	@Override
	protected void onPause(){
		super.onPause();

		//Asta
		{
			iconAdTitle.stopLoading();
			iconAdEnding.stopLoading();
		}
	}

	private void initIconAd(){
		
		// アイコン型広告識別コード設定
		iconAdTitle = new IconLoader<Integer>(_MEDIA_CODE_TITLE, this);
		iconAdEnding = new IconLoader<Integer>(_MEDIA_CODE_ENDING, this);
		
		// 更新インターバル15秒
		iconAdTitle.setRefreshInterval(15);
		iconAdEnding.setRefreshInterval(15);
		
		// アイコンサイズ指定
		int iconSize =  (int)(80 * getResources().getDisplayMetrics().density);
	
		// タイトル画面
		iconAdView = new LinearLayout(this); 
		
		// 広告のサイズを指定
		RelativeLayout.LayoutParams layoutParams3 = 
				new RelativeLayout.LayoutParams(FP,iconSize);

		// Viewにレイアウトパラメータをセット
		iconAdView.setGravity(Gravity.CENTER_HORIZONTAL);
		iconAdView.setLayoutParams(layoutParams3);

		// 上部に自社アイコンを設置
		int iconMax;
		if (isPortrait == true){
			iconMax = 5;
		} else {
			iconMax = 7;
		}
		for (int ii=0; ii<iconMax; ii++) {
			final LinearLayout iconAdSubView = new LinearLayout(this);
			LinearLayout.LayoutParams layoutSubParams = new LinearLayout.LayoutParams(1, iconSize, 1.0f);
			iconAdSubView.setLayoutParams(layoutSubParams);
			
			// ここでIconAd.javaを呼び出します
			new Asta(this, iconAdSubView, iconAdTitle);
			
			// 設定したアイコンデータをViewに追加する
			iconAdView.addView(iconAdSubView);
		}
		
		// 最初は広告を非表示にする
		iconAdView.setVisibility(View.GONE);

		
		// エンディング画面
		iconAdViewRight = new LinearLayout(this);
		iconAdViewLeft = new LinearLayout(this);
		
		if (isPortrait == true){
			// 縦画面の場合
			// 広告のサイズを指定
			RelativeLayout.LayoutParams layoutParams1 = 
					new RelativeLayout.LayoutParams(iconSize,iconSize * 3);
			RelativeLayout.LayoutParams layoutParams2 = 
					new RelativeLayout.LayoutParams(iconSize,iconSize * 3);
			
			// 広告の表示位置を指定
			layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			layoutParams1.addRule(RelativeLayout.CENTER_VERTICAL);
			layoutParams2.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			layoutParams2.addRule(RelativeLayout.CENTER_VERTICAL);
			
			// Viewにレイアウトパラメータをセット
			iconAdViewRight.setLayoutParams(layoutParams1);
			iconAdViewRight.setOrientation(LinearLayout.VERTICAL);
			iconAdViewLeft.setLayoutParams(layoutParams2);
			iconAdViewLeft.setOrientation(LinearLayout.VERTICAL);
			
			// 右側に3つのアイコンを設置
			for (int ii=0; ii<3; ii++) {
				final LinearLayout iconAdSubView1 = new LinearLayout(this);
				LinearLayout.LayoutParams layoutSubParams1 = new LinearLayout.LayoutParams(iconSize, iconSize*3, 1.0f);
				iconAdSubView1.setLayoutParams(layoutSubParams1);
				
				// ここでIconAd.javaを呼び出します
				new Asta(this, iconAdSubView1, iconAdEnding);
				
				// 設定したアイコンデータをViewに追加する
				iconAdViewRight.addView(iconAdSubView1);
			}

			// 左側に3つのアイコンを設置
			for (int ii=0; ii<3; ii++) {
				final LinearLayout iconAdSubView2 = new LinearLayout(this);
				LinearLayout.LayoutParams layoutSubParams2 = new LinearLayout.LayoutParams(iconSize, iconSize*3, 1.0f);
				iconAdSubView2.setLayoutParams(layoutSubParams2);
				
				// ここでIconAd.javaを呼び出します
				new Asta(this, iconAdSubView2, iconAdEnding);
				
				// 設定したアイコンデータをViewに追加する
				iconAdViewLeft.addView(iconAdSubView2);
			}
			
		} else {
			// 横画面の場合
			// 広告のサイズを指定
			RelativeLayout.LayoutParams layoutParams1 = 
					new RelativeLayout.LayoutParams(iconSize * 2,(int)(iconSize*1.5));
			RelativeLayout.LayoutParams layoutParams2 = 
					new RelativeLayout.LayoutParams(iconSize * 2,(int)(iconSize*1.5));
			
			// 広告の表示位置を指定
			layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			layoutParams1.addRule(RelativeLayout.CENTER_VERTICAL);
			layoutParams2.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			layoutParams2.addRule(RelativeLayout.CENTER_VERTICAL);
			
			// Viewにレイアウトパラメータをセット
			iconAdViewRight.setLayoutParams(layoutParams1);
			iconAdViewLeft.setLayoutParams(layoutParams2);
			
			// 右側に2つのアイコンを設置
			for (int ii=0; ii<2; ii++) {
				final LinearLayout iconAdSubView1 = new LinearLayout(this);
				LinearLayout.LayoutParams layoutSubParams1 = new LinearLayout.LayoutParams(1, iconSize, 1.0f);
				iconAdSubView1.setLayoutParams(layoutSubParams1);
				
				// ここでIconAd.javaを呼び出します
				new Asta(this, iconAdSubView1, iconAdEnding);
				
				// 設定したアイコンデータをViewに追加する
				iconAdViewRight.addView(iconAdSubView1);
			}
			
			// 左側に2つのアイコンを設置
			for (int ii=0; ii<2; ii++) {
				final LinearLayout iconAdSubView2 = new LinearLayout(this);
				LinearLayout.LayoutParams layoutSubParams2 = new LinearLayout.LayoutParams(1, iconSize, 1.0f);
				iconAdSubView2.setLayoutParams(layoutSubParams2);
				
				// ここでIconAd.javaを呼び出します
				new Asta(this, iconAdSubView2, iconAdEnding);
				
				// 設定したアイコンデータをViewに追加する
				iconAdViewLeft.addView(iconAdSubView2);
			}
		}

		// 最初は広告を非表示にする
		iconAdViewRight.setVisibility(View.GONE);
		iconAdViewLeft.setVisibility(View.GONE);
	}
}
