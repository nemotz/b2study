package jp.co.goodia.TemplateProject215;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import org.cocos2dx.lib.Cocos2dxActivity;

import com.flurry.android.FlurryAgent;

public class FlurryActivity extends HouseAdActivity{

	private static final String TAG = "FlurryActivity";

 	// ===========================================================
	// 解析　Flurry
	// ===========================================================
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStart() {
		super.onStart();

		// Flurry
		FlurryAgent.onStartSession(this, FLURRY_API_KEY);
	}

	@Override
	protected void onStop() {
		super.onStop();
		
		// Flurry
		FlurryAgent.onEndSession(this);
	}

	// Flurry
	public static void reportGameCountToFlurry(int count){
		String strEvent = "PLAY_COUNT:" + count;
		if(count <= 10){
			FlurryAgent.logEvent(strEvent);
		}else if(count <= 100 && count % 10 == 0){
			FlurryAgent.logEvent(strEvent);
		}else if(count <= 1000 && count % 100 == 0){
			FlurryAgent.logEvent(strEvent);
		}else if(count <= 10000 && count % 1000 == 0){
			FlurryAgent.logEvent(strEvent);
		}else if(count <= 100000 && count % 10000 == 0){
			FlurryAgent.logEvent(strEvent);
		}
	}
	
	public static void reportFlurryWithEvent(String eventName){
		Log.d(TAG, "reportFlurryWithEvent:" + eventName);
		FlurryAgent.logEvent(eventName);
	}
}
