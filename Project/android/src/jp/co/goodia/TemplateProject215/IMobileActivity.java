package jp.co.goodia.TemplateProject215;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.cocos2dx.lib.Cocos2dxActivity;

import com.ad_stir.AdstirTerminate;
import com.ad_stir.AdstirView;

import jp.co.imobile.android.AdIconViewParams;
import jp.maru.mrd.IconLoader;

public class IMobileActivity extends AstaActivity{

	private static final String TAG = "IMobileActivity";

	// ===========================================================
	// アイコン広告 iMoblie
	// ===========================================================
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		// アイコン型広告処理
		RelativeLayout adIcon = new  RelativeLayout(this);
		
		//解像度によらないサイズ指定(高さ75dp)
		DisplayMetrics metrics = new DisplayMetrics();
		((android.view.WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);

		int width = 0;
		int height = 0;
		
		//横画面の場合
		if (isPortrait == false){
			width = (int)(400.0f * metrics.density + 0.5f);
		}
		height = (int)(75.0f * metrics.density + 0.5f);

		//横画面の場合
		if (isPortrait == false){
			adIcon.setLayoutParams(new RelativeLayout.LayoutParams(width, height));
		}
		
		this.initIconAdImobile();
		
		if(isPortrait == true){
			//縦画面の場合
			adIcon.addView(iconAdViewImobile, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,height));
		} else {
			//横画面の場合
			adIcon.addView(iconAdViewImobile, new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
		}
		
		mFramelayout.addView(adIcon);
	}

	@Override
	protected void onStart(){
		super.onStart();
	}

	@Override
	protected void onStop(){
		super.onStop();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
	}

	@Override
	protected void onResume(){
		super.onResume();
	}
	
	@Override
	protected void onPause(){
		super.onPause();
	}

	//imobile
	/**
	 * アイコン型広告初期化
	 */
	private void initIconAdImobile(){
			
		//AdIconView用表示パラメータオブジェクトの作成
		AdIconViewParams params = new AdIconViewParams(this);
		
		//AdIconView用表示パラメータオブジェクトへ表示パラメータを設定
		params.setIconSize(57);
		params.setIconGravity(Gravity.CENTER_HORIZONTAL);
		
		int iconNum = 0;
		
		if (isPortrait){
			iconNum = 4;
		} else {
			iconNum = 5;
		}
		
		//AdIconView作成
		this.iconAdViewImobile = jp.co.imobile.android.AdIconView.create(this, IMOBILE_MEDIA_ID, IMOBILE_SPOT_ID, iconNum, params);
		
		//広告配信スタート
		this.iconAdViewImobile.start();
		
		//デフォルトは非表示
		this.iconAdViewImobile.setVisibility(View.GONE);
	}
}
