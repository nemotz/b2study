package jp.co.goodia.TemplateProject215;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;

import org.cocos2dx.lib.Cocos2dxActivity;

import com.ad_stir.AdstirTerminate;
import com.ad_stir.AdstirView;

public class AdstirActivity extends GoodiaActivity{

	private static final String TAG = "AdstirActivity";

	// ===========================================================
	// バナー広告 AdStir
	// ===========================================================
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		//AdStir
		AdstirView adstirView = new AdstirView(this, _MEDIA_CODE_ADSTIR, 1);
		adstirView.setGravity(Gravity.BOTTOM | Gravity.CENTER);
		mFramelayout.addView(adstirView);
	}

	@Override
	protected void onStart(){
		super.onStart();
	}

	@Override
	protected void onStop(){
		super.onStop();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();

		AdstirTerminate.init(this);
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		
		AdstirTerminate.init(this);
	}
}
