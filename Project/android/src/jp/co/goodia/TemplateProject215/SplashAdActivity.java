package jp.co.goodia.TemplateProject215;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.util.Log;

import org.cocos2dx.lib.Cocos2dxActivity;
import jp.beyond.sdk.Bead;
import jp.live_aid.aid.AdController;
import jp.co.imobile.sdkads.android.FailNotificationReason;
import jp.co.imobile.sdkads.android.ImobileSdkAd;
import jp.co.imobile.sdkads.android.ImobileSdkAdListener;

public class SplashAdActivity extends FlurryActivity{

	private static final String TAG = "SplashAdActivity";

	// ===========================================================
	// CheckSplSpfJson
	// ===========================================================
	private static CheckSplSpfJson cssj;

	// ===========================================================
	// ダイアログ広告 AID
	// ===========================================================
	/** AID */
	private static AdController  _adController;

	// ===========================================================
	// ダイアログ広告 BEAD
	// ===========================================================
	/** Exit版BEAD */
	private static Bead mBeadExit = null;

	/** Optional版BEAD */
	private static Bead mBeadOptional = null;
	
	// ===========================================================
	// ダイアログ広告 i-mobile(内部クラス)※ID、Pass未実装
	// ===========================================================
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);

		// CheckSplSpfJson
		cssj = new CheckSplSpfJson();

		// Bead
		{
			// アプリ終了時に表示するBEADを生成し、広告取得を開始
			mBeadExit = Bead.createExitInstance(this.BeadSID);
			mBeadExit.requestAd(this);

			// 表示するBEADを生成し、広告取得を開始
			mBeadOptional = Bead.createOptionalInstance(this.BeadSID, 0);
			mBeadOptional.requestAd(this);

			// アプリ終了ボタンを押した時のイベント
			mBeadExit.setOnFinishClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// 広告処理終了
					mBeadExit.endAd();
					mBeadOptional.endAd();

					// アプリを終了
					android.os.Process.killProcess(android.os.Process.myPid());
				}
			});
		}

		// AID
		{
			if(_adController == null)
			{
				// AdControllerを生成
				_adController = new AdController(_MEDIA_CODE_AID, this);
			}
		}
		
		// i-mobile
		{
			ImobileSdkAd.registerSpot(me, SpotParams.PUBLISHER_ID, SpotParams.MEDIA_ID, SpotParams.SPOT_ID);
			
			ImobileSdkAd.setImobileSdkAdListener(SpotParams.SPOT_ID, new ImobileSdkAdListener() {
				@Override
				public void onAdReadyCompleted(){
					Log.d(TAG, "i-mobile App 正常 コールバック 広告の準備ができました");
				}
				@Override
				public void onAdShowCompleted() {
					Log.d(TAG, "i-mobile 広告表示完了");
					if(!ImobileSdkAd.isShowAd(SpotParams.SPOT_ID)) {
						Log.d(TAG, "i-mobile 広告準備中");
					}
					Log.d(TAG, "i-mobile App 正常 コールバック 広告が表示されました");
				}
				@Override
				public void onAdCliclkCompleted() {
					Log.d(TAG, "i-mobile App 正常 コールバック 広告のがクリックされました");
				}
				@Override
				public void onAdCloseCompleted() {
					Log.d(TAG, "i-mobile App 広告クローズ");
				}
				@Override
				public void onFailed(FailNotificationReason reason) {
					Log.d(TAG, "i-mobile App エラーコールバック:" + reason.toString());
				}
			});
			
			ImobileSdkAd.start(SpotParams.SPOT_ID);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		// AID
		{
			// アプリがアクティブになったら、広告を先読み込みする
			_adController.startPreloading();
		}
	}

	@Override
	protected void onPause(){
		super.onPause();
		
		// AID
		{
			// Activityがバックグラウンドに行ったら、不要な通信を止める
			_adController.stopPreloading();
		}
	}
	
	@Override
	protected void onDestroy(){
		
		super.onDestroy();

		// Bead
		{
			// 広告処理終了
			mBeadExit.endAd();
			mBeadOptional.endAd();
		}
		
		// I-mobile
		{
			ImobileSdkAd.activityDestory();
		}
	}
	
	// Bead/Aid Optional
	public static void showOptionalAd(int adCount){
		
		Log.d(TAG, "showOptionalAd:" + adCount);
		
		Log.d(TAG, "adSplashFrequency:" + cssj.getAdSplashFrequencyStrStr());
		
		int frequencyCount = Integer.parseInt(cssj.getAdSplashFrequencyStrStr());
		int modCount = 0;
		
		if(frequencyCount == 0){
			// 0の場合は、3回に1回の割合で発生
			modCount = adCount % 3;
		}else{
			// 0以外の場合は、frequencyCountの回数毎に発生
			modCount = adCount % frequencyCount;
		}
		
		if(modCount == 0){
			
			String type = cssj.getSplashTypeStr();
			
			// Bead / Aid
			if(type.equals("B")){
				// Bead
				showBeadOptionalAd();
			}else if(type.equals("A")){
				// Aid
				showAidOptionalAd();
			}else if(type.equals("I")){
				// i-mobile
				showIMobileOptionalAd();
			}else{
				// Bead
				showBeadOptionalAd();
			}
		}
	}
	
	public static void showOptionalAdWall(){
		
		Log.d(TAG, "showOptionalAdWall");

		Log.d(TAG, "splashTypeWallStr:" + cssj.getSplashTypeWallStr());

		int frequencyCount = Integer.parseInt(cssj.getAdSplashFrequencyStrStr());
		
		// 0の時は表示しない
		if(frequencyCount != 0){
			
			String type = cssj.getSplashTypeWallStr();
			
			// Bead / Aid
			if(type.equals("B")){
				// Bead
				showBeadOptionalAd();
			}else if(type.equals("A")){
				// Aid
				showAidOptionalAd();
			}else if(type.equals("I")){
				// i-mobile
				showIMobileOptionalAd();
			}else{
				// Bead
				showBeadOptionalAd();
			}
		}
	}
	
	public static void showOptionalAdRank(){
		
		Log.d(TAG, "showOptionalAdRank");

		Log.d(TAG, "splashTypeRankStr:" + cssj.getSplashTypeRankStr());

		int frequencyCount = Integer.parseInt(cssj.getAdSplashFrequencyStrStr());
		
		// 0の時は表示しない
		if(frequencyCount != 0){

			String type = cssj.getSplashTypeRankStr();
			
			// Bead / Aid
			if(type.equals("B")){
				// Bead
				showBeadOptionalAd();
			}else if(type.equals("A")){
				// Aid
				showAidOptionalAd();
			}else if(type.equals("I")){
				// i-mobile
				showIMobileOptionalAd();
			}else{
				// Bead
				showBeadOptionalAd();
			}
		}
	}
	
	// Bead/Aid Exit
	public static void showExitAd(){
		
		Log.d(TAG, "showExitAd");
		
		String type = cssj.getSplashTypeStr();

		// Bead / Aid
		if(type.equals("B")){
			// Bead
			showBeadExitAd();
		}else if(type.equals("A")){
			// Aid
			showAidExitAd();
		}else{
			// Bead
			showBeadExitAd();
		}
	}

	// Bead
	public static void showBeadOptionalAd(){
		
		me.runOnUiThread(new Runnable(){
			@Override
			public void run(){
				// 広告ダイアログ表示
				mBeadOptional.showAd(me);
			}
		});
	}

	// Bead
	public static void showBeadExitAd(){
		
		me.runOnUiThread(new Runnable(){
			@Override
			public void run(){
				// 広告ダイアログ表示 (Exit広告)
				mBeadExit.showAd(me);
			}
		});
	}

	// AID
	public static void showAidOptionalAd(){
		
		me.runOnUiThread(new Runnable(){
			@Override
			public void run(){
				// 広告ダイアログ表示
				_adController.showDialog(AdController.DialogType.ON_DEMAND);
			}
		});
	}

	// AID
	public static void showAidExitAd(){
		
		me.runOnUiThread(new Runnable(){
			@Override
			public void run(){
				// 広告ダイアログ表示 (Exit広告)
				_adController.showDialog(AdController.DialogType.ON_EXIT);
			}
		});
	}

	// i-mobile
	public static void showIMobileOptionalAd(){
		
		me.runOnUiThread(new Runnable(){
			@Override
			public void run(){
				// 広告ダイアログ表示
				ImobileSdkAd.showAd(me, SpotParams.SPOT_ID);
			}
		});
	}
}
