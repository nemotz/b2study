package jp.co.goodia.TemplateProject215;

import jp.co.goodia.TemplateProject215.R;
import jp.maru.mrd.IconCell;
import jp.maru.mrd.IconLoader;
import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;

public class Asta{
	
	private Activity mActivity;
	private LinearLayout mLayout;
	private IconLoader<Integer> mIconLoader;
	
	public Asta(final Activity act, final LinearLayout layout, final IconLoader<Integer> iconLoader){
		
		mActivity = act;  
		mLayout = layout;
		mIconLoader = iconLoader;
		adInit();
	}
	
	/**
	 * 広告初期化
	 */
	private void adInit(){
		
		try{
			// ここでxmlファイルを読み込み
			View view = mActivity.getLayoutInflater().inflate(R.layout.asta, null);
			if (view instanceof IconCell){
				((IconCell)view).addToIconLoader(mIconLoader);
			}
			mLayout.addView(view);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}