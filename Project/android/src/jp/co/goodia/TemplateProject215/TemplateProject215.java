/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package jp.co.goodia.TemplateProject215;

import java.util.List;

import jp.co.goodia.TemplateProject215.CheckRecomJson;

import org.cocos2dx.lib.Cocos2dxActivity;

import com.flurry.android.FlurryAgent;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.util.Log;


public class TemplateProject215 extends TapJoyActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		//サウンド調整用
		{
			this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		}

		if(isOnline()){
			Log.d("myTag", "online");
			
			// CheckRecomJson
			CheckRecomJson cr = new CheckRecomJson();
			Log.d("myTag", "flg:" + cr.checkFlg());
			Log.d("myTag", "installed:" + this.checkInstalled(cr.getScheme()));
			if(cr.checkFlg() && !checkInstalled(cr.getScheme())){
				
				createRecomDialog(cr.getTitle(), cr.getUrl());
				
				// Flurry
				FlurryAgent.logEvent("POPUP_SHOW:" + getAppName(cr.getTitle()));
			}
		}
	}

	static
	{
		System.loadLibrary("game");
	}

	public static void showIconAd(int scene){
		
		// 1:タイトル画面 2:エンディング画面 0:それ以外
		switch (scene) {
		case 1: // タイトル画面の広告を表示
			me.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					iconAdView.setVisibility(View.VISIBLE);
					iconAdViewRight.setVisibility(View.GONE);
					iconAdViewLeft.setVisibility(View.GONE);
					iconAdViewImobile.setVisibility(View.GONE);
				}
			});
			break;

		case 2: // エンディング画面の広告を表示
			me.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					iconAdView.setVisibility(View.GONE);
					iconAdViewRight.setVisibility(View.VISIBLE);
					iconAdViewLeft.setVisibility(View.VISIBLE);
					iconAdViewImobile.setVisibility(View.VISIBLE);
				}
			});
			break;

		default: // 広告を非表示
			me.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					iconAdView.setVisibility(View.GONE);
					iconAdViewRight.setVisibility(View.GONE);
					iconAdViewLeft.setVisibility(View.GONE);
					iconAdViewImobile.setVisibility(View.GONE);
				}
			});
			break;
		}
	}

	private String getAppName(String title){
        String str = title;
        String[] strsA = str.split("「");
        String[] strsB = strsA[1].split("」");
        //Log.d("myTag", strsB[0]);

        return strsB[0];
	}
	
    private boolean checkInstalled(String scheme){
        PackageManager pm = this.getPackageManager();
        List<ApplicationInfo> infos = pm.getInstalledApplications(0);
        for(ApplicationInfo ai:infos){
            if(scheme != null && scheme.equals(ai.packageName)){
                return true;
            }
        }
        return false;
    }

    // オンラインかどうか
    public boolean isOnline(){
        ConnectivityManager cm = 
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        
        if(info != null){
            int type = info.getType();
            if(type == ConnectivityManager.TYPE_MOBILE || type == ConnectivityManager.TYPE_WIFI){
                Log.d("myTag", "NetworkInfo:" + info.getTypeName());
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    private void createRecomDialog(final String title, final String url){
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("お知らせ");
        adb.setMessage(title);
        adb.setPositiveButton("はい", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                // Flurry
                FlurryAgent.logEvent("POPUP_TAP_YES:" + getAppName(title));

                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        adb.setNegativeButton("いいえ", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
               
            }
        });
        AlertDialog ad = adb.create();
        ad.show();
    }
}
