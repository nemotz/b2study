package jp.co.goodia.TemplateProject215;

import java.util.Hashtable;
import org.cocos2dx.lib.Cocos2dxActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import com.tapjoy.TapjoyAwardPointsNotifier;
import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyConnectFlag;
import com.tapjoy.TapjoyConnectNotifier;
import com.tapjoy.TapjoyConstants;
import com.tapjoy.TapjoyDisplayAdNotifier;
import com.tapjoy.TapjoyEarnedPointsNotifier;
import com.tapjoy.TapjoyFullScreenAdNotifier;
import com.tapjoy.TapjoyLog;
import com.tapjoy.TapjoyNotifier;
import com.tapjoy.TapjoyOffersNotifier;
import com.tapjoy.TapjoySpendPointsNotifier;
import com.tapjoy.TapjoyVideoNotifier;
import com.tapjoy.TapjoyViewNotifier;
import com.tapjoy.TapjoyViewType;

public class TapJoyActivity extends SplashAdActivity{
	
	private static final String TAG = "TapJoy";

	// ===========================================================
	// TapJoy
	// ===========================================================
	
	private static boolean movieEnabled = false;
	private static boolean movieCompleted = false;
	
	private static AlertDialog ad;
	private AlertDialog.Builder adb;
	
	// Cメソッドの定義
	public native int continueToGame(boolean bool);
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);
		
		// AlertDialog
		adb = new AlertDialog.Builder(this);
		adb.setTitle("コンテニュー");
		adb.setMessage("ムービーを最後まで見ると、コンテニューが出来るよ!");
		adb.setPositiveButton("OK", 
				new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// DialogでOKを押した場合
				TapjoyConnect.getTapjoyConnectInstance().showFullScreenAd();
			}
		});
		ad = adb.create();
		
		// TapJoy
		Hashtable<String, String> flags = new Hashtable<String, String>();
		flags.put(TapjoyConnectFlag.ENABLE_LOGGING, "true");
		
		TapjoyConnect.requestTapjoyConnect(
				getApplicationContext(),
				tapjoyAppID, 
				tapjoySecretKey, 
				flags, new TapjoyConnectNotifier(){
			
			@Override
			public void connectSuccess(){
				onConnectSuccess();
			}
			
			@Override
			public void connectFail() {
				onConnectFail();
			}
		});
	}
	
	private void onConnectSuccess(){
		Log.d(TAG, "onConnectSuccess");
		
		// Get notifications whenever Tapjoy currency is earned.
		TapjoyConnect.getTapjoyConnectInstance().setEarnedPointsNotifier(
				new TapjoyEarnedPointsNotifier(){
			@Override
			public void earnedTapPoints(int amount){
				Log.d(TAG, "You've just earned " + amount + " Tap Points!");
			}
		});

		TapjoyConnect.getTapjoyConnectInstance().setVideoNotifier(
				new TapjoyVideoNotifier(){
					@Override
					public void videoComplete() {
						// TODO Auto-generated method stub
						Log.d(TAG, "videoComplete");
						movieCompleted = true;
					}

					@Override
					public void videoError(int arg0) {
						// TODO Auto-generated method stub
						Log.d(TAG, "videoError");
						movieCompleted = false;
					}

					@Override
					public void videoStart() {
						// TODO Auto-generated method stub
						Log.d(TAG, "videoStart");
						movieCompleted = false;
					}
				}
			);
		
		// Get notifications when Tapjoy views open or close.
		TapjoyConnect.getTapjoyConnectInstance().setTapjoyViewNotifier(
				new TapjoyViewNotifier(){
			@Override
			public void viewWillOpen(int viewType){
				Log.i(TAG, "viewWillOpen:" + viewType);
				
				if(viewType == TapjoyViewType.FULLSCREEN_AD){
					Log.d(TAG, "TapjoyViewType.FULLSCREEN_AD");
				}
			}
			
			@Override
			public void viewWillClose(int viewType){
				Log.i(TAG, "viewWillClose:" + viewType);

				if(viewType == TapjoyViewType.FULLSCREEN_AD){
					Log.d(TAG, "TapjoyViewType.FULLSCREEN_AD");
				}
			}
			
			@Override
			public void viewDidOpen(int viewType){
				Log.i(TAG, "viewDidOpen:" + viewType);

				if(viewType == TapjoyViewType.FULLSCREEN_AD){
					Log.d(TAG, "TapjoyViewType.FULLSCREEN_AD");
				}
			}
			
			@Override
			public void viewDidClose(int viewType){
				Log.i(TAG, "viewDidClose:" + viewType);

				if(viewType == TapjoyViewType.FULLSCREEN_AD){
					Log.d(TAG, "TapjoyViewType.FULLSCREEN_AD");

					if(movieCompleted == true){
						// 全てのポイントを消費
						spendAllPoints();
					}

					// コンテニューへ(ムービーを見ていれば)
					continueToGame(movieCompleted);
				}
			}
		});
		
		// 広告の在庫を確認する
		TapjoyConnect.getTapjoyConnectInstance().getFullScreenAd(
				new TapjoyFullScreenAdNotifier(){
			@Override
			public void getFullScreenAdResponse(){
				Log.i(TAG, "getFullScreenAdResponse:OK");
				movieEnabled = true;
			}
			@Override
			public void getFullScreenAdResponseFailed(int error){
				Log.e(TAG, "getFullScreenAdResponseFailed:NG_" + error);
				movieEnabled = false;
			}
		});
		
		//==========
		// updatePoints
		//this.updatePoints();
		
		//==========
		// spendPoints
		//this.spendPoints(25);
		
		//==========
		// showFullScreenAd
		//this.showFullScreenAd();
	}
	
	private void onConnectFail(){
		Log.e(TAG, "onConnectFail");
	}
	
	// Movieの在庫確認
	public static boolean getMovieEnabled(){
		
		return movieEnabled;
	}
	
	// Adの表示
	public static void showFullScreenAd(){
		Log.d(TAG, "showFullScreenAd");
		Log.i(TAG, "movieEnabled:" + movieEnabled);
		
		if(movieEnabled == true){

			me.runOnUiThread(new Runnable(){
				@Override
				public void run() {
					// TODO Auto-generated method stub
					ad.show();
				}
			});
		}
	}
	
	// 現在のポイント
	public static void updatePoints(){
		Log.d(TAG, "updatePoints");
		
		TapjoyConnect.getTapjoyConnectInstance().getTapPoints(
				new TapjoyNotifier(){
			@Override
			public void getUpdatePoints(String currencyName, int pointTotal){
				Log.d(TAG, "getUpdatePoints");
				Log.i(TAG, "currencyName: " + currencyName);
				Log.i(TAG, "pointTotal: " + pointTotal);
			}
			@Override
			public void getUpdatePointsFailed(String error){
				Log.e(TAG, "getUpdatePointsFailed");
			}
		});
	}
	
	// ポイントの消費
	public static void spendPoints(int point){
		Log.d(TAG, "spendPoints");
		
		TapjoyConnect.getTapjoyConnectInstance().spendTapPoints(
				point,
				new TapjoySpendPointsNotifier(){
			@Override
			public void getSpendPointsResponse(String currencyName, int pointTotal){
				Log.d(TAG, "getSpendPointsResponse");
				Log.i(TAG, "currencyName: " + currencyName);
				Log.i(TAG, "pointTotal: " + pointTotal);
			}
			@Override
			public void getSpendPointsResponseFailed(String error){
				Log.e(TAG, "getSpendPointsResponseFailed");
			}
		});
	}
	
	// ポイントの消費(全てを使う)
	public static void spendAllPoints(){
		Log.d(TAG, "spendAllPoints");

		TapjoyConnect.getTapjoyConnectInstance().getTapPoints(
				new TapjoyNotifier(){
			@Override
			public void getUpdatePoints(String currencyName, int pointTotal){
				Log.d(TAG, "getUpdatePoints");
				Log.i(TAG, "currencyName: " + currencyName);
				Log.i(TAG, "pointTotal: " + pointTotal);
				
				// 全てのポイントを消費
				TapjoyConnect.getTapjoyConnectInstance().spendTapPoints(
						pointTotal,
						new TapjoySpendPointsNotifier(){
					@Override
					public void getSpendPointsResponse(String currencyName, int pointTotal){
						Log.d(TAG, "getSpendPointsResponse");
						Log.i(TAG, "currencyName: " + currencyName);
						Log.i(TAG, "pointTotal: " + pointTotal);
					}
					@Override
					public void getSpendPointsResponseFailed(String error){
						Log.e(TAG, "getSpendPointsResponseFailed");
					}
				});
			}
			@Override
			public void getUpdatePointsFailed(String error){
				Log.e(TAG, "getUpdatePointsFailed");
			}
		});
	}
}
