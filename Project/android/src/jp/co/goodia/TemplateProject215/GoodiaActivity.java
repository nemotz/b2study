package jp.co.goodia.TemplateProject215;

import jp.basicinc.gamefeat.android.sdk.controller.GameFeatAppController;
import jp.beyond.sdk.Bead;
import jp.live_aid.aid.AdController;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import org.cocos2dx.lib.Cocos2dxActivity;

import com.amoad.amoadsdk.AMoAdSdk;
import com.amoad.amoadsdk.AMoAdSdkWallActivity;

public class GoodiaActivity extends Cocos2dxActivity{

	private static final String TAG = "GoodiaActivity";
	
	// ===========================================================
	// アイコン広告 astrisk
	// ===========================================================
	protected static final String _MEDIA_CODE_TITLE = "jp.co.goodia.Jet";	//タイトル画面用識別コード（今後はジェットマンのコードを使い回す）
	protected static final String _MEDIA_CODE_ENDING = "ast00284lt0wy9z0ljcs";	//エンディング画面用識別コード

	// ===========================================================
	// インターステイシャル広告 AID
	// ===========================================================
	protected static final String _MEDIA_CODE_AID = "jp.co.goodia.RegsWIG";
	
	// ===========================================================
	// ウォール広告 GAMEFEAT
	// ===========================================================
	// マニフェストで設定

	// ===========================================================
	// バナー広告 AdStir
	// ===========================================================
	//AdStirサイト上で処理（iMobile,Nend,InMobi）
	protected static final String _MEDIA_CODE_ADSTIR = "MEDIA-47223b34";

	// ===========================================================
	// インターステイシャル広告 BEAD
	// ===========================================================
	/** BEAD SID */
	protected final String BeadSID = "94ed3cfaf694595124471db7f234d5939b36f2f138b65eb9";
	
	// ===========================================================
	// アイコン広告 iMoblie
	// ===========================================================
	protected static jp.co.imobile.android.AdIconView iconAdViewImobile;
	protected static final int IMOBILE_MEDIA_ID = 83886;//メディアID
	protected static final int IMOBILE_SPOT_ID = 176914;//スポットID

	// ===========================================================
	// インターステイシャル広告 i-mobile(内部クラス)
	// ===========================================================
	protected class SpotParams{
		static final String PUBLISHER_ID = "24099";
		static final String MEDIA_ID     = "86043";
		static final String SPOT_ID      = "183545";
	}
	
	// ===========================================================
	// ランキングシステム RankPlat
	// ===========================================================
	protected static String RankPlatAppKey = "ef79403cc9794a637dc6bd16a705de9f395f268e";
	protected static int RankPlatMode = 1;

 	// ===========================================================
	// 解析　Flurry
	// ===========================================================
	/** Flurry */
	protected static String FLURRY_API_KEY = "HV2TTNZSTFQBD294SF98";

	// ===========================================================
	// 動画広告 TapJoy
	// ===========================================================
	protected static final String tapjoyAppID = "b3ea25b6-5be6-4bc8-869f-4f6c423c99db";
	protected static final String tapjoySecretKey = "BnpjDadUhDYOPpexnVjH";

	// ===========================================================
	// ウォール広告 AppliPromotion
	// ===========================================================
	// マニフェストで設定
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		// AppliPromotion コンバージョン送信
		{
			AMoAdSdk.sendUUID(this);
		}
	}

	@Override
	protected void onStart(){
		super.onStart();
	}

	@Override
	protected void onStop(){
		super.onStop();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
	}
	
	@Override
	protected void onPause(){
		super.onPause();
	}

	//MoreApps
	public static void launchUrl(String path){
		
		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(path));
		me.startActivity(i);
	}

	//Twitter
	public static void launchTwitter(String tweet){
		
		try{
			Intent tweetIntent = new Intent(Intent.ACTION_SEND);
			
			tweetIntent.setAction(Intent.ACTION_SEND);
			tweetIntent.setType("text/plain");
			tweetIntent.putExtra(Intent.EXTRA_TEXT, tweet);
			
			me.startActivity(tweetIntent);
		}catch (Exception e){
			Log.d("Twitter起動失敗", "Error");
		}
	}

	public static void showAd(){
	}

	public static void vibrate(){
		
		//バイブレーション
//		//0ミリ秒OFF→500ミリ秒ON→100ミリ秒OFF→500ミリ秒ON→100ミリ秒OFF→500ミリ秒ON
//		long pattern[] = {100, 250, 100, 250, 100, 250};
//
//		// 定義したパターン・リピートなしでバイブレーション開始
//		((Vibrator)me.getSystemService(VIBRATOR_SERVICE)).vibrate(pattern, -1);
	}

	//GameFeat
	public static void showGameFeat(){
		
		GameFeatAppController.show(me);
	}

	//AppliPromotion (リスト表示)
	public static void showAppliPromotion(){
		
		Intent intent = new Intent(me, AMoAdSdkWallActivity.class);
		me.startActivity(intent); 
	}
}
