package jp.co.goodia.TemplateProject215;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONArray;

public class CheckSplSpfJson{

	private static String TAG = "CheckSplSpfJson";
	
	private static String APP_ID = "REG_A";
	private static String SPL_ID = "SPL01";
	private static String SPF_ID = "SPF01";
	
	private String splashTypeStr;
	private String splashTypeWallStr;
	private String splashTypeRankStr;
	private String adSplashFrequencyStr;
	
	public CheckSplSpfJson(){

		// Android3.0からの端末に必須になる、StrictModeの設定(StrictModeクラスは、Android2.3以降に存在)
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
		StrictMode.ThreadPolicy policy = new StrictMode.
				ThreadPolicy.Builder().permitAll().build();
				StrictMode.setThreadPolicy(policy);
		
		checkSplashTypeStr();
		checkFrequencyStr();
	}
	
	private void checkSplashTypeStr(){
		
		// DefaultHttpClient
		HttpClient httpClient = new DefaultHttpClient();
		
		// 発行したいHTTPリクエストを生成
		String url = "http://goodiafms.appspot.com/v1/value_of/" + APP_ID + "/" + SPL_ID;
		HttpGet request = new HttpGet(url.toString());
		
		// Getリクエストを送信
		HttpResponse httpResponse = null;
		try{
			httpResponse = httpClient.execute(request);
		}catch(ClientProtocolException e){
			Log.e(TAG, "CPE:" + e.toString());
		}catch(IOException e){
			Log.e(TAG, "IOE:" + e.toString());
		}catch(Exception e){
			Log.e(TAG, "Ex:" + e.toString());
		}
		
		// httpResponseが空でなかったら処理を続行
		if(httpResponse != null){
			int status = httpResponse.getStatusLine().getStatusCode();
			if(status == HttpStatus.SC_OK){
				try{
					// データの取得
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					httpResponse.getEntity().writeTo(baos);
					String data = baos.toString();
					Log.d(TAG, "-data-\n" + data);
					
					try{
						JSONObject rootObj = new JSONObject(data);
						splashTypeStr = rootObj.getString("splashType");
						splashTypeWallStr = rootObj.getString("splashTypeWall");
						splashTypeRankStr = rootObj.getString("splashTypeRank");
						
					}catch(JSONException e){
						splashTypeStr = "0";
						Log.e(TAG, "JSONE:" + e.toString());
					}
				}catch(IOException e){
					splashTypeStr = "0";
					Log.e(TAG, "IOE:" + e.toString());
				}
			}else{
				splashTypeStr = "0";
				Log.e(TAG, "status:" + status);
			}
		}else{
			splashTypeStr = "0";
			Log.e(TAG, "httpResponse:null");
		}
	}
	
	private void checkFrequencyStr(){

		// DefaultHttpClient
		HttpClient httpClient = new DefaultHttpClient();
		
		// 発行したいHTTPリクエストを生成
		String url = "http://goodiafms.appspot.com/v1/value_of/" + APP_ID + "/" + SPF_ID;
		HttpGet request = new HttpGet(url.toString());
		
		// Getリクエストを送信
		HttpResponse httpResponse = null;
		try{
			httpResponse = httpClient.execute(request);
		}catch(ClientProtocolException e){
			Log.e(TAG, "CPE:" + e.toString());
		}catch(IOException e){
			Log.e(TAG, "IOE:" + e.toString());
		}catch(Exception e){
			Log.e(TAG, "Ex:" + e.toString());
		}
		
		// httpResponseが空でなかったら処理を続行
		if(httpResponse != null){
			int status = httpResponse.getStatusLine().getStatusCode();
			if(status == HttpStatus.SC_OK){
				try{
					// データの取得
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					httpResponse.getEntity().writeTo(baos);
					String data = baos.toString();
					Log.d(TAG, "-data-\n" + data);
					
					try{
						JSONObject rootObj = new JSONObject(data);
						adSplashFrequencyStr = rootObj.getString("AdSplashFrequency");
					}catch(JSONException e){
						adSplashFrequencyStr = "0";
						Log.e(TAG, "JSONE:" + e.toString());
					}
				}catch(IOException e){
					adSplashFrequencyStr = "0";
					Log.e(TAG, "IOE:" + e.toString());
				}
			}else{
				adSplashFrequencyStr = "0";
				Log.e(TAG, "status:" + status);
			}
		}else{
			adSplashFrequencyStr = "0";
			Log.e(TAG, "httpResponse:null");
		}
	}
	
	public String getSplashTypeStr(){
		return splashTypeStr;
	}
	
	public String getSplashTypeWallStr(){
		return splashTypeWallStr;
	}
	
	public String getSplashTypeRankStr(){
		return splashTypeRankStr;
	}
	
	public String getAdSplashFrequencyStrStr(){
		return adSplashFrequencyStr;
	}
}
