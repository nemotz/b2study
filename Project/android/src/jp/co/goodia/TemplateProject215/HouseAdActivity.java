package jp.co.goodia.TemplateProject215;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.cocos2dx.lib.Cocos2dxActivity;

public class HouseAdActivity extends RankPlatActivity{

	private static final String TAG = "HouseAdActivity";

	// ===========================================================
	// ハウスアド Housead
	// ===========================================================
	/** Housead */
	private static FrameLayout houseAd;
	private static boolean houseAdShow = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		// Housead
		// ハウスアド用のエリア
		houseAd = new FrameLayout(this);
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		houseAd.setLayoutParams(params);
		houseAd.setVisibility(ViewGroup.GONE);

		if(isOnline()){
			CheckHouseadJson ch = new CheckHouseadJson();
			Log.d(TAG, "flg:" + ch.checkFlg());
			Log.d(TAG, "scheme:" + ch.getScheme());
			Log.d(TAG, "title:" + ch.getTitle());
			Log.d(TAG, "url:" + ch.getUrl());
			
			if(ch.checkFlg() && !checkInstalled(ch.getScheme())){
				houseAdShow = ch.checkFlg();
				String path = ch.getUrl();
				WebView webView = new WebView(this);
				DisplayMetrics metrics = new DisplayMetrics();
				((android.view.WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
				int height = (int)(100.0f * metrics.density);
				ViewGroup.LayoutParams paramsWeb = new ViewGroup.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT, height);
				webView.setLayoutParams(paramsWeb);
				webView.loadUrl(path);
				webView.setBackgroundColor(Color.TRANSPARENT);
				webView.getSettings().setJavaScriptEnabled(true);
				webView.getSettings().setAppCacheEnabled(false);
				houseAd.addView(webView);
				
				Button btn = new Button(this);
				ViewGroup.LayoutParams paramsBtn = new ViewGroup.LayoutParams(80,80);
				btn.setLayoutParams(paramsBtn);
				Drawable dwb = getResources().getDrawable(jp.co.goodia.TemplateProject215.R.drawable.close_button);
				btn.setBackgroundDrawable(dwb);
				btn.setOnClickListener(new View.OnClickListener(){
					@Override
					public void onClick(View view){
						houseAd.setVisibility(ViewGroup.GONE);
					}
				});
				houseAd.addView(btn);
			}
		}

		// ...add to FrameLayout
		mFramelayout.addView(houseAd);
	}

	@Override
	protected void onStart(){
		super.onStart();
	}

	@Override
	protected void onStop(){
		super.onStop();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
	}

	@Override
	protected void onResume(){
		super.onResume();
	}
	
	@Override
	protected void onPause(){
		super.onPause();
	}

	//gifアニメーション 自社広告
	//adFlag ... とりあえず 0:非表示, 1:表示 (本当は Boolean にしたかったけど)
	public static void showGenuineAd(int adFlag){
		
		Log.d(TAG, "showGenuineAd(adFlag) adFlag..." + adFlag);
		
		if(adFlag == 0){
			me.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					houseAd.setVisibility(ViewGroup.GONE);
				}
			});
		}else if(adFlag == 1){
			me.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if(houseAdShow){
						houseAd.setVisibility(ViewGroup.VISIBLE);
						/*
						// Tweenアニメーション(現状入れると、消えない様だ。。。)
						Animation animation = AnimationUtils.loadAnimation(me, jp.co.goodia.Onimoguri.R.anim.housead);
						animation.setFillAfter(true);
						houseAd.startAnimation(animation);
						*/
					}
				}
			});
		}
	}

	//gifアニメーション 自社広告
	//iOSでは準備用メソッド(事前に読み込んでおく用)
	public static void genuineAdPreparation(){
		
		Log.d(TAG, "genuineAdPreparation()");
		me.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				
			}
		});
	}

	// オンラインかどうか
	public boolean isOnline(){
		ConnectivityManager cm = 
				(ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = cm.getActiveNetworkInfo();
		
		if(info != null){
			int type = info.getType();
			if(type == ConnectivityManager.TYPE_MOBILE || type == ConnectivityManager.TYPE_WIFI){
				Log.d("myTag", "NetworkInfo:" + info.getTypeName());
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	// 特定のパッケージがインストールされているか
	private boolean checkInstalled(String scheme){
		PackageManager pm = this.getPackageManager();
		List<ApplicationInfo> infos = pm.getInstalledApplications(0);
		for(ApplicationInfo ai:infos){
			if(scheme.equals(ai.packageName)){
				return true;
			}
		}
		return false;
	}
}