package jp.co.goodia.TemplateProject215.rankplat;

import jp.co.goodia.TemplateProject215.rankplat.RankPlatDataManager;
import jp.co.goodia.TemplateProject215.R;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.SpannableStringBuilder;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

/**
 * ランキングページダイアログFragment.
 * @author t_moriizumi
 */
public class RankingPageDialogFragment extends DialogFragment {
	/** ダイアログタグ. */
	private static final String TAG = "RankingPageDialogFragment";
	/** アプリキー. */
    private static final String APP_KEY = "appKey";
	/**
	 * インスタンス取得.
	 * @param appKey アプリキー
	 * @return インスタンス
	 */
	public static RankingPageDialogFragment newInstance(String appKey) {
		RankingPageDialogFragment dialogFragment = new RankingPageDialogFragment();
		final Bundle bundle = new Bundle();
        bundle.putString(APP_KEY, appKey);
		dialogFragment.setArguments(bundle);
		return dialogFragment;
	}

	/**
	 * ダイアログを表示.
	 * @param fragmentManager フラグメントマネージャ
	 */
	public void show(FragmentManager fragmentManager) {
		show(fragmentManager, TAG);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Bundle bundle = getArguments();
		return new RankingPageDialog(getActivity(), bundle.getString(APP_KEY));
	}

	/**
	 * ランキングダイアログ.
	 * @author t_moriizumi
	 */
	public class RankingPageDialog extends Dialog implements OnClickListener {
		/** アプリキー. */
		private String appKey = null;
		/** OKボタン. */
		private Button okButton = null;
		/** NGボタン. */
		private Button ngButton = null;
		/** モードエディタ. */
		private EditText modeEdit = null;
		
		/**
		 * コンストラクタ.
		 * @param activity アクティビティ
		 */
		public RankingPageDialog(FragmentActivity activity, String appKey) {
			super(activity);
			setOwnerActivity(activity);
			setContentView(R.layout.ranking_page_dialog);

			this.appKey = appKey;

			// OKボタン取得
			okButton = (Button) findViewById(R.id.button_ok);
			okButton.setOnClickListener(this);

			// NGボタン取得
			ngButton = (Button) findViewById(R.id.button_ng);
			ngButton.setOnClickListener(this);

			// モードエディタ取得
			modeEdit = (EditText) findViewById(R.id.edit_mode);
			modeEdit.setOnKeyListener(new View.OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					//EnterKeyが押されたかを判定
					if (event.getAction() == KeyEvent.ACTION_DOWN
							&& keyCode == KeyEvent.KEYCODE_ENTER) {
						//ソフトキーボードを閉じる
						InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
						inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
						return true;
					}
					return false;
				}
			});
		}

		/**
		 * 移動.
		 */
		private void move() {
			// モード取得
			final String mode = ((SpannableStringBuilder) modeEdit.getText()).toString();
			
			move(mode);
		}

		/**
		 * 移動.
		 * @param mode モード
		 */
		private void move(final String mode) {
			Context context = getOwnerActivity().getApplicationContext();
			
			RankPlatDataManager rpdm = RankPlatDataManager.getInstance();
			
			Integer modeInt = null;
			if (mode != null) { modeInt = Integer.parseInt(mode); }
			// ブラウザを開く
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(rpdm.getRankingPageUrl(appKey, modeInt)));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			// アクティビティの呼び出し
			context.startActivity(intent);
		}

		/**
		 * クリックイベント.
		 * @param view クリックされたView
		 */
		@Override
		public void onClick(View view) {
			// OKボタン
			if (view == okButton) {
				move();
				dismiss();
				return;
			}
			// NGボタン
			if (view == ngButton) {
				dismiss();
				return;
			}
		}
	}
}
