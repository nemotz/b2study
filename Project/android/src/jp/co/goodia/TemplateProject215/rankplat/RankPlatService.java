package jp.co.goodia.TemplateProject215.rankplat;

import com.rank_plat.service.RankPlatBaseService;
import com.rank_plat.util.RankPlatDataBaseManager;

import jp.co.goodia.TemplateProject215.rankplat.RankPlatDataManager;

/**
 * ランキングデータ送信Service.
 * @author t_moriizumi
*/
public class RankPlatService extends RankPlatBaseService{
	@Override
	public RankPlatDataBaseManager getRankPlatDataManager() {
		return RankPlatDataManager.getInstance();
	}
}
