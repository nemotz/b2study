package jp.co.goodia.TemplateProject215.rankplat;

import jp.co.goodia.TemplateProject215.rankplat.RankPlatService;
import jp.co.goodia.TemplateProject215.R;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.SpannableStringBuilder;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

/**
 * ランキングダイアログFragment.
 * @author t_moriizumi
 */
public class RankingDialogFragment extends DialogFragment {
	/** ダイアログタグ. */
	private static final String TAG = "RankingDialogFragment";
	/** アプリキー. */
    private static final String APP_KEY = "appKey";
	/** スコア. */
    private static final String SCORE = "score";
	/** モード. */
    private static final String MODE = "mode";
    
	/**
	 * インスタンス取得.
	 * @param appKey アプリキー
	 * @return インスタンス
	 */
	public static RankingDialogFragment newInstance(String appKey, int score, int mode) {
		RankingDialogFragment dialogFragment = new RankingDialogFragment();
		final Bundle bundle = new Bundle();
        bundle.putString(APP_KEY, appKey);
        bundle.putInt(SCORE, score);
        bundle.putInt(MODE, mode);
		dialogFragment.setArguments(bundle);
		return dialogFragment;
	}

	/**
	 * ダイアログを表示.
	 * @param fragmentManager フラグメントマネージャ
	 */
	public void show(FragmentManager fragmentManager) {
		show(fragmentManager, TAG);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Bundle bundle = getArguments();
		return new RankingDialog(getActivity(), bundle.getString(APP_KEY), bundle.getInt(SCORE), bundle.getInt(MODE));
	}

	/**
	 * ランキングダイアログ.
	 * @author t_moriizumi
	 */
	public class RankingDialog extends Dialog implements OnClickListener {
		/** アプリキー. */
		private String appKey = null;
		/** スコア */
		private int score = 0;
		/** モード */
		private int mode = 1;
		/** OKボタン. */
		private Button okButton = null;
		/** NGボタン. */
		private Button ngButton = null;
		/** ユーザー名エディタ. */
		private EditText userNameEdit = null;
		/** モードエディタ. */
		private EditText modeEdit = null;
//		/** キャラクターエディタ. */
//		private EditText charaKeyEdit = null;
//		/** スコアエディタ. */
//		private EditText scoreEdit = null;
//		/** コメントエディタ. */
//		private EditText commentEdit = null;
		
		/**
		 * コンストラクタ.
		 * @param activity アクティビティ
		 */
		public RankingDialog(FragmentActivity activity, String appKey, int score, int mode) {
			super(activity);
			setOwnerActivity(activity);
			setContentView(R.layout.ranking_dialog);
			setTitle(R.string.ranking_title);

			this.appKey = appKey;
			this.score = score;
			this.mode = mode;

			// OKボタン取得
			okButton = (Button) findViewById(R.id.button_ok);
			okButton.setOnClickListener(this);

			// NGボタン取得
			ngButton = (Button) findViewById(R.id.button_ng);
			ngButton.setOnClickListener(this);

			// ユーザー名エディタ取得
			userNameEdit = (EditText) findViewById(R.id.edit_name);
			userNameEdit.setOnKeyListener(new View.OnKeyListener() {
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					//EnterKeyが押されたかを判定
					if (event.getAction() == KeyEvent.ACTION_DOWN
							&& keyCode == KeyEvent.KEYCODE_ENTER) {
						//ソフトキーボードを閉じる
						InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
						inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
						return true;
					}
					return false;
				}
			});
//			// モードエディタ取得
//			modeEdit = (EditText) findViewById(R.id.edit_mode);
//			modeEdit.setOnKeyListener(new View.OnKeyListener() {
//				@Override
//				public boolean onKey(View v, int keyCode, KeyEvent event) {
//					//EnterKeyが押されたかを判定
//					if (event.getAction() == KeyEvent.ACTION_DOWN
//							&& keyCode == KeyEvent.KEYCODE_ENTER) {
//						//ソフトキーボードを閉じる
//						InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//						inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
//						return true;
//					}
//					return false;
//				}
//			});
//			// キャラクターエディタ取得
//			charaKeyEdit = (EditText) findViewById(R.id.chara_key_mode);
//			charaKeyEdit.setOnKeyListener(new View.OnKeyListener() {
//				@Override
//				public boolean onKey(View v, int keyCode, KeyEvent event) {
//					//EnterKeyが押されたかを判定
//					if (event.getAction() == KeyEvent.ACTION_DOWN
//							&& keyCode == KeyEvent.KEYCODE_ENTER) {
//						//ソフトキーボードを閉じる
//						InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//						inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
//						return true;
//					}
//					return false;
//				}
//			});
//			// スコアエディタ取得
//			scoreEdit = (EditText) findViewById(R.id.edit_score);
//			scoreEdit.setOnKeyListener(new View.OnKeyListener() {
//				@Override
//				public boolean onKey(View v, int keyCode, KeyEvent event) {
//					//EnterKeyが押されたかを判定
//					if (event.getAction() == KeyEvent.ACTION_DOWN
//							&& keyCode == KeyEvent.KEYCODE_ENTER) {
//						//ソフトキーボードを閉じる
//						InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//						inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
//						return true;
//					}
//					return false;
//				}
//			});
//			// コメントエディタ取得
//			commentEdit = (EditText) findViewById(R.id.edit_comment);
//			commentEdit.setOnKeyListener(new View.OnKeyListener() {
//				@Override
//				public boolean onKey(View v, int keyCode, KeyEvent event) {
//					//EnterKeyが押されたかを判定
//					if (event.getAction() == KeyEvent.ACTION_DOWN
//							&& keyCode == KeyEvent.KEYCODE_ENTER) {
//						//ソフトキーボードを閉じる
//						InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//						inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
//						return true;
//					}
//					return false;
//				}
//			});
		}

		/**
		 * 保存して終了.
		 */
		private void save() {
			// ユーザー名取得
			final String userName = ((SpannableStringBuilder) userNameEdit.getText()).toString();
			// モード取得
			final String mode = String.valueOf( this.mode );
//			// キャラクター取得
//			final String charaKey = ((SpannableStringBuilder) charaKeyEdit.getText()).toString();
//			// スコア取得
//			final String score = ((SpannableStringBuilder) scoreEdit.getText()).toString();
//			// コメント取得
//			final String comment = ((SpannableStringBuilder) commentEdit.getText()).toString();
			
//			save(userName, mode, charaKey, score, comment);
			save(userName, mode, "", "", "");
		}

		/**
		 * 保存して終了.
		 * @param appKey アプリキー
		 * @param mode モード
		 * @param charaKey キャラクター
		 * @param score スコア
		 * @param commen コメント
		 */
		private void save(final String userName, final String mode, final String charaKey,
				final String score, final String comment) {			
			Context context = getOwnerActivity().getApplicationContext();
			
			// ランキング作成
			Intent rankingIntent = new Intent(context, RankPlatService.class);
			// アプリキー登録
			rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.APP_KEY, appKey);
			// ユーザー名登録
			rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.USER_NAME, userName);
			
			// ユーザ名をプリファレンスに保存
			SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(context);
			SharedPreferences.Editor editor = sharedpreferences.edit();
			editor.putString("username", userName);
			editor.commit();
			
			// モード登録
			if (mode != null && !"".equals(mode)) {
				rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.MODE, Long.parseLong(mode));
			}
			// キャラクター登録
			if (charaKey != null && !"".equals(charaKey)) {
				rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.CHARA_KEY, Long.parseLong(charaKey));
			}
			// スコア登録
			String str = String.valueOf( this.score );
//			if (score != null && !"".equals(score)) {
				rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.SCORE, Long.parseLong(str));
//			}
			// コメント登録
			rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.COMMENT, comment);
			context.startService(rankingIntent);
		}

		/**
		 * クリックイベント.
		 * @param view クリックされたView
		 */
		@Override
		public void onClick(View view) {
			// OKボタン
			if (view == okButton) {
				save();
				dismiss();
				return;
			}
			// NGボタン
			if (view == ngButton) {
				dismiss();
				return;
			}
		}
	}
}
