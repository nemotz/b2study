package jp.co.goodia.TemplateProject215.rankplat;

import com.rank_plat.util.RankPlatDataBaseManager;
import com.rank_plat.util.SaveDataUtil;
import com.rank_plat.util.SaveFileUtil;
import com.rank_plat.util.SaveStorageUtil;

/**
 * ランキングデータ管理.
 * ※ マルチスレッド対応のためシングルトンにする.
 * @author t_moriizumi
 */
public class RankPlatDataManager extends RankPlatDataBaseManager {
	/** インスタンス. */
	private static final RankPlatDataManager instance = new RankPlatDataManager();

	/**
	 * コンストラクタ.
	 */
	private RankPlatDataManager() {
		// データ保存クラス登録
		SaveDataUtil saveDataUtil = null;
		// SDカードに保存する場合
		saveDataUtil = new SaveStorageUtil("jp.co.goodia.TemplateProject215/data", "save.json");
		// アプリ内に保存する場合
//		saveDataUtil = new SaveFileUtil(App.getInstance(), "save.json");
		setSaveDataUtil(saveDataUtil);
	}

	/**
	 * インスタンス取得メソッド
	 * ※多重起動防止のためsynchronized.
	 * @return インスタンス
	 */
	public static synchronized RankPlatDataManager getInstance() {
		return instance;
	}
}
