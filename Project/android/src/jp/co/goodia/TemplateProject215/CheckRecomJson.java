package jp.co.goodia.TemplateProject215;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONArray;

public class CheckRecomJson{

	private static String TAG = "myTag";
	
	private static String APP_ID = "OMG_A";
	private static String FLG_ID = "POP01";
	
	private String flgString;
	private String schemeString;
	private String titleString;
	private String urlString;
	
	public CheckRecomJson(){

		// Android3.0からの端末に必須になる、StrictModeの設定(StrictModeクラスは、Android2.3以降に存在)
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
		StrictMode.ThreadPolicy policy = new StrictMode.
				ThreadPolicy.Builder().permitAll().build();
				StrictMode.setThreadPolicy(policy);
		
		// DefaultHttpClient
		HttpClient httpClient = new DefaultHttpClient();
		
		// 発行したいHTTPリクエストを生成
		String url = "http://goodiafms.appspot.com/v1/value_of/" + APP_ID + "/" + FLG_ID;
		HttpGet request = new HttpGet(url.toString());
		
		// Getリクエストを送信
		HttpResponse httpResponse = null;
		try{
			httpResponse = httpClient.execute(request);
		}catch(ClientProtocolException e){
			Log.e(TAG, "CPE:" + e.toString());
		}catch(IOException e){
			Log.e(TAG, "IOE:" + e.toString());
		}catch(Exception e){
			Log.e(TAG, "Ex:" + e.toString());
		}
		
		// httpResponseが空でなかったら処理を続行
		if(httpResponse != null){
			int status = httpResponse.getStatusLine().getStatusCode();
			if(status == HttpStatus.SC_OK){
				try{
					// データの取得
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					httpResponse.getEntity().writeTo(baos);
					String data = baos.toString();
					Log.d(TAG, "-data-\n" + data);
					
					try{
						JSONObject rootObj = new JSONObject(data);
						flgString    = rootObj.getString("flg");
						schemeString = rootObj.getString("scheme");
						titleString  = rootObj.getString("title");
						urlString    = rootObj.getString("url");
					}catch(JSONException e){
						Log.e(TAG, "JSONE:" + e.toString());
					}
				}catch(IOException e){
					Log.e(TAG, "IOE:" + e.toString());
				}
			}else{
				Log.e(TAG, "status:" + status);
			}
		}else{
			Log.e(TAG, "httpResponse:null");
		}
	}
	
	public boolean checkFlg(){
		if(flgString != null){
			int flgInt = Integer.parseInt(flgString);
			if(flgInt == 1){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public String getScheme(){
		return schemeString;
	}
	
	public String getTitle(){
		return titleString;
	}
	
	public String getUrl(){
		return urlString;
	}
}
