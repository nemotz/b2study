package jp.co.goodia.TemplateProject215;

import jp.co.goodia.TemplateProject215.rankplat.RankPlatDataManager;
import jp.co.goodia.TemplateProject215.rankplat.RankPlatService;
import jp.co.goodia.TemplateProject215.rankplat.RankingDialogFragment;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import org.cocos2dx.lib.Cocos2dxActivity;

public class RankPlatActivity extends IMobileActivity{

	private static final String TAG = "RankPlatActivity";

	// ===========================================================
	// ランキングシステム RankPlat
	// ===========================================================
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStart(){
		super.onStart();
	}

	@Override
	protected void onStop(){
		super.onStop();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
	}
	
	@Override
	protected void onPause(){
		super.onPause();
	}

	//RankPlat
	public static void rankingLeaderBoard(){
		
		RankPlatDataManager rpdm = RankPlatDataManager.getInstance();

		//*** ブラウザを開く ***
		//単体の場合 (デフォルトのランキング表示へ)
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(rpdm.getRankingPageUrl(RankPlatAppKey, RankPlatMode)));
		//複数の場合 (モードの一覧へ)
		//Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(rpdm.getRankingPageUrl(RankPlatAppKey, null)));

		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// アクティビティの呼び出し
		me.startActivity(intent);
	}

	public static void rankingReportScore(int score, int mode){
		
		Log.d("myTag", "score:" + score + ", mode:" + mode);

		RankPlatDataManager rpdm = RankPlatDataManager.getInstance();
		// ユーザキーを取得
		String userkey = rpdm.getUserKey (RankPlatAppKey);
		// プリファレンスからユーザ名を取得
		SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(me);
		String userName = sharedpreferences.getString("username", "");

		//ユーザ登録しているか判定
		if (userkey != null && !"".equals(userkey) && userName != null && !"".equals(userName))
		{
			Context context = me.getApplicationContext();

			// ランキング作成
			Intent rankingIntent = new Intent(context, RankPlatService.class);
			// アプリキー登録
			rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.APP_KEY, RankPlatAppKey);
			// ユーザー名登録
			rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.USER_NAME, userName);
			// モード登録
			String strMode = String.valueOf( mode );
			rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.MODE, Long.parseLong(strMode));
			// キャラクター登録
			//rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.CHARA_KEY, Long.parseLong(charaKey));
			// スコア登録
			String strScore = String.valueOf( score );
			rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.SCORE, Long.parseLong(strScore));
			// コメント登録
			rankingIntent.putExtra(RankPlatService.START_COMMAND_BUNDLE.COMMENT, "");

			context.startService(rankingIntent);
			//RankingDialogFragment.newInstance(RankPlatAppKey, score).show(me.getSupportFragmentManager());
		}
		else
		{
			// ユーザー情報入力ダイアログを表示
			RankingDialogFragment.newInstance(RankPlatAppKey, score, mode).show(me.getSupportFragmentManager());
		}
	}
}
