LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := game_shared

LOCAL_MODULE_FILENAME := libgame


#--------------------------------------------------------
#For DEBUG (リリース時はコメント化してください)
#LOCAL_CFLAGS := -DDEBUG
#LOCAL_CFLAGS += -DCOCOS2D_DEBUG=1


#--------------------------------------------------------
#コンパイル対象
CPP_FILES := $(shell find $(LOCAL_PATH)/../../Classes -name *.cpp)

LOCAL_SRC_FILES := hellocpp/main.cpp 
LOCAL_SRC_FILES += $(CPP_FILES:$(LOCAL_PATH)/%=%)


#--------------------------------------------------------
#インクルードパス
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Common
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Common/XTLayer
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Common/addLibFiles
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Common/SneakyInput
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/NativeInterface
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Game
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Game/Contents


#--------------------------------------------------------
LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static cocos_extension_static
            

#--------------------------------------------------------
include $(BUILD_SHARED_LIBRARY)

$(call import-module,Box2D) \
$(call import-module,chipmunk) \
$(call import-module,CocosDenshion/android) \
$(call import-module,cocos2dx) \
$(call import-module,extensions)\
$(call import-module,libwebsockets/android)
