//
//  CommonFunction.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __TemplateProject__CommonFunction__
#define __TemplateProject__CommonFunction__

#include "cocos2d.h"


//プロパティ用 ... isXxxx()
//#define CC_SYNTHESIZE_ISPROPERTY(varType, varName, funName)\
//protected: varType varName;\
//public: virtual varType is##funName(void) const { return varName; }

#define CC_SYNTHESIZE_ISPROPERTY(varName, funName)\
protected: bool varName;\
public: virtual bool is##funName(void) const { return varName; }


class CommonFunction
{
//============================================================
#pragma mark - サウンド系
public:
    static unsigned int myEffectPlayWithFile(const char * lpName);
    static unsigned int myEffectPlayWithFile(const char * lpName, bool loop);
    static void myEffectSetVolume(float volume);

    static void myBackgroundMusicPlayWithFile(const char * lpName);
    static void myBackgroundMusicPlayWithFile(const char * lpName, bool loop);
    static void myBackgroundMusicSetVolume(float volume);

    static void stopBackPlayMusic();
    static void stopAllMyEffects();

    static void stopEffect(unsigned int nSoundId);


//============================================================
#pragma mark - 解像度関連
public:
    //解像度計算
    static float getScaleRate();

    //固定値の解像度を考慮するための変数
    static float getAdjustRate();

    //解像度計算
    static float getScaleRateFill();

    //固定値の解像度を考慮するための変数
    static float getAdjustRateFill();
	
	//背景の隙間埋め用解像度計算
	static cocos2d::CCSize getBGScaleRate();


    //固定値の解像度を考慮するための変数
    static float getAdHeight();

    //解像度を考慮した値の取得 (iOS/Android)
    static float getValueOnScale(float value);

    //縦向きか横向きかの取得
    static bool isOrientationPortrait();


//============================================================
#pragma mark - Cocos2d-x用
public:
    //CCMenuItemSprite の画像差し替え
    static void setNodeFrameName(cocos2d::CCMenuItemSprite * itemSprite, const char * frameName);

    //CCNode の画像差し替え
    static void setNodeFrameName(cocos2d::CCNode * node, const char * frameName);


    //CCAnimation の作成 (ファイル名は連番指定できるタイプで)
    static cocos2d::CCAnimation * createCCAnimation(const char * frameNameFormat,
                                                    int startIndex,
                                                    int endIndex,
                                                    float frameTime);

    //CCAnimation の作成 (ファイル名の配列を渡すタイプ)
    static cocos2d::CCAnimation * createCCAnimation(cocos2d::CCArray * frameNames,
                                                    float frameTime);


    //CCAnimate の作成 (ファイル名は連番指定できるタイプで)
    static cocos2d::CCAnimate * createCCAnimate(const char * frameNameFormat,
                                                int startIndex,
                                                int endIndex,
                                                float frameTime);

    //CCAnimate の作成 (ファイル名の配列を渡すタイプ)
    static cocos2d::CCAnimate * createCCAnimate(cocos2d::CCArray * frameNames,
                                                float frameTime);


//============================================================
#pragma mark - 共通処理
public:
    /** 配列をシャッフル */
    static void randomizedArray(cocos2d::CCArray * array);

//TODO: 要修正
//    //範囲指定の乱数値(min <= result <=max)
    static int getRandInt(int min, int max);
    
    //
    static bool coinToss();


//============================================================
#pragma mark - ゲーム用処理
public:
    //タイマーカウント用のテキストを取得
    static cocos2d::CCString * getFormatTimerString(float time);

    //定義した 申請時のリリース予定日を取得 (定数は PrivateConfig.h で定義)
    static  time_t getEntryReleaseDate();

    //角度の計算
    static float getAngle(cocos2d::CCPoint pos1, cocos2d::CCPoint pos2);
    
    
    //重み付けした乱数の取り出し
    static int getRandomByWeight(int *weight,int count);
    //    例：敵のタイプ 0-4 で、0は出さない、1は1/10,2は2/10,3は3/10,4は4/10の確率で出したいとき
    //    int hoge[5] = {1,1,1,1,1};
    
    //    if(条件){
    //        hoge[0] = 0;
    //        hoge[1] = 1;
    //        hoge[2] = 2;
    //        hoge[3] = 3;
    //        hoge[4] = 4;
    //    }//的な
    
    //    int count = sizeof(hoge) / sizeof(int);
    //    int type = CommonFunction::getRandomByWeight(hoge, count);
};

#endif
