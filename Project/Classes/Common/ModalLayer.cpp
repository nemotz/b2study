//
//  ModalLayer.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include "ModalLayer.h"


USING_NS_CC;


bool ModalLayer::init()
{
    bool result = false;

    do
    {
        if (!CCLayer::init())
        {
            break;
        }

        //TODO: Ver.2.0.3 だと モーダルレイヤーとして実装はできないみたい (Ver.2.1.x 以降？)
        //this->setTouchEnabled(true);

        //ココまで来たら正常終了
        return true;

    } while (0);

    return result;
}


//ModalLayer::ModalLayer()
//{
//}
//
//ModalLayer::~ModalLayer()
//{
//}


////タッチイベントをとるオブジェクトの設定(イベントを検出するオブジェクトとして設定)
//void ModalLayer::registerWithTouchDispatcher()
//{
//    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, kModalLayerPriority, true);
//}

bool ModalLayer::ccTouchBegan(CCTouch * pTouch, CCEvent * pEvent)
{
    //モーダルレイヤーですべてのタッチイベントを拾う
    return true;
}


//閉じる処理
void ModalLayer::menuCloseCallBack(CCObject *sender)
{
    this->removeFromParentAndCleanup(true);
}
