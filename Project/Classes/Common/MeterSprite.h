//
//  MeterSprite.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/10/30.
//
//

#ifndef __TemplateProject__MeterSprite__
#define __TemplateProject__MeterSprite__

#include "cocos2d.h"


class MeterSprite : public cocos2d::CCSprite
{
public:
    //アクションタグ
    typedef enum _ActionTags
    {
        ActionTagNone,

    } ActionTags;


    //Z順 & タグ
    typedef enum _ChildTags
    {
        ChildTagNone,

        //同一サイズを想定
//        ChildTagBackSprite,                 //背景
        ChildTagMeterProgressTimer,         //メーター値

    } ChildTags;


public:
    //CREATE_FUNC(MeterSprite);
    static MeterSprite * create(const char * backImageFrameName, const char * meterImageFrameName);

private:
    bool init(const char * backImageFrameName, const char * meterImageFrameName);

private:
    cocos2d::CCProgressTimer * getProgress();


//### プロパティ ###
private:
    float _percentage;

public:
    void setPercentage(float percentage);
    float getPercentage();
};

#endif
