//
//  ModalLayer.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __TemplateProject__ModalLayer__
#define __TemplateProject__ModalLayer__

#include "cocos2d.h"


//モーダルレイヤーの優先順位
#define kModalLayerPriority (kCCMenuHandlerPriority - 1)

//モーダルレイヤー上のCCMenuの優先順位
#define kModalLayerMenuPriority (kModalLayerPriority-1)


class ModalLayer : public cocos2d::CCLayer
{

//### 内部で使用しているタグ番号 ###
public:
    //アクションタグ
    typedef enum _ActionTags
    {
        ActionTagNone,
    } ActionTags;

    //Z順 & タグ
    typedef enum _ChildTags
    {
        ChildTagNone = 0,
    } ChildTags;


public:
    CREATE_FUNC(ModalLayer);
    virtual bool init();

//protected:
//    ModalLayer();
//    virtual ~ModalLayer();


//タッチ操作
private:
//    //タッチイベントコールバックをオーバーライド
//    virtual void registerWithTouchDispatcher();

    virtual bool ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);

//    virtual void ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);


public:
    virtual void menuCloseCallBack(cocos2d::CCObject * sender);
};

#endif
