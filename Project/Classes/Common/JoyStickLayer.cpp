//
//  JoyStickLayer.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include "JoyStickLayer.h"

#include "CommonFunction.h"
#include "SneakyJoystick.h"


USING_NS_CC;


JoyStickLayer::JoyStickLayer()
{
    _delegate = NULL;
}


JoyStickLayer::~JoyStickLayer()
{
    this->unscheduleUpdate();
}


bool JoyStickLayer::init()
{
    bool result = false;

    do
    {
        //変数の初期化
        {
            _delegate = NULL;
        }


        //JoyStick
        SneakyJoystick * joystick = new SneakyJoystick();
        if (!joystick)
        {
            CC_SAFE_DELETE(joystick);
            break;
        }
        joystick->autorelease();

        if ( !SneakyJoystickSkinnedBase::init() )
        {
            break;
        }


        //画像の設定
        {
            //TODO: JoyStick用の画像を用意
            CCSprite * backSprite  = CCSprite::create("sneaky/sneaky_joystick_back.png");
            CCSprite * stickSprite = CCSprite::create("sneaky/sneaky_joystick_thumb.png");
//            CCSprite * backSprite  = CCSprite::createWithSpriteFrameName("sneaky_joystick_back.png");
//            CCSprite * stickSprite = CCSprite::createWithSpriteFrameName("sneaky_joystick_thumb.png");

            this->setBackgroundSprite(backSprite);
            this->setThumbSprite(stickSprite);
        }

        //ジョイスティックの設定
        this->setJoystick(joystick);
        {
            //ジョイスティックを自動的に真ん中に戻すか
            joystick->setAutoCenter(true);

//            //デッドゾーンの設定
//            joystick->setHasDeadzone(true);
//            joystick->setDeadRadius(20.0f);
        }

        this->scheduleUpdate();

        //ココまで来たら正常終了
        result = true;
    } while (0);

    return result;
}


void JoyStickLayer::update(float delta)
{
    do
    {
        //ジョイスティックの参照
        SneakyJoystick * joystick = this->getJoystick();
        if (!joystick)
        {
            break;
        }

        //入力値の取得
        CCPoint velocity = joystick->getVelocity();
        if (this->getDelegate())
        {
            this->getDelegate()->changeVelocity(velocity);
        }

    } while (0);
}


CCSize JoyStickLayer::getJoyStickSize()
{
    float adJustRate = CommonFunction::getAdjustRate();

    //iPhone描画サイズ (土台 + ジョイスティック部)
    float ds = 120.0f + 60.0f;
    ds *= adJustRate;

    CCSize size = CCSizeMake(ds, ds);

    return size;
}
