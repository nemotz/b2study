//
//  Encryption.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __Common__EncryptionXorCalc__
#define __Common__EncryptionXorCalc__

#include <iostream>


class Encryption
{
public:
    //XOR演算による暗号化・復号化処理
    static std::string xorCalc(const int key, const char * data);

};

#endif
