//
//  JoyStickLayer.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __MasterThief__JoyStickLayer__
#define __MasterThief__JoyStickLayer__

#include "cocos2d.h"
#include "SneakyJoystickSkinnedBase.h"


class JoyStickLayerDelegate;


class JoyStickLayer : public SneakyJoystickSkinnedBase
{
public:
//    //Z順 & タグ
//    typedef enum _ChildTags
//    {
//        ChildTagNone = 0,
//
//    } ChildTags;


public:
    CREATE_FUNC(JoyStickLayer);

protected:
    JoyStickLayer();
    ~JoyStickLayer();

protected:
    virtual bool init();

protected:
    virtual void update(float delta);

public:
    static cocos2d::CCSize getJoyStickSize();

    CC_SYNTHESIZE(JoyStickLayerDelegate *, _delegate, Delegate);
};


class JoyStickLayerDelegate
{
public:
    virtual void changeVelocity(cocos2d::CCPoint velocity) {};
};

#endif
