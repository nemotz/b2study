//
//  CommonFunction.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include "PrivateUserDefault.h"
#include "CommonFunction.h"
#include "SimpleAudioEngine.h"

#include "PrivateConfig.h"


using namespace cocos2d;
using namespace CocosDenshion;


//============================================================
#pragma mark - サウンド系

unsigned int CommonFunction::myEffectPlayWithFile(const char * lpName)
{
    unsigned int result = 0;
    bool flg = PrivateUserDefault::getSound();

    if (flg)
    {
        CCString * fileName = CCString::create(lpName);
        result = SimpleAudioEngine::sharedEngine()->playEffect(fileName->getCString());
    }

    return result;
}

unsigned int CommonFunction::myEffectPlayWithFile(const char * lpName, bool loop)
{
    unsigned int result = 0;
    bool flg = PrivateUserDefault::getSound();

    if (flg)
    {
        CCString * fileName = CCString::create(lpName);
        result = SimpleAudioEngine::sharedEngine()->playEffect(fileName->getCString(), loop);
    }

    return result;
}

void CommonFunction::myEffectSetVolume(float volume)
{
    SimpleAudioEngine::sharedEngine()->setEffectsVolume(volume);
}

void CommonFunction::stopEffect(unsigned int nSoundId)
{
    SimpleAudioEngine::sharedEngine()->stopEffect(nSoundId);
}


void CommonFunction::myBackgroundMusicPlayWithFile(const char * lpName)
{
    bool flg = PrivateUserDefault::getSound();

    if (flg)
    {
        CCString * fileName = CCString::create(lpName);
        SimpleAudioEngine::sharedEngine()->playBackgroundMusic(fileName->getCString());
    }
}

void CommonFunction::myBackgroundMusicPlayWithFile(const char * lpName, bool loop)
{
    bool flg = PrivateUserDefault::getSound();

    if (flg)
    {
        CCString * fileName = CCString::create(lpName);
        SimpleAudioEngine::sharedEngine()->playBackgroundMusic(fileName->getCString(), loop);
    }
}

void CommonFunction::myBackgroundMusicSetVolume(float volume)
{
    SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
}

void CommonFunction::stopBackPlayMusic()
{
    SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
}

void CommonFunction::stopAllMyEffects()
{
    SimpleAudioEngine::sharedEngine()->stopAllEffects();
}


//============================================================
#pragma mark - 解像度関連

/** 解像度計算 */
float CommonFunction::getScaleRate()
{
    float scaleRate = 1.0f;

//    //画面解像度用
//    {
//        CCSize winSize = CCDirector::sharedDirector()->getWinSize();
//
//        //iOS/Android 位置調整分岐用
//        TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
//
//
//        //ベースサイズ
//        CCSize baseSize = CCSizeZero;
//
//        if (target == kTargetIphone)
//        {
//            if (winSize.width < winSize.height)
//            {
//                //縦
//                baseSize = CCSizeMake(320.0f, 480.0f);
//            }
//            else
//            {
//                //横
//                baseSize = CCSizeMake(480.0f, 320.0f);
//            }
//        }
//        else
//        {
//            if (winSize.width < winSize.height)
//            {
//                //縦
//                baseSize = CCSizeMake(640.0f, 960.0f);
//            }
//            else
//            {
//                //横
//                baseSize = CCSizeMake(960.0f, 640.0f);
//            }
//        }
//
//        //比率 (必ず収まる比率) <- ただし、正方形に近い端末は余白が生まれる...
//        {
//            float kw = winSize.width  / baseSize.width;
//            float kh = winSize.height / baseSize.height;
//
//            scaleRate = MIN(kw, kh);
//        }
//    }

    return scaleRate;
}


/** 配置座標計算 */
float CommonFunction::getAdjustRate()
{
    float adJustRate = 1.0f;

//    //画面解像度用
//    adJustRate = CommonFunction::getScaleRate();
//
//    TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
//    if (target == kTargetIphone)
//    {
//        adJustRate *= 1.0f;
//    }
//    else
//    {
//        adJustRate *= 2.0f;
//    }

    return adJustRate;
}


/** 解像度計算 */
float CommonFunction::getScaleRateFill()
{
    float scaleRate = 1.0f;

//    //画面解像度用
//    {
//        CCSize winSize = CCDirector::sharedDirector()->getWinSize();
//
//        //iOS/Android 位置調整分岐用
//        TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
//
//        const float base_AspectRatio = 960.0f/640.0f;
//        float aspectRatio = 0.0f;
//
//        //ベースサイズ
//        CCSize baseSize = CCSizeZero;
//        //if (target == kTargetIphone || target == kTargetIpad)
//        if (target == kTargetIphone)
//        {
//            if (winSize.width < winSize.height)
//            {
//                //縦
//                baseSize = CCSizeMake(320.0f, 480.0f);
//                aspectRatio = winSize.height/winSize.width;
//            }
//            else
//            {
//                //横
//                baseSize = CCSizeMake(480.0f, 320.0f);
//                aspectRatio = winSize.width/winSize.height;
//            }
//        }
//        else
//        {
//            if (winSize.width < winSize.height)
//            {
//                //縦
//                baseSize = CCSizeMake(640.0f, 960.0f);
//                aspectRatio = winSize.height/winSize.width;
//            }
//            else
//            {
//                //横
//                baseSize = CCSizeMake(960.0f, 640.0f);
//                aspectRatio = winSize.width/winSize.height;
//            }
//        }
//
//        //比率
//        {
//            float kw = winSize.width  / baseSize.width;
//            float kh = winSize.height / baseSize.height;
//
//            scaleRate = MAX(kw, kh);
//        }
//
//
//        //縦向きの場合
//        if (CommonFunction::isOrientationPortrait())
//        {
//            //アスペクト比が基準の比率より縦長なら
//            if (aspectRatio > base_AspectRatio)
//            {
//                scaleRate = CommonFunction::getScaleRate();
//            }
//        }
//    }

    return scaleRate;
}


/** 配置座標計算 */
float CommonFunction::getAdjustRateFill()
{
    float adJustRate = 1.0f;

//    //画面解像度用
//    adJustRate = CommonFunction::getScaleRateFill();
//
//    TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
//    //if (target == kTargetIphone || target == kTargetIpad)
//    if (target == kTargetIphone)
//    {
//        adJustRate *= 1.0f;
//    }
//    else
//    {
//        adJustRate *= 2.0f;
//    }

    return adJustRate;
}

//背景の隙間埋め用解像度計算
cocos2d::CCSize CommonFunction::getBGScaleRate()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCSize baseSize;
	if (winSize.width < winSize.height)
	{
		//縦
		baseSize = CCSizeMake(320.0f, 480.0f);
	}
	else
	{
		//横
		baseSize = CCSizeMake(480.0f, 320.0f);
	}
	float kw = winSize.width / baseSize.width;
	float kh = winSize.height / baseSize.height;
	
	return CCSizeMake(kw, kh);
}


float CommonFunction::getAdHeight()
{
    float adHeight = 0;

    {
        TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();

        if (target == kTargetIphone)
        {
            adHeight = 50;
        }
        else
        {
            adHeight = 100;
        }
    }

    return adHeight;
}


float CommonFunction::getValueOnScale(float value)
{
    float ds = 0.0f;
    {
        float adJustRate = CommonFunction::getAdjustRate();

        ds = value * adJustRate;
    }

    return ds;
}


//縦向きか横向きかの取得
bool CommonFunction::isOrientationPortrait()
{
    bool isPortrait;
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

    if (winSize.width < winSize.height)
    {
        //縦
        isPortrait = true;
    }
    else
    {
        //横
        isPortrait = false;
    }

    return isPortrait;
}


//============================================================
#pragma mark - Cocos2d-x用

/** CCMenuItemSprite の画像差し替え */
void CommonFunction::setNodeFrameName(cocos2d::CCMenuItemSprite * itemSprite, const char * frameName)
{
    do
    {
        if (!itemSprite || !frameName)
        {
            break;
        }

        {
            CCSprite * images[] = {
                (CCSprite *)itemSprite->getNormalImage(),
                (CCSprite *)itemSprite->getSelectedImage(),
                (CCSprite *)itemSprite->getDisabledImage(),
            };

            for (int i = 0; i < sizeof(images)/sizeof(*images); i++)
            {
                CCSprite * sprite = images[i];
                CommonFunction::setNodeFrameName(sprite, frameName);
            }
        }

    } while (0);
}


/** CCNode の画像差し替え */
void CommonFunction::setNodeFrameName(CCNode * node, const char * frameName)
{
    do {
        if (!node || !frameName)
        {
            break;
        }

        CCAnimate * animate = NULL;
        {
            CCAnimation * animation = NULL;
            {
                CCSpriteFrameCache * frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();

                CCArray * frames = CCArray::create();
                {
                    frames->addObject(frameCache->spriteFrameByName(frameName));
                }

                static const float frameTime = (1.0f / 60.0f);
                animation = CCAnimation::createWithSpriteFrames(frames, frameTime);
                animation->setRestoreOriginalFrame(false);
            }

            animate = CCAnimate::create(animation);
        }

        node->runAction(animate);

    } while (0);
}


/** CCAnimation */
CCAnimation * CommonFunction::createCCAnimation(const char * frameNameFormat,
                                            int startIndex,
                                            int endIndex,
                                            float frameTime)
{
    CCAnimation * animation = NULL;
    {
        CCSpriteFrameCache * frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
        CCArray * frames = CCArray::create();

        for (int i = startIndex;
             (startIndex <= endIndex)?(i <= endIndex):(i >= endIndex);
             (startIndex <= endIndex)?(i++):(i--))
        {
            CCString * frameName = CCString::createWithFormat(frameNameFormat, i);

            frames->addObject(frameCache->spriteFrameByName(frameName->getCString()));
        }

        animation = CCAnimation::createWithSpriteFrames(frames, frameTime);
        animation->setRestoreOriginalFrame(false);
    }

    return animation;
}


/** CCAnimation */
CCAnimation * CommonFunction::createCCAnimation(cocos2d::CCArray * frameNames,
                                            float frameTime)
{
    CCAnimation * animation = NULL;
    {
        CCSpriteFrameCache * frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
        CCArray * frames = CCArray::create();

        int count = frameNames->count();
        for (int i = 0; i < count; i++)
        {
            CCString * frameName = (CCString *)frameNames->objectAtIndex(i);

            frames->addObject(frameCache->spriteFrameByName(frameName->getCString()));
        }

        animation = CCAnimation::createWithSpriteFrames(frames, frameTime);
        animation->setRestoreOriginalFrame(false);
    }

    return animation;
}


/** CCAnimate */
CCAnimate * CommonFunction::createCCAnimate(const char * frameNameFormat,
                                            int startIndex,
                                            int endIndex,
                                            float frameTime)
{
    CCAnimate * animate = NULL;
    {
        CCAnimation * animation = CommonFunction::createCCAnimation(frameNameFormat,
                                                                    startIndex,
                                                                    endIndex,
                                                                    frameTime);
        animate = CCAnimate::create(animation);
    }

    return animate;
}


/** CCAnimate */
CCAnimate * CommonFunction::createCCAnimate(cocos2d::CCArray * frameNames,
                                            float frameTime)
{
    CCAnimate * animate = NULL;
    {
        CCAnimation * animation = CommonFunction::createCCAnimation(frameNames,
                                                                    frameTime);

        animate = CCAnimate::create(animation);
    }

    return animate;
}


//============================================================
#pragma mark - 共通処理

/** 配列をシャッフル */
void CommonFunction::randomizedArray(CCArray * array)
{
    //http://iphone-dev.g.hatena.ne.jp/ktakayama/20091121/1258785421

    int i = array->count();

    while (--i)
    {
        int j = rand() % (i+1);
        array->exchangeObjectAtIndex(i, j);
    }
}


//TODO: 要修正
int CommonFunction::getRandInt(int min, int max)
{
    int range = max - min + 1;
    int result = min + abs(arc4random() % range);
    return result;
}

bool CommonFunction::coinToss(){
    bool result = (arc4random()%2 == 0 ? (true):(false));
    return result;
}
//============================================================
#pragma mark - ゲーム用処理

//タイマーカウント用のテキストを取得
CCString * CommonFunction::getFormatTimerString(float time)
{
    return CCString::createWithFormat("%02d:%02d",
                                      (int)time,
                                      ((int)(time * 100) % 100)
                                      );
}


//定義した 申請時のリリース予定日を取得 (定数は PrivateConfig.h で定義)
time_t CommonFunction::getEntryReleaseDate()
{
    time_t work_t;
    {
        struct tm work_tm;
        {
            work_tm.tm_year = RELEASE_DAY__YEAR - 1900;
            work_tm.tm_mon  = RELEASE_DAY__MONTH - 1;
            work_tm.tm_mday = RELEASE_DAY__DAY;
            work_tm.tm_hour = 0;
            work_tm.tm_min = 0;
            work_tm.tm_sec = 0;
            work_tm.tm_isdst = -1;
        }

        work_t = mktime(&work_tm);
    }

    return work_t;
}


//角度の計算
float CommonFunction::getAngle(CCPoint pos1, CCPoint pos2)
{
    float angle = 0;
    {
        CCPoint offsetPos = ccpSub(pos2, pos1);

        float angleRadians = atan2f(offsetPos.x, offsetPos.y);
        float angleDegrees = CC_RADIANS_TO_DEGREES(angleRadians);

        angle = angleDegrees;
    }

    return angle;
}

int CommonFunction::getRandomByWeight(int *weight,int count){
    int sum = 0;
    for (int i = 0; i < count; ++i) {
        sum += weight[i];
    }
    int t = arc4random() % sum;
    int value = 0;
    for (int i = 0; i < count; ++i) {
        if (t < weight[i]) {
            value = i;
            break;
        }
        t -= weight[i];
    }
    int result = value;
    
    return result;
}