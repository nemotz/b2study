//
//  MeterSprite.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/10/30.
//
//  Note.
//  このクラスのインスタンスは CCSpriteBatchNode には addChild できない。
//  (CCSprite 以外のクラスを使用しているため ... CCProgressTimer)
//

#include "MeterSprite.h"


USING_NS_CC;


#pragma mark - ### 生成 ###
MeterSprite * MeterSprite::create(const char * backImageFrameName, const char * meterImageFrameName)
{
    MeterSprite * pRep = new MeterSprite();
    if (pRep && pRep->init(backImageFrameName, meterImageFrameName))
    {
        pRep->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(pRep);
        pRep = NULL;
    }

    return pRep;
}


bool MeterSprite::init(const char * backImageFrameName, const char * meterImageFrameName)
{
    bool result = false;

    //*** 基本どこかで不備があればその時点で処理を抜ける ***
    do
    {
        //前提条件
        if (!backImageFrameName || !meterImageFrameName)
        {
            break;
        }

        if ( !CCSprite::initWithSpriteFrameName(backImageFrameName) )
        {
            break;
        }

        //プログレスバー
        {
            CCPoint position;
            {
                CCSize size = this->boundingBox().size;

                position = ccpMult(ccpFromSize(size), 0.5f);
            }

            //プログレスバー用CCSprite
            CCSprite * sprite = CCSprite::createWithSpriteFrameName(meterImageFrameName);
            if (!sprite)
            {
                break;
            }

            //プログレスバーの生成
            CCProgressTimer * progress = CCProgressTimer::create(sprite);
            if (!progress)
            {
                break;
            }

            progress->setPosition(position);

            int tagId = ChildTagMeterProgressTimer;
            this->addChild(progress, tagId, tagId);


            //*** プログレスバーの設定 ***
            //左から右へ増える(右から左へ減っていく)
            {
                progress->setType(kCCProgressTimerTypeBar);

                //Setup for a bar starting from the left since the midpoint is 0 for the x
                progress->setMidpoint(ccp(0, 0));

                //Setup for a horizontal bar since the bar change rate is 0 for y meaning no vertical change
                progress->setBarChangeRate(ccp(1, 0));
            }
        }

        //プログレスバーの初期値 (0〜100)
        this->setPercentage(100.0f);


        //ココまで来たら正常終了
        result = true;
    } while (0);

    return result;
}


CCProgressTimer * MeterSprite::getProgress()
{
    CCNode * node = this->getChildByTag(ChildTagMeterProgressTimer);
    return (CCProgressTimer *)node;
}


void MeterSprite::setPercentage(float percentage)
{
    if (percentage < 0.0f)
    {
        percentage = 0.0f;
    }
    else if (100.0f < percentage)
    {
        percentage = 100.0f;
    }
    this->getProgress()->setPercentage(percentage);
}

float MeterSprite::getPercentage()
{
    float value = 0.0f;

    CCProgressTimer * progress = this->getProgress();
    if (progress)
    {
        value = progress->getPercentage();
    }

    return value;
}