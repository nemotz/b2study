//
//  TwitterConfig.h
//  Common
//
//  Created by Goodia.Inc on 2013/07/01.
//
//

#ifndef Common_TwitterConfig_h
#define Common_TwitterConfig_h

//TitleLayer用
#define TWITTER_BEST_I "ベストスコア[%d%s] iPhoneゲーム「%s」http://itunes.apple.com/app/id%s #%s"
#define TWITTER_BEST_A "ベストスコア[%d%s] Androidゲーム「%s」https://play.google.com/store/apps/details?id=jp.co.goodia.%s #%s"

//ClearLayer用
#define TWITTER_I "今回の結果[%d%s] iPhoneゲーム「%s」http://itunes.apple.com/app/id%s #%s"
#define TWITTER_A "今回の結果[%d%s] Androidゲーム「%s」https://play.google.com/store/apps/details?id=jp.co.goodia.%s #%s"

#endif
