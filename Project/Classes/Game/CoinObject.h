//
//  WallObject.h
//  Knock
//
//  Created by 逢坂 隆司 on 13/03/01.
//
//

#ifndef __Knock__CoinObject__
#define __Knock__CoinObject__

#include "cocos2d.h"
#include "PrivateConfig.h"

//アクションタグ

class CoinObject : public cocos2d::CCNode
{
public:
    static CoinObject * create();

private:
    bool init();
    void remove();
    
    bool _isGot;
public:
    cocos2d::CCRect hitRect();
    cocos2d::CCRect boundingBox();
    void startAction();
    
    bool getIsGot(){return _isGot;}
    void setIsGot(){_isGot = true;}
    
    
private:
    void invisible();
protected:
    CoinObject();
    virtual ~CoinObject();

};

#endif /* defined(__Knock__CoinObject__) */
