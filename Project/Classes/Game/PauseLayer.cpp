//
//  PauseLayer.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include "PauseLayer.h"
#include "CommonFunction.h"


USING_NS_CC;


PauseLayer * PauseLayer::create(PauseDelegate *delegate)
{
    PauseLayer * pRep = new PauseLayer();
    if (pRep && pRep->init(delegate))
    {
        pRep->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(pRep);
    }

    return pRep;
}


bool PauseLayer::init(PauseDelegate * delegate)
{
    bool result = false;

    do
    {
        if (!ModalLayer::init())
        {
            break;
        }

        _delegate = delegate;

        //構成
        {
            CCSize winSize = CCDirector::sharedDirector()->getWinSize();
            CCPoint center = ccpMult(ccpFromSize(winSize), 0.5f);

            _scaleRate = CommonFunction::getScaleRate();
            _adjustRate = CommonFunction::getAdjustRate();

            {
                CCLayerColor * layer = CCLayerColor::create(ccc4(0, 0, 0, 128), winSize.width, winSize.height);

                int tagId = ChildTagBackLayer;
                this->addChild(layer, tagId, tagId);
            }


            //背景
            CCSprite * backSprite = NULL;
            {
                CCSprite * sprite = CCSprite::createWithSpriteFrameName("main_pause.png");

                sprite->setScale(_scaleRate);
                sprite->setPosition(center);

                int tagId = ChildTagBackSprite;
                this->addChild(sprite, tagId, tagId);

                backSprite = sprite;
            }


            //メニュー
            {
                CCMenuItemSprite * itemRestart = NULL;
                {
                    CCString * fileName = CCString::create("main_btn_backgame.png");

                    CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    selected->setColor(ccGRAY);

                    itemRestart = CCMenuItemSprite::create(normal, selected, this, menu_selector(PauseLayer::restartTouched));
                    itemRestart->setScale(_scaleRate);
                }

                CCMenuItemSprite * itemTitle = NULL;
                {
                    CCString * fileName = CCString::create("main_btn_backtitle.png");

                    CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    selected->setColor(ccGRAY);

                    itemTitle = CCMenuItemSprite::create(normal, selected, this, menu_selector(PauseLayer::titleTouched));
                    itemTitle->setScale(_scaleRate);
                }

                CCMenuItemSprite * itemMoreApps = NULL;
                {
                    CCString * fileName = CCString::create("main_btn_moreapps.png");

                    CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    selected->setColor(ccGRAY);

                    itemMoreApps = CCMenuItemSprite::create(normal, selected, this, menu_selector(PauseLayer::moreAppsTouched));
                    itemMoreApps->setScale(_scaleRate);
                }

                //メニュー作成
                {
                    CCMenu * menu = CCMenu::create(itemRestart, itemTitle, itemMoreApps, NULL);
                    if (menu)
                    {
                        float ds = 15.0f * _adjustRate;

                        menu->alignItemsVerticallyWithPadding(ds);
                        menu->setPosition(backSprite->getPosition());

                        int tagId = ChildTagPauseMenu;
                        this->addChild(menu, tagId, tagId);

                        //TODO: Ver.2.0.3 だと モーダルレイヤーとして実装はできないみたい (Ver.2.1.x 以降？)
                        //ココで タッチ優先度を調整する予定
                    }
                }
            }
        }

        result = true;
    } while (0);

    return result;
}


CCMenu * PauseLayer::getMenu()
{
    CCNode * node = this->getChildByTag(ChildTagPauseMenu);
    return (CCMenu *)node;
}


void PauseLayer::restartTouched(CCNode * sender)
{
    if (this->getDelegate())
    {
        this->getDelegate()->restartTouched();
    }

    //画面を閉じる
    this->menuCloseCallBack(sender);
}

void PauseLayer::titleTouched(CCNode * sender)
{
    if (this->getDelegate())
    {
        this->getDelegate()->titleTouched();
    }

    //画面を閉じる
    this->menuCloseCallBack(sender);
}

void PauseLayer::moreAppsTouched(CCNode * sender)
{
    if (this->getDelegate())
    {
        this->getDelegate()->moreAppsTouched();
    }

    //画面は閉じない!!
}
