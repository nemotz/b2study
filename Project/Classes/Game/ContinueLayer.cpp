//
//  ContinueLayer.cpp
//  Elevator
//
//  Created by Goodia.Inc on 2013/09/11.
//
//

#include "ContinueLayer.h"
#include "CommonFunction.h"
#include "HelloWorldScene.h"


USING_NS_CC;


ContinueLayer * ContinueLayer::create(ContinueDelegate *delegate,bool isCoin,bool isReward)
{
    ContinueLayer * pRep = new ContinueLayer();
    if (pRep && pRep->init(delegate,isCoin,isReward))
    {
        pRep->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(pRep);
    }
    
    return pRep;
}


bool ContinueLayer::init(ContinueDelegate * delegate,bool isCoin,bool isReward)
{
    bool result = false;
    
    do
    {
        if (!ModalLayer::init())
        {
            break;
        }
        
        _delegate = delegate;
        
        //構成
        {
            CCSize winSize = CCDirector::sharedDirector()->getWinSize();
            CCPoint center = ccpMult(ccpFromSize(winSize), 0.5f);
            
            float scaleRate = CommonFunction::getScaleRateFill();
            float adjustRate = CommonFunction::getAdjustRateFill();
            
            bool isPortrait = CommonFunction::isOrientationPortrait();
            
            {
                CCLayerColor * layer = CCLayerColor::create(ccc4(0, 0, 0, 128), winSize.width, winSize.height);
                
                int tagId = ChildTagBackLayer;
                this->addChild(layer, tagId, tagId);
            }
            
            
            //背景
            CCSprite * backSprite = NULL;
            {
                CCSprite * sprite = CCSprite::createWithSpriteFrameName("main_continue.png");
                
                sprite->setPosition(center);
                
                int tagId = ChildTagBackSprite;
                this->addChild(sprite, tagId, tagId);
                
                backSprite = sprite;
            }
            
            //コインの枚数表示
            {
                //枚数
                CCLabelTTF* coinNum = NULL;
                {
                    int value = HelloWorld::sharedHelloWorldLayer()->getCoin();
                    CCString * text = CCString::createWithFormat("%d", value);
                    coinNum = CCLabelTTF::create(text->getCString(), fontList[0].c_str(), CommonFunction::getValueOnScale(38));
                    coinNum->setColor(ccBLACK);
                    coinNum->setPosition(CCPointZero);
                    coinNum->setVerticalAlignment(kCCVerticalTextAlignmentBottom);
                    coinNum->setHorizontalAlignment(kCCTextAlignmentCenter);
                    coinNum->setAnchorPoint(CCPointZero);
                }
                
                CCLabelTTF* coinSuffix = NULL;
                {
                    CCString * text = CCString::createWithFormat("%s", COIN_SUFFIX);
                    coinSuffix = CCLabelTTF::create(text->getCString(), fontList[0].c_str(), CommonFunction::getValueOnScale(28));
                    coinSuffix->setColor(ccBLACK);
                    coinSuffix->setVerticalAlignment(kCCVerticalTextAlignmentBottom);
                    coinSuffix->setHorizontalAlignment(kCCTextAlignmentCenter);
                    coinSuffix->setPosition(ccp(coinNum->boundingBox().size.width,0));
                    coinSuffix->setAnchorPoint(ccp(0,-0.14f));
                }
                
                {
                    CCLayerColor * layer = CCLayerColor::create(ccc4(0, 0, 0, 0));
                    
                    float layerW = coinNum->boundingBox().size.width + coinSuffix->boundingBox().size.width;
                    float layerH = coinNum->boundingBox().size.height;
                    
                    layer->setContentSize(CCSizeMake(layerW, layerH));
                    
                    layer->addChild(coinNum);
                    layer->addChild(coinSuffix);
                    
                    if (isPortrait) {
                        layer->setPosition(ccp(backSprite->boundingBox().size.width/2 - layer->boundingBox().size.width/2,
                                               backSprite->boundingBox().size.height * 0.5f));
                    }
                    else {
                        layer->setPosition(ccp(backSprite->boundingBox().size.width/2 - layer->boundingBox().size.width/2,backSprite->boundingBox().size.height/2 + 40 * CommonFunction::getAdjustRateFill()));
                    }
                    backSprite->addChild(layer, ChildTagNumberLabel, ChildTagNumberLabel);
                }
            }
            
            //メニュー
            {
                CCMenuItemSprite * itemYes = NULL;
                {
                    CCString * fileName = CCString::create("main_btn_continue_1.png");
                    
                    CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    selected->setColor(ccGRAY);
                    
                    itemYes = CCMenuItemSprite::create(normal, selected, this, menu_selector(ContinueLayer::restartYesTouched));
                    
                }
                
                CCMenuItemSprite * itemNo = NULL;
                {
                    CCString * fileName = CCString::create("main_btn_continue_cancel.png");
                    
                    CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    selected->setColor(ccGRAY);
                    itemNo = CCMenuItemSprite::create(normal, selected, this, menu_selector(ContinueLayer::restartNoTouched));

                }
                
                CCMenuItemSprite * itemReward = NULL;
                {
                    CCString * fileName = CCString::create("main_btn_continue_2.png");
                    
                    CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    selected->setColor(ccGRAY);
                    itemReward = CCMenuItemSprite::create(normal, selected, this, menu_selector(ContinueLayer::rewardCotinueTouched));
                    
                }

                
                //メニュー作成
                {
                    CCMenu * menu = NULL;
                    if (isCoin && isReward) {
                        menu = CCMenu::create(itemReward,itemYes, NULL);
                    }
                    else if (isCoin){
                        menu = CCMenu::create(itemYes, NULL);
                    }
                    else if (isReward){
                        menu = CCMenu::create(itemReward, NULL);
                    }
                    
                    if (menu)
                    {
                        float ds = 10.0f * adjustRate;
                        
                        if(isPortrait)
                        {
                            menu->alignItemsVerticallyWithPadding(ds);
                            menu->setPosition(ccp(backSprite->boundingBox().size.width/2,
                                                  backSprite->boundingBox().size.height * 0.27f));
                        }
                        else
                        {
                            menu->alignItemsHorizontallyWithPadding(ds);
                            menu->setPosition(ccp(backSprite->boundingBox().size.width/2,
                                                  backSprite->boundingBox().size.height/2 - 85.0f * adjustRate));
                            
                        }
                        
                        int tagId = ChildTagContinueMenu;
                        backSprite->addChild(menu, tagId, tagId);
                        
                        //TODO: Ver.2.0.3 だと モーダルレイヤーとして実装はできないみたい (Ver.2.1.x 以降？)
                        //ココで タッチ優先度を調整する予定
                    }
                }
                
                //メニュー作成
                {
                    CCMenu * cancelMenu = CCMenu::create(itemNo,NULL);
                    
                    if (cancelMenu)
                    {
                        
                        cancelMenu->setPosition(ccp(backSprite->boundingBox().size.height - itemNo->boundingBox().size.width * 0.75f,
                                                backSprite->boundingBox().size.width + itemNo->boundingBox().size.height * 0.25f));
                        
                        int tagId = ChildTagContinueMenu;
                        backSprite->addChild(cancelMenu, tagId, tagId);
                        
                        //TODO: Ver.2.0.3 だと モーダルレイヤーとして実装はできないみたい (Ver.2.1.x 以降？)
                        //ココで タッチ優先度を調整する予定
                    }
                }
            }
            backSprite->setOpacity(128);
            backSprite->setScale(0.5f * scaleRate);
        }
        this->startAction();
        result = true;
    } while (0);
    
    return result;
}

void ContinueLayer::startAction(){
    CCSprite * backSprite = this->getBackSprite();
    CCFiniteTimeAction * action = NULL;
    {
        CCActionInterval * scale = CCScaleTo::create(0.6f, CommonFunction::getScaleRateFill());
        CCFiniteTimeAction * ease  = CCEaseBounceOut::create(scale);
        
        CCFiniteTimeAction * fade  = CCFadeIn::create(0.6f);
        
        action = CCSpawn::createWithTwoActions(ease, fade);
    }
    backSprite->runAction(action);
    
    this->scheduleOnce(schedule_selector(ContinueLayer::playSE), 0.3f);
}

void ContinueLayer::playSE(){
    CommonFunction::myEffectPlayWithFile("SE_CoinChance.mp3");
}

cocos2d::CCSprite * ContinueLayer::getBackSprite(){
    CCSprite * sprite = (CCSprite *)this->getChildByTag(ChildTagBackSprite);
    return sprite;
}

CCMenu * ContinueLayer::getMenu()
{
    CCNode * node = this->getChildByTag(ChildTagContinueMenu);
    return (CCMenu *)node;
}


void ContinueLayer::restartYesTouched(CCNode * sender)
{
    if (this->getDelegate())
    {
        this->getDelegate()->restartYesTouched();
    }
    
    //画面を閉じる
    this->menuCloseCallBack(sender);
}

void ContinueLayer::restartNoTouched(CCNode * sender)
{
    if (this->getDelegate())
    {
        this->getDelegate()->restartNoTouched();
    }
    
    //画面を閉じる
    this->menuCloseCallBack(sender);
}

void ContinueLayer::rewardCotinueTouched(CCNode * sender)
{
    if (this->getDelegate())
    {
        this->getDelegate()->rewardCotinueTouched();
    }
    
    //画面を閉じる
    this->menuCloseCallBack(sender);
}