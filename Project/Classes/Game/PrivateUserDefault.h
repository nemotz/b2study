//
//  PrivateUserDefault.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __TemplateProject__PrivateUserDefault__
#define __TemplateProject__PrivateUserDefault__

#include "cocos2d.h"
#include "PrivateConfig.h"


////*** 定数 ***
//全面広告用
#define AD_COUNT        (5)     //ゲームが終わる度にカウントするときのリミット
#define SPLASH_TYPE     "B"    //BEAD 100%の場合"B" AID 100%の場合"A" BEAD:AID 8:2の場合"BA" AID:BEAD 8:2の場合"AB"

//ゲーム
#define RANKING_MAX     (3)     //ランキング数
#define ITEM_MAX        (10)    //アイテム数


//クラス
class PrivateUserDefault
{

public:
    //キー名
    static std::string getKeyName(std::string key);
    static std::string getKeyName(std::string key, int number);

public:
    //値の取得
    static std::string getValue(std::string key, std::string defaultValue = "");
    static bool getValue(std::string key, bool defaultValue = false);
    static int getValue(std::string key, int defaultValue = 0);
    static float getValue(std::string key, float defaultValue = 0.0f);

    //値の設定
    static void setValue(std::string key, std::string value);
    static void setValue(std::string key, bool value);
    static void setValue(std::string key, int value);
    static void setValue(std::string key, float value);


public:
    //USERDEF_KEY_UUID
    static std::string getUUID();
    static void setUUID(std::string value);

    //USERDEF_KEY_AD_COUNT
    static int getADCount();
    static void setADCount(int value);

    //USERDEF_KEY_EXIT_AD_COUNT
    static int getExitADCount();
    static void setExitADCount(int value);

    //USERDEF_KEY_RETRY_AD_COUNT
    static int getRetryADCount();
    static void setRetryADCount(int value);
    
    //USERDEF_KEY_LASTVIEWDATE_MOREAPPS
    static int getLastViewDateMoreApps();
    static void setLastViewDateMoreApps(int value);

    //USERDEF_KEY_SOUND
    static bool getSound();
    static void setSound(bool value);

    //USERDEF_KEY_INITSHOW_HELP
    static bool getInitShowHelp();
    static void setInitShowHelp(bool value);


    //USERDEF_KEY_FORMAT__NORMAL__BEST_POINT
    static SCORE_DATA_TYPE getNormalBestScore(int number);
    static void setNormalBestScore(int number, SCORE_DATA_TYPE value);

    //USERDEF_KEY_FORMAT__PREMIUM__BEST_POINT
    static SCORE_DATA_TYPE getPremiumBestScore(int number);
    static void setPremiumBestScore(int number, SCORE_DATA_TYPE value);


    //USERDEF_KEY_FORMAT__NORMAL__ITEM_COUNT
    static int getNormalItemCount(int number);
    static void setNormalItemCount(int number, int value);

    //USERDEF_KEY_FORMAT__NORMAL__ITEM_VISIBLED
    static bool getNormalItemVisibled(int number);
    static void setNormalItemVisibled(int number, bool value);


    //USERDEF_KEY_FORMAT__PREMIUM__ITEM_COUNT
    static int getPremiumItemCount(int number);
    static void setPremiumItemCount(int number, int value);

    //USERDEF_KEY_FORMAT__PREMIUM__ITEM_VISIBLED
    static bool getPremiumItemVisibled(int number);
    static void setPremiumItemVisibled(int number, bool value);


    //USERDEF_KEY__NORMAL__NEXT_ITEM_INDEX
    static int getNormalNextItemIndex();
    static void setNormalNextItemIndex(int value);

    //USERDEF_KEY__PREMIUM__NEXT_ITEM_INDEX
    static int getPremiumNextItemIndex();
    static void setPremiumNextItemIndex(int value);
	
	//USERDEF_KEY__COIN_COUNT
    static int getCoinCount();
    static void setCoinCount(int value);
};

#endif
