//
//  TitleLayer.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include "SimpleAudioEngine.h"

#include "PrivateUserDefault.h"

#include "TitleLayer.h"
#include "HelloWorldScene.h"
#include "HelpLayer.h"
#include "NativeInterface.h"

#include "CommonFunction.h"
#include "PrivateConfig.h"
#include "TwitterConfig.h"

#include "Encryption.h"


//タイトル用アニメーション処理をするかどうか
//TODO: 残務 ... アニメーションの初期位置の調整、アニメーション中の分岐
//#define TITLE_ANIMATION


//iOS プレミアムモード解除チェック処理をスルーするかどうか (デバッグ用)
//#define DEBUG_PREMIUM_UNLOCK
#ifndef DEBUG
    #ifdef DEBUG_PREMIUM_UNLOCK
        #undef DEBUG_PREMIUM_UNLOCK
    #endif
#endif

//審査前用 プレミアムモード解除チェック処理をスルーするかどうか (デバッグ用)
//#define DEBUG_PASS__BEFORE_RELEASE_JUDGE
#ifndef DEBUG
    #ifdef DEBUG_PASS__BEFORE_RELEASE_JUDGE
        #undef DEBUG_PASS__BEFORE_RELEASE_JUDGE
    #endif
#endif


using namespace cocos2d;
using namespace CocosDenshion;


CCScene * TitleLayer::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    CCLayer *layer = TitleLayer::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}


bool TitleLayer::init()
{
    if ( CCLayer::init() )
    {
        //内部変数の初期化
        {
        }


        CCSize winSize = CCDirector::sharedDirector()->getWinSize();
        CCPoint center = ccpMult(ccpFromSize(winSize), 0.5f);

        //iOS/Android 分岐用
        TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();

        //縦横取得
        _isPortrait = CommonFunction::isOrientationPortrait();


        //背景
        CCSprite * backSprite;
        {
            CCSprite * sprite = CCSprite::createWithSpriteFrameName("title_back_1.png");
			
			//上下余白ができるぶんを埋めるため
			{
                CCSize size = sprite->getContentSize();
                float kw = winSize.width / size.width;
                float kh = winSize.height / size.height;
				sprite->setScaleX(kw);
				sprite->setScaleY(kh);
            }

            sprite->setPosition(center);

            int tagId = ChildTagBackSprite;
            this->addChild(sprite, tagId, tagId);

            backSprite = sprite;
        }

        //画像
        {
            CCSprite * sprite = CCSprite::createWithSpriteFrameName("title_back_2.png");
            sprite->setAnchorPoint(ccp(0.5f, 0.5f));
            sprite->setPosition(ccp(winSize.width * 0.5f, winSize.height * 0.5f));

            int tagId = ChildTagImage1Sprite;
            this->addChild(sprite, tagId, tagId);
        }

        //タイトル
        CCRect boundBoxTitle;
        {
            CCSprite * sprite = CCSprite::createWithSpriteFrameName("title_title.png");

            if (_isPortrait)
            {
                CCPoint position;
                {
                    position = ccp(winSize.width * 0.5f, winSize.height * 0.725f);

#ifdef TITLE_ANIMATION
                    position = ccpAdd(position, ccp(winSize.width, 0.0f));
#endif
                }

                sprite->setPosition(position);
            }
            else
            {
                float dx = 25;
                CCPoint position;
                {
                    position = ccp(winSize.width - dx,
                                   center.y + (sprite->boundingBox().size.height * 0.275f));

#ifdef TITLE_ANIMATION
                    position = ccpAdd(position, ccp(winSize.width, 0.0f));
#endif
                }
                sprite->setAnchorPoint(ccp(1.0f, 0.5f));
                sprite->setPosition(position);
            }

            int tagId = ChildTagTitleSprite;
            this->addChild(sprite, tagId, tagId);

            boundBoxTitle = sprite->boundingBox();
			
#ifdef TITLE_ANIMATION_BOUNCE
			CCAction * action = NULL;
			{
				CCFiniteTimeAction * zoomin = CCScaleTo::create(0.8f, sprite->getScaleX()*0.95f,sprite->getScaleY()*0.90f);
				CCFiniteTimeAction * zoomout = CCScaleTo::create(0.8f, sprite->getScale());
				
				CCFiniteTimeAction * sequence = CCSequence::create(zoomin, zoomout, NULL);
				action = CCRepeatForever::create((CCActionInterval *)sequence);
				sprite->runAction(action);
			}
#endif
        }


        //ベストスコア表示位置 (実際の設定箇所はコード内を変数名で検索してください)
        CCPoint bestScorePosition = CCPointZero;

		//iPadのときはボタンを90%のサイズにする
		float scale = 1.0f;
		if (target == kTargetIpad) {
			scale = 0.9f;
		}
		
        //メニュー
        {
            //スタート
            CCMenuItemSprite * itemStart = NULL;
            {
                CCString * fileName = CCString::create("title_btn_start.png");

                CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                selected->setColor(ccGRAY);

                CCMenuItemSprite * item = CCMenuItemSprite::create(normal, selected, this, menu_selector(TitleLayer::startTouched));
                item->setTag(ChildTagMenu_StartItemSprite);
				item->setScale(scale);

                itemStart = item;
            }

            //アプリ広告
            CCMenuItem * itemAdApps = NULL;
            {
                CCString * fileName = CCString::create("title_btn_moreapps.png");

                CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                selected->setColor(ccGRAY);

                CCMenuItemSprite * item = CCMenuItemSprite::create(normal, selected, this, menu_selector(TitleLayer::moreAppsTouched));
                item->setTag(ChildTagMenu_AdAppsItemSprite);
				item->setScale(scale);

                itemAdApps = item;
            }

            //ランキング（縦画面のみ使用）
            CCMenuItem * itemRanking = NULL;
            {
                CCString * fileName = CCString::create("title_btn_worldranking.png");

                CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                selected->setColor(ccGRAY);

                CCMenuItemSprite * item = CCMenuItemSprite::create(normal, selected, this, menu_selector(TitleLayer::rankingTouched));
                item->setTag(ChildTagMenu_RankingItemSprite);
				item->setScale(scale);

                itemRanking = item;
            }

            //自社広告
            CCMenuItem * itemGoodiaApps = NULL;
            {
                CCString * fileName = CCString::create("title_btn_goodia.png");

                CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                selected->setColor(ccGRAY);

                CCMenuItemSprite * item = CCMenuItemSprite::create(normal, selected, this, menu_selector(TitleLayer::goodiaMoreAppsTouched));
                item->setTag(ChildTagMenu_GoodiaAppsItemSprite);
				item->setScale(scale);

                itemGoodiaApps = item;
            }

            //メニュー作成
            {
                CCMenu * menu1 = NULL;
                CCMenu * menu2 = NULL;

                if (_isPortrait)
                {
                    float ds = 20.0f;

                    menu1 = CCMenu::create(itemStart,
                                           itemRanking,
                                           NULL);
                    if (menu1)
                    {
                        CCPoint position = CCPointZero;
                        {
                            position = ccp(winSize.width * 0.5f, winSize.height * 0.30f);
#ifdef TITLE_ANIMATION
                            position = ccpAdd(position, ccp(winSize.width, 0.0f));
#endif
                        }

                        menu1->alignItemsHorizontallyWithPadding(ds);
                        menu1->setPosition(position);

                        int tagId = ChildTagMainMenu1;
                        this->addChild(menu1, tagId, tagId);
                    }

                    menu2 = CCMenu::create(
                                           itemAdApps,
                                           itemGoodiaApps,
                                           NULL);
                    if (menu2)
                    {
                        CCPoint position = CCPointZero;
                        {
                            position = ccp(winSize.width * 0.5f, winSize.height * 0.17f);
#ifdef TITLE_ANIMATION
                            position = ccpAdd(position, ccp(winSize.width, 0.0f));
#endif
                        }

                        menu2->alignItemsHorizontallyWithPadding(ds);
                        menu2->setPosition(position);

                        int tagId = ChildTagMainMenu2;
                        this->addChild(menu2, tagId, tagId);
                    }
                }
                else
                {
                    //下揃え
                    {
                        CCMenuItem * items[] = {
                            itemStart,
                            itemAdApps,
                            itemGoodiaApps,
                        };

                        for (int i = 0; i < sizeof(items)/sizeof(*items); i++)
                        {
                            items[i]->setAnchorPoint(ccp(0.5f, 0.0f));
                        }
                    }

                    //下を基準に配置
                    float dy = 55.5f;

                    menu1 = CCMenu::create(itemStart,
                                           itemAdApps,
                                           itemGoodiaApps,
                                           NULL);

                    menu1->alignItemsHorizontallyWithPadding(20.0f);
                    menu1->setPosition(ccp(winSize.width * 0.5f, dy));

                    //横配置用 ベストスコア表示位置
                    bestScorePosition = ccp(menu1->getPositionX() - itemAdApps->boundingBox().size.width/2 - (20.0f),
                                            menu1->getPositionY() + itemStart->boundingBox().size.height + (2.5f));

                    if (menu1)
                    {
                        int tagId = ChildTagMainMenu1;
                        this->addChild(menu1, tagId, tagId);
                    }
                }
            }
        }

        //システムメニュー
        {
            //サウンドボタン
            CCMenuItem * itemSound = NULL;
            {
                CCSprite * normal   = CCSprite::createWithSpriteFrameName("title_icon_speaker_on.png");
                CCSprite * selected = CCSprite::createWithSpriteFrameName("title_icon_speaker_off.png");

                CCMenuItemToggle * item = CCMenuItemToggle::createWithTarget(this,
                                                                             menu_selector(TitleLayer::soundTouched),
                                                                             CCMenuItemSprite::create(normal, NULL),
                                                                             CCMenuItemSprite::create(selected, NULL),
                                                                             NULL);

                item->setTag(ChildTagUpperMenu_SoundItemToggle);

                //初期設定
                {
                    bool flag = PrivateUserDefault::getSound();
                    item->setSelectedIndex((flag)?(0):(1));
                }

                itemSound = item;
            }

            //アプリ広告
            CCMenuItem * itemAdApps = NULL;
            {
                //ファイル名は特に意識しないで大丈夫です(おすすめアプリ)
                CCString * fileName = CCString::create("title_icon_moreapps.png");

                CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                selected->setColor(ccGRAY);

                CCMenuItemSprite * item = CCMenuItemSprite::create(normal, selected, this, menu_selector(TitleLayer::moreAppsIconTouched));
                item->setTag(ChildTagUpperMenu_AdAppsItemSprite);

                itemAdApps = item;
            }

            //ヘルプ
            CCMenuItem * itemHelp = NULL;
            {
                CCString * fileName = CCString::create("title_icon_help.png");

                CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                selected->setColor(ccGRAY);

                CCMenuItemSprite * item = CCMenuItemSprite::create(normal, selected, this, menu_selector(TitleLayer::helpTouched));
                item->setTag(ChildTagUpperMenu_HelpItemSprite);

                itemHelp = item;
            }

            //ランキング（横画面のときのみ使用）
            CCMenuItem * itemRanking;
            if (!_isPortrait)
            {
                CCString * fileName;
                if (target == kTargetIphone || target == kTargetIpad)
                {
                    fileName = CCString::create("title_icon_worldranking_ios.png");
                }
                else
                {
                    fileName = CCString::create("title_icon_worldranking_android.png");
                }

                CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                selected->setColor(ccGRAY);

                itemRanking = CCMenuItemSprite::create(normal, selected, this, menu_selector(TitleLayer::rankingTouched));
            }

            //Twitter
            CCMenuItem * itemTwitter = NULL;
            {
                CCString * fileName = CCString::create("title_icon_twitter.png");

                CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                selected->setColor(ccGRAY);

                CCMenuItemSprite * item = CCMenuItemSprite::create(normal, selected, this, menu_selector(TitleLayer::twitterTouched));
                item->setTag(ChildTagUpperMenu_TwitterItemSprite);

                itemTwitter = item;
            }

            //画面上部 メニュー作成
            {
                if (_isPortrait)
                {
                    //配置位置（たて）
                    float ds = 10.0f;
                    float dy = 50.0f;

                    CCSize size1 = itemSound->boundingBox().size;
                    CCSize size2 = itemTwitter->boundingBox().size;

                    //MenuItem Array
                    CCArray * items = CCArray::create();
                    {
                        CCMenuItem * _items[] = {
                            itemSound,
                            itemTwitter,
                            itemAdApps,
                            itemHelp,
                        };
                        
                        int count = sizeof(_items)/sizeof(*_items);
                        for (int i = 0; i < count; i++)
                        {
                            if (_items[i])
                            {
                                items->addObject(_items[i]);
                            }
                        }
                    }

                    //ボタン数
                    int numberOfButtons = items->count();

                    CCPoint position = ccp(ds + (((size2.width + ds) * numberOfButtons - ds) * 0.5f),
                                           winSize.height - (size1.height * 0.5f) - (dy + ds));

                    CCMenu * menu = NULL;
                    {
                        menu = CCMenu::createWithArray(items);

                        menu->alignItemsHorizontallyWithPadding(ds);
                        menu->setPosition(position);
                    }

                    if (menu)
                    {
                        this->addChild(menu, ChildTagSystemUpperMenu1, ChildTagSystemUpperMenu1);
                    }

                    //縦配置用 ベストスコア表示位置
                    bestScorePosition = ccp(winSize.width - (10.0f),
                                            position.y);
                }
                else
                {
                    //配置位置（よこ）
                    float ds =5.0f;
                    float dx = ((320.0f + (160.0f * 0.5f)) * 0.5f);
                    float dy = 0.0f;

                    CCRect systemMenuBoundingBox;

                    //上部ボタン
                    {
                        //サウンドボタン、ヘルプ
                        {
                            //配置位置
                            CCPoint position = CCPointZero;

                            //メニューの配置
                            {
                                CCMenu * menu = NULL;
                                
                                //iOS
                                if (target == kTargetIphone || target == kTargetIpad) //ユニバーサル対応でも消さないこと
                                {
                                    menu = CCMenu::create(itemSound, itemHelp, NULL);
                                    menu->alignItemsHorizontallyWithPadding(ds);

                                    //4inch用
                                    if ((target == kTargetIphone) && (winSize.width > 480.0f))
                                    {
                                        menu->alignItemsHorizontallyWithPadding(ds * 4);
                                        position = ccp(center.x - (dx + ds * 4),
                                                       winSize.height - (itemSound->boundingBox().size.height * 0.5f) - (dy + ds));
                                    }
                                    else
                                    {
                                        position = ccp((itemSound->boundingBox().size.width + itemHelp->boundingBox().size.width + ds * 3) * 0.5f,
                                                       winSize.height - (itemSound->boundingBox().size.height * 0.5f) - (dy + ds));
                                    }
                                }
                                else
                                {
                                    menu = CCMenu::create(itemSound, itemHelp, itemTwitter, itemAdApps, NULL);
                                    menu->alignItemsHorizontallyWithPadding(ds * 2);
                                    {
                                        position = ccp(itemSound->boundingBox().size.width + itemHelp->boundingBox().size.width + itemTwitter->boundingBox().size.width,
                                                       winSize.height - itemSound->boundingBox().size.height - 50.0f);
                                    }
                                }

                                menu->setPosition(position);

                                if (menu)
                                {
                                    this->addChild(menu, ChildTagSystemUpperMenu1, ChildTagSystemUpperMenu1);
                                }

                                systemMenuBoundingBox = menu->boundingBox();
                            }
                        }

                        //ランキングボタン
                        {
                            //配置位置
                            CCPoint position = CCPointZero;

                            //メニューの配置
                            {
                                CCMenu * menu = NULL;

                                //iOS
                                if (target == kTargetIphone || target == kTargetIpad)
                                {
                                    menu = CCMenu::create(itemRanking, NULL);
                                    position = ccp(center.x + dx,
                                                   winSize.height - (itemSound->boundingBox().size.height * 0.5f) - (dy + ds));
                                }
                                else
                                {
                                    menu = CCMenu::create(itemRanking, NULL);
                                    position = ccp(winSize.width - itemRanking->boundingBox().size.width,
                                                   winSize.height - itemSound->boundingBox().size.height - 100.0f);
                                }

                                menu->setPosition(position);

                                if (menu)
                                {
                                    int tagId = ChildTagSystemUpperMenu2;
                                    this->addChild(menu, tagId, tagId);
                                }
                            }
                        }
                    }


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
                    //下部ボタン
                    {
                        //Twitterボタン
                        {
                            //メニュー作成
                            {
                                //配置位置
                                CCPoint position = CCPointZero;
                                {
                                    position = ccp(center.x + dx,
                                                   0.0f + (itemTwitter->boundingBox().size.height * 0.5f) + (dy + ds));
                                }

                                CCMenu * menu = CCMenu::create(itemTwitter, NULL);;
                                menu->setPosition(position);

                                if (menu)
                                {
                                    int tagId = ChildTagUpperMenu_TwitterItemSprite;
                                    this->addChild(menu, tagId, tagId);
                                }
                            }
                        }

                        //おすすめアイコン
                        {
                            //メニュー作成
                            {
                                //配置位置
                                CCPoint position = CCPointZero;
                                {
                                    position = ccp(center.x - dx,
                                                   0.0f + (itemAdApps->boundingBox().size.height * 0.5f) + (dy + ds));
                                }

                                CCMenu * menu = CCMenu::create(itemAdApps, NULL);;
                                menu->setPosition(position);

                                if (menu)
                                {
                                    int tagId = ChildTagUpperMenu_AdAppsItemSprite;
                                    this->addChild(menu, tagId, tagId);
                                }
                            }
                        }
                    }
#endif
                }
            }

            //ベストスコアを表示
			float fontSize = 17.0f;
            {

                if (_isPortrait)
                {
                    CCString * text = CCString::createWithFormat("best 0%s", SUFFIX);

                    CCLabelTTF * label = CCLabelTTF::create(text->getCString(), fontList[0].c_str(), fontSize);

                    label->setColor(ccBLACK);
                    label->setAnchorPoint(ccp(1.0f, 0.5f));
                    label->setPosition(bestScorePosition);

                    int tagId = ChildTagBestLabel;
                    this->addChild(label, tagId, tagId);
                }
                else
                {
                    CCString * text = CCString::createWithFormat("best 0%s", SUFFIX);

                    CCLabelTTF * label = CCLabelTTF::create(text->getCString(), fontList[0].c_str(), fontSize);

                    label->setColor(ccBLACK);
                    label->setAnchorPoint(ccp(1.0f, 0.0f));
                    label->setPosition(bestScorePosition);

                    int tagId = ChildTagBestLabel;
                    this->addChild(label, tagId, tagId);
                }
            }
			
#ifdef USE_COIN_CONTINUE
			//coin
            {
                if (_isPortrait)
                {
                    CCString * text = CCString::createWithFormat("累計コイン 0%s", COIN_SUFFIX);
                    
                    CCLabelTTF * label = CCLabelTTF::create(text->getCString(), fontList[0].c_str(), fontSize);
                    
                    label->setColor(ccc3(233, 78, 42));
                    label->setAnchorPoint(ccp(1.0f, 0.5f));
                    label->setPosition(ccpSub(bestScorePosition, ccp(0, fontSize)));
                    
                    int tagId = ChildTagCoinLabel;
                    this->addChild(label, tagId, tagId);
                }
                else
                {
                    CCString * text = CCString::createWithFormat("累計コイン 0%s", COIN_SUFFIX);

                    CCLabelTTF * label = CCLabelTTF::create(text->getCString(), fontList[0].c_str(), fontSize);
                    
                    label->setColor(ccBLACK);
                    label->setAnchorPoint(ccp(1.0f, 0.0f));
                    label->setPosition(ccpSub(bestScorePosition, ccp(0, fontSize)));
                    
                    int tagId = ChildTagCoinLabel;
                    this->addChild(label, tagId, tagId);
                }
            }
#endif
        }

        //Android Backキー, Homeキー対応用
        if (target == kTargetAndroid)
        {
            this->setKeypadEnabled(true);
        }

        return true;
    }
    else
    {
        return false;
    }
}

TitleLayer::TitleLayer()
{
    //スプライトシートの読み込み
    {
        CCSpriteFrameCache * frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();

        frameCache->addSpriteFramesWithFile("title_gameimage.plist");
    }
}

TitleLayer::~TitleLayer()
{
    //スプライトシートの解放
    {
        CCSpriteFrameCache * frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();

        frameCache->removeSpriteFrameByName("title_gameimage.plist");
    }
}


void TitleLayer::onEnter()
{
    CCLayer::onEnter();

    //ベストスコアをセット
    {
        SCORE_DATA_TYPE rankScore = PrivateUserDefault::getNormalBestScore(1);

        CCString * score = NULL;
        if (rankScore <= 0)
        {
            score = CCString::create("");
        }
        else
        {
            //ベストスコア
            //score = CCString::createWithFormat("best %d", rankScore);
            score = CCString::createWithFormat("best %d%s", rankScore, SUFFIX);
            //score = CCString::createWithFormat("best %.2lf %s", (float)rankScore/100.0f, SUFFIX);

            //ベストタイム
            //score = CCString::createWithFormat("best %s", CommonFunction::getFormatTimerString((float)rankScore * 0.01f)->getCString());
        }

        this->getBestLabel()->setString(score->getCString());
    }
	
#ifdef USE_COIN_CONTINUE
	//コイン枚数をセット
	{
		CCString * coin = NULL;
		if (PrivateUserDefault::getCoinCount() <= 0)
        {
            coin = CCString::create("");
        }
        else
        {
            coin = CCString::createWithFormat("累計コイン %d%s", PrivateUserDefault::getCoinCount(), COIN_SUFFIX);
        }
		this->getCoinLabel()->setString(coin->getCString());
	}
#endif

    //onEnterTransitionDidFinish()でも同じ処理
    //(たまにアイコン型広告が残ってしまう症状がある対策？)
    {
        //広告表示
        Cocos2dExt::NativeInterface::showAd(true);
        Cocos2dExt::NativeInterface::showHouseAd(true);
        Cocos2dExt::NativeInterface::showGenuineAd(false);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        Cocos2dExt::NativeInterface::showIconAd(1, true);
#else
        Cocos2dExt::NativeInterface::showIconAd(1);
#endif
    }


    //BGM
    CommonFunction::myBackgroundMusicPlayWithFile("BGM_Title.mp3", true);
}


void TitleLayer::onEnterTransitionDidFinish()
{
    CCLayer::onEnterTransitionDidFinish();

#ifdef TITLE_ANIMATION
    //タイトルアニメーション
    this->startAnimation1();
#else
    //ボタンのバッジ処理のみ
    this->setNewBadge();
#endif
}


void TitleLayer::onExit()
{
    CCLayer::onExit();
}


CCLabelTTF * TitleLayer::getBestLabel()
{
    CCNode * node = this->getChildByTag(ChildTagBestLabel);
    return (CCLabelTTF *)node;
}

#ifdef USE_COIN_CONTINUE
CCLabelTTF * TitleLayer::getCoinLabel()
{
    CCNode * node = this->getChildByTag(ChildTagCoinLabel);
    return (CCLabelTTF *)node;
}
#endif

void TitleLayer::keyBackClicked()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    //Exit広告配信率設定 ... Androidのみ
    Cocos2dExt::NativeInterface::showExitAd();
#else
    //アプリ終了
    CCDirector::sharedDirector()->end();
#endif
}

void TitleLayer::keyMenuClicked()
{
}


void TitleLayer::setTitleMenusEnabled(bool flag)
{
    int tagId[] = {

        //メニュー
        ChildTagMainMenu1,
        ChildTagMainMenu2,

        //上部ボタン
        ChildTagSystemUpperMenu1,
        ChildTagSystemUpperMenu2,

        //下部ボタン
        ChildTagSystemBottomMenu,
    };

    for (int i = 0; i < sizeof(tagId)/sizeof(*tagId); i++)
    {
        CCMenu * menu = (CCMenu *)this->getChildByTag(tagId[i]);
        if (menu)
        {
            menu->setEnabled(flag);
        }
    }
}

void TitleLayer::gameStart()
{
    this->gameStart(GameMode_Normal);
}

void TitleLayer::gameStart(GameMode mode)
{
    CCScene * scene = HelloWorld::sceneWithGameMode(mode);
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(0.8f, scene));
}


void TitleLayer::startTouched()
{
    //BGM OFF
    SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();

    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
	
	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("T_GAME_START");

    this->gameStart(GameMode_Normal);
}

void TitleLayer::helpTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
	
	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("T_HELP");

    CCLayer * layer = HelpLayer::create();
    this->addChild(layer, ChildTagHelpLayer, ChildTagHelpLayer);
	
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	Cocos2dExt::NativeInterface::showIconAd(0, false);
#else
	Cocos2dExt::NativeInterface::showIconAd(0);
#endif
}

void TitleLayer::rankingTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    Cocos2dExt::NativeInterface::leaderBoard();
//#endif
	
	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("T_WORLD_RANKING");
	
	//インターステイシャル広告表示
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	Cocos2dExt::NativeInterface::showAdSplashAddRanking();
#else
	Cocos2dExt::NativeInterface::showOptionalAdRank();
#endif
}


void TitleLayer::goodiaMoreAppsTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
	
    CCString * url = NULL;
	
    TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
    switch (target)
    {
            //iOS
        case kTargetIpad:
        case kTargetIphone:
        {
            url = CCString::create("http://goodiaappcan.appspot.com/appinfo/appinfo_ending.html");
            break;
        }
			
            //Android
        case kTargetAndroid:
        {
            url = CCString::create("http://goodiaappcan.appspot.com/appinfo/appinfo_A_ending.html");
            break;
        }
			
        default:
            break;
    }

    if (url)
    {
        Cocos2dExt::NativeInterface::launchUrl(url->getCString());
    }


    //ユーザデフォルトの最終表示日を更新
    {
        //現時間の取得
        time_t now;
        time(&now);

        PrivateUserDefault::setLastViewDateMoreApps(now);
    }

    //Newバッヂを表示していれば消す
    {
        CCSprite * sprite = (CCSprite *)this->getChildByTag(ChildTagGoodiaAppsBadgeSprite);
        if (sprite)
        {
            sprite->removeFromParentAndCleanup(true);
        }
    }
	
	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("T_GOODIA_APPS");
}


void TitleLayer::soundTouched()
{
    CCMenu * menu = (CCMenu *)this->getChildByTag(ChildTagSystemUpperMenu1);
    if (menu)
    {
        CCMenuItemToggle * toggle = (CCMenuItemToggle *)menu->getChildByTag(ChildTagUpperMenu_SoundItemToggle);
        if (toggle)
        {
            CCLOG("%s toggle.selected:%d", __FUNCTION__, toggle->getSelectedIndex());
            
            switch (toggle->getSelectedIndex())
            {
                    //Sound On
                case (0):
                {
                    PrivateUserDefault::setSound(true);

                    //BGM
                    CommonFunction::myBackgroundMusicPlayWithFile("BGM_Title.mp3", true);
					
					//FLURRY
					Cocos2dExt::NativeInterface::reportFlurryWithEvent("T_SOUND_ON");

                    break;
                }
                    //Sound Off
                case (1):
                {
                    PrivateUserDefault::setSound(false);

                    //BGM OFF
                    SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
					
					//FLURRY
					Cocos2dExt::NativeInterface::reportFlurryWithEvent("T_SOUND_OFF");

                    break;
                }
                default:
                    break;
            }

            CCUserDefault::sharedUserDefault()->flush();
        }
    }
}

void TitleLayer::twitterTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");

    //Tweetする文章
    CCString * tweet = NULL;

    //スコア値
    SCORE_DATA_TYPE score = PrivateUserDefault::getNormalBestScore(1);

    TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
    switch (target)
    {
        case kTargetIpad:
        case kTargetIphone:
        {
            tweet = CCString::createWithFormat(TWITTER_BEST_I, score, SUFFIX, PROJECT_NAME, IOS_APP_ID, PROJECT_NAME);
            break;
        }
        case kTargetAndroid:
        {
            tweet = CCString::createWithFormat(TWITTER_BEST_A, score, SUFFIX, PROJECT_NAME, ANDROID_APP_ID, PROJECT_NAME);
            break;
        }

        default:
            break;
    }
	
	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("T_TWITTER");

    Cocos2dExt::NativeInterface::launchTwitter(tweet->getCString());
}


void TitleLayer::moreAppsTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");

    CCLOG("%s", __PRETTY_FUNCTION__);
	
	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("T_RECOMMEND_BUTTON");
	
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	
#else
	Cocos2dExt::NativeInterface::showOptionalAdWall();
#endif

    Cocos2dExt::NativeInterface::showAppliPromotion();
}

void TitleLayer::moreAppsIconTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");

    CCLOG("%s", __PRETTY_FUNCTION__);

    Cocos2dExt::NativeInterface::showGameFeat();
	
	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("T_RECOMMEND_ICON");
	
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	
#else
	Cocos2dExt::NativeInterface::showOptionalAdWall();
#endif
}


void TitleLayer::startAnimation1()
{
    CCSprite * sprite = (CCSprite *)this->getChildByTag(ChildTagTitleSprite);

    if (!sprite)
    {
        return;
    }

    CCAction * action = NULL;
    {
        CCSize winSize = CCDirector::sharedDirector()->getWinSize();

        //CCFiniteTimeAction * delay = CCDelayTime::create(0.5f);
        CCFiniteTimeAction * ease = CCEaseElasticOut::create(CCMoveBy::create(0.3f, ccp(-winSize.width, 0.0f)),1.0f);
        CCFiniteTimeAction * func = CCCallFunc::create(this, callfunc_selector(TitleLayer::startAnimation2));

        action = CCSequence::create(
                                    //delay,
                                    ease,
                                    func,
                                    NULL);
        action->setTag(ActionTagTitleAnimation);
    }

    if (action)
    {
        sprite->stopActionByTag(ActionTagTitleAnimation);
        sprite->runAction(action);
    }
}


void TitleLayer::startAnimation2()
{
    CCMenu * menu1 = (CCMenu *)this->getChildByTag(ChildTagMainMenu1);

    if (!menu1)
    {
        return;
    }

    CCAction * action = NULL;
    {
        CCSize winSize = CCDirector::sharedDirector()->getWinSize();

        CCFiniteTimeAction * ease = CCEaseElasticOut::create(CCMoveBy::create(0.3f, ccp(-winSize.width, 0.0f)),1.0f);

        CCFiniteTimeAction * func;
        if (_isPortrait){
            //縦画面
            func = CCCallFunc::create(this, callfunc_selector(TitleLayer::startAnimation3));
        } else {
            //横画面
            func = CCCallFunc::create(this, callfunc_selector(TitleLayer::setNewBadge));
        }
        action = CCSequence::create(
                                    ease,
                                    func,
                                    NULL);
        action->setTag(ActionTagMenu1Animation);
    }

    if (action)
    {
        menu1->stopActionByTag(ActionTagMenu1Animation);
        menu1->runAction(action);
    }
}


void TitleLayer::startAnimation3()
{
    CCMenu * menu2 = (CCMenu *)this->getChildByTag(ChildTagMainMenu2);

    if (!menu2)
    {
        return;
    }

    CCAction * action = NULL;
    {
        CCSize winSize = CCDirector::sharedDirector()->getWinSize();

        CCFiniteTimeAction * ease = CCEaseElasticOut::create(CCMoveBy::create(0.3f, ccp(-winSize.width, 0.0f)),1.0f);
        CCFiniteTimeAction * func = CCCallFunc::create(this, callfunc_selector(TitleLayer::setNewBadge));

        action = CCSequence::create(
                                    ease,
                                    func,
                                    NULL);
        action->setTag(ActionTagMenu2Animation);
    }

    if (action)
    {
        menu2->stopActionByTag(ActionTagMenu2Animation);
        menu2->runAction(action);
    }

}


void TitleLayer::setNewBadge()
{
    int menuTagId = 0;
    if (_isPortrait){
    //縦画面
        menuTagId = ChildTagMainMenu2;
    } else {
    //横画面
        menuTagId = ChildTagMainMenu1;
    }

    //Goodia MoreApps Newバッヂ
    {
        //申請時点のリリース予定日の取得
        time_t work_t = CommonFunction::getEntryReleaseDate();

        //Newバッヂを表示するかどうか
        bool blnShowBadge = false;
        {
            int last_date = PrivateUserDefault::getLastViewDateMoreApps();

            //リリース日より前の日付なら強制的に表示
            if (last_date < work_t)
            {
                blnShowBadge = true;
            }
            else
            {
                //現時間の取得
                time_t now;
                time(&now);

                //86400 ... 60 * 60 * 24 (一日の秒数)
                static const int ONEDAY_COUNT_SEC = 86400;

                //TEST 60秒後
                //if ((now - last_date) >= 60)
                //指定した日数以上の間隔が空いていれば
                if ((now - last_date) >= (ONEDAY_COUNT_SEC * INTERVAL_MOREAPPS_LASTVIEW))
                {
                    blnShowBadge = true;
                }
            }
        }

        if (blnShowBadge)
        {
            CCMenu * menu = (CCMenu *)this->getChildByTag(menuTagId);
            if (menu)
            {
                CCMenuItemSprite * item = (CCMenuItemSprite *)menu->getChildByTag(ChildTagMenu_GoodiaAppsItemSprite);
                if (item)
                {
                    CCRect boundingBox = item->boundingBox();
                    CCPoint position;

                    CCSprite * sprite = CCSprite::createWithSpriteFrameName("title_new.png");
                    {
                        CCSize size = sprite->boundingBox().size;

                        position = menu->getPosition();
                        position = ccpAdd(position, ccp(boundingBox.getMaxX() - size.width * 0.25f,
                                                        boundingBox.getMaxY()));
                    }
                    sprite->setPosition(position);

                    int tagId = ChildTagGoodiaAppsBadgeSprite;
                    this->addChild(sprite, tagId, tagId);
                }
            }
        }
    }

    //おすすめアプリ　イチオシバッジ
    {
        CCMenu * menu = (CCMenu *)this->getChildByTag(menuTagId);
        if (menu)
        {
            CCMenuItemSprite * item = (CCMenuItemSprite *)menu->getChildByTag(ChildTagMenu_AdAppsItemSprite);
            if (item)
            {
                CCRect boundingBox = item->boundingBox();
                CCPoint position;

                CCSprite * sprite = CCSprite::createWithSpriteFrameName("title_badge.png");
                {
                    CCSize size = sprite->boundingBox().size;

                    position = menu->getPosition();
                    position = ccpAdd(position, ccp(boundingBox.getMaxX() - size.width * 0.1f,
                                                    boundingBox.getMaxY()));
                }
                sprite->setPosition(position);

                int tagId = ChildTagAdAppsBadgeSprite;
                this->addChild(sprite, tagId, tagId);

                CCAction * action = NULL;
                {
                    CCFiniteTimeAction * zoomin = CCScaleTo::create(0.8f, 0.8f);
                    CCFiniteTimeAction * zoomout = CCScaleTo::create(0.8f, 1.0f);

                    CCFiniteTimeAction * sequence = CCSequence::create(zoomin, zoomout, NULL);
                    action = CCRepeatForever::create((CCActionInterval *)sequence);
                    sprite->runAction(action);
                }
            }
         }
    }
}
