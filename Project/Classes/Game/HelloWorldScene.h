//
//  HelloWorldScene.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __TemplateProject__HELLOWORLD_SCENE_H__
#define __TemplateProject__HELLOWORLD_SCENE_H__


#include "cocos2d.h"
#include "PrivateConfig.h"
#include "PauseLayer.h"

#ifdef USE_COIN_CONTINUE
#include "ContinueLayer.h"
#endif

class GameLayer;
class InputLayer;

#ifdef USE_JOYSTICK
class JoyStickLayer;
#endif


class HelloWorld
: public cocos2d::CCLayer
, public PauseDelegate
#ifdef USE_COIN_CONTINUE
, public ContinueDelegate
#endif
{

//### 内部で使用しているタグ番号 ###
public:
    //アクションタグ
    typedef enum _ActionTags
    {
        ActionTagNone,
    } ActionTags;


    //Z順 & タグ
    typedef enum _ChildTags
    {
        ChildTagNone = 0,

        //*** ゲーム用 ***
        //GameLayer
        ChildTagGameLayer,

        //JoyStick
        ChildTagJoyStick,

//        //入力用レイヤー
//        ChildTagInputLayer,


        //*** 共通 ***
        ChildTagInfoLayer,

        //時間表示
        ChildTagTimeBackSprite,
        ChildTagTimeLabel,

        //スコア表示用ラベル
        ChildTagScoreBackSprite,
        ChildTagScoreLabel,

        //コイン表示用ラベル
        ChildTagScoreBackSprite_coin,
        ChildTagScoreLabel_coin,
		
        //*** 一時停止(ボタン) ***
        ChildTagPauseMenu,
		
        //*** Tap Start ***
        ChildTagHelpBackLayer,
        ChildTagTapStartSprite = 200,
		
        //コンティニュー
        ChildTagContinueMenu = 300,
        ChildTagContinueLayer,
		
        //*** Help ***
        ChildTagHelpSprite,

        //*** 一時停止(最前面) ***
        ChildTagPauseLayer,

    } ChildTags;


protected:
    //HelloWorld();
    virtual ~HelloWorld();

public:
    /** returns a shared instance of the director */
    static HelloWorld * sharedHelloWorldLayer();


//拡張 (ゲームモード指定)
public:
    static cocos2d::CCScene* sceneWithGameMode(GameMode mode);
    static HelloWorld * createWithGameMode(GameMode mode);
    bool initWithGameMode(GameMode mode);


//ゲームモード
public:
    CC_SYNTHESIZE_READONLY(GameMode, _gameMode, GameMode);

//モード別: リソース名の取得
public:
    static cocos2d::CCString * getResourceSuffix(GameMode mode);
    static cocos2d::CCString * getResourceName(GameMode mode, const char * fileName);
    cocos2d::CCString * getResourceName(const char * fileName);


//モード別: ユーザーデフォルトの値を取得
public:
    static SCORE_DATA_TYPE getBestPoint(GameMode mode, int number);
    static void setBestPoint(GameMode mode, int number, SCORE_DATA_TYPE value);

    static int getItemCount(GameMode mode, int number);
    static void setItemCount(GameMode mode, int number, int value);

    static bool getItemVisibled(GameMode mode, int number);
    static void setItemVisibled(GameMode mode, int number, bool value);

    static int getNextItemIndex(GameMode mode);
    static void setNextItemIndex(GameMode mode, int value);
	
#ifdef USE_COIN_CONTINUE
    static int getCoinCount();
    static void setCoinCount(int value);
#endif


private:
    virtual void onEnter();
    virtual void onExit();


//### 一時停止 ###
public:
    void pauseGame();

private:
    void pauseTouched();

//一時停止 デリゲート処理
private:
    void restartTouched();
    void titleTouched();
    void moreAppsTouched();


//タイマー処理
private:
    void update(float delta);
    void clockUpdate(float delta);


//スコア
public:
    CC_SYNTHESIZE(SCORE_DATA_TYPE, _score, Score);//累計距離
	
#ifdef USE_COIN_CONTINUE
    CC_SYNTHESIZE(SCORE_DATA_TYPE, _coin, Coin);//累計コイン
    CC_SYNTHESIZE(int, _coinMax, CoinMax);//最大コイン数
#endif

public:
    void addScore(SCORE_DATA_TYPE score);
    void updateScore(SCORE_DATA_TYPE score);
	
#ifdef USE_COIN_CONTINUE
    void updateCoin(SCORE_DATA_TYPE coin);
#endif

//ゲーム共通処理
public:
    void gameStart();
    void gameEnd(bool clear);

private:
    //画面タップでゲーム開始
    void gameTapStart();

    //ゲーム終了
    void gameClear();


private:
    void nodeRemoveFromParent(CCNode * sender);


//構成物
public:
#ifdef USE_JOYSTICK
    JoyStickLayer * getJoyStickLayer();
#endif

    InputLayer * getInputLayer();
    GameLayer * getGameLayer();

    //スコア用ラベル
    cocos2d::CCLabelTTF * getScoreLabel();

#ifdef USE_COIN_CONTINUE
	//コイン用ラベル
    cocos2d::CCLabelTTF * getCoinLabel();
#endif
	
    //一時停止メニュー
    cocos2d::CCMenu * getPauseMenu();


public:
    //ゲーム開始判定用フラグ
    CC_SYNTHESIZE(bool, _isGameStart, IsGameStart);

    //ゲーム終了判定用フラグ
    CC_SYNTHESIZE(bool, _isGameOver, IsGameOver);
	
#ifdef USE_COIN_CONTINUE
	//ゲーム再開判定用フラグ
    CC_SYNTHESIZE(bool, _isGameRestart, IsGameRestart);
	
	//TapJoy判定用フラグ
	CC_SYNTHESIZE(bool, _isTapJoy, IsTapJoy);
#endif
	
#ifdef USE_COIN_CONTINUE
//### 以下、コンティニュー関係 ###
public:
    void continueGame();
    void gameRestart();
	
//コンティニュー デリゲート処理
private:
    void restartYesTouched();
    void restartNoTouched();
    void rewardCotinueTouched();
    void useCoin();
    void gameTapRestart();
    void createContinueLayer();
	
private:
    //tapjoy
    void getVideoReward();//動画広告があるかどうか調べさせる。
    void showVideoReward();//動画広告を表示
    int _countOfRewardContinue;//１ゲーム中に動画コンテニュー出来る回数をしばる時に使ってた
public:
    bool getIsVideoRewardEnabled();
    void rewardMovieEnded(bool isContinueEnabled);
#endif

};

#endif
