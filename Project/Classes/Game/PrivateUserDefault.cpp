//
//  PrivateUserDefault.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include <iostream>
//#include <boost/format.cpp>

#include "PrivateUserDefault.h"



//*** 定数 ***
//今後 キー名の接頭語に アプリ名 + アンダーバーをつける
//キー名の接頭語はPrivateConfig.hにて設定

//UUID
#define USERDEF_KEY_UUID                    "personalIdentifiter"

//AppDriver インターステイシャル広告 (全面広告) ... 最初 と 以後 5回ごとに1回表示
#define USERDEF_KEY_AD_COUNT                "adCount"
//#define AD_COUNT                            (5)         //ゲームが終わる度にカウントするときのリミット

//Exit広告用 カウント(BEADとAIDを交互に表示)
#define USERDEF_KEY_EXIT_AD_COUNT           "ExitAdCount"

//Retry広告用 カウント(５回ごとに１回表示)
#define USERDEF_KEY_RETRY_AD_COUNT          "RetryAdCount"


//[Goodia]ボタン用 Newバッヂ (10日以上で再表示)
#define USERDEF_KEY_LASTVIEWDATE_MOREAPPS   "LastViewDateMoreApps"


//サウンド ON/OFF
#define USERDEF_KEY_SOUND                   "Sound"

//App最初の起動の時にヘルプを表示したかどうか
#define USERDEF_KEY_INITSHOW_HELP           "help"


////プレミアムモード解除 (UUIDを利用)
//#define USERDEF_KEY_PREMIUM_UNLOCK_CODE     "PreminumModeUnLockCode"


//=== ベストスコア ===
#define USERDEF_KEY_FORMAT__NORMAL__BEST_POINT      "BestScore_Point_%d"
#define USERDEF_KEY_FORMAT__PREMIUM__BEST_POINT     "BestScore_Premium_Point_%d"


//=== アイテム・コレクション ===
//- 通常 -
#define USERDEF_KEY_FORMAT__NORMAL__ITEM_COUNT      "Item%d_Count"
#define USERDEF_KEY_FORMAT__NORMAL__ITEM_VISIBLED   "Item%d_Visibled"
//- プレミアム -
#define USERDEF_KEY_FORMAT__PREMIUM__ITEM_COUNT     "Premium_Item%d_Count"
#define USERDEF_KEY_FORMAT__PREMIUM__ITEM_VISIBLED  "Premium_Item%d_Visibled"


//=== 次に表示するアイテム ===   (-1:空, 0:未設定, 1〜10:アイテム番号)
#define USERDEF_KEY__NORMAL__NEXT_ITEM_INDEX        "NextItemIndex"
#define USERDEF_KEY__PREMIUM__NEXT_ITEM_INDEX       "Premium_NextItemIndex"


//=== コイン ===
#define USERDEF_KEY__COIN_COUNT						"CoinCount"

USING_NS_CC;


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === 共通メソッド ===
#endif

//キー名
std::string PrivateUserDefault::getKeyName(std::string key)
{
    std::string value = std::string(USERDEF_PREFIX_PROJECTNAME) + std::string("_") + key;

    return value;
}

std::string PrivateUserDefault::getKeyName(std::string key, int number)
{
    //boost::io::format(key % number);
    //... #include <boost/format.cpp> でエラーが発生するので CCString を使用した

    std::string subKey = CCString::createWithFormat(key.c_str(), number)->getCString();
    std::string value = std::string(USERDEF_PREFIX_PROJECTNAME) + std::string("_") + subKey;

    return value;
}



//値の取得
std::string PrivateUserDefault::getValue(std::string key, std::string defaultValue)
{
    std::string result = std::string("");

    {
        //ユーザーデフォルトファイル
        CCUserDefault * userDefault = CCUserDefault::sharedUserDefault();

        //値の取得
        result = userDefault->getStringForKey(key.c_str(), defaultValue);
    }

    return result;
}

bool PrivateUserDefault::getValue(std::string key, bool defaultValue)
{
    bool result = false;

    {
        //ユーザーデフォルトファイル
        CCUserDefault * userDefault = CCUserDefault::sharedUserDefault();

        //値の取得
        result = userDefault->getBoolForKey(key.c_str(), defaultValue);
    }

    return result;
}

int PrivateUserDefault::getValue(std::string key, int defaultValue)
{
    int result = 0;

    {
        //ユーザーデフォルトファイル
        CCUserDefault * userDefault = CCUserDefault::sharedUserDefault();

        //値の取得
        result = userDefault->getIntegerForKey(key.c_str(), defaultValue);
    }

    return result;
}

float PrivateUserDefault::getValue(std::string key, float defaultValue)
{
    float result = 0.0f;

    {
        //ユーザーデフォルトファイル
        CCUserDefault * userDefault = CCUserDefault::sharedUserDefault();

        //値の取得
        result = userDefault->getFloatForKey(key.c_str(), defaultValue);
    }

    return result;
}

//値の設定
void PrivateUserDefault::setValue(std::string key, std::string value)
{
    //ユーザーデフォルトファイル
    CCUserDefault * userDefault = CCUserDefault::sharedUserDefault();

    //値の取得
    userDefault->setStringForKey(key.c_str(), value.c_str());
    userDefault->flush();
}

void PrivateUserDefault::setValue(std::string key, bool value)
{
    //ユーザーデフォルトファイル
    CCUserDefault * userDefault = CCUserDefault::sharedUserDefault();

    //値の取得
    userDefault->setBoolForKey(key.c_str(), value);
    userDefault->flush();
}

void PrivateUserDefault::setValue(std::string key, int value)
{
    //ユーザーデフォルトファイル
    CCUserDefault * userDefault = CCUserDefault::sharedUserDefault();

    //値の取得
    userDefault->setIntegerForKey(key.c_str(), value);
    userDefault->flush();
}

void PrivateUserDefault::setValue(std::string key, float value)
{
    //ユーザーデフォルトファイル
    CCUserDefault * userDefault = CCUserDefault::sharedUserDefault();

    //値の取得
    userDefault->setFloatForKey(key.c_str(), value);
    userDefault->flush();
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_UUID ===
#endif
std::string PrivateUserDefault::getUUID()
{
    std::string result = std::string("");

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_UUID);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setUUID(std::string value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_UUID);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_AD_COUNT ===
#endif
int PrivateUserDefault::getADCount()
{
    int result = 0;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_AD_COUNT);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setADCount(int value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_AD_COUNT);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_EXIT_AD_COUNT ===
#endif
int PrivateUserDefault::getExitADCount()
{
    int result = 0;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_EXIT_AD_COUNT);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setExitADCount(int value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_EXIT_AD_COUNT);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_RETRY_AD_COUNT ===
#endif
int PrivateUserDefault::getRetryADCount()
{
    int result = 0;
    
    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_RETRY_AD_COUNT);
        
        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }
    
    return result;
}

void PrivateUserDefault::setRetryADCount(int value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_RETRY_AD_COUNT);
    
    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}



#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_LASTVIEWDATE_MOREAPPS ===
#endif
int PrivateUserDefault::getLastViewDateMoreApps()
{
    int result = 0;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_LASTVIEWDATE_MOREAPPS);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setLastViewDateMoreApps(int value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_LASTVIEWDATE_MOREAPPS);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_SOUND ===
#endif
bool PrivateUserDefault::getSound()
{
    bool result = true;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_SOUND);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setSound(bool value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_SOUND);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_INITSHOW_HELP ===
#endif
bool PrivateUserDefault::getInitShowHelp()
{
    bool result = false;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_INITSHOW_HELP);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setInitShowHelp(bool value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_INITSHOW_HELP);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_FORMAT__NORMAL__BEST_POINT ===
#endif
SCORE_DATA_TYPE PrivateUserDefault::getNormalBestScore(int number)
{
    SCORE_DATA_TYPE result = 0;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__NORMAL__BEST_POINT, number);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setNormalBestScore(int number, SCORE_DATA_TYPE value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__NORMAL__BEST_POINT, number);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_FORMAT__PREMIUM__BEST_POINT ===
#endif
SCORE_DATA_TYPE PrivateUserDefault::getPremiumBestScore(int number)
{
    SCORE_DATA_TYPE result = 0;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__PREMIUM__BEST_POINT, number);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setPremiumBestScore(int number, SCORE_DATA_TYPE value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__PREMIUM__BEST_POINT, number);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_FORMAT__NORMAL__ITEM_COUNT ===
#endif
int PrivateUserDefault::getNormalItemCount(int number)
{
    int result = 0;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__NORMAL__ITEM_COUNT, number);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setNormalItemCount(int number, int value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__NORMAL__ITEM_COUNT, number);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_FORMAT__NORMAL__ITEM_VISIBLED ===
#endif
bool PrivateUserDefault::getNormalItemVisibled(int number)
{
    bool result = false;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__NORMAL__ITEM_VISIBLED, number);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setNormalItemVisibled(int number, bool value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__NORMAL__ITEM_VISIBLED, number);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_FORMAT__PREMIUM__ITEM_COUNT ===
#endif
int PrivateUserDefault::getPremiumItemCount(int number)
{
    int result = 0;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__PREMIUM__ITEM_COUNT, number);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setPremiumItemCount(int number, int value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__PREMIUM__ITEM_COUNT, number);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY_FORMAT__PREMIUM__ITEM_VISIBLED ===
#endif
bool PrivateUserDefault::getPremiumItemVisibled(int number)
{
    bool result = false;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__PREMIUM__ITEM_VISIBLED, number);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setPremiumItemVisibled(int number, bool value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY_FORMAT__PREMIUM__ITEM_VISIBLED, number);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY__NORMAL__NEXT_ITEM_INDEX ===
#endif
int PrivateUserDefault::getNormalNextItemIndex()
{
    int result = 0;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY__NORMAL__NEXT_ITEM_INDEX);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setNormalNextItemIndex(int value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY__NORMAL__NEXT_ITEM_INDEX);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === USERDEF_KEY__PREMIUM__NEXT_ITEM_INDEX ===
#endif
int PrivateUserDefault::getPremiumNextItemIndex()
{
    int result = 0;

    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY__PREMIUM__NEXT_ITEM_INDEX);

        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }

    return result;
}

void PrivateUserDefault::setPremiumNextItemIndex(int value)
{
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY__PREMIUM__NEXT_ITEM_INDEX);

    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}

//USERDEF_KEY__COIN_COUNT
int PrivateUserDefault::getCoinCount(){
    int result = 0;
    
    {
        //キー名
        std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY__COIN_COUNT);
        
        //値の取得
        result = PrivateUserDefault::getValue(keyName, result);
    }
    
    return result;
}
void PrivateUserDefault::setCoinCount(int value){
    //キー名
    std::string keyName = PrivateUserDefault::getKeyName(USERDEF_KEY__COIN_COUNT);
    
    //値の設定
    PrivateUserDefault::setValue(keyName, value);
}

