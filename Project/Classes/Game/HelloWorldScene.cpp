//
//  HelloWorldScene.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include "SimpleAudioEngine.h"

#include "NativeInterface.h"

#include "CommonFunction.h"
#include "PrivateUserDefault.h"

#include "HelloWorldScene.h"
#include "TitleLayer.h"
#include "PauseLayer.h"
#include "ClearLayer.h"
#include "GameLayer.h"
#include "InputLayer.h"

#ifdef USE_JOYSTICK
#include "JoyStickLayer.h"
#endif

#ifdef USE_COIN_CONTINUE
#include "ContinueLayer.h"
#define COIN_MAX (100)
#define REWARD_ONE_TIME_PER_ONE_GAME (1) //0は１ゲーム何回でも動画コンテニュー。1:１ゲーム一度しか動画コンテニューできない。
#endif

using namespace cocos2d;
using namespace CocosDenshion;


// ChildTags ... ヘッダーで定義


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === シングルトン処理 ===
#endif

static HelloWorld * _sharedHelloWorldLayer = NULL;

HelloWorld * HelloWorld::sharedHelloWorldLayer()
{
    return _sharedHelloWorldLayer;
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === Scene ===
#endif

//CCScene* HelloWorld::scene()
CCScene* HelloWorld::sceneWithGameMode(GameMode mode)
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    HelloWorld *layer = HelloWorld::createWithGameMode(mode);

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === 基本処理(生成〜破棄) ===
#endif

HelloWorld * HelloWorld::createWithGameMode(GameMode mode)
{
    HelloWorld * pRet = new HelloWorld();
    if (pRet && pRet->initWithGameMode(mode))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;

        return NULL;
    }
}


// on "init" you need to initialize your instance
bool HelloWorld::initWithGameMode(GameMode mode)
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }


    //生成したオブジェクト(シングルトン)
    _sharedHelloWorldLayer = this;


    //内部変数の初期化
    {
        //ゲームモード
        _gameMode = mode;

        //スコア用
        _score = 0;

        //ゲーム開始判定用フラグ
        _isGameStart = false;

        //ゲーム終了判定用フラグ
        _isGameOver = false;
		
#ifdef USE_COIN_CONTINUE
		//コインコンティニュー系
		_countOfRewardContinue = 0;
		_isTapJoy = false;
#endif
		
    }


    /////////////////////////////
    // 3. add your codes below...
    {
        CCSize winSize = CCDirector::sharedDirector()->getWinSize();

        //画面上部
        {
            float frame_pos_x = 0.0f;
            float ds = 10.0f;
            float dy = 5.0f;

            //背景(コレを基準に配置 ... 非表示)
            CCSprite * frameBack = NULL;
            {
                CCSprite * sprite = CCSprite::createWithSpriteFrameName("main_frame_scorelong.png");
                {
                    sprite->setAnchorPoint(ccp(0.0f,1.0f));
                    sprite->setPosition(ccp(0, winSize.height - dy));
                    sprite->setScaleX(winSize.width /sprite->boundingBox().size.width);
                    sprite->setVisible(false);
                }

                int tagId = ChildTagInfoLayer;
                this->addChild(sprite, tagId, tagId);

                frameBack = sprite;
            }

            //スコアを表示
            CCRect scoreBoundingBox = CCRectZero;
            {
                //スコアの背景
                {
                    CCSprite* sprite = CCSprite::createWithSpriteFrameName("main_frame_score.png");

                    sprite->setAnchorPoint( ccp(1.0f, 0.5f));
                    sprite->setPosition(ccp(winSize.width - ds, frameBack->boundingBox().getMidY()));

                    int tagId = ChildTagScoreBackSprite;
                    this->addChild(sprite, tagId, tagId);
                    scoreBoundingBox = sprite->boundingBox();
                }

                //ラベル
                {
                    float fontSize = 20.0f;
                    CCString * text = CCString::createWithFormat("0%s", SUFFIX);
                    CCLabelTTF * label = CCLabelTTF::create(text->getCString(), fontList[0].c_str(), fontSize);

                    label->setAnchorPoint( ccp(1.0f, 0.5f));
                    label->setPosition(ccp(winSize.width - ds * 2,
                                                           frameBack->boundingBox().getMidY()));

                    int tagId = ChildTagScoreLabel;
                    this->addChild(label, tagId, tagId);
                }
            }

            //一時停止ボタン
            CCMenuItemSprite * itemPause;
            CCRect pauseBoundingBox = CCRectZero;
            {
                {
                    CCString * fileName = CCString::create("main_frame_pause.png");
                    CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    selected->setColor(ccGRAY);
                    itemPause = CCMenuItemSprite::create(normal, selected, this, menu_selector(HelloWorld::pauseTouched));
                }

                //メニュー作成
                {
                    CCMenu * menu = CCMenu::create(itemPause, NULL);
                    if (menu)
                    {
                        frame_pos_x = 30.0f;
                        menu->setPosition(ccp(frame_pos_x, frameBack->boundingBox().getMidY()));

                        int tagId = ChildTagPauseMenu;
                        this->addChild(menu, tagId, tagId);

                        //ゲーム開始後に有効
                        menu->setTouchEnabled(false);
                    }
					pauseBoundingBox = CCRectMake(menu->getPositionX() - itemPause->boundingBox().size.width/2,
												  menu->getPositionY() - itemPause->boundingBox().size.height/2,
												  itemPause->boundingBox().size.width,
												  itemPause->boundingBox().size.height);

                }
				
#ifdef USE_COIN_CONTINUE
				{
					//コイン枚数の背景
					CCRect coinCounterBoundingBox = CCRectZero;
					{
						CCSprite* sprite = CCSprite::createWithSpriteFrameName("main_frame_score.png");
						
						sprite->setAnchorPoint( ccp(0.5f, 0.5f));
						sprite->setPosition(ccp((pauseBoundingBox.getMaxX() + scoreBoundingBox.getMinX()) / 2,scoreBoundingBox.getMidY()));
						
						int tagId = ChildTagScoreBackSprite_coin;
						this->addChild(sprite, tagId, tagId);
						
						coinCounterBoundingBox = sprite->boundingBox();
					}
					
					//ラベル
					{
						float fontSize = 20.0f;
						CCString * text = CCString::createWithFormat("0%s", COIN_SUFFIX);
						CCLabelTTF * label = CCLabelTTF::create(text->getCString(), fontList[0].c_str(), fontSize);
						
						label->setAnchorPoint( ccp(1.2f, 0.5f));
						label->setPosition(ccp(coinCounterBoundingBox.getMaxX(),coinCounterBoundingBox.getMidY()));
						
						int tagId = ChildTagScoreLabel_coin;
						this->addChild(label, tagId, tagId);
					}
				}
#endif

            }
        }

        //ゲーム部分
        GameLayer * gameLayer = NULL;
        {
            GameLayer * layer = GameLayer::create();
            this->addChild(layer, ChildTagGameLayer, ChildTagGameLayer);

            gameLayer = layer;
        }

//        //入力用レイヤー
//        {
//            InputLayer * layer = InputLayer::create();
//            int tagId = ChildTagInputLayer;
//            this->addChild(layer, tagId, tagId);
//
//            //デリゲート先の指定
//            layer->setTouchDelegate(gameLayer);
//
//            //layer->setTouchDelegate(gameLayer->getPlayer());
//            layer->setTouchEnabled(true);
//        }


        //ゲーム開始処理
        {
            CCFiniteTimeAction * func = CCCallFunc::create(this, callfunc_selector(HelloWorld::gameTapStart));

            CCFiniteTimeAction * action = CCSequence::create(func, NULL);

            this->runAction(action);
        }
#ifdef USE_COIN_CONTINUE
		_coinMax = COIN_MAX;
        this->getVideoReward();
#endif
    }

    return true;
}


//HelloWorld::HelloWorld()
//{
//}

HelloWorld::~HelloWorld()
{
    if (_sharedHelloWorldLayer)
    {
        _sharedHelloWorldLayer = NULL;
    }
}


void HelloWorld::onEnter()
{
    CCLayer::onEnter();

    //広告表示
    Cocos2dExt::NativeInterface::showAd(true);
    Cocos2dExt::NativeInterface::showHouseAd(false);

    Cocos2dExt::NativeInterface::showGenuineAd(false);
    Cocos2dExt::NativeInterface::genuineAdPreparation();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    Cocos2dExt::NativeInterface::showIconAd(0, false);
#else
    Cocos2dExt::NativeInterface::showIconAd(0);
#endif

    CommonFunction::myBackgroundMusicPlayWithFile("BGM_InGame.mp3", true);

    //パーティクルのキャッシュ
    CCParticleSystemQuad::create("flower_particle2.plist");
}

void HelloWorld::onExit()
{
    CCLayer::onExit();
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === タイマー処理 ===
#endif

void HelloWorld::update(float delta)
{
    //ゲーム終了していたら処理しない
    if (this->getIsGameOver())
    {
        return;
    }

    if (!this->getIsGameStart())
    {
        return;
    }

    return;
}


void HelloWorld::clockUpdate(float delta)
{
    //ゲーム終了していたら処理しない
    if (this->getIsGameOver())
    {
        return;
    }
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === 構成物の取得 ===
#endif

//JoyStick
#ifdef USE_JOYSTICK
JoyStickLayer * HelloWorld::getJoyStickLayer()
{
    CCNode * node = this->getChildByTag(ChildTagJoyStick);
    return (JoyStickLayer *)node;
}
#endif


//入力用レイヤー
InputLayer * HelloWorld::getInputLayer()
{
    CCNode * node = NULL;

//    node = this->getChildByTag(ChildTagInputLayer);
	
    //GameLayerから取得
    {
        GameLayer * layer = this->getGameLayer();
        if (layer)
        {
            node = layer->getInputLayer();
        }
    }

    return (InputLayer *)node;
}

//ゲーム用レイヤー
GameLayer * HelloWorld::getGameLayer()
{
    CCNode * node = this->getChildByTag(ChildTagGameLayer);
    return (GameLayer *)node;
}

//スコア用ラベル
CCLabelTTF * HelloWorld::getScoreLabel()
{
    CCNode * node = this->getChildByTag(ChildTagScoreLabel);
    return (CCLabelTTF *)node;
}

#ifdef USE_COIN_CONTINUE
//コイン用ラベル
CCLabelTTF * HelloWorld::getCoinLabel()
{
    CCNode * node = this->getChildByTag(ChildTagScoreLabel_coin);
    return (CCLabelTTF *)node;
}
#endif

//一時停止用メニュー
CCMenu * HelloWorld::getPauseMenu()
{
    CCNode * node = this->getChildByTag(ChildTagPauseMenu);
    return (CCMenu *)node;
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === スコア処理 ===
#endif

//スコア
void HelloWorld::addScore(SCORE_DATA_TYPE score)
{
    if (!this->getIsGameStart())
    {
        return;
    }

    //スコア
    _score += score;

    //画面表示
    CCString * text = CCString::createWithFormat("%d%s", this->getScore(), SUFFIX);
    this->getScoreLabel()->setString(text->getCString());
}

void HelloWorld::updateScore(SCORE_DATA_TYPE score)
{
    //スコアの値更新チェック
    if (this->getScore() < score)
    {
        _score = score;

        //画面表示
        CCString * text = CCString::createWithFormat("%d%s", this->getScore(), SUFFIX);
        this->getScoreLabel()->setString(text->getCString());
    }
}

#ifdef USE_COIN_CONTINUE
//コイン加算
void HelloWorld::updateCoin(SCORE_DATA_TYPE coin){
    {
        _coin = coin;
        
        //画面表示
        CCString * text = CCString::createWithFormat("%d%s", this->getCoin(), COIN_SUFFIX);
        this->getCoinLabel()->setString(text->getCString());
    }
}
#endif


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === ゲーム制御 ===
#endif

void HelloWorld::gameTapStart()
{
    {
        CCSize  winSize = CCDirector::sharedDirector()->getWinSize();
        CCPoint center  = ccpMult(ccpFromSize(winSize), 0.5f);
		
		//iOS/Android 分岐用
        TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
		float scaleRate = 1.0f;
		if (target == kTargetIpad) {
			scaleRate *= 0.9f;
		}


        //簡易ヘルプの後ろに半透明のグレー
        {
            CCLayerColor * layer = CCLayerColor::create(ccc4(0, 0, 0, 128), winSize.width, winSize.height);
            this->addChild(layer, ChildTagHelpBackLayer, ChildTagHelpBackLayer);
        }

        //簡易ヘルプを表示
        CCRect helpBoundingBox;
        {
            CCSprite * sprite = CCSprite::createWithSpriteFrameName("help_help.png");

            sprite->setPosition(center);
			sprite->setScale(scaleRate);
			
            this->addChild(sprite, ChildTagHelpSprite, ChildTagHelpSprite);

            helpBoundingBox = sprite->boundingBox();
        }

        //ラベルを表示
        {
            CCPoint pos;
            {
                float dy = 0.0f;
                pos = ccp(center.x,
                          helpBoundingBox.getMaxY() + dy);
            }

            CCSprite * sprite = CCSprite::createWithSpriteFrameName("main_taptostart.png");

            sprite->setAnchorPoint(ccp(0.5f, 0.0f));
            sprite->setPosition(pos);
			sprite->setScale(scaleRate);

            this->addChild(sprite, ChildTagTapStartSprite, ChildTagTapStartSprite);

            //点滅アニメーション
            {
                CCFiniteTimeAction * sequence = CCSequence::create(
                                                                   CCFadeIn::create(0.5f),
                                                                   CCDelayTime::create(1.0f),
                                                                   CCFadeOut::create(0.5f),
                                                                   CCDelayTime::create(1.0f),
                                                                   NULL);

                CCFiniteTimeAction * action = CCRepeatForever::create((CCActionInterval *)sequence);

                sprite->runAction(action);
            }
        }
    }
}

void HelloWorld::gameStart()
{
    //ヘルプとラベルを削除
    {
#ifdef USE_COIN_CONTINUE
		_isTapJoy = false;
#endif
		
        int tagId[] = {
            ChildTagTapStartSprite,
            ChildTagHelpSprite,
            ChildTagHelpBackLayer,
        };

        for (int i = 0; i < sizeof(tagId)/sizeof(*tagId); i++)
        {
            CCNode * node = this->getChildByTag(tagId[i]);
            if (node)
            {
                node->removeFromParentAndCleanup(true);
            }
        }
    }


    //JoyStick
#ifdef USE_JOYSTICK
    {
        const CCSize winSize = CCDirector::sharedDirector()->getWinSize();
        const CCPoint center = ccpMult(ccpFromSize(winSize), 0.5f);

        GameLayer * gameLayer = this->getGameLayer();
        if (gameLayer)
        {
            //### JoyStickを使用する場合 ###
            //PrivateConfig.h で以下のフラグのコメントを外してください。
            //  //JoyStickをつかうかどうか
            //  #define USE_JOYSTICK

            JoyStickLayer * joystick = JoyStickLayer::create();

            //TODO: JoyStick仮配置(調整願います)
            joystick->setScale(_scaleRate);
            joystick->setPosition(center);

            int tagId = ChildTagJoyStick;
            this->addChild(joystick, tagId, tagId);

            //デリゲート先の指定
            joystick->setDelegate(gameLayer);

            joystick->setTouchEnabled(false);
        }
    }
#endif


    //スケジューラの登録
    this->schedule(schedule_selector(HelloWorld::update), (1.0f / 60.0f));
    this->schedule(schedule_selector(HelloWorld::clockUpdate), (1.0f / 60.0f));

    //タッチ操作
    this->getGameLayer()->gameStart();
    this->getPauseMenu()->setEnabled(true);
    this->getPauseMenu()->setTouchEnabled(true);

#ifdef USE_JOYSTICK
    this->getJoyStickLayer()->setTouchEnabled(true);
#endif

    //フラグを立てる
    _isGameStart = true;

}


void HelloWorld::gameEnd(bool clear)
{
    //### ココは一回だけ通過するようにする ###
    //ゲーム終了
    if (this->getIsGameOver())
    {
        return;
    }
    this->_isGameOver = true;


    //操作不可
    {
        this->getPauseMenu()->setEnabled(false);
        this->getInputLayer()->setTouchEnabled(false);

#ifdef USE_JOYSTICK
        this->getJoyStickLayer()->setTouchEnabled(false);
#endif
    }

    //BGMを止める
    {
        SimpleAudioEngine * audioEngine = SimpleAudioEngine::sharedEngine();
        if (audioEngine)
        {
            if (audioEngine->isBackgroundMusicPlaying())
            {
                audioEngine->stopBackgroundMusic();
            }
        }
    }

    //終了処理
    {
        CCFiniteTimeAction * action = NULL;
        {
            //待ち時間 (キューブが着地し、少しして爆発した後、 プレイヤー落ちてくる)
            float actionTime = 2.0f;

            //アニメーション待ち
            CCFiniteTimeAction * wait = CCDelayTime::create(actionTime);

            //終了処理
            CCFiniteTimeAction * func = CCCallFunc::create(this, callfunc_selector(HelloWorld::gameClear));

            action = CCSequence::create(wait, func, NULL);
        }
        this->runAction(action);
    }
}


void HelloWorld::nodeRemoveFromParent(CCNode * sender)
{
    if (sender)
    {
        sender->removeFromParentAndCleanup(true);
    }
}


void HelloWorld::gameClear()
{
    SimpleAudioEngine::sharedEngine()->stopAllEffects();


    this->unschedule(schedule_selector(HelloWorld::clockUpdate));
    this->unscheduleUpdate();

    this->_isGameOver = true;


    //操作させない
    this->setTouchEnabled(false);
#ifdef USE_JOYSTICK
    this->getJoyStickLayer()->setTouchEnabled(false);
#endif

    this->getPauseMenu()->setEnabled(false);
    this->getInputLayer()->setTouchEnabled(false);

//    //タイムをスコアとする
//    {
//        float time = this->getTimeCount();
//        _score = ((((int)time) * 100) + ((int)(time * 100) % 100));
//    }

    TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
    //GameCenter
    switch (target)
    {
        case kTargetIpad:
        case kTargetIphone:
        {
//GameCenter
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

            int64_t score = this->getScore();
            //int64_t score = (int)round(this->getScore() * 100);     //ex. floatの場合

            Cocos2dExt::NativeInterface::reportScore(score, GAMECENTER_ID);
#endif
            break;
        }
        case kTargetAndroid:
        {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

            int score = this->getScore();
            //int score = (int)round(this->getScore() * 100);         //ex. floatの場合
//            CCLog("スコア：%d",score);

            Cocos2dExt::NativeInterface::reportScoreAndroid(score);
#endif
            break;
        }
        default:
            break;
    }

    //UserDefault.xmlへ保存
    switch (target)
    {
        case kTargetIpad:
        case kTargetIphone:
        case kTargetAndroid:
        {
            //ベストスコア ランキング の更新 (上位からチェック)
            {
                for (int i = 0; i < RANKING_MAX; i++)
                {
                    //ファイルを更新したかどうか
                    bool fileUpdate = false;

                    //float型は未定
                    SCORE_DATA_TYPE scorePoint = HelloWorld::getBestPoint(this->getGameMode(), (i+1));

                    //更新が必要な場合
                    if (this->getScore() > scorePoint)                        //...スコア
//                    if ((scorePoint <= 0) ||(this->getScore() < scorePoint))    //...タイム
                    {
                        HelloWorld::setBestPoint(this->getGameMode(), (i+1), this->getScore());
                        fileUpdate = true;

                        //下位順位をずらす
                        for (int j = (i + 1); j < RANKING_MAX; j++)
                        {
                            SCORE_DATA_TYPE tmpScorePoint = HelloWorld::getBestPoint(this->getGameMode(), (j+1));

                            HelloWorld::setBestPoint(this->getGameMode(), (j+1), scorePoint);

                            scorePoint = tmpScorePoint;
                        }
                    }

                    if (fileUpdate)
                    {
                        CCUserDefault::sharedUserDefault()->flush();
                        break;
                    }
                }
            }
            break;

        }

        default:
            break;
    }

#ifdef USE_COIN_CONTINUE
    //コイン
    PrivateUserDefault::setCoinCount(_coin);
#endif
	
    //シーン切り替え
    {
        CCScene * scene = ClearLayer::sceneWithResult(this->getGameMode(), this->getScore());
        CCDirector::sharedDirector()->replaceScene(CCTransitionMoveInT::create(0.3f, scene));
    }
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === 一時停止処理 ===
#endif

void HelloWorld::pauseGame()
{
    //一時停止画面の表示
    {
        int tagId = ChildTagPauseLayer;
        PauseLayer * layer = NULL;

        layer = (PauseLayer *)this->getChildByTag(tagId);
        if (!layer)
        {
            layer = PauseLayer::create(this);
            this->addChild(layer, tagId, tagId);
        }
    }

    //ボタンを無効化
    this->setTouchEnabled(false);
    this->getPauseMenu()->setEnabled(false);
    this->getPauseMenu()->setTouchEnabled(false);
    this->getInputLayer()->setTouchEnabled(false);
#ifdef USE_JOYSTICK
    this->getJoyStickLayer()->setTouchEnabled(false);
#endif


    //ゲーム一時停止
    CCDirector::sharedDirector()->pause();
    SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
    
}

void HelloWorld::pauseTouched()
{
    SimpleAudioEngine::sharedEngine()->pauseAllEffects();
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
    this->pauseGame();
}

void HelloWorld::restartTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");

    //ゲーム復帰
    CCDirector::sharedDirector()->resume();

    // if you use SimpleAudioEngine, it must resume here
    bool flg = PrivateUserDefault::getSound();
    if (flg)
    {
        SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    }

    //ボタンを有効化
    this->setTouchEnabled(true);
    this->getPauseMenu()->setEnabled(true);
    this->getPauseMenu()->setTouchEnabled(true);
    this->getInputLayer()->setTouchEnabled(true);
#ifdef USE_JOYSTICK
    this->getJoyStickLayer()->setTouchEnabled(true);
#endif
}


void HelloWorld::titleTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");

    //ゲーム中の動作を止める
    {
        this->stopAllActions();
        this->unscheduleUpdate();
    }

    _isGameOver = true;

    CCDirector::sharedDirector()->resume();

    //画面遷移
    CCScene * scene = TitleLayer::scene();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(0.8f, scene));
}

void HelloWorld::moreAppsTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");

    TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
    switch (target)
    {
            //iOS
        case kTargetIpad:
        case kTargetIphone:
        {
			Cocos2dExt::NativeInterface::showAppliPromotion();
			
            break;
        }

            //Android
        case kTargetAndroid:
        {
            //GameFeat
            Cocos2dExt::NativeInterface::showGameFeat();

            break;
        }

        default:
            break;
    }
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === ゲームモード用(ノーマル/追加) ===
#endif

CCString * HelloWorld::getResourceSuffix(GameMode mode)
{
    CCString * result = NULL;

    switch (mode)
    {
        case GameMode_Normal:
        {
            break;
        }

        case GameMode_Premium:
        {
            result = CCString::create("_sp");
            break;
        }

        default:
            break;
    }

    return result;
}


CCString * HelloWorld::getResourceName(GameMode mode, const char * fileName)
{
    CCString * result = NULL;
    CCString * suffix = HelloWorld::getResourceSuffix(mode);

    if (fileName)
    {
        if (suffix)
        {
            result = CCString::createWithFormat("%s%s.png", fileName, suffix->getCString());
        }
        else
        {
            result = CCString::createWithFormat("%s.png", fileName);
        }
    }

    return result;
}

CCString * HelloWorld::getResourceName(const char * fileName)
{
    return HelloWorld::getResourceName(this->getGameMode(), fileName);
}


SCORE_DATA_TYPE HelloWorld::getBestPoint(GameMode mode, int number)
{
    SCORE_DATA_TYPE result = 0;

    switch (mode)
    {
        case GameMode_Normal:
        {
            result = PrivateUserDefault::getNormalBestScore(number);
            break;
        }

        case GameMode_Premium:
        {
            result = PrivateUserDefault::getPremiumBestScore(number);
            break;
        }

        default:
            break;
    }

    return result;
}

void HelloWorld::setBestPoint(GameMode mode, int number, SCORE_DATA_TYPE value)
{
    switch (mode)
    {
        case GameMode_Normal:
        {
            PrivateUserDefault::setNormalBestScore(number, value);
            break;
        }

        case GameMode_Premium:
        {
            PrivateUserDefault::setPremiumBestScore(number, value);
            break;
        }

        default:
            break;
    }
}


int HelloWorld::getItemCount(GameMode mode, int number)
{
    int result = 0;

    switch (mode)
    {
        case GameMode_Normal:
        {
            result = PrivateUserDefault::getNormalItemCount(number);
            break;
        }

        case GameMode_Premium:
        {
            result = PrivateUserDefault::getPremiumItemCount(number);
            break;
        }

        default:
            break;
    }

    return result;
}

void HelloWorld::setItemCount(GameMode mode, int number, int value)
{
    switch (mode)
    {
        case GameMode_Normal:
        {
            PrivateUserDefault::setNormalItemCount(number, value);
            break;
        }

        case GameMode_Premium:
        {
            PrivateUserDefault::setPremiumItemCount(number, value);
            break;
        }

        default:
            break;
    }
}



bool HelloWorld::getItemVisibled(GameMode mode, int number)
{
    int result = 0;

    switch (mode)
    {
        case GameMode_Normal:
        {
            result = PrivateUserDefault::getNormalItemVisibled(number);
            break;
        }

        case GameMode_Premium:
        {
            result = PrivateUserDefault::getPremiumItemVisibled(number);
            break;
        }

        default:
            break;
    }

    return result;
}

void HelloWorld::setItemVisibled(GameMode mode, int number, bool value)
{
    switch (mode)
    {
        case GameMode_Normal:
        {
            PrivateUserDefault::setNormalItemVisibled(number, value);
            break;
        }

        case GameMode_Premium:
        {
            PrivateUserDefault::setPremiumItemVisibled(number, value);
            break;
        }

        default:
            break;
    }
}

#ifdef USE_COIN_CONTINUE
//コイン枚数取得
int HelloWorld::getCoinCount(){
    int result = 0;
    
    result = PrivateUserDefault::getCoinCount();
    
    return result;
}

void HelloWorld::setCoinCount(int value){
	
}
#endif


int HelloWorld::getNextItemIndex(GameMode mode)
{
    int result = 0;

    switch (mode)
    {
        case GameMode_Normal:
        {
            result = PrivateUserDefault::getNormalNextItemIndex();
            break;
        }

        case GameMode_Premium:
        {
            result = PrivateUserDefault::getPremiumNextItemIndex();
            break;
        }

        default:
            break;
    }

    return result;
}

void HelloWorld::setNextItemIndex(GameMode mode, int value)
{
    switch (mode)
    {
        case GameMode_Normal:
        {
            PrivateUserDefault::setNormalNextItemIndex(value);
            break;
        }

        case GameMode_Premium:
        {
            PrivateUserDefault::setPremiumNextItemIndex(value);
            break;
        }

        default:
            break;
    }
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === コインコンティニュー用 ===
#endif

#ifdef USE_COIN_CONTINUE
//コンティニュー
void HelloWorld::continueGame()
{
    //ボタンを無効化
    this->setTouchEnabled(false);
    this->getPauseMenu()->setEnabled(false);
    this->getPauseMenu()->setTouchEnabled(false);
    this->getInputLayer()->setTouchEnabled(false);
    
    //ゲーム一時停止
    this->scheduleOnce(schedule_selector(HelloWorld::createContinueLayer), 1.0f);
}

void HelloWorld::createContinueLayer(){
    //コンティニュー画面の表示
    int tagId = ChildTagContinueLayer;
    ContinueLayer * layer = NULL;
    
    bool isVideoRewardEnabled = false;
    if (this->getIsVideoRewardEnabled()){//在庫あるよ
        isVideoRewardEnabled = true;
    }
    
    bool isCoinContinueEnabled = _coin >= COIN_MAX ? (true):(false);
    
    layer = (ContinueLayer *)this->getChildByTag(tagId);
    if (!layer)
    {
        layer = ContinueLayer::create(this,isCoinContinueEnabled,isVideoRewardEnabled);
        this->addChild(layer, tagId, tagId);
    }
    CommonFunction::myBackgroundMusicSetVolume(0.2f);
}


void HelloWorld::rewardCotinueTouched()
{
	_isTapJoy = true;
	
    this->showVideoReward();
    
    CommonFunction::myBackgroundMusicSetVolume(0.0f);
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
    
}

void HelloWorld::rewardMovieEnded(bool isContinueEnabled){
    if (this == NULL) {
        return;
    }
    if (isContinueEnabled) {
        CommonFunction::myBackgroundMusicSetVolume(1.0f);
        CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
        
        //コイン減らす
        _countOfRewardContinue += 1;
        
        //フラグを立てる
        _isGameRestart = true;
        this->setTouchEnabled(true);
        this->getInputLayer()->setTouchEnabled(true);
        
        //リスタート処理
        this->gameTapRestart();
    }
    else{
        this->restartNoTouched();
    }
    this->getVideoReward();
}

void HelloWorld::restartYesTouched()
{
    CommonFunction::myBackgroundMusicSetVolume(1.0f);
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
    
    //コイン減らす
    this->useCoin();
    
    //フラグを立てる
    _isGameRestart = true;
    this->setTouchEnabled(true);
    this->getInputLayer()->setTouchEnabled(true);
    
    //リスタート処理
    this->gameTapRestart();
}

void HelloWorld::restartNoTouched()
{
    CommonFunction::myBackgroundMusicSetVolume(1.0f);
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
    
    //ゲーム復帰
    this->getGameLayer()->resumeSchedulerAndActions();
    this->gameEnd(false);
}

void HelloWorld::useCoin()
{
    if (!this->getIsGameStart())
    {
        return;
    }
    
    //スコア
    _coin -= COIN_MAX;
    
    //画面表示
    CCString * text = CCString::createWithFormat("%d枚", (int)this->getCoin());
    this->getCoinLabel()->setString(text->getCString());
    PrivateUserDefault::setCoinCount((int)this->getCoin());
}

void HelloWorld::gameRestart()
{
    _isGameRestart = false;
    
    //ヘルプとラベルを削除
    {
        int tagId[] = {
            ChildTagTapStartSprite,
            ChildTagHelpBackLayer,
        };
        
        for (int i = 0; i < sizeof(tagId)/sizeof(*tagId); i++)
        {
            CCNode * node = this->getChildByTag(tagId[i]);
            if (node)
            {
                node->removeFromParentAndCleanup(true);
            }
        }
    }
    
    //ゲーム復帰
    this->getGameLayer()->resumeSchedulerAndActions();
    
    // if you use SimpleAudioEngine, it must resume here
    bool flg = PrivateUserDefault::getSound();
    if (flg)
    {
        SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    }
    
    //ボタンを有効化
    this->setTouchEnabled(true);
    this->getPauseMenu()->setEnabled(true);
    this->getPauseMenu()->setTouchEnabled(true);
    this->getInputLayer()->setTouchEnabled(true);
    
    this->getGameLayer()->gameRestart();
    
}
void HelloWorld::gameTapRestart()
{
    CCSize  winSize = CCDirector::sharedDirector()->getWinSize();
    CCPoint center  = ccpMult(ccpFromSize(winSize), 0.5f);
    
    //比率
    float scaleRate  = CommonFunction::getScaleRate();
    
    //簡易ヘルプの後ろに半透明のグレー
    {
        CCLayerColor * layer = CCLayerColor::create(ccc4(0, 0, 0, 128), winSize.width, winSize.height);
        this->addChild(layer, ChildTagHelpBackLayer, ChildTagHelpBackLayer);
    }
    
    //ラベルを表示
    {
        CCSprite * sprite = CCSprite::createWithSpriteFrameName("main_taptostart.png");
        
        sprite->setScale(scaleRate);
        sprite->setPosition(center);
        
        this->addChild(sprite, ChildTagTapStartSprite, ChildTagTapStartSprite);
        
        //点滅アニメーション
        {
            CCFiniteTimeAction * sequence = CCSequence::create(
                                                               CCFadeIn::create(0.5f),
                                                               CCDelayTime::create(1.0f),
                                                               CCFadeOut::create(0.5f),
                                                               CCDelayTime::create(1.0f),
                                                               NULL);
            
            CCFiniteTimeAction * action = CCRepeatForever::create((CCActionInterval *)sequence);
            
            sprite->runAction(action);
        }
    }
    
	
	//this->getGameLayer()->playerRestart();
}

void HelloWorld::getVideoReward(){
    Cocos2dExt::NativeInterface::getVideoReward();
}

void HelloWorld::showVideoReward(){
    Cocos2dExt::NativeInterface::showVideoReward();
}

bool HelloWorld::getIsVideoRewardEnabled(){
    bool result = false;
    if (REWARD_ONE_TIME_PER_ONE_GAME == 1) {//１ゲーム１回だけ動画コンテニュー
        if (_countOfRewardContinue == 0 && Cocos2dExt::NativeInterface::getIsVideoRewardEnabled() == true) {
            result = true;
        }
        else{
            result = false;
        }
    }
    else{//１ゲーム何回でも動画コンテニュー
        result = Cocos2dExt::NativeInterface::getIsVideoRewardEnabled();
    }
    return result;
}
#endif

