//
//  PlayerSprite.cpp
//  Knock
//
//  Created by 逢坂 隆司 on 13/03/01.
//
//

#include "CoinObject.h"

#include "HelloWorldScene.h"
#include "PrivateConfig.h"
#include "CommonFunction.h"
#include "GameLayer.h"

USING_NS_CC;

#define WINSIZE CCDirector::sharedDirector()->getWinSize()

//Z順 & タグ
typedef enum _ChildTags
{
    ChildTagNone,
    ChildTagSprite,
} ChildTags;

typedef enum _SoundTags
{
    SoundTagNone = 100,
} SoundTags;

CoinObject * CoinObject::create()
{
    CoinObject * pRet = new CoinObject();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool CoinObject::init()
{
    
    bool result = false;
    do
    {
        CCString * fileName = CCString::create("main_coin.png");
        CCSprite * sprite = CCSprite::createWithSpriteFrameName(fileName->getCString());
        sprite->setAnchorPoint(ccp(0.5f, 0.5f));
        sprite->setScale(CommonFunction::getScaleRateFill());
        
        this->addChild(sprite,ChildTagSprite,ChildTagSprite);
        _isGot = false;
        result = true;
    } while (0);
    return result;
}

void CoinObject::startAction(){
    CCSprite * sprite = (CCSprite *)this->getChildByTag(ChildTagSprite);
    
    float duration = 0.5f;
    
    CCRepeatForever * rep = NULL;
    {
        CCFiniteTimeAction * obit = CCOrbitCamera::create(duration/4, 1, 0, 0, 90, 0, 0);
        rep = CCRepeatForever::create((CCActionInterval *)obit);
    }
    sprite->runAction(rep);
    
    CCFiniteTimeAction * action = NULL;
    {
        CCFiniteTimeAction * move = CCMoveBy::create(duration,
                                                     ccp(0,150 * CommonFunction::getAdjustRate()));
        CCFiniteTimeAction * wait = CCDelayTime::create(0.3f);
        CCFiniteTimeAction * fade = CCFadeOut::create(0.2f);
        CCFiniteTimeAction * seq  = CCSequence::createWithTwoActions(wait, fade);
        CCFiniteTimeAction * spa  = CCSpawn::createWithTwoActions(move, seq);
        
        CCFiniteTimeAction * func = CCCallFunc::create(this, callfunc_selector(CoinObject::invisible));
        
        action = CCSequence::createWithTwoActions(spa, func);
    }
    sprite->runAction(action);
}

void CoinObject::invisible(){
    this->setVisible(false);
}

void CoinObject::remove(){
    this->removeChildByTag(ChildTagSprite, true);
    
    GameLayer * game = (GameLayer *)this->getParent();
    game->removeCoin(this);
}

CoinObject::CoinObject(){
    
}
CoinObject::~CoinObject(){
    
}

cocos2d::CCRect CoinObject::hitRect(){
    CCRect result = CCRectZero;
    if (this->getChildByTag(ChildTagSprite) != NULL) {
        result = CCRectMake(this->getPositionX() + this->getChildByTag(ChildTagSprite)->getPositionX() + this->getChildByTag(ChildTagSprite)->boundingBox().size.width * -0.3f,
                            this->getPositionY() + this->getChildByTag(ChildTagSprite)->getPositionY() + this->getChildByTag(ChildTagSprite)->boundingBox().size.height * -0.3f,
                            this->getChildByTag(ChildTagSprite)->boundingBox().size.width * 0.6f,
                            this->getChildByTag(ChildTagSprite)->boundingBox().size.height * 0.6f);
    
    }
    return result;
}
cocos2d::CCRect CoinObject::boundingBox(){
    CCRect result = CCRectZero;
    if (this->getChildByTag(ChildTagSprite) != NULL) {
        result = CCRectMake(this->getPositionX() + this->getChildByTag(ChildTagSprite)->getPositionX() + this->getChildByTag(ChildTagSprite)->boundingBox().size.width * -0.5f,
                            this->getPositionY() + this->getChildByTag(ChildTagSprite)->getPositionY() + this->getChildByTag(ChildTagSprite)->boundingBox().size.height * -0.5f,
                            this->getChildByTag(ChildTagSprite)->boundingBox().size.width,
                            this->getChildByTag(ChildTagSprite)->boundingBox().size.height);
        
    }
    return result;
}
