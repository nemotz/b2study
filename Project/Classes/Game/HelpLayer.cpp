//
//  HelpLayer.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include "HelpLayer.h"
#include "CommonFunction.h"
#include "NativeInterface.h"


USING_NS_CC;


HelpLayer::HelpLayer()
{
}

HelpLayer::~HelpLayer()
{
}


bool HelpLayer::init()
{
    if ( CCLayer::init() )
    {
        CCSize winSize = CCDirector::sharedDirector()->getWinSize();
        CCPoint center = ccpMult(ccpFromSize(winSize), 0.5f);
		
		_scaleRate = CommonFunction::getScaleRate();
		_adjustRate = CommonFunction::getAdjustRate();
		
        //iOS/Android 分岐用
        TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
		if (target == kTargetIpad) {
			_scaleRate *= 0.9f;
			_adjustRate *= 0.9f;
		}

        //メニュー
        {
            CCMenuItemSprite * itemHelp;
            {
                CCString * fileName = CCString::create("help_back.png");
                CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());

                itemHelp = CCMenuItemSprite::create(normal, selected, this, menu_selector(HelpLayer::closeHelp));

				{
					CCSize size = itemHelp->boundingBox().size;
					float kw = winSize.width / size.width;
					float kh = winSize.height / size.height;
					
					itemHelp->setScaleX(kw);
					itemHelp->setScaleY(kh);
				}
            }

            //メニュー
            {
                CCMenu * menu = CCMenu::create(itemHelp, NULL);
                menu->setPosition(center);

                this->addChild(menu, ChildTagHelpMenu, ChildTagHelpMenu);
            }
        }

        //ヘルプ
        {
			float dy = 0.0f;
			CCSprite * sprite = CCSprite::createWithSpriteFrameName("help_help.png");

			CCSize sptSize = sprite->getContentSize();

			sprite->setScale(_scaleRate);
			sprite->setPosition(ccpSub(center, ccp(0.0f, dy * _adjustRate)));

			int tagId = ChildTagHelpSprite;
			this->addChild(sprite, tagId, tagId);
        }

        return true;
    }
    else
    {
        return false;
    }
}


void HelpLayer::closeHelp()
{
    //メニュー
    {
        CCMenu * menu = (CCMenu *)this->getChildByTag(ChildTagHelpMenu);
        if (menu)
        {
            CCFiniteTimeAction * fadeOut = CCFadeOut::create(0.3f);
            CCFiniteTimeAction * endFunc = CCCallFunc::create(this, callfunc_selector(HelpLayer::removeHelp));

            CCFiniteTimeAction * action = CCSequence::create(fadeOut, endFunc, NULL);

            menu->runAction(action);
        }
    }

    //ヘルプ内容
    {
        int tagId[] = {
            //ヘルプ
            ChildTagHelpSprite,

            //タイトル
            ChildTagTitleSprite,
        };

        for (int i = 0; i < sizeof(tagId)/sizeof(*tagId); i++)
        {
            CCSprite * sprite = (CCSprite *)this->getChildByTag(tagId[i]);
            if (sprite)
            {
                CCFiniteTimeAction * fadeOut = CCFadeOut::create(0.2f);
                CCFiniteTimeAction * endFunc = CCCallFunc::create(this, callfunc_selector(HelpLayer::removeHelp));

                CCFiniteTimeAction * action = CCSequence::create(fadeOut, endFunc, NULL);

                sprite->runAction(action);
            }
        }
    }
}

void HelpLayer::removeHelp()
{
    this->removeFromParentAndCleanup(true);
	
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	Cocos2dExt::NativeInterface::showIconAd(1, true);
#else
	Cocos2dExt::NativeInterface::showIconAd(1);
#endif

}
