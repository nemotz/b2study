//
//  InputLayer.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include "CommonFunction.h"

#include "HelloWorldScene.h"
#include "InputLayer.h"


USING_NS_CC;


#pragma mark - ### TouchPoint Class ###
class TouchPoint : public CCNode
{
public:
    TouchPoint()
    {
        setShaderProgram(CCShaderCache::sharedShaderCache()->programForKey(kCCShader_PositionTextureColor));
    }

    virtual void draw()
    {
        ccDrawColor4B(m_TouchColor.r, m_TouchColor.g, m_TouchColor.b, 255);
        glLineWidth(10);
        ccDrawLine( ccp(0, m_pTouchPoint.y), ccp(getContentSize().width, m_pTouchPoint.y) );
        ccDrawLine( ccp(m_pTouchPoint.x, 0), ccp(m_pTouchPoint.x, getContentSize().height) );
        glLineWidth(1);
        ccPointSize(30);
        ccDrawPoint(m_pTouchPoint);
    }

    void setTouchPos(const CCPoint& pt)
    {
        m_pTouchPoint = pt;
    }

    void setTouchColor(ccColor3B color)
    {
        m_TouchColor = color;
    }

    static TouchPoint* touchPointWithParent(CCNode* pParent)
    {
        TouchPoint* pRet = new TouchPoint();

        pRet->setContentSize(pParent->getContentSize());
        pRet->setAnchorPoint(ccp(0.0f, 0.0f));
        pRet->autorelease();

        return pRet;
    }

private:
    CCPoint m_pTouchPoint;
    ccColor3B m_TouchColor;
};


#pragma mark - ### InputLayer Class ###

InputLayer::InputLayer()
{
    //初期化
    {
        _touchIdArray = NULL;
        _dicPosition = NULL;
    }
}

InputLayer::~InputLayer()
{
    CC_SAFE_RELEASE_NULL(_touchIdArray);
    CC_SAFE_RELEASE_NULL(_dicPosition);
}

bool InputLayer::init()
{
    bool result = false;

    do
    {
        if ( !CCLayer::init() )
        {
            break;
        }

//        //マルチタップ
//        this->setTouchMode(kCCTouchesAllAtOnce);

        //変数の初期化
        {
            _isTapStart = false;

            _touchStatus = TouchStatus_None;
            _movePosition = CCPointZero;

            _touchDelegate = NULL;

            _touchIdArray = CCArray::create();
            _touchIdArray->retain();

            _dicPosition = CCDictionary::create();
            _dicPosition->retain();
        }

        //ココまで来たら正常終了
        return true;

    } while (0);

    return result;
}

void InputLayer::setTouchStatus(cocos2d::CCPoint touchPoint, bool isTouch)
{
    if (isTouch)
    {
        CCSize winSize = CCDirector::sharedDirector()->getWinSize();
        if (touchPoint.x <= (winSize.width * 0.5f))
        {
            _touchStatus = TouchStatus_Left;
        }
        else
        {
            _touchStatus = TouchStatus_Right;
        }
    }
    else
    {
        _touchStatus = TouchStatus_None;
    }

    //デリゲートが指定されていれば
    if (_touchDelegate)
    {
        _touchDelegate->changeTouchStatus(_touchStatus);
    }
}

void InputLayer::setTouchMoved(CCPoint touchPoint)
{
    CCPoint positon = CCPointZero;
    if (ccpDistance(_movePosition, CCPointZero) != 0.0f)
    {
        positon = ccpSub(touchPoint, _movePosition);
    }
    _movePosition = touchPoint;

    //デリゲートが設定されていれば
    if (_touchDelegate)
    {
        //移動量を通知
        _touchDelegate->changeTouchMoved(positon);
    }
}


bool InputLayer::isInGame()
{
    bool result = false;

    do
    {
        if (!_isTapStart)
        {
            break;
        }

        HelloWorld * hello = HelloWorld::sharedHelloWorldLayer();
        if (!hello)
        {
            break;
        }

        //ゲーム開始していない
        if (!hello->getIsGameStart())
        {
            break;
        }

        //ゲームオーバー
        if (hello->getIsGameOver())
        {
            break;
        }

        result = true;
    } while (0);

    return result;
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === タップ操作(XTLayerなし) ===
#endif

////タッチイベントをとるオブジェクトの設定(イベントを検出するオブジェクトとして設定)
//void InputLayer::registerWithTouchDispatcher()
//{
//    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 0, true);
//}
//
//
//bool InputLayer::ccTouchBegan(CCTouch * pTouch, CCEvent * pEvent)
//{
//    //UI座標 -> OpenGL座標
//    CCPoint location = pTouch->getLocation();
//
//    do
//    {
//        if (!this->isInGame())
//        {
//            break;
//        }
//
//        //### ゲーム処理 ###
//        {
//            CCLOG("%s position:%lf, %lf", __PRETTY_FUNCTION__, location.x, location.y);
//
//            this->setTouchStatus(location);
//        }
//    } while (0);
//
//    return true;
//}
//
//
//void InputLayer::ccTouchMoved(CCTouch * pTouch, CCEvent * pEvent)
//{
//    //UI座標 -> OpenGL座標
//    CCPoint location = pTouch->getLocation();
//
//    do
//    {
//        if (!this->isInGame())
//        {
//            break;
//        }
//
//        //### ゲーム処理 ###
//        {
//            CCLOG("%s position:%lf, %lf", __PRETTY_FUNCTION__, location.x, location.y);
//
//            this->setTouchStatus(location);
//        }
//    } while (0);
//}
//
//
//void InputLayer::ccTouchEnded(CCTouch * pTouch, CCEvent * pEvent)
//{
//    //UI座標 -> OpenGL座標
//    CCPoint location = pTouch->getLocation();
//
//    do
//    {
//        //タップ開始処理
//        if (!this->isInGame())
//        {
//            if (_isTapStart)
//            {
//                break;
//            }
//
//            //ゲーム開始
//            HelloWorld * hello = HelloWorld::sharedHelloWorldLayer();
//            if (hello)
//            {
//                _isTapStart = true;
//
//                hello->gameStart();
//            }
//
//            break;
//        }
//
//        //### ゲーム処理 ###
//        {
//            CCLOG("%s position:%lf, %lf", __PRETTY_FUNCTION__, location.x, location.y);
//
//            //触れていない
//            this->setTouchStatus(location, false);
//        }
//    } while (0);
//}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === XTLayer:マルチタップ ===
#endif

//TODO: *** マルチタップ処理でアプリが落ちる？(2014/01/20 調査中) ***
//
//void InputLayer::xtTouchesBegan(CCSet * _touches, CCEvent * event)
//{
//    do
//    {
//        if (!this->isInGame())
//        {
//            break;
//        }
//
//        //移動量チェック用変数の初期化
//        _movePosition = CCPointZero;
//
//        //### ゲーム処理 ###
//        {
//            CCSetIterator iter = _touches->begin();
//            for (; iter != _touches->end(); iter++)
//            {
//                CCTouch* pTouch = (CCTouch*)(*iter);
//
//                //CCDictionaryにタッチポジションをセット
//                CCPoint * position = new CCPoint(pTouch->getLocation());
//
//                _dicPosition->setObject(pTouch, pTouch->getID());
//
//                //配列にIDをセット
//                _touchIdArray->addObject(CCInteger::create(pTouch->getID()));
//
//                //タッチステータスをセット
//                this->setTouchStatus(pTouch->getLocation());
//
//                //イベントを通知
//                if (this->getTouchDelegate())
//                {
//                    this->getTouchDelegate()->touchBegan(*position);
//                }
//            }
//       }
//    } while (0);
//}
//
//
//void InputLayer::xtTouchesMoved(CCSet * _touches, CCEvent * event)
//{
//    do
//    {
//        if (!this->isInGame())
//        {
//            break;
//        }
//
//        //### ゲーム処理 ###
//        {
//            //タッチポジションの更新
//            CCSetIterator iter = _touches->begin();
//            for (; iter != _touches->end(); iter++)
//            {
//                CCTouch* pTouch = (CCTouch*)(*iter);
//                CCPoint* pPoint = (CCPoint*)_dicPosition->objectForKey(pTouch->getID());
//                if (pPoint != NULL)
//                {
//                    CCPoint pos = pTouch->getLocation();
//                    pPoint->setPoint(pos.x, pos.y);
//                }
//            }
//
//            //最後のタッチポジションを有効にする
//            if (_touchIdArray != NULL && _touchIdArray->count() > 0)
//            {
//                CCInteger * intID = (CCInteger*)_touchIdArray->lastObject();
//                CCPoint* pPoint = (CCPoint*)_dicPosition->objectForKey(intID->getValue());
//
//				if (pPoint != NULL)
//                {
//					//タッチステータスをセット
//					this->setTouchStatus(*pPoint);
//
//					//移動量
//					this->setTouchMoved(*pPoint);
//
//
//					//イベントを通知
//					if (this->getTouchDelegate())
//					{
//						this->getTouchDelegate()->touchMoved(*pPoint);
//					}
//				}
//            }
//        }
//
//    } while (0);
//}
//
//void InputLayer::xtTouchesEnded(CCSet * _touches, CCEvent * event)
//{
//    do
//    {
//        //タップ開始処理
//        if (!this->isInGame())
//        {
//            if (_isTapStart)
//            {
//                break;
//            }
//
//            //ゲーム開始
//            HelloWorld * hello = HelloWorld::sharedHelloWorldLayer();
//            if (hello)
//            {
//                _isTapStart = true;
//
//                hello->gameStart();
//            }
//
//            break;
//        }
//
//#ifdef USE_COIN_CONTINUE
//        HelloWorld * hello = HelloWorld::sharedHelloWorldLayer();
//        if (hello->getIsGameRestart())
//        {
//            _isTapStart = true;
//            
//            hello->gameRestart();
//            
//            return;
//        }
//#endif
//		
//        //### ゲーム処理 ###
//        {
//            //タッチを削除する
//            CCSetIterator iter = _touches->begin();
//            for (; iter != _touches->end(); iter++)
//            {
//                CCTouch* pTouch = (CCTouch*)(*iter);
//                CCPoint* pPoint = (CCPoint*)_dicPosition->objectForKey(pTouch->getID());
//
//                if (pPoint != NULL)
//                {
//                    //タッチステータスをセット (触れていない)
//                    this->setTouchStatus(*pPoint, false);
//
//                    //イベントを通知
//                    if (this->getTouchDelegate())
//                    {
//                        this->getTouchDelegate()->touchEnded(*pPoint);
//                    }
//
//
//                    //辞書から削除
//                    _dicPosition->removeObjectForKey(pTouch->getID());
//
//                    //配列から削除
//                    if (_touchIdArray != NULL && _touchIdArray->count() > 0)
//                    {
//                        //IDと一致するデータを配列から削除
//                        for (int i = 0; i < _touchIdArray->count(); i++)
//                        {
//                            if (((CCInteger*)_touchIdArray->objectAtIndex(i))->getValue() == pTouch->getID())
//                            {
//                                _touchIdArray->removeObjectAtIndex(i);
//                            }
//                        }
//                    }
//                }
//            }
//
//            //有効なタッチがある場合はその中で最後にタッチした場所を送る
//            if (_touchIdArray != NULL && _touchIdArray->count() > 0)
//            {
//                CCInteger * intID = (CCInteger*)_touchIdArray->lastObject();
//                CCPoint* pPoint = (CCPoint*)_dicPosition->objectForKey(intID->getValue());
//                if (pPoint != NULL) {
//                    this->setTouchStatus(*pPoint);
//                }
//            }
//            else
//            {
//                this->resetTouchStatus();
//            }
//       }
//
//    } while (0);
//}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#pragma mark - === XTLayer:シングルタップ ===
#endif

void InputLayer::xtTouchesBegan(CCPoint position)
{
    //移動量チェック用変数の初期化
    _movePosition = CCPointZero;

    do
    {
        if (!this->isInGame())
        {
            break;
        }

        //touchBeganの通知
        if (this->getTouchDelegate())
        {
            this->getTouchDelegate()->touchBegan(position);
        }

        //### ゲーム処理 ###
        {
            //CCLOG("%s position:%lf, %lf", __PRETTY_FUNCTION__, position.x, position.y);

            this->setTouchStatus(position);
        }
    } while (0);
}


void InputLayer::xtTouchesMoved(CCPoint position)
{
    do
    {
        if (!this->isInGame())
        {
            break;
        }

        //touchMoveの通知
        if (this->getTouchDelegate())
        {
            this->getTouchDelegate()->touchMoved(position);
        }

        //### ゲーム処理 ###
        {
            //CCLOG("%s position:%lf, %lf", __PRETTY_FUNCTION__, position.x, position.y);

            this->setTouchStatus(position);
            this->setTouchMoved(position);
        }
    } while (0);
}


void InputLayer::xtTouchesEnded(CCPoint position)
{
    do
    {
        //タップ開始処理
        if (!this->isInGame())
        {
            if (_isTapStart)
            {
                break;
            }

            //ゲーム開始
            HelloWorld * hello = HelloWorld::sharedHelloWorldLayer();
            if (hello)
            {
                _isTapStart = true;

                hello->gameStart();
            }

            break;
        }

        //touchEndの通知
        if (this->getTouchDelegate())
        {
            this->getTouchDelegate()->touchEnded(position);
        }

        //### ゲーム処理 ###
        {
            //CCLOG("%s position:%lf, %lf", __PRETTY_FUNCTION__, position.x, position.y);

            //触れていない
            this->setTouchStatus(position, false);
        }
//        else
//        {
//            CCLOG("%s touchStayCount:%d", __PRETTY_FUNCTION__, this->getTouchStayCount());
//        }

    } while (0);
}
