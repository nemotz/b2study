//
//  ExplosionSprite.h
//  AtomicMan
//
//  Created by Goodia.Inc on 2013/07/11.
//
//

#ifndef __AtomicMan__ExplosionSprite__
#define __AtomicMan__ExplosionSprite__

#include "cocos2d.h"
#include "PrivateConfig.h"

class ExplosionSprite : public cocos2d::CCSprite
{
public:
    CREATE_FUNC(ExplosionSprite);
    
private:
    bool init();
    
protected:
    ExplosionSprite();
    virtual ~ExplosionSprite();
    
private:
    void startAction();//通常のアニメーション
    
};
#endif /* defined(__AtomicMan__ExplosionSprite__) */
