//
//  TitleLayer.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __TemplateProject__TitleLayer__
#define __TemplateProject__TitleLayer__

#include "cocos2d.h"
#include "PrivateConfig.h"


class TitleLayer : public cocos2d::CCLayer
{

//### 内部で使用しているタグ番号 ###
public:
    //アクションタグ
    typedef enum _ActionTags
    {
        ActionTagNone                   = 0x0000,

        //移動アクション
        ActionTagTitleAnimation         = 0x0002,
        ActionTagMenu1Animation         = 0x0003,
        ActionTagMenu2Animation         = 0x0004,

        //全指定(フラグ処理用)
        ActionTagAll                    = 0xFFFF,

    } ActionTags;


    //Z順 & タグ
    typedef enum _ChildTags
    {
        ChildTagNone,
        ChildTagBackSprite,

        //画像
        ChildTagImage1Sprite,
        ChildTagImage2Sprite,
        ChildTagImage3Sprite,
        ChildTagImage4Sprite,

        //タイトル
        ChildTagTitleSprite,

        //メニュー
        ChildTagMainMenu1,
        ChildTagMainMenu2,

        //システムメニュー
        ChildTagSystemUpperMenu1,
        ChildTagSystemUpperMenu2,
        ChildTagSystemBottomMenu,

        //メニュー項目
        ChildTagMenu_StartItemSprite,               //ゲーム開始ボタン
        ChildTagMenu_RankingItemSprite,             //ランキングボタン
        ChildTagMenu_HelpItemSprite,                //ヘルプボタン
        ChildTagMenu_AdAppsItemSprite,              //おすすめアプリボタン
        ChildTagMenu_GoodiaAppsItemSprite,          //Goodiaボタン
        //ChildTagMenu_CollectionItemSprite,          //コレクション

        //画面上部 メニュー項目
        ChildTagUpperMenu_SoundItemToggle,          //サウンド ON/OFF ボタン
        ChildTagUpperMenu_TwitterItemSprite,        //Twitterボタン
        ChildTagUpperMenu_AdAppsItemSprite,         //おすすめアプリボタン
        ChildTagUpperMenu_HelpItemSprite,           //HELPボタン

        //MoreAppsバッヂ (newを表示)
        ChildTagGoodiaAppsBadgeSprite,

        //おすすめバッヂ
        ChildTagAdAppsBadgeSprite,

        //ベストスコア
        ChildTagSuffixSprite,
        ChildTagBestLabel,
		
		//コイン
		ChildTagCoinLabel,


        //ヘルプ画面
        ChildTagHelpLayer,

    } ChildTags;


private:
    TitleLayer();
    ~TitleLayer();

public:
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene * scene();

protected:
    CREATE_FUNC(TitleLayer);
    bool init();

private:
    virtual void onEnter();
    virtual void onExit();
    virtual void onEnterTransitionDidFinish();


//構成物
private:
    cocos2d::CCLabelTTF * getBestLabel();
	
#ifdef USE_COIN_CONTINUE
    cocos2d::CCLabelTTF * getCoinLabel();
#endif

    bool _isPortrait;

//Android Backキー, Homeキー対応用
private:
    void keyBackClicked();
    void keyMenuClicked();


private:
    void setTitleMenusEnabled(bool flag);
    void setModeMenusEnabled(bool flag);
    void setMessageMenusEnabled(bool flag);


//ゲーム開始
private:
    void gameStart();
    void gameStart(GameMode mode);


//メインメニュー イベント処理
private:
    void startTouched();
    void rankingTouched();
    void moreAppsTouched();
    void goodiaMoreAppsTouched();

    void soundTouched();
    void helpTouched();
    void twitterTouched();
    void moreAppsIconTouched();


//タイトルアニメーション
private:
    void startAnimation1();
    void startAnimation2();
    void startAnimation3();

    void setNewBadge();
};

#endif
