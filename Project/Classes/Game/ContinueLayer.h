//
//  ContinueLayer.h
//  Turtle
//
//  Created by Goodia.Inc on 2013/09/11.
//
//

#ifndef __Turtle__ContinueLayer__
#define __Turtle__ContinueLayer__

#include "cocos2d.h"
#include "ModalLayer.h"


class ContinueDelegate;


class ContinueLayer : public ModalLayer
{
    
//### 内部で使用しているタグ番号 ###
public:
    //アクションタグ
    typedef enum _ActionTags
    {
        ActionTagNone,
    } ActionTags;
    
    //Z順 & タグ
    typedef enum _ChildTags
    {
        ChildTagNone = 0,
        
        //背景(の後ろのグレイ)
        ChildTagBackLayer,
        
        //ダイアログのフレーム
        ChildTagBackSprite,
        
        //画像
        ChildTagNumberLayer,
        ChildTagNumberSprite,
        ChildTagNumberLabel,
        
        //メニュー
        ChildTagContinueMenu,
        
    } ChildTags;
    
    
public:
    static ContinueLayer * create(ContinueDelegate * delegate,bool isCoin,bool isReward);
    
protected:
    bool init(ContinueDelegate * delegate,bool isCoin,bool isReward);
    
protected:
    ContinueLayer() : _delegate(NULL) {};
    //virtual ~ContinueLayer();
    
public:
    cocos2d::CCMenu * getMenu();
    
private:
    ContinueDelegate * _delegate;
    ContinueDelegate * getDelegate(){ return _delegate; };
    
private:
    void startAction();
    void playSE();
    cocos2d::CCSprite * getBackSprite();
    void restartYesTouched(cocos2d::CCNode * sender);
    void restartNoTouched(cocos2d::CCNode * sender);
    void rewardCotinueTouched(cocos2d::CCNode * sender);
};


//デリゲート
class ContinueDelegate
{
public:
    //ゲーム再開
    virtual void restartYesTouched(){};
    
    //ゲームオーバー
    virtual void restartNoTouched(){};
    
    virtual void rewardCotinueTouched(){};
    
};

#endif /* defined(__Turtle__ContinueLayer__) */
