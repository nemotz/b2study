//
//  PauseLayer.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __TemplateProject__PauseLayer__
#define __TemplateProject__PauseLayer__

#include "cocos2d.h"
#include "ModalLayer.h"


class PauseDelegate;


class PauseLayer : public ModalLayer
{

//### 内部で使用しているタグ番号 ###
public:
    //アクションタグ
    typedef enum _ActionTags
    {
        ActionTagNone,
    } ActionTags;

    //Z順 & タグ
    typedef enum _ChildTags
    {
        ChildTagNone = 0,

        //背景(の後ろのグレイ)
        ChildTagBackLayer,

        //ダイアログのフレーム
        ChildTagBackSprite,

        //メニュー
        ChildTagPauseMenu,

    } ChildTags;


public:
    static PauseLayer * create(PauseDelegate * delegate);

protected:
    bool init(PauseDelegate * delegate);

protected:
    PauseLayer() : _delegate(NULL) {};
    //virtual ~PauseLayer();

public:
    cocos2d::CCMenu * getMenu();
	
	CC_SYNTHESIZE_READONLY(float, _adjustRate, AdjustRate);
    CC_SYNTHESIZE_READONLY(float, _scaleRate, ScaleRate);

private:
    PauseDelegate * _delegate;
    PauseDelegate * getDelegate(){ return _delegate; };

private:
    void restartTouched(cocos2d::CCNode * sender);
    void titleTouched(cocos2d::CCNode * sender);
    void moreAppsTouched(cocos2d::CCNode * sender);
};


//デリゲート
class PauseDelegate
{
public:
    //ゲーム再開
    virtual void restartTouched(){};

    //タイトルに戻る
    virtual void titleTouched(){};

    //おすすめアプリ
    virtual void moreAppsTouched(){};
};

#endif
