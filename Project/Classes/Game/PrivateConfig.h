//
//  PrivateConfig.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __TemplateProject__PrivateConfig__
#define __TemplateProject__PrivateConfig__


//*** XOR演算による暗号・復号化用 キー ***  (int ... 1 〜 9999999999)
//UUID
#define ENCRYPTION_XOR_KEY__UUID            1111111111
#define ENCRYPTION_XOR_KEY__PREMIUM_UNLOCK  9999999999


//リリース日 (新規・アップデート リリースごとに毎回処理)
//使用例
//  ... MoreAppsボタンの Newバッヂ 表示用
//  ... リワード広告判定用に使用予定(リリース前ならリワード広告表示しない)
#define RELEASE_DAY__YEAR   2013
#define RELEASE_DAY__MONTH  9
#define RELEASE_DAY__DAY    9

//MoreApps 確認間隔日数
#define INTERVAL_MOREAPPS_LASTVIEW 10


//助数詞
#define SUFFIX                      "m"
#define COIN_SUFFIX                 "枚"		//コイン用

//プロジェクト名（日本語） InfoPlist.stringsで設定したもの
#define PROJECT_NAME                "アプリ名"                 //TODO:APPINFO

//App Store アプリID
#define IOS_APP_ID                  "000000000"               //TODO:APPINFO

//GooglePlay アプリID
#define ANDROID_APP_ID              "TemplateProject"         //TODO:APPINFO

//キーの接頭語(プロジェクト名)
#define USERDEF_PREFIX_PROJECTNAME  "TemplateProject"         //TODO:APPINFO

//ゲームセンターID
#define GAMECENTER_ID               "bestscore_template"      //TODO:APPINFO

//スコア データ型 int/float
#define SCORE_DATA_TYPE int
//#define SCORE_DATA_TYPE float

//コインコンティニュー有無を指定（未使用の場合はコメントアウト）
#define USE_COIN_CONTINUE

//JoyStickをつかうかどうか
//#define USE_JOYSTICK

//タイトルアニメーションをするかどうか（ぼよんぼよん）
#define TITLE_ANIMATION_BOUNCE


//フォント
//static int fontIdx = 0;
static std::string fontList[] =
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    "Goodia_Basic.ttf",
    "font/time_font_white.fnt", //bmpfont
#else
    "ttf_fonts/Goodia_Basic.ttf",
    "font/time_font_white.fnt", //bmpfont
#endif
};


//ゲームモード
typedef enum _GameMode
{
    GameMode_Normal,
    GameMode_Premium,

} GameMode;


//スワイプ種類
typedef enum _TouchStatus
{
    TouchStatus_None = 0,

    TouchStatus_Left,
    TouchStatus_Right,
} TouchStatus;

#endif
