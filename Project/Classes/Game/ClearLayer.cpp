//
//  ClearLayer.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include "SimpleAudioEngine.h"
#include "CommonFunction.h"
#include "PrivateUserDefault.h"
#include "PrivateConfig.h"
#include "TwitterConfig.h"

#include "NativeInterface.h"

#include "HelloWorldScene.h"
#include "TitleLayer.h"
#include "ClearLayer.h"



using namespace cocos2d;
using namespace CocosDenshion;


CCScene * ClearLayer::sceneWithResult(GameMode mode, SCORE_DATA_TYPE score)
{
    // 'scene' is an autorelease object
    CCScene * scene = CCScene::create();

    // 'layer' is an autorelease object
    //ClearLayer * layer = ClearLayer::create();
    ClearLayer * layer = ClearLayer::createWithResult(mode, score);

    // add layer as a child to scene
    scene->addChild(layer, 1, 1);

    // return the scene
    return scene;
}


ClearLayer * ClearLayer::createWithResult(GameMode mode, SCORE_DATA_TYPE score)
{
    ClearLayer * pRet = new ClearLayer();
    //if (pRet && pRet->init())
    if (pRet && pRet->initWithResult(mode, score))
    {
        pRet->autorelease();
    }
    else
    {
        delete pRet;
        pRet = NULL;
    }

    return pRet;
}


//bool ClearLayer::init()
bool ClearLayer::initWithResult(GameMode mode, SCORE_DATA_TYPE score)
{
    if ( CCLayer::init() )
    {
        //内部変数
        {
            _gameMode = mode;
            _scorePoint = score;
			
			//縦横
			_isPortrait = CommonFunction::isOrientationPortrait();
		}

        CCSize winSize = CCDirector::sharedDirector()->getWinSize();
        CCPoint center = ccpMult(ccpFromSize(winSize), 0.5f);

        //iOS/Android 位置調整分岐用
        TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
        if (target == kTargetIpad) {
            this->setScale(0.9f);
        }
        //背景
        {
            CCSprite * sprite = CCSprite::createWithSpriteFrameName("ending_back.png");
            if (target == kTargetIpad) {
                sprite->setScaleX(CommonFunction::getBGScaleRate().width * 1.12f);
                sprite->setScaleY(CommonFunction::getBGScaleRate().height * 1.12f);
            }
            else{
                sprite->setScaleX(CommonFunction::getBGScaleRate().width);
                sprite->setScaleY(CommonFunction::getBGScaleRate().height);
            }
            sprite->setPosition(center);

            this->addChild(sprite, ChildTagBackSprite, ChildTagBackSprite);
        }

        if (!_isPortrait) //横画面の場合
        {
            //タイトル
            CCRect titleBoundingBox;
            {
                //上を基準に配置
                CCPoint position;
                {
                    float dy = CommonFunction::getAdHeight() + CommonFunction::getValueOnScale(5.0f);
                    position = ccp(center.x, winSize.height - dy);
                }
                
                CCSprite * sprite = CCSprite::createWithSpriteFrameName("ending_title.png");
                
                sprite->setAnchorPoint(ccp(0.5f, 1.0f));
                sprite->setPosition(position);
                
                this->addChild(sprite, ChildTagDetailBackSprite, ChildTagDetailBackSprite);
                
                titleBoundingBox = sprite->boundingBox();
            }

            //今回の記録表示
            this->setScoreLabel(titleBoundingBox.getMinY() - 10.0*CommonFunction::getAdjustRate());
        }

        //詳細
        CCRect detailSpriteBoundingBox;
        {
            if (_isPortrait)    //縦向き
            {
                {
                    CCSprite * sprite = CCSprite::createWithSpriteFrameName("ending_detailback.png");

                    sprite->setPosition(ccp(winSize.width * 0.5f, winSize.height * 0.54f));
                    detailSpriteBoundingBox = sprite->boundingBox();

                    this->addChild(sprite, ChildTagDetailBackSprite, ChildTagDetailBackSprite);
                }

                //今回の記録表示
                this->setScoreLabel(detailSpriteBoundingBox.getMaxY() - 100.0*CommonFunction::getAdjustRate());
            }

            //新記録
            {
                CCSprite * sprite = CCSprite::createWithSpriteFrameName("ending_newrecord.png");

                CCPoint position;
                {
                    float ds = CommonFunction::getValueOnScale(5.0f);;
                    
                    CCSize size = sprite->boundingBox().size;
                    position = ccp(detailSpriteBoundingBox.getMaxX() - size.width - ds*3,
                                   detailSpriteBoundingBox.getMaxY() - size.height * 0.5f - ds);
                }
                
                sprite->setPosition(position);
                
                this->addChild(sprite, ChildTagNewRecordSprite, ChildTagNewRecordSprite);
                sprite->setVisible(false);
            }
            
            //Twitterボタン
            {
                CCMenuItemSprite * itemTwitter;
                CCSize twitterSize;
                {
                    CCString * fileName = CCString::create("ending_icon_twitter.png");
                    
                    CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    selected->setColor(ccGRAY);
                    
                    itemTwitter = CCMenuItemSprite::create(normal, selected, this, menu_selector(ClearLayer::twitterTouched));
                    twitterSize = itemTwitter->boundingBox().size;
                }
                
                //メニュー作成
                {
                    if (_isPortrait)   //縦向き
                    {
                        float ds = CommonFunction::getValueOnScale(10.0f);
                        
                        CCPoint position = ccp(detailSpriteBoundingBox.getMinX() + (ds + twitterSize.width  * 0.5f),
                                               detailSpriteBoundingBox.getMaxY() - (ds + twitterSize.height * 0.5f));
                        
                        CCMenu * menu = CCMenu::create(itemTwitter, NULL);
                        menu->setPosition(position);
                        
                        if (menu)
                        {
                            this->addChild(menu, ChildTagTwitterMenu, ChildTagTwitterMenu);
                        }
                    }
                    else
                    {
                        if (target == kTargetIphone)
                        {
                            float dx = (320.0f + (160.0f * 0.5f)) * 0.5f;
                            float ds = CommonFunction::getValueOnScale(5.0f);
                            
                            CCPoint position = ccp(center.x + dx,
                                                   winSize.height - (twitterSize.height * 0.5f) - ds);
                            
                            CCMenu * menu = CCMenu::create(itemTwitter, NULL);
                            menu->setPosition(position);
                            
                            if (menu)
                            {
                                this->addChild(menu, ChildTagTwitterMenu, ChildTagTwitterMenu);
                            }
                            
                        }
                    }
                }
                
                //MoreAppsボタン
                CCMenuItem * itemMoreApps;
                {
                    CCString * fileName = CCString::create("ending_icon_moreapps.png");
                    
                    CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    selected->setColor(ccGRAY);
                    
                    CCMenuItemSprite * item = CCMenuItemSprite::create(normal, selected, this, menu_selector(ClearLayer::moreApps2Touched));
                    
                    itemMoreApps = item;
                }
                
                //メニュー作成
                {
                    //配置位置
                    CCMenu * menu = NULL;
                    CCPoint position = CCPointZero;
                    {
                        float dx = (320.0f + (160.0f * 0.5f)) * 0.5f;
                        float ds = CommonFunction::getValueOnScale(5.0f);
                        
                        if (_isPortrait)   //縦向き
                        {
                            menu = CCMenu::create(itemMoreApps, NULL);
                            float ds = CommonFunction::getValueOnScale(10.0f);
                            
                            position = ccp(detailSpriteBoundingBox.getMaxX() - (ds + twitterSize.width  * 0.5f),
                                           detailSpriteBoundingBox.getMaxY() - (ds + twitterSize.height * 0.5f));
                            
                        }
                        else
                        {
                            if (target == kTargetIphone)
                            {
                                menu = CCMenu::create(itemMoreApps, NULL);
                                position = ccp(center.x - dx,
                                               winSize.height - (itemTwitter->boundingBox().size.height * 0.5f) - ds);
                            }
                            else
                            {
                                menu = CCMenu::create(itemMoreApps, itemTwitter, NULL);
                                menu->alignItemsHorizontallyWithPadding(ds*2);
                                position = ccp(winSize.width - itemTwitter->boundingBox().size.width - (itemMoreApps->boundingBox().size.width*0.5f),
                                               winSize.height - (itemTwitter->boundingBox().size.height * 0.5f) - ds*2);
                            }
                        }
                    }
                    menu->setPosition(position);
                    
                    if (menu)
                    {
                        this->addChild(menu, ChildTagMoreApps2Menu, ChildTagMoreApps2Menu);
                    }
                }
            }
            
        
            //おすすめアプリ（縦向きの場合のみ）
            if (_isPortrait)
            {
                //アイコン
                {
                    CCMenuItemSprite * itemMoreAppsIcon;
                    CCSize moreAppsSize;
                    {
                        CCString * fileName = CCString::create("ending_icon_moreapps.png");
                        
                        CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                        CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                        selected->setColor(ccGRAY);
                        
                        itemMoreAppsIcon = CCMenuItemSprite::create(normal, selected, this, menu_selector(ClearLayer::moreApps2Touched));
                        moreAppsSize = itemMoreAppsIcon->boundingBox().size;
                    }
                    
                    //メニュー作成
                    {
                        //配置位置
                        CCPoint position = CCPointZero;
                        {
                            float ds = CommonFunction::getValueOnScale(10.0f);
                            
                            position = ccp(detailSpriteBoundingBox.getMaxX() - (ds + moreAppsSize.width  * 0.5f),
                                           detailSpriteBoundingBox.getMaxY() - (ds + moreAppsSize.height * 0.5f));
                            
                        }
                        
                        CCMenu * menu = CCMenu::create(itemMoreAppsIcon, NULL);
                        menu->setPosition(position);
                        
                        if (menu)
                        {
                            this->addChild(menu, ChildTagMoreApps2Menu, ChildTagMoreApps2Menu);
                        }
                    }
                }
                
                //おすすめボタン
                CCMenuItemSprite * itemMoreApps;
                {
                    CCString * fileName = CCString::create("ending_btn_moreapps.png");
                    CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    selected->setColor(ccGRAY);
                    
                    itemMoreApps = CCMenuItemSprite::create(normal, selected, this, menu_selector(ClearLayer::moreAppsTouched));
                    itemMoreApps->setTag(ChildTagMoreAppsSprite);
                    
                }
                
                //メニューの作成
                {
                    CCMenu * menu = CCMenu::create(itemMoreApps, NULL);
                    
                    CCPoint position;
                    {
                        float dy = CommonFunction::getValueOnScale(5.0f);;
                        position = ccp(center.x,
                                       detailSpriteBoundingBox.getMinY() + itemMoreApps->boundingBox().size.height * 0.5f + dy);
                    }
                    menu->setPosition(position);
                    
                    this->addChild(menu, ChildTagMoreAppsMenu, ChildTagMoreAppsMenu);
                }
                
                //おすすめアプリ　イチオシバッジ
                {
                    CCMenu * menu = (CCMenu *)this->getChildByTag(ChildTagMoreAppsMenu);
                    if (menu)
                    {
                        CCMenuItemSprite * item = (CCMenuItemSprite *)menu->getChildByTag(ChildTagMoreAppsSprite);
                        if (item)
                        {
                            CCRect boundingBox = item->boundingBox();
                            CCPoint position;
                            
                            CCSprite * sprite = CCSprite::createWithSpriteFrameName("ending_badge.png");
                            {
                                CCSize size = sprite->boundingBox().size;
                                
                                position = menu->getPosition();
                                position = ccpAdd(position, ccp(boundingBox.getMaxX() - size.width * 0.1f,
                                                                boundingBox.getMaxY()));
                            }
                            sprite->setPosition(position);
                            
                            this->addChild(sprite, ChildTagMoreAppsBadgeSprite, ChildTagMoreAppsBadgeSprite);
                            
                            CCAction * action = NULL;
                            {
                                CCFiniteTimeAction * zoomin = CCScaleTo::create(0.8f, 0.8f);
                                CCFiniteTimeAction * zoonout = CCScaleTo::create(0.8f, 1.0f);
                                
                                CCFiniteTimeAction * sequence = CCSequence::create(zoomin, zoonout, NULL);
                                action = CCRepeatForever::create((CCActionInterval *)sequence);
                                sprite->runAction(action);
                            }
                        }
                    }
                }
            }
        }

        //詳細
        CCRect smallBackBoundingBox;
        {
            CCSprite * sprite = CCSprite::createWithSpriteFrameName("ending_detailsmallback.png");
            
            sprite->setAnchorPoint(ccp(0.5f, 0.0f));
            
            CCPoint position;
            {
                if (_isPortrait)    //縦向き
                {
                    float dy = CommonFunction::getValueOnScale(40.0f);
                    position = ccp(center.x,
                                   detailSpriteBoundingBox.getMinY() + dy);
                }
                else
                {
                    float dy = CommonFunction::getValueOnScale(55.0f);
                    position = ccp(center.x, dy);
                }
            }
            sprite->setPosition(position);
            
            this->addChild(sprite, ChildTagDetailSmallBackSprite, ChildTagDetailSmallBackSprite);
            smallBackBoundingBox = sprite->boundingBox();
            
//            if (_isPortrait)    //縦向き
//            {
//                this->setBestScoreLabel(smallBackBoundingBox.getMaxY());
//            }
            
            //Top3スコア表示
            {
                float d_center = 0.0f;
                
                //タイトル
                CCRect rankTitleBoundingBox;
                {
                    float fontSize = CommonFunction::getValueOnScale(18.0f);
                    
                    CCPoint position;
                    {
						position = ccp(smallBackBoundingBox.getMidX(),
									   smallBackBoundingBox.getMaxY() - CommonFunction::getValueOnScale(18.0f));
                    }
                    
                    CCLabelTTF * label = CCLabelTTF::create("Top 3 Scores", fontList[0].c_str(), fontSize);
                    label->setColor(ccc3(0, 0, 0));
                    label->setPosition(position);
                    this->addChild(label, ChildTagRankingTitleLabel, ChildTagRankingTitleLabel);
                    rankTitleBoundingBox = label->boundingBox();
                    
                    //スコアの中心の高さ
                    {
                        d_center = smallBackBoundingBox.size.height - (CommonFunction::getValueOnScale(25.0f) + fontSize);
                        d_center *= 0.5f;
                    }
                    
                }
                
                //ランキング
                {
                    float fontSize = CommonFunction::getValueOnScale(18.0f);
                    
                    int tagId[] = {
                        ChildTagRanking1Label,
                        ChildTagRanking2Label,
                        ChildTagRanking3Label,
                    };
                    
                    for (int i = 0; i < sizeof(tagId)/sizeof(*tagId); i++)
                    {
                        //Top3スコア
                        {
                            CCPoint position;
                            {
                                position = ccp(smallBackBoundingBox.getMaxX() - CommonFunction::getValueOnScale(30.0f),
                                               smallBackBoundingBox.getMinY() + CommonFunction::getValueOnScale(10.0f) + (d_center - ((fontSize + CommonFunction::getValueOnScale(2.5f)) * (i - 1))));
                            }
                            
                            CCLabelTTF * label = CCLabelTTF::create("", fontList[0].c_str(), fontSize);
                            
                            label->setColor(ccc3(0, 0, 0));
                            label->setAnchorPoint(ccp(1.0f, 0.5f));
                            label->setPosition(position);
                            
                            this->addChild(label, tagId[i], tagId[i]);
                        }

                        //ラベル
                        {
                            CCPoint position;
                            {
                                position = ccp(smallBackBoundingBox.getMinX() + CommonFunction::getValueOnScale(10.0f),
                                               smallBackBoundingBox.getMinY() + CommonFunction::getValueOnScale(10.0f) + (d_center - ((fontSize + CommonFunction::getValueOnScale(2.5f)) * (i - 1))));
                            }
                            
                            CCString * str = CCString::createWithFormat("%d:",i+1);
                            CCLabelTTF * label = CCLabelTTF::create(str->getCString(), fontList[0].c_str(), fontSize);
                            
                            label->setColor(ccc3(0, 0, 0));
                            label->setAnchorPoint(ccp(0.0f, 0.5f));
                            label->setPosition(position);
                            
                            this->addChild(label, tagId[i]);
                        }
                    }
                }
            }
        }

        //メニュー (Title, Retry)
        {
            CCMenuItemSprite * itemTitle;
            {
                CCString * fileName = CCString::create("ending_btn_title.png");
                CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                selected->setColor(ccGRAY);
                
                itemTitle = CCMenuItemSprite::create(normal, selected, this, menu_selector(ClearLayer::titleTouched));
            }
            
            CCMenuItemSprite * itemRetry;
            {
                CCString * fileName = CCString::create("ending_btn_retry.png");
                CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                selected->setColor(ccGRAY);
                
                itemRetry = CCMenuItemSprite::create(normal, selected, this, menu_selector(ClearLayer::retryTouched));
            }
            
            //メニューの作成
            {
                CCMenu * menu = CCMenu::create(itemTitle, itemRetry, NULL);

                //メニューの配置位置
                {
                    CCPoint position;

                    //縦向き
                    if (_isPortrait)
                    {
                        float dy = CommonFunction::getValueOnScale(5.0f);
                        position = ccp(center.x,
                                       detailSpriteBoundingBox.getMinY() - (itemTitle->boundingBox().size.height * 0.5f + dy));
                    }
                    else
                    {
                        position = ccp((smallBackBoundingBox.getMaxX() + winSize.width)/2,
                                       smallBackBoundingBox.getMidY());
                    }

                    menu->setPosition(position);
                }

                //ボタン配置
                {
                    float ds = CommonFunction::getValueOnScale(10.0f);

                    //縦向き
                    if (_isPortrait)
                    {
                        menu->alignItemsHorizontallyWithPadding(ds);
                    }
                    else
                    {
                        menu->alignItemsVerticallyWithPadding(ds);
                    }
                }

                this->addChild(menu, ChildTagMainMenu, ChildTagMainMenu);
            }
        }

        //おすすめアプリ（横向きの場合のみ）
        if (!_isPortrait)
        {
            //おすすめアプリ
            {
                CCMenuItemSprite * itemMoreApps;
                {
                    CCString * fileName = CCString::create("ending_btn_moreapps.png");
                    CCSprite * normal = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    CCSprite * selected = CCSprite::createWithSpriteFrameName(fileName->getCString());
                    selected->setColor(ccGRAY);

                    itemMoreApps = CCMenuItemSprite::create(normal, selected, this, menu_selector(ClearLayer::moreAppsTouched));
                    itemMoreApps->setTag(ChildTagMoreAppsSprite);
                }

                //メニューの作成
                {
                    CCMenu * menu = CCMenu::create(itemMoreApps, NULL);
                    CCPoint position = ccp(smallBackBoundingBox.getMinX()/2,smallBackBoundingBox.getMidY());
                    menu->setPosition(position);
                    this->addChild(menu, ChildTagMoreAppsMenu, ChildTagMoreAppsMenu);
                }
            }

            //おすすめアプリ　イチオシバッジ
            {
                CCMenu * menu = (CCMenu *)this->getChildByTag(ChildTagMoreAppsMenu);
                if (menu)
                {
                    CCMenuItemSprite * item = (CCMenuItemSprite *)menu->getChildByTag(ChildTagMoreAppsSprite);
                    if (item)
                    {
                        CCRect boundingBox = item->boundingBox();
                        CCPoint position;
                        
                        CCSprite * sprite = CCSprite::createWithSpriteFrameName("ending_badge.png");
                        {
                            CCSize size = sprite->boundingBox().size;
                            
                            position = menu->getPosition();
                            position = ccpAdd(position, ccp(boundingBox.getMaxX() - size.width * 0.1f,
                                                            boundingBox.getMaxY()));
                        }
                        sprite->setPosition(position);
                        
                        this->addChild(sprite, ChildTagMoreAppsBadgeSprite, ChildTagMoreAppsBadgeSprite);
                        
                        CCAction * action = NULL;
                        {
                            CCFiniteTimeAction * zoomin = CCScaleTo::create(0.8f, 0.8f);
                            CCFiniteTimeAction * zoonout = CCScaleTo::create(0.8f, 1.0f);
                            
                            CCFiniteTimeAction * sequence = CCSequence::create(zoomin, zoonout, NULL);
                            action = CCRepeatForever::create((CCActionInterval *)sequence);
                            sprite->runAction(action);
                        }
                    }
                }
            }
        }

        //Android Backキー, Homeキー対応用
        if (target == kTargetAndroid)
        {
            this->setKeypadEnabled(true);
        }
        
        return true;
    }
    else
    {
        return false;
    }
}


ClearLayer::ClearLayer()
{
    {
        _scoreLabel = NULL;
        _scorePtLabel = NULL;
        _bestLabel = NULL;
        _bestPtLabel = NULL;
        _rankingTitleLabel = NULL;
    }
    
    //スプライトシートの読み込み
    {
        CCSpriteFrameCache * frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
        
        frameCache->addSpriteFramesWithFile("ending_gameimage.plist");
    }
}

ClearLayer::~ClearLayer()
{
    if (_scoreLabel)
    {
        _scoreLabel->release();
        _scoreLabel = NULL;
    }
    if (_scorePtLabel)
    {
        _scorePtLabel->release();
        _scorePtLabel = NULL;
    }
    if (_bestLabel)
    {
        _bestLabel->release();
        _bestLabel = NULL;
    }
    if (_bestPtLabel)
    {
        _bestPtLabel->release();
        _bestPtLabel = NULL;
    }
    if (_rankingTitleLabel)
    {
        _rankingTitleLabel->release();
        _rankingTitleLabel = NULL;
    }
    
    //スプライトシートの解放
    {
        CCSpriteFrameCache * frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();

        frameCache->removeSpriteFrameByName("ending_gameimage.plist");
    }

}


void ClearLayer::onEnter()
{
    CCLayer::onEnter();

    int tagId[] = {
        ChildTagRanking1Label,
        ChildTagRanking2Label,
        ChildTagRanking3Label,
    };

    //トップ３のスコアをセット
    for (int i = 0; i < RANKING_MAX; i++)
    {
        //float型は未定
        SCORE_DATA_TYPE rankScore = HelloWorld::getBestPoint(this->getGameMode(), (i+1));

        CCString * score = NULL;
//        if (rankScore <= 0.0f)
        if (rankScore <= 0)
        {
            score = CCString::create("");
        }
        else
        {
            score = CCString::createWithFormat("%d%s", rankScore, SUFFIX);
//            score = CCString::createWithFormat("%.2lf %s", (float)rankScore/100.0f, SUFFIX);
//            score = CommonFunction::getFormatTimerString((float)rankScore/100.0f);
        }

        CCLabelTTF * label = (CCLabelTTF *)this->getChildByTag((tagId[i]));
        label->setString(score->getCString());
    }


    //記録更新
    {
        CCSprite * sprite = (CCSprite *)this->getChildByTag(ChildTagNewRecordSprite);
        if (sprite)
        {
            //ベストスコア
            SCORE_DATA_TYPE bestScore = HelloWorld::getBestPoint(this->getGameMode(), 1);

            //ベストタイム
            sprite->setVisible(((this->getScorePoint() > 0) && (this->getScorePoint() >= bestScore))?(true):(false));
//            bool newRecord = false;
//            {
//                if ((this->getScorePoint() > 0) &&
//                    ((bestScore <= 0) || (this->getScorePoint() <= bestScore)))
//                {
//                    newRecord = true;
//                }
//            }
//            sprite->setVisible((newRecord)?(true):(false));

            if (sprite->isVisible())
            {
                this->particleEnding();
                this->scheduleOnce(schedule_selector(ClearLayer::callReviewPopup), 1.0f);
            }
        }
    }

    //広告表示
    Cocos2dExt::NativeInterface::showAd(true);
    Cocos2dExt::NativeInterface::showGenuineAd(true);
    Cocos2dExt::NativeInterface::showHouseAd(true);
}

void ClearLayer::callReviewPopup()
{
    Cocos2dExt::NativeInterface::showReviewPopup();
}

void ClearLayer::setScoreLabel(float positionY)
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

    //今回のスコア
    CCString * score = CCString::createWithFormat("%d", this->getScorePoint());
//    CCString * score = CCString::createWithFormat("%.2lf", this->getScorePoint());
//    CCString * score = CCString::createWithFormat("%.2lf", (float)this->getScorePoint()/100.0f);
//    CCString * score = CommonFunction::getFormatTimerString((float)this->getScorePoint()/100.0f);
//    this->getScoreLabel()->setString(score->getCString());

    //今回スコアの表示
    float y;
    {
        //数値
        {
            float fontSize = CommonFunction::getValueOnScale(40.0f);
            y = fontSize;

            _scoreLabel = CCLabelTTF::create(score->getCString(), fontList[0].c_str(), fontSize);
            _scoreLabel->retain();
            _scoreLabel->setAnchorPoint(ccp(0.0f,0.0f));
            _scoreLabel->setPosition(CCPointZero);
            _scoreLabel->setVerticalAlignment(kCCVerticalTextAlignmentCenter);
            _scoreLabel->setHorizontalAlignment(kCCTextAlignmentRight);
        }

        //単位
        {
            float fontSize = CommonFunction::getValueOnScale(30.0f);

            _scorePtLabel = CCLabelTTF::create(SUFFIX, fontList[0].c_str(), fontSize);
            _scorePtLabel->retain();
            _scorePtLabel->setAnchorPoint(ccp(0.0f,0.6f));
            _scorePtLabel->setPosition(ccp(_scoreLabel->boundingBox().size.width, _scoreLabel->boundingBox().size.height/2));
            _scorePtLabel->setVerticalAlignment(kCCVerticalTextAlignmentBottom);
            _scorePtLabel->setHorizontalAlignment(kCCTextAlignmentLeft);
        }
    }

    //一枚に乗っける
    {
        //デバッグ用に色つけれるからイイネ
        //CCLayerColor * scoreLayer = CCLayerColor::create(ccc4(255, 0, 0, 128));//赤
        CCLayerColor * scoreLayer = CCLayerColor::create(ccc4(0, 0, 0, 0));//赤
        float scoreLayerWidth   = _scoreLabel->boundingBox().size.width + _scorePtLabel->boundingBox().size.width;
        float scoreLayerHeight = _scoreLabel->boundingBox().size.height;
        if (scoreLayerHeight < _scorePtLabel->boundingBox().size.height) {
            scoreLayerHeight = _scorePtLabel->boundingBox().size.height;
        }
        scoreLayer->setContentSize(CCSizeMake(scoreLayerWidth,scoreLayerHeight));

        if (_isPortrait) //縦向き
        {
            scoreLayer->setPosition(ccp((winSize.width - scoreLayer->boundingBox().size.width)/2,positionY));
        }
        else
        {
            scoreLayer->setPosition(ccp((winSize.width - scoreLayer->boundingBox().size.width)/2,positionY - y));
        }

        scoreLayer->addChild(_scoreLabel, ChildTagscoreLabel, ChildTagscoreLabel);
        scoreLayer->addChild(_scorePtLabel, ChildTagscorePtLabel, ChildTagscorePtLabel);
        this->addChild(scoreLayer,ChildTagscoreLabelLayer,ChildTagscoreLabelLayer);
    }
}

void ClearLayer::setBestScoreLabel(float positionY)
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

    //ベストスコアをセット
    SCORE_DATA_TYPE value = HelloWorld::getBestPoint(this->getGameMode(), 1);
    CCString * score = CCString::createWithFormat("%d", value);
//    CCString * score = CCString::createWithFormat("%.2lf", value);
//    CCString * score = CCString::createWithFormat("%.2lf", (float)value/100.0f);
//    CCString * score = CommonFunction::getFormatTimerString((float)value/100.0f);

    //自己ベスト表示
    {
        //数値
        {
            float fontSize = CommonFunction::getValueOnScale(30.0f);
            _bestLabel = CCLabelTTF::create(score->getCString(), fontList[0].c_str(), fontSize);
            _bestLabel->retain();
            _bestLabel->setAnchorPoint(ccp(0.0f,0.0f));
            _bestLabel->setPosition(CCPointZero);
            _bestLabel->setVerticalAlignment(kCCVerticalTextAlignmentCenter);
            _bestLabel->setHorizontalAlignment(kCCTextAlignmentRight);
        }

        //単位
        {
            float fontSize = CommonFunction::getValueOnScale(20.0f);
            _bestPtLabel = CCLabelTTF::create(SUFFIX, fontList[0].c_str(), fontSize);
            _bestPtLabel->retain();
            _bestPtLabel->setAnchorPoint(ccp(0.0f,0.6f));
            _bestPtLabel->setPosition(ccp(_bestLabel->boundingBox().size.width, _bestLabel->boundingBox().size.height/2));
            _bestPtLabel->setVerticalAlignment(kCCVerticalTextAlignmentBottom);
            _bestPtLabel->setHorizontalAlignment(kCCTextAlignmentLeft);
        }
    }

    //一枚に乗っける
    {
        //デバッグ用に色つけれるからイイネ
        //CCLayerColor * bestScoreLayer = CCLayerColor::create(ccc4(0, 255, 0, 128));//緑
        CCLayerColor * bestScoreLayer = CCLayerColor::create(ccc4(0, 0, 0, 0));
        float scoreLayerWidth   = _bestLabel->boundingBox().size.width + _bestPtLabel->boundingBox().size.width;
        float scoreLayerHeight = _bestLabel->boundingBox().size.height;
        if (scoreLayerHeight < _bestPtLabel->boundingBox().size.height) {
            scoreLayerHeight = _bestPtLabel->boundingBox().size.height;
        }
        bestScoreLayer->setContentSize(CCSizeMake(scoreLayerWidth,scoreLayerHeight));
        bestScoreLayer->setPosition(ccp((winSize.width - bestScoreLayer->boundingBox().size.width)/2,positionY));

        bestScoreLayer->addChild(_bestLabel, ChildTagbestLabel, ChildTagbestLabel);
        bestScoreLayer->addChild(_bestPtLabel, ChildTagbestPtLabel, ChildTagbestPtLabel);
        this->addChild(bestScoreLayer,ChildTagbestLabelLayer,ChildTagbestLabelLayer);
    }
}


void ClearLayer::onExit()
{
    CCLayer::onExit();
}

void ClearLayer::onEnterTransitionDidFinish()
{
    CCLayer::onEnterTransitionDidFinish();

    CommonFunction::myBackgroundMusicPlayWithFile("BGM_Result.mp3", true);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    Cocos2dExt::NativeInterface::showIconAd(2, true);
    
#else
    Cocos2dExt::NativeInterface::showIconAd(2);
    
#endif

    //全面広告
    {
        int adCount = PrivateUserDefault::getADCount();

        //更新
        PrivateUserDefault::setADCount( ++adCount );
        //FLURRY
        Cocos2dExt::NativeInterface::reportGameCountToFlurry(adCount);
		
        
        //Ad
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        Cocos2dExt::NativeInterface::showAdSplash(adCount);
#else
        Cocos2dExt::NativeInterface::showOptionalAd(adCount);
#endif
    }
}


void ClearLayer::moreAppsTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
	
	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("E_RECOMMEND_BUTTON");

    Cocos2dExt::NativeInterface::showAppliPromotion();
}

void ClearLayer::moreApps2Touched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
	
	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("E_RECOMMEND_ICON");

    Cocos2dExt::NativeInterface::showGameFeat();
}


void ClearLayer::titleTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
    SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	
	//タイトルでエンディング用の広告が残ってしまう現象の対応
	{
        //広告表示
        Cocos2dExt::NativeInterface::showAd(true);
        Cocos2dExt::NativeInterface::showHouseAd(true);
        Cocos2dExt::NativeInterface::showGenuineAd(false);
		
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        Cocos2dExt::NativeInterface::showIconAd(1, true);
#else
        Cocos2dExt::NativeInterface::showIconAd(1);
#endif
    }

	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("E_TITLE_BUTTON");
	
	//インターステイシャル広告表示
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	Cocos2dExt::NativeInterface::showAdSplashAddRanking();
#else
	Cocos2dExt::NativeInterface::showOptionalAdRank();
#endif
    
    CCScene * scene = TitleLayer::scene();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(0.8f, scene));
}

void ClearLayer::retryTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");
    SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	
	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("E_RETRY_BUTTON");

    //画面切り替え (ゲーム画面へ)
    CCScene * scene = HelloWorld::sceneWithGameMode(GameMode_Normal);
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(0.8f, scene));
}

void ClearLayer::setRetryButtonEnabled()
{
    CCMenu * menu = (CCMenu *)this->getChildByTag(ChildTagMainMenu);
    if (menu)
    {
        CCMenuItemSprite * item = (CCMenuItemSprite *)menu->getChildByTag(ChildTagMenu_ReStartItemSprite);
        item->setEnabled(true);
    }
}

void ClearLayer::particleEnding()
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

    CCParticleSystemQuad * ps = CCParticleSystemQuad::create("ending_particle.plist");

    ps->setPosition(ccp(winSize.width * 0.5, winSize.height + 150));
    ps->isAutoRemoveOnFinish();

    this->addChild(ps, ChildTagParticle, ChildTagParticle);

}

void ClearLayer::twitterTouched()
{
    CommonFunction::myEffectPlayWithFile("SE_Enter.mp3");

    //Tweetする文章
    CCString * tweet = NULL;

    //スコア値
    //float score = this->getScorePoint();
    int score = this->getScorePoint();

    TargetPlatform target = CCApplication::sharedApplication()->getTargetPlatform();
    switch (target)
    {
        case kTargetIpad:
        case kTargetIphone:
        {
            tweet = CCString::createWithFormat(TWITTER_I, score, SUFFIX, PROJECT_NAME, IOS_APP_ID, PROJECT_NAME);
            break;
        }
        case kTargetAndroid:
        {
            tweet = CCString::createWithFormat(TWITTER_A, score, SUFFIX, PROJECT_NAME, ANDROID_APP_ID, PROJECT_NAME);
            break;
        }

        default:
            break;
    }
	
	//FLURRY
	Cocos2dExt::NativeInterface::reportFlurryWithEvent("E_TWITTER");

    Cocos2dExt::NativeInterface::launchTwitter(tweet->getCString());
}


void ClearLayer::keyBackClicked()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    //Exit広告配信率設定 ... Androidのみ
    Cocos2dExt::NativeInterface::showExitAd();
#else
    //アプリ終了
    CCDirector::sharedDirector()->end();
#endif
}

void ClearLayer::keyMenuClicked()
{
}
