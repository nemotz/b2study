//
//  ExplosionSprite.cpp
//  AtomicMan
//
//  Created by Goodia.Inc on 2013/07/11.
//
//

#include "ExplosionSprite.h"

#include "GameLayer.h"
#include "CommonFunction.h"

USING_NS_CC;

typedef enum _ChildTags
{
    ChildTagNone,
    ChildTagSprite,
} ChildTags;

typedef enum _ActionTags
{
    ActionTagNone = 10,
    ActionTagNormal,
} ActionTags;


bool ExplosionSprite::init()
{
    if ( CCSprite::initWithSpriteFrameName("main_effect_1.png"))
    {
        this->startAction();
        
        return true;
    }
    else
    {
        return false;
    }
    
}

ExplosionSprite::ExplosionSprite()
{
    
}

ExplosionSprite::~ExplosionSprite()
{
    
}

void ExplosionSprite::startAction()
{
    CCAction * action = NULL;
    {
        {
            const char * frameName = "main_effect_%d.png";
            
            //色を元に戻す(黒だとエフェクトが黒になる)
            CCTintTo *tint = CCTintTo::create(0.0f, 255, 255, 255);
            CCAnimate *animate = CommonFunction::createCCAnimate(frameName, 1, 8, 0.10f);
            CCFiniteTimeAction * fadeOut = CCFadeOut::create(0.1f);
            
            CCFiniteTimeAction * sequence = CCSequence::create(
                                                               tint,
                                                               animate,
                                                               fadeOut,
                                                               NULL);
            action = sequence;
        }
    }
    if (action)
    {
        this->runAction(action);
    }
}
