//
//  HelpLayer.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __TemplateProject__HelpLayer__
#define __TemplateProject__HelpLayer__

#include "cocos2d.h"


class HelpLayer : public cocos2d::CCLayer
{

public:
    typedef enum _ChildTags
    {
        ChildTagNone,

        //ヘルプの場合はボタンを最背面に配置
        ChildTagHelpMenu,

        //ヘルプ
        ChildTagHelpSprite,

        //タイトル
        ChildTagTitleSprite,

    } ChildTags;


public:
    CREATE_FUNC(HelpLayer);
    //static HelpLayer * create();
	
	CC_SYNTHESIZE_READONLY(float, _adjustRate, AdjustRate);
    CC_SYNTHESIZE_READONLY(float, _scaleRate, ScaleRate);


protected:
    HelpLayer();
    virtual ~HelpLayer();

protected:
    bool init();

private:
    void closeHelp();
    void removeHelp();
};

#endif
