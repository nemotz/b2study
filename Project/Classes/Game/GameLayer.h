//
//  GameLayer.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __TemplateProject__GameLayer__
#define __TemplateProject__GameLayer__

#include "cocos2d.h"
#include "PrivateConfig.h"

#include "CommonFunction.h"
#include "InputLayer.h"

#ifdef USE_JOYSTICK
#include "JoyStickLayer.h"
#endif

#ifdef USE_COIN_CONTINUE
#include "CoinObject.h"
#endif

class GameLayer
: public cocos2d::CCLayer
, public TouchDelegate
#ifdef USE_JOYSTICK
, public JoyStickLayerDelegate
#endif
{

//### 内部で使用しているタグ番号 ###
public:

    //アクションタグ
    typedef enum _ActionTags
    {
        ActionTagNone = 0,

    } ActionTags;


    //Z順 & タグ
    typedef enum _ChildTags
    {
        ChildTagNone = 0,

        //背景
        ChildTagBackImage,

        //InputLayer
        ChildTagInputLayer,

        //Debug
        ChildTagDebug,

    } ChildTags;

    typedef enum _AccelType
    {
        AccelType_None = 0,
        AccelType_Accel,
    } AccelType;


#pragma mark - ### 生成 ###
public:
    CREATE_FUNC(GameLayer);
    //static GameLayer * create();

protected:
    virtual bool init();

protected:
    GameLayer();
    virtual ~GameLayer();


#pragma mark - ### タイマー ###
protected:
    void update(float delta);


#pragma mark - ### ゲーム用プロパティ ###
//public:
//    CC_SYNTHESIZE_READONLY(varType, varName, funName);


#pragma mark - ### 構成 ###
public:
    InputLayer * getInputLayer();


#pragma mark - ### TouchDelegate (InputLayer) ###
protected:
    //タップ用
    virtual void changeTouchStatus(InputLayer::TouchStatus status);

    //移動量
    virtual void changeTouchMoved(cocos2d::CCPoint position);

    //ゲーム中のタップ操作の通知用
    virtual void touchBegan(cocos2d::CCPoint position);
    virtual void touchMoved(cocos2d::CCPoint position);
    virtual void touchEnded(cocos2d::CCPoint position);


#ifdef USE_JOYSTICK
#pragma mark - ### JoyStickLayerDelegate (JoyStickLayer) ###
protected:
        virtual void changeVelocity(cocos2d::CCPoint velocity);
#endif


#pragma mark - ### ゲーム制御 ###
public:
    void gameStart();
    void gameEnd();
	
#ifdef USE_COIN_CONTINUE
    void gameRestart();
	void removeCoin(CoinObject * coin);
#endif
	
private:
#ifdef USE_COIN_CONTINUE
	cocos2d::CCArray * _coinArray;
    int _coinCount;
    void createCoin(cocos2d::CCPoint position);
    void getCoin(CoinObject * coin);
#endif


#pragma mark - ### その他 ###
private:
    void nodeRemoveFromParentAndCleanup(cocos2d::CCNode * node);

    //debug
    void showDebugView(cocos2d::CCRect rect, cocos2d::ccColor4B color, int tag);
    void showDebugView(cocos2d::CCRect rect,GLubyte r,GLubyte g,GLubyte b,GLubyte o,int tag);
};

#endif
