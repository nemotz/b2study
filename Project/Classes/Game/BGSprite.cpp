//
//  PlayerSprite.cpp
//  Unicycle
//
//  Created by Goodia.Inc on 13/03/01.
//
//

#include "BGSprite.h"

#include "HelloWorldScene.h"
#include "PrivateConfig.h"
#include "CommonFunction.h"
#include "GameLayer.h"

USING_NS_CC;

#define WINSIZE CCDirector::sharedDirector()->getWinSize()
#define ANGLE (18)
#define ANGLE_MAX (90)
#define ANGLE_MIN (-90)

//Z順 & タグ
typedef enum _ChildTags
{
    ChildTagNone,
    ChildTagBG2,
    ChildTagBG1,
    
} ChildTags;

typedef enum _SoundTags
{
    SoundTagNone = 100,
    SoundTagForward,
    SoundTagBack,
} SoundTags;


BGSprite * BGSprite::create()
{
    BGSprite * pRet = new BGSprite();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool BGSprite::init()
{
    if ( CCSprite::initWithSpriteFrameName("main_back_1_3.png") )
    {
        CCSprite * sprite2 = CCSprite::createWithSpriteFrameName("main_back_1_2.png");
        sprite2->setAnchorPoint(ccp(0.0f, 0.0f));
        sprite2->setPosition(CCPointZero);
        this->addChild(sprite2,ChildTagBG2,ChildTagBG2);
        
        CCSprite * sprite1 = CCSprite::createWithSpriteFrameName("main_back_1_1.png");
        sprite1->setAnchorPoint(ccp(0.0f, 0.0f));
        sprite1->setPosition(CCPointZero);
        this->addChild(sprite1,ChildTagBG1,ChildTagBG1);
        
        _changedCount = 0;
        return true;
    }
    else
    {
        return false;
    }
}

void BGSprite::changeBGSpriteType(float duration){
    
    int tagId = ChildTagBG1 - _changedCount;
    if (tagId < ChildTagBG2) {
        return;
    }

    CCFiniteTimeAction * fadeout = CCFadeOut::create(duration);
    CCSprite * sprite = (CCSprite *)this->getChildByTag(tagId);
    if (sprite->getOpacity() != 0) {
        sprite->runAction(fadeout);
    }
    _changedCount++;
}
