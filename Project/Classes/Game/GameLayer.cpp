//
//  GameLayer.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include "CommonFunction.h"
#include "NativeInterface.h"
#include "PrivateUserDefault.h"

#include "GameLayer.h"
#include "HelloWorldScene.h"
//#include "InputLayer.h"

#ifdef USE_COIN_CONTINUE
//コインを一枚取った時にaddするカウント
#define ADD_COIN (1)
#endif

USING_NS_CC;


//GameLayer * GameLayer::create()
//{
//    GameLayer * pRep = new GameLayer();
//    if ( pRep && pRep->init() )
//    {
//        pRep->autorelease();
//    }
//    else
//    {
//        CC_SAFE_DELETE(pRep);
//        pRep = NULL;
//    }
//    return pRep;
//}

bool GameLayer::init()
{
    bool result = false;
    do
    {
        if ( !CCLayer::init() )
        {
            break;
        }

        CCSize winSize = CCDirector::sharedDirector()->getWinSize();
        CCPoint center = ccpMult(ccpFromSize(winSize), 0.5f);

        //内部変数の初期化
        {
#ifdef USE_COIN_CONTINUE
            _coinCount = PrivateUserDefault::getCoinCount();
#endif
        }


        //構成
        {
            //背景
            {
                CCSprite * sprite = CCSprite::createWithSpriteFrameName("main_back_1.png");

                sprite->setPosition(center);

				{
					CCSize size = sprite->boundingBox().size;
					float kw = winSize.width / size.width;
					float kh = winSize.height / size.height;
					
					sprite->setScaleX(kw);
					sprite->setScaleY(kh);
				}
				
                int tagId = ChildTagBackImage;
                this->addChild(sprite, tagId, tagId);
            }


            //入力用レイヤー
            {
                InputLayer * layer = InputLayer::create();

                int tagId = ChildTagInputLayer;
                this->addChild(layer, tagId, tagId);

                layer->setTouchEnabled(true);
                layer->setTouchDelegate(this);
            }
        }


        HelloWorld::sharedHelloWorldLayer()->updateCoin(_coinCount);

        //ココまで来たら正常終了
        result = true;
    } while (0);

    return result;
}


GameLayer::GameLayer()
{
    //初期化
    {
#ifdef USE_COIN_CONTINUE
		_coinArray = CCArray::create();
		_coinArray->retain();
#endif
    }
}

GameLayer::~GameLayer()
{
#ifdef USE_COIN_CONTINUE
    CC_SAFE_RELEASE_NULL(_coinArray);
#endif

    this->unscheduleAllSelectors();
}


#pragma mark - ### タイマー ###
void GameLayer::update(float delta)
{
    //TODO: 仮対応 終了 (ゲーム実装時に削除)
    this->gameEnd();
}

#ifdef USE_COIN_CONTINUE
void GameLayer::getCoin(CoinObject *coin){
    CommonFunction::myEffectPlayWithFile("SE_Coin.mp3");
    coin->setIsGot();
    coin->startAction();
    _coinCount += ADD_COIN;
    HelloWorld::sharedHelloWorldLayer()->updateCoin(_coinCount);
}

void GameLayer::removeCoin(CoinObject *coin){
    CoinObject * _coin = coin;
    if (_coinArray->containsObject(_coin)) {
        _coinArray->removeObject(_coin);
        this->removeChild(_coin, true);
        _coin = NULL;
    }
}
#endif

#pragma mark - ### 構成 ###

InputLayer * GameLayer::getInputLayer()
{
    CCNode * node = this->getChildByTag(ChildTagInputLayer);
    return (InputLayer *)node;
}


#pragma mark - ### TouchDelegate (InputLayer) ###

//タップ用 状態:左, 右, 無し(指が離れてる)
void GameLayer::changeTouchStatus(InputLayer::TouchStatus status)
{
    CCLOG("%s status:%d", __PRETTY_FUNCTION__, status);
}

//移動量
void GameLayer::changeTouchMoved(cocos2d::CCPoint position)
{
    CCLOG("%s position:%f, %f", __PRETTY_FUNCTION__, position.x, position.y);
}

//ゲーム中のタップ操作の通知用
void GameLayer::touchBegan(cocos2d::CCPoint position)
{
    CCLOG("%s position:%f, %f", __PRETTY_FUNCTION__, position.x, position.y);
}

void GameLayer::touchMoved(cocos2d::CCPoint position)
{
    CCLOG("%s position:%f, %f", __PRETTY_FUNCTION__, position.x, position.y);
}

void GameLayer::touchEnded(cocos2d::CCPoint position)
{
    CCLOG("%s position:%f, %f", __PRETTY_FUNCTION__, position.x, position.y);
}


#ifdef USE_JOYSTICK
#pragma mark - ### JoyStickLayerDelegate (JoyStickLayer) ###
void GameLayer::changeVelocity(cocos2d::CCPoint velocity)
{
    CCLOG("%s velocity:%f, %f", __PRETTY_FUNCTION__, velocity.x, velocity.y);
}
#endif


#pragma mark - ### ゲーム制御 ###

void GameLayer::gameStart()
{
    this->schedule(schedule_selector(GameLayer::update));
}


void GameLayer::gameEnd()
{
    if (this->isAccelerometerEnabled() == true)
    {
        this->setAccelerometerEnabled(false);
    }
    this->unschedule(schedule_selector(GameLayer::update));

    //ゲーム終了(ゲームオーバー)
#ifdef USE_COIN_CONTINUE
	CCLog("%d",HelloWorld::sharedHelloWorldLayer()->getIsVideoRewardEnabled());
    if(_coinCount >= HelloWorld::sharedHelloWorldLayer()->getCoinMax() ||
       HelloWorld::sharedHelloWorldLayer()->getIsVideoRewardEnabled()){
		HelloWorld::sharedHelloWorldLayer()->continueGame();
    }
    else{
		HelloWorld::sharedHelloWorldLayer()->gameEnd(false);
	}
#else
    HelloWorld::sharedHelloWorldLayer()->gameEnd(false);
#endif
}

#ifdef USE_COIN_CONTINUE
void GameLayer::gameRestart()
{
    _coinCount = PrivateUserDefault::getCoinCount();
	
	//リスタート処理
}
#endif

#pragma mark - ### その他 ###

void GameLayer::nodeRemoveFromParentAndCleanup(CCNode *node)
{
    if (node)
    {
        node->removeFromParentAndCleanup(true);
    }
}


void GameLayer::showDebugView(cocos2d::CCRect rect, ccColor4B color, int tag)
{
    this->showDebugView(rect, color.r, color.g, color.b, color.a, tag);
}

void GameLayer::showDebugView(cocos2d::CCRect rect, GLubyte r, GLubyte g, GLubyte b, GLubyte o, int tag)
{
    CCLayerColor * debug = (CCLayerColor *)this->getChildByTag(tag);
    if (debug != NULL)
    {
        this->removeChildByTag(tag, true);
    }

    CCLayerColor * newDebug = CCLayerColor::create(ccc4(r, g, b, o), rect.size.width, rect.size.height);
    if (newDebug)
    {
        newDebug->setPosition(ccp(rect.origin.x, rect.origin.y));
        this->addChild(newDebug, tag, tag);
    }
}
