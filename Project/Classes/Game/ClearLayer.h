//
//  ClearLayer.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __TemplateProject__ClearLayer__
#define __TemplateProject__ClearLayer__

#include "cocos2d.h"
#include "PrivateConfig.h"


class ClearLayer : public cocos2d::CCLayer
{
//### 定数 ###
public:
    typedef enum _ChildTags
    {
        ChildTagNone,

        //背景
        ChildTagBackSprite,
        ChildTagBackSprite2,

        //タイトル
        ChildtagTitleSprite,

        //枠
        ChildTagDetailBackSprite,
        ChildTagDetailSmallBackSprite,

        //アイテム
        ChildTagItemSprite,
        ChildTagBadgeSprite,

        //スコア
        ChildTagscoreTitleLabel,
        ChildTagscoreLabelLayer,
        ChildTagscoreLabel,
        ChildTagscorePtLabel,

        //自己ベスト
        ChildTagbestTitleLabel,
        ChildTagbestLabelLayer,
        ChildTagbestLabel,
        ChildTagbestPtLabel,

        //ランキング
        ChildTagRankingTitleLabel,
        ChildTagRankingLabel,

        //ランキング 1〜3
        ChildTagRanking1Label,
        ChildTagRanking2Label,
        ChildTagRanking3Label,

        //新記録
        ChildTagNewRecordSprite,

        //MoreApps
        ChildTagMoreAppsSprite,

        //メニュー
        ChildTagMainMenu,
        ChildTagMoreAppsMenu,

        //メニュー項目
        ChildTagMenu_TitleItemSprite,
        ChildTagMenu_ReStartItemSprite,
        
        //MoreAppsバッジ
        ChildTagMoreAppsBadgeSprite,

        //Twitter画面
        ChildTagTwitterMenu,

        //おすすめゲームアイコン
        ChildTagMoreApps2Menu,

        //パーティクル
        ChildTagParticle,

    } ChildTags;


private:
    ClearLayer();
    ~ClearLayer();

public:
    static cocos2d::CCScene * sceneWithResult(GameMode mode, SCORE_DATA_TYPE score);

private:
    static ClearLayer * createWithResult(GameMode mode, SCORE_DATA_TYPE score);
    bool initWithResult(GameMode mode, SCORE_DATA_TYPE score);


private:
    void onEnter();
    void onExit();
    void onEnterTransitionDidFinish();

    void particleEnding();


//Android Backキー, Homeキー対応用
private:
    void keyBackClicked();
    void keyMenuClicked();


public:
    CC_SYNTHESIZE_READONLY(GameMode, _gameMode, GameMode);
    CC_SYNTHESIZE_READONLY(bool, _isPortrait, IsPortrait);

public:
    CC_SYNTHESIZE(SCORE_DATA_TYPE, _scorePoint, ScorePoint);


private:
    CC_SYNTHESIZE_READONLY(cocos2d::CCLabelTTF*, _scoreLabel, ScoreLabel);
    CC_SYNTHESIZE_READONLY(cocos2d::CCLabelTTF*, _scorePtLabel, ScorePtLabel);
    CC_SYNTHESIZE_READONLY(cocos2d::CCLabelTTF*, _bestLabel, BestLabel);
    CC_SYNTHESIZE_READONLY(cocos2d::CCLabelTTF*, _bestPtLabel, BestPtLabel);
    CC_SYNTHESIZE_READONLY(cocos2d::CCLabelTTF*, _rankingTitleLabel, RankingTitleLabel);
    CC_SYNTHESIZE_READONLY(cocos2d::CCLabelTTF*, _newRecord, NewRecord)

private:
    void moreAppsTouched();
    void moreApps2Touched();
    void titleTouched();
    void retryTouched();
    void twitterTouched();

    //今回の記録表示
    void setScoreLabel(float positionY);//AncherPointはccp(0.5f,0.0f)
    //自己ベスト表示
    void setBestScoreLabel(float positionY);//AncherPointはccp(0.5f,0.0f)

    void callReviewPopup();
    
    //リトライボタン有効
    void setRetryButtonEnabled();

    
};

#endif
