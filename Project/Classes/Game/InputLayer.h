//
//  InputLayer.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#ifndef __TemplateProject__InputLayer__
#define __TemplateProject__InputLayer__

#include "cocos2d.h"
#include "PrivateConfig.h"
#include "XTLayer.h"


class TouchDelegate;


class InputLayer
//    : public cocos2d::CCLayer
    : public XTLayer
{
public:
    typedef enum _TouchStatus
    {
        TouchStatus_None    = 1 << 0,

        TouchStatus_Left    = 1 << 1,
        TouchStatus_Right   = 1 << 2,

        TouchStatus_On      = (TouchStatus_Left + TouchStatus_Right),
    } TouchStatus;


public:
    CREATE_FUNC(InputLayer);

private:
    bool init();

protected:
    InputLayer();
    virtual ~InputLayer();

private:
    bool _isTapStart;

    cocos2d::CCArray * _touchIdArray;
    cocos2d::CCDictionary * _dicPosition;

private:
    bool isInGame();


//タッチ操作
private:
    TouchStatus _touchStatus;
    cocos2d::CCPoint _movePosition;
    void setTouchStatus(cocos2d::CCPoint touchPoint, bool isTouch = true);
    void setTouchMoved(cocos2d::CCPoint touchPoint);

public:
    TouchStatus getTouchStatus(){ return _touchStatus; }
    void resetTouchStatus(){ _touchStatus = TouchStatus_None; }

    //デリゲート処理
    CC_SYNTHESIZE(TouchDelegate *, _touchDelegate, TouchDelegate);


//タッチ操作
private:
//    //タッチイベントコールバックをオーバーライド
//    void registerWithTouchDispatcher();
//
//    bool ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
//    void ccTouchMoved(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
//    void ccTouchEnded(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);


//XTLayer
private:
//TODO: *** マルチタップ処理でアプリが落ちる？(2014/01/20 調査中) ***
//
//    #pragma mark - XTLayer (マルチタップ)
//    virtual void xtTouchesBegan(cocos2d::CCSet* _touches, cocos2d::CCEvent* event);
//    virtual void xtTouchesMoved(cocos2d::CCSet* _touches, cocos2d::CCEvent* event);
//    virtual void xtTouchesEnded(cocos2d::CCSet* _touches, cocos2d::CCEvent* event);

    #pragma mark - XTLayer (シングルタップ)
    virtual void xtTouchesBegan(CCPoint position);
    virtual void xtTouchesMoved(CCPoint position);
    virtual void xtTouchesEnded(CCPoint position);

//    #pragma mark - XTLayer (タップ)
//    virtual void xtTapGesture(CCPoint position);
//    virtual void xtDoubleTapGesture(CCPoint position);
//    virtual void xtLongTapGesture(CCPoint position);
//    virtual void xtSwipeGesture(XTTouchDirection direction, float distance, float speed);
};


//デリゲート (シングルタップ用)
class TouchDelegate
{
public:
    //タップ用
    virtual void changeTouchStatus(InputLayer::TouchStatus status){};

    //移動量
    virtual void changeTouchMoved(cocos2d::CCPoint position){};

    //ゲーム中のタップ操作の通知用
    virtual void touchBegan(cocos2d::CCPoint position){};
    virtual void touchMoved(cocos2d::CCPoint position){};
    virtual void touchEnded(cocos2d::CCPoint position){};
};

#endif
