//
//  PlayerSprite.h
//  Unicycle
//
//  Created by Goodia.Inc on 13/03/01.
//
//

#ifndef __Knock__BGSprite__
#define __Knock__BGSprite__

#include "cocos2d.h"
#include "PrivateConfig.h"

class BGSprite : public cocos2d::CCSprite
{
public:
    static BGSprite * create();

private:
    bool init();
    int _changedCount;
    
    
public:
    void changeBGSpriteType(float duration);
    int getChangedCount(){return _changedCount;}
private:
    
};

#endif /* defined(__Knock__BGSprite__) */
