//
//  NativeInterface.h
//  GameProject
//
//  Created by Tomoaki Shimizu on 12/04/04.
//  Copyright (c) 2012 TKS2. All rights reserved.
//

#ifndef _BROWSER_LAUNCHER_H_
#define _BROWSER_LAUNCHER_H_

#include <stddef.h>

namespace Cocos2dExt {

    class NativeInterface
    {
    public:

//UUID
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        static char * stringWithUUID();
#endif

//GameCenter
        static void leaderBoard();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        static void reportScore(long score, char const * category);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        static void reportScoreAndroid(int score);
        static void reportScoreAndroid(int score, int mode);
#endif

//AppliPromotion
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        static bool isFirstTimeInToday();
#endif

        static void launchUrl(char const *pszUrl);
        static void launchTwitter(char const *tweet);

        static void showAd(bool adFlag);
        static void showHouseAd(bool adFlag);

        static void showGenuineAd(bool adFlag);
        static void genuineAdPreparation();

        static void showReviewPopup();

//アイコン広告
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        static void showIconAd(int scene);
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        static void showIconAd(int scene, bool adFlag);
#endif

        static void vibrate();

        //GameFeat
        static void showGameFeat();

        //AppliPromotion（リスト表示）
        static void showAppliPromotion();

        //AppliPromotion (全面広告対応用)
        static void initAppliPromotionSplash();
        static void showAppliPromotionSplash();

        //Bead (広告ダイアログ表示)
        static void showBeadOptionalAd();

//#if (CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        //Bead (Exit広告 ... Androidのみ)
        static void showBeadExitAd();
#endif


//AID
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        //Aid (広告ダイアログ表示 ... Androidのみ)
        static void showAidOptionalAd();

        //Aid (Exit広告 ... Androidのみ)
        static void showAidExitAd();
#endif


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        //広告ダイアログ配信率設定メソッド ... Androidのみ
        static void showOptionalAd(int adCount);
		static void showOptionalAdWall();
		static void showOptionalAdRank();
        
        //Exit広告配信率設定メソッド ... Androidのみ
        static void showExitAd();
#endif
        
		//i-mobileスプラッシュ広告
		static void showImobileSplashAd();
		
//ZOON
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        //ZOON - EQSSDK (おすすめアプリ) ... iOSのみ
        static void showZoonStart();

		//インターステイシャル広告表示
        static void showAdSplash(int adCount);
        static void showAdSplashAddRanking();

        
#endif

//TapJoy
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        static void getVideoReward();
        static void showVideoReward();
        static bool getIsVideoRewardEnabled();
#endif
//FLURRY
        static void reportGameCountToFlurry(int count);
        static void reportFlurryWithEvent(char const *eventName);

    };
} // end of namespace Cocos2dExt

#endif // _BROWSER_LAUNCHER_H_
