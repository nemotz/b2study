//
//  NativeInterface_java.h
//  GameProject
//
//  Created by Tomoaki Shimizu on 12/04/04.
//  Copyright (c) 2012 TKS2. All rights reserved.
//

#ifndef __BROWSER_LAUNCHER_JNI__
#define __BROWSER_LAUNCHER_JNI__

#include <jni.h>

extern "C"
{
    extern void launchUrlJNI(char const *pszUrl);
    extern void launchTwitterJNI(char const *tweet);

    extern void showAdJNI();
//    extern void showHouseAdJNI();

    // にょきっと出る純広告
    extern void showGenuineAdJNI(bool adFlag);
    extern void genuineAdPreparationJNI();
    
    //Asta
    extern void showIconAdJNI(int scene);

    extern void vibrateJNI();

    //GameFeat
    extern void showGameFeatJNI();

    //AppliPromotion (リスト表示)
    extern void showAppliPromotionJNI();
    
    //AppliPromotion (全面広告対応用)
    extern void initAppliPromotionSplashJNI();
    extern void showAppliPromotionSplashJNI();
	
	// Bead/Aid(広告ダイアログ表示...Androidのみ)
	extern void showOptionalAdJNI(int adCount);
	extern void showOptionalAdWallJNI();
	extern void showOptionalAdRankJNI();
	
	// Bead/Aid(Exitダイアログ表示...Androidのみ)
	extern void showExitAdJNI();

    //Bead (広告ダイアログ表示)
    extern void showBeadOptionalAdJNI();

    //Bead (Exit広告 ... Androidのみ)
    extern void showBeadExitAdJNI();

    //Aid (広告ダイアログ表示 ... Androidのみ)
    extern void showAidOptionalAdJNI();

    //Aid (Exit広告 ... Androidのみ)
    extern void showAidExitAdJNI();


    //Ranking
    extern void rankingLeaderBoardJNI();
    extern void rankingReportScoreJNI(int score, int mode);

	//TapJoy
	extern void getVideoRewardJNI();
	extern void showVideoRewardJNI();
	extern bool getIsVideoRewardEnabledJNI();
	extern void continueToRestart();
	extern JNIEXPORT void JNICALL Java_jp_co_goodia_TemplateProject215_TapJoyActivity_continueToGame   //TODO:APPINFO
	(JNIEnv* env, jobject obj, jboolean b);
	
	//FLURRY
	extern void reportGameCountToFlurryJNI(int count);
	extern void reportFlurryWithEventJNI(char const * eventName);

}
#endif
