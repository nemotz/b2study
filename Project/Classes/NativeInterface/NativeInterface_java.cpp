//
//  NativeInterface_java.cpp
//  GameProject
//
//  Created by Tomoaki Shimizu on 12/04/04.
//  Copyright (c) 2012 TKS2. All rights reserved.
//

#include "NativeInterface_java.h"
#include <android/log.h>
#include "platform/android/jni/JniHelper.h"

#include "HelloWorldScene.h"

#define  LOG_TAG    "NativeInterface"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  CLASS_NAME "jp/co/goodia/TemplateProject215/TemplateProject215"//TODO:APPINFO

typedef struct JniMethodInfo_
{
    JNIEnv *    env;
    jclass      classID;
    jmethodID   methodID;
} JniMethodInfo;

extern "C"
{
	// get env and cache it
	static JNIEnv* getJNIEnv(void)
	{
		JNIEnv *env = 0;

		// get jni environment
		if (cocos2d::JniHelper::getJavaVM()->GetEnv((void**)&env, JNI_VERSION_1_4) != JNI_OK)
		{
			LOGD("Failed to get the environment using GetEnv()");
		}

		if (cocos2d::JniHelper::getJavaVM()->AttachCurrentThread(&env, 0) < 0)
		{
			LOGD("Failed to get the environment using AttachCurrentThread()");
		}

		return env;
	}

	// get class and make it a global reference, release it at endJni().
	static jclass getClassID(JNIEnv *pEnv)
	{
		jclass ret = pEnv->FindClass(CLASS_NAME);
		if (! ret)
		{
			LOGD("Failed to find class of %s", CLASS_NAME);
		}

		return ret;
	}

	static bool getStaticMethodInfo(JniMethodInfo &methodinfo, const char *methodName, const char *paramCode)
	{
		jmethodID methodID = 0;
		JNIEnv *pEnv = 0;
		bool bRet = false;

		do
		{
			pEnv = getJNIEnv();
			if (! pEnv)
			{
				break;
			}

			jclass classID = getClassID(pEnv);

			methodID = pEnv->GetStaticMethodID(classID, methodName, paramCode);
			if (! methodID)
			{
				LOGD("Failed to find static method id of %s", methodName);
				break;
			}

			methodinfo.classID = classID;
			methodinfo.env = pEnv;
			methodinfo.methodID = methodID;

			bRet = true;
		} while (0);

		return bRet;
	}

    //ブラウザ起動
	void launchUrlJNI(char const *pszUrl)
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "launchUrl", "(Ljava/lang/String;)V"))
		{			
			return;
		}

		jstring stringArg = methodInfo.env->NewStringUTF(pszUrl);
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, stringArg);
		methodInfo.env->DeleteLocalRef(stringArg);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

	void addWebViewJNI(char const *pszUrl)
	{
		JniMethodInfo methodInfo;
		if (!getStaticMethodInfo(methodInfo, "addWebView", "(Ljava/lang/String;)V"))
		{
			return;
		}

		jstring stringArg = methodInfo.env->NewStringUTF(pszUrl);
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, stringArg);
		methodInfo.env->DeleteLocalRef(stringArg);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

	void closeWebViewJNI()
	{
		JniMethodInfo methodInfo;
		if (!getStaticMethodInfo(methodInfo, "closeWebView", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

    //Twitter
	void launchTwitterJNI(char const *tweet)
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "launchTwitter", "(Ljava/lang/String;)V"))
		{
			return;
		}

		jstring stringArg = methodInfo.env->NewStringUTF(tweet);
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, stringArg);
		methodInfo.env->DeleteLocalRef(stringArg);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

    //バナー広告
	void showAdJNI()
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showAd", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
    
    
	void showGenuineAdJNI(bool adFlag)
	{
		//bool型が送れなかったので暫定処理
		//adFlag ... とりあえず 0:非表示, 1:表示 (本当は Boolean にしたかったけど)
		int tmpFlg = (adFlag)?(1):(0);
        
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showGenuineAd", "(I)V"))
		{
			return;
		}
        
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, tmpFlg);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
    
    
	void genuineAdPreparationJNI()
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "genuineAdPreparation", "()V"))
		{
			return;
		}
        
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
    
    //バイブ
	void vibrateJNI()
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "vibrate", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

    //Asta（アイコン広告）
    void showIconAdJNI(int scene)
    {
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showIconAd", "(I)V"))
		{
			return;
		}
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, scene);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
        
    }
    
    //GameFeat (リスト表示)
	void showGameFeatJNI()
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showGameFeat", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}


    //AppliPromotion (リスト表示)
    void showAppliPromotionJNI()
    {
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showAppliPromotion", "()V"))
		{
			return;
		}
        
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
        
    }

    //AppliPromotion (スプラッシュ表示)
	void initAppliPromotionSplashJNI()
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "initAppliPromotionSplash", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

	void showAppliPromotionSplashJNI()
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showAppliPromotionSplash", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	
	// Bead/Aid(広告ダイアログ表示...Androidのみ)
	void showOptionalAdJNI(int adCount)
	{
		
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showOptionalAd", "(I)V"))
		{
			return;
		}
		
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, adCount);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	
	void showOptionalAdWallJNI()
	{
		
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showOptionalAdWall", "()V"))
		{
			return;
		}
		
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	
	void showOptionalAdRankJNI()
	{
		
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showOptionalAdRank", "()V"))
		{
			return;
		}
		
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	
	// Bead/Aid(Exitダイアログ表示...Androidのみ)
	void showExitAdJNI()
	{
		
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showExitAd", "()V"))
		{
			return;
		}
		
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

	//Bead (広告ダイアログ表示)
	void showBeadOptionalAdJNI()
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showBeadOptionalAd", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

	//Bead (Exit広告 ... Androidのみ)
	void showBeadExitAdJNI()
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showBeadExitAd", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}


	//Aid (広告ダイアログ表示)
	void showAidOptionalAdJNI()
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showAidOptionalAd", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

	//Aid (Exit広告 ... Androidのみ)
	void showAidExitAdJNI()
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "showAidExitAd", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

    //ランキング
	void rankingLeaderBoardJNI()
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "rankingLeaderBoard", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

	void rankingReportScoreJNI(int score, int mode)
	{
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "rankingReportScore", "(II)V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, score, mode);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

	//TapJoy
	void getVideoRewardJNI()
	{
		LOGD("TapJoy:getVideoRewardJNI");
		// ※ActivityのonCreateメソッドで代用
	}
	
	void showVideoRewardJNI()
	{
		LOGD("TapJoy:showVideoRewardJNI");
		
		JniMethodInfo methodInfo;
		if(!getStaticMethodInfo(methodInfo, "showFullScreenAd", "()V"))
		{
			return;
		}
		
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	
	bool getIsVideoRewardEnabledJNI()
	{
		LOGD("TapJoy:getIsVideoRewardEnabledJNI");
		
		JniMethodInfo methodInfo;
		if(!getStaticMethodInfo(methodInfo, "getMovieEnabled", "()Z"))
		{
			return false;
		}
		
		bool result = methodInfo.env->CallStaticBooleanMethod(methodInfo.classID, methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
		
		LOGD("TapJoy:getMovieEnabled:%d", result);
		
		return result;
	}
	
	JNIEXPORT void JNICALL Java_jp_co_goodia_TemplateProject215_TapJoyActivity_continueToGame    //TODO:APPINFO
	(JNIEnv* env, jobject obj, jboolean b){
		
		LOGD("TapJoy:continueToGame");
		
		HelloWorld::sharedHelloWorldLayer()->rewardMovieEnded(b);
	}
	
	//FLURRY
	void reportGameCountToFlurryJNI(int count){
		
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "reportGameCountToFlurry", "(I)V"))
		{
			return;
		}
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, count);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
	
	void reportFlurryWithEventJNI(char const * eventName){
		
		JniMethodInfo methodInfo;
		if (! getStaticMethodInfo(methodInfo, "reportFlurryWithEvent", "(Ljava/lang/String;)V"))
		{
			return;
		}
		
		jstring stringArg = methodInfo.env->NewStringUTF(eventName);
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID, stringArg);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
}
