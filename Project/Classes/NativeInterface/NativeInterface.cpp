//
//  NativeInterface.cpp
//  TemplateProject
//
//  Created by Tomoaki Shimizu on 12/04/04.
//  Copyright (c) 2012 TKS2. All rights reserved.
//

#include "NativeInterface.h"
#include "NativeInterface_java.h"
#include "PrivateUserDefault.h"

namespace Cocos2dExt
{
    void NativeInterface::launchUrl(char const* pszUrl)
    {
        launchUrlJNI(pszUrl);
    }

    void NativeInterface::launchTwitter(char const* tweet)
    {
        launchTwitterJNI(tweet);
    }


    void NativeInterface::showAd(bool adFlag)
    {
        showAdJNI();
    }

    void NativeInterface::showHouseAd(bool adFlag)
    {
//        showHouseAdJNI();
    }
    
    void NativeInterface::showGenuineAd(bool adFlag)
    {
        showGenuineAdJNI(adFlag);
    }
    
    void NativeInterface::genuineAdPreparation()
    {
        genuineAdPreparationJNI();
    }

    //Asta
    void NativeInterface::showIconAd(int scene)
    {
        showIconAdJNI(scene);
    }

    void NativeInterface::vibrate()
    {
        vibrateJNI();
    }


    //GameFeat
    void NativeInterface::showGameFeat()
    {
        showGameFeatJNI();
    }


    //AppliPromotion（リスト表示）
    void NativeInterface::showAppliPromotion()
    {
        showAppliPromotionJNI();
    }
    
    //AppliPromotion (全面広告対応用)
    void NativeInterface::initAppliPromotionSplash()
    {
        initAppliPromotionSplashJNI();
    }

    //AppliPromotion (全面広告対応用)
    void NativeInterface::showAppliPromotionSplash()
    {
        showAppliPromotionSplashJNI();
    }

    //広告ダイアログ配信率設定
    void NativeInterface::showOptionalAd(int adCount)
    {
		showOptionalAdJNI(adCount);
    }
	
    void NativeInterface::showOptionalAdWall()
    {
		showOptionalAdWallJNI();
    }
	
    void NativeInterface::showOptionalAdRank()
    {
		showOptionalAdRankJNI();
    }

    //Exit広告配信率設定
    void NativeInterface::showExitAd()
    {
		showExitAdJNI();
    }

    //Bead (広告ダイアログ表示)
    void NativeInterface::showBeadOptionalAd()
    {
        showBeadOptionalAdJNI();
    }

    //Bead (Exit広告 ... Androidのみ)
    void NativeInterface::showBeadExitAd()
    {
        showBeadExitAdJNI();
    }

    //Aid (広告ダイアログ表示)
    void NativeInterface::showAidOptionalAd()
    {
        showAidOptionalAdJNI();
    }

    //Aid (Exit広告 ... Androidのみ)
    void NativeInterface::showAidExitAd()
    {
        showAidExitAdJNI();
    }

	//i-mobileスプラッシュ広告
	static void showImobileSplashAd()
	{
		//showImobileSplashAdJNI();
	}

    //Ranking
    void NativeInterface::leaderBoard()
    {
        rankingLeaderBoardJNI();
    }

    //Ranking Score (1)
    void NativeInterface::reportScoreAndroid(int score)
    {
        //RankPlatMode = 1
        NativeInterface::reportScoreAndroid(score, 1);
    }

    //Ranking Score (2)
    void NativeInterface::reportScoreAndroid(int score, int mode)
    {
        rankingReportScoreJNI(score, mode);
    }

    void NativeInterface::showReviewPopup(){
        //showReviewPopupJni();
    }
	
	//TapJoy
    void NativeInterface::getVideoReward(){
        getVideoRewardJNI();
    }
    
    void NativeInterface::showVideoReward(){
        showVideoRewardJNI();
    }
    
    bool NativeInterface::getIsVideoRewardEnabled(){
        bool result = getIsVideoRewardEnabledJNI();
        return result;
    }
	
	//FLURRY
	void NativeInterface::reportGameCountToFlurry(int count){
		reportGameCountToFlurryJNI(count);
	}
	
	void NativeInterface::reportFlurryWithEvent(char const* eventName){
		reportFlurryWithEventJNI(eventName);
	}

}
