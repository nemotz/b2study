
//  AppDelegate.cpp
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#include "AppDelegate.h"

#include "SimpleAudioEngine.h"
#include "cocos2d.h"

#include "PrivateUserDefault.h"
#include "HelloWorldScene.h"
#include "TitleLayer.h"

#include "PrivateConfig.h"
#include "Encryption.h"
#include "NativeInterface.h"


USING_NS_CC;
using namespace CocosDenshion;


AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate()
{
}

bool AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
	CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();
    pDirector->setOpenGLView(pEGLView);

    // turn on display FPS
    pDirector->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);
	
	
	
    CCSize screenSize = CCEGLView::sharedOpenGLView()->getFrameSize();
	CCSize designSize;
	if (screenSize.width < screenSize.height){
		designSize = CCSizeMake(320, 480);
	} else {
		designSize = CCSizeMake(480, 320);
	}
    std::vector<std::string> searchPaths;
	
	if (screenSize.width < screenSize.height){
		if (screenSize.width > 320)
		{
			searchPaths.push_back("iphonehd");
			pDirector->setContentScaleFactor(960.0f/designSize.height);
		}
		else
		{
			searchPaths.push_back("iphone");
			pDirector->setContentScaleFactor(480.0f/designSize.height);
		}
	} else {
		if (screenSize.width > 480)
		{
			searchPaths.push_back("iphonehd");
			pDirector->setContentScaleFactor(640.0f/designSize.height);
		}
		else
		{
			searchPaths.push_back("iphone");
			pDirector->setContentScaleFactor(320.0f/designSize.height);
		}
	}
	
    CCEGLView::sharedOpenGLView()->setDesignResolutionSize(designSize.width, designSize.height, kResolutionFixedWidth);
	CCFileUtils::sharedFileUtils()->setSearchPaths(searchPaths);

	
    // create a scene. it's an autorelease object
    CCScene *pScene = TitleLayer::scene();

    // run
    pDirector->runWithScene(pScene);


    //### サウンド ###
    {
        SimpleAudioEngine * audioEngine = SimpleAudioEngine::sharedEngine();

        //BGM
        {
            const char * fileName [] = {
                "BGM_InGame.mp3",
                "BGM_Result.mp3",
                "BGM_Title.mp3",
            };

            int count = sizeof(fileName)/sizeof(*fileName);
            for (int i = 0; i < count; i++)
            {
                audioEngine->preloadBackgroundMusic(fileName[i]);
            }
        }

        //効果音
        {
            const char * fileName [] = {
                //ボタン音 (共通)
                "SE_Enter.mp3",

                //ゲーム効果音
                "SE_Bomb.mp3",
                "SE_ElectoroShock.mp3",
                "SE_Jump.mp3",
            };

            int count = sizeof(fileName)/sizeof(*fileName);
            for (int i = 0; i < count; i++)
            {
                audioEngine->preloadEffect(fileName[i]);
            }
        }

//        //ボリューム調整
//        audioEngine->setEffectsVolume(0.75f);
    }

    //### スプライトシート ###
    {
        CCSpriteFrameCache * frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();

        const char * fileName [] = {
            //ゲーム用
            "main_gameimage.plist",
            "main_gameimage2.plist",

            //ヘルプ用
            "help_gameimage.plist",
        };

        int count = sizeof(fileName)/sizeof(*fileName);
        for (int i = 0; i < count; i++)
        {
            frameCache->addSpriteFramesWithFile(fileName[i]);
        }
    }


    //UUID
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    {
        //UUIDの取得
        std::string uuid = PrivateUserDefault::getUUID();
        if (uuid.size()<= 0)
        {
            //UUIDの生成
            char * tmpUuid = Cocos2dExt::NativeInterface::stringWithUUID();

            //暗号化
            std::string data = Encryption::xorCalc(ENCRYPTION_XOR_KEY__UUID, tmpUuid);

            PrivateUserDefault::setUUID(tmpUuid);
        }

#ifdef DEBUG
        {
            std::string test1 = PrivateUserDefault::getUUID();
            std::string test2 = Encryption::xorCalc(ENCRYPTION_XOR_KEY__UUID, test1.c_str());

            CCLOG("%s test(読出): %s", __PRETTY_FUNCTION__, test1.c_str());
            CCLOG("%s test(復号): %s", __PRETTY_FUNCTION__, test2.c_str());
        }
#endif
    }
#endif

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    HelloWorld * hello = HelloWorld::sharedHelloWorldLayer();
    if (hello)
    {
//        if (hello->isAccelerometerEnabled() == true) {
//            hello->setAccelerometerEnabled(false);
//        }
   
#ifdef USE_COIN_CONTINUE
        if ((hello->getIsGameStart()) && (!hello->getIsGameOver()) && (!hello->getIsTapJoy()))
#else
		if ((hello->getIsGameStart()) && (!hello->getIsGameOver()))
#endif
        {
            //一時停止処理を実行
            hello->pauseGame();
        }
        CCDirector::sharedDirector()->pause();
    }
    else
    {
        CCDirector::sharedDirector()->pause();
    }

    //一般的な停止処理を実行
    {
        CCDirector::sharedDirector()->stopAnimation();

        // if you use SimpleAudioEngine, it must be paused
        SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
        //SimpleAudioEngine::sharedEngine()->pauseAllEffects();
    }
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    bool blnResume = true;

    //ゲーム中一時停止の場合は、レジュームしない
    HelloWorld * hello = HelloWorld::sharedHelloWorldLayer();
    if (hello)
    {
//        if (hello->isAccelerometerEnabled() == false) {
//            hello->setAccelerometerEnabled(true);
//        }

#ifdef USE_COIN_CONTINUE
        if ((hello->getIsGameStart()) && (!hello->getIsGameOver()) && (!hello->getIsTapJoy()))
#else
		if ((hello->getIsGameStart()) && (!hello->getIsGameOver()))
#endif
        {
//            CCLayer * layer = hello->getPauseLayer();
//            if (layer)
            {
                blnResume = false;
            }
        }
    }

    if (blnResume)
    {
        cocos2d::CCDirector::sharedDirector()->resume();
        CCDirector::sharedDirector()->startAnimation();

        // if you use SimpleAudioEngine, it must resume here
        bool flg = PrivateUserDefault::getSound();
        if (flg)
        {
            //再開
            // if you use SimpleAudioEngine, it must resume here
            CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
            //CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeAllEffects();
        }
    }
    else
    {
        CCDirector::sharedDirector()->pause();
        CCDirector::sharedDirector()->startAnimation();

        //SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
        //SimpleAudioEngine::sharedEngine()->pauseAllEffects();
    }
}
