//
//  NativeInterface_objc.h
//
//  Created by Tomoaki Shimizu on 12/04/04.
//  Copyright (c) 2012 TKS2. All rights reserved.
//

#import "CocosDenshion.h"
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 30000
#import <AVFoundation/AVFoundation.h>
#else
#import "CDXMacOSXSupport.h"
#endif

@interface NativeInterface : NSObject

//UUID
+ (char *) stringWithUUID;


//GameCenter
+ (void)leaderBoard;
+ (void)reportScore:(long)score Category:(NSString *)category;


//GameFeat
+ (void)showGameFeat;

//AppliPromotion  - Wall
+ (void)showAppliPromotion;
+ (bool)isFirstTimeInToday;

//AppliPromotion - Splash
+ (void)initAppliPromotionSplash;
+ (void)showAppliPromotionSplash;


//Bead (広告ダイアログ表示)
+ (void)showBeadOptionalAd;

//i-mobileスプラッシュ広告
+ (void)showImobileSplashAd;

//ZOON (おすすめアプリ)
+ (void)showZoonStart;

//Aid（スプラッシュ広告）
+ (void) showAid;

//Goodia More App
+ (void)launchUrl:(NSString *)pszUrl;

//Twitter
+ (void)launchTwitter:(NSString *)tweet;

//Ad
+ (void)showHouseAd:(BOOL)adFlag;
+ (void)showGenuineAd:(BOOL)adFlag;
+ (void)genuineAdPreparation;
+ (void)showAd:(BOOL)adFlag;
+ (void)showIconAd:(int)scene flag:(BOOL)adFlag;

+ (void)showAdSplash:(int)adCount;
+ (void)showAdSplashAddWall;
+ (void)showAdSplashAddRanking;

//Review
+ (void)showReviewPopup;

//Tapjoy
+ (void)getVideoReward;
+ (void)showVideoReward;
+ (bool)getIsVideoRewardEnabled;

//FLURRY
+ (void)reportGameCountToFlurry:(int)count;
+ (void)reportFlurryWithEvent:(NSString *)eventName;

+ (void)vibrate;

@end
