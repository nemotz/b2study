//
//  RootViewController.h
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//

#import "RootViewController.h"


//縦向きの場合は定義(横向きはコメント化)
#define APP_ORIENTATION_IS_PORTRAIT


@implementation RootViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


// Override to allow orientations other than the default portrait orientation.
// This method is deprecated on ios6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
#ifdef APP_ORIENTATION_IS_PORTRAIT
    //縦向き
    return UIInterfaceOrientationIsPortrait( interfaceOrientation );
#else
    //横向き
    return UIInterfaceOrientationIsLandscape( interfaceOrientation );
#endif
}


// For ios6, use supportedInterfaceOrientations & shouldAutorotate instead
- (NSUInteger) supportedInterfaceOrientations
{
#ifdef __IPHONE_6_0
    #ifdef APP_ORIENTATION_IS_PORTRAIT
        //縦向き
        return UIInterfaceOrientationMaskPortrait;
    #else
        //横向き
        return UIInterfaceOrientationMaskLandscape;
    #endif
#endif
}


- (BOOL) shouldAutorotate
{
    return YES;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc
{
    [super dealloc];
}

@end
