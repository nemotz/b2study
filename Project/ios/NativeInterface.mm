//
//  NativeInterface.mm
//  TemplateProject
//
//  Created by Tomoaki Shimizu on 12/04/04.
//  Copyright (c) 2012 TKS2. All rights reserved.
//

#include "NativeInterface.h"
#include "NativeInterface_objc.h"
#include "ClearLayer.h"

static char * static_StringWithUUID()
{
    return [NativeInterface stringWithUUID];
}


static void static_LeaderBoard()
{
    [NativeInterface leaderBoard];
}

static void static_ReportScore(long score, char const * category)
{
    [NativeInterface reportScore:score Category:[NSString stringWithUTF8String:category]];
}


static void static_LaunchUrl(const char* pszUrl)
{
    [NativeInterface launchUrl:[NSString stringWithUTF8String: pszUrl]];
}

static void static_LaunchTwitter(char const* tweet)
{
    [NativeInterface launchTwitter:[NSString stringWithUTF8String:tweet]];
}


static void static_showAd(bool adFlag)
{
    [NativeInterface showAd:adFlag];
}

static void static_showHouseAd(bool adFlag)
{
    [NativeInterface showHouseAd:adFlag];
}

static void static_showGenuineAd(bool adFlag)
{
    [NativeInterface showGenuineAd:adFlag];
}

static void static_genuineAdPreparation()
{
    [NativeInterface genuineAdPreparation];
}

static void static_showIconAd(int scene, bool adFlag)
{
    [NativeInterface showIconAd:scene flag:adFlag];
}

static void static_vibrate()
{
    [NativeInterface vibrate];
}


static void static_showAppliPromotion()
{
    [NativeInterface showAppliPromotion];
}

static void static_showGameFeat()
{
    [NativeInterface showGameFeat];
}

static bool static_isFirstTimeInToday()
{
    return [NativeInterface isFirstTimeInToday];
}

static void static_initAppliPromotionSplash()
{
    [NativeInterface initAppliPromotionSplash];
}

static void static_showAppliPromotionSplash()
{
    [NativeInterface showAppliPromotionSplash];
}

static void static_showBeadOptionalAd()
{
    [NativeInterface showBeadOptionalAd];
}

//i-mobileスプラッシュ広告
static void static_showImobileSplashAd()
{
    [NativeInterface showImobileSplashAd];
}

static void static_showZoonStart()
{
    [NativeInterface showZoonStart];
}

static void static_showAdSplash(int adCount)
{
    [NativeInterface showAdSplash:adCount];
}
static void static_showAdSplashAddRanking(){
    [NativeInterface showAdSplashAddRanking];
}

//Review
static void static_showReviewPopup(){
    [NativeInterface showReviewPopup];
}

//Tapjoy
static void static_getVideoReward(){
    [NativeInterface getVideoReward];
}

static void static_showVideoReward(){
    [NativeInterface showVideoReward];
}

static bool static_getIsVideoRewardEnabled(){
    return [NativeInterface getIsVideoRewardEnabled];
}

//FLURRY
static void static_reportGameCountToFlurry(int count){
    [NativeInterface reportGameCountToFlurry:count];
}
static void static_reportFlurryWithEvent(char const * eventName){
    [NativeInterface reportFlurryWithEvent:[NSString stringWithUTF8String:eventName]];
}


namespace Cocos2dExt
{
    //UUID
    char * NativeInterface::stringWithUUID()
    {
        char * value = static_StringWithUUID();
        return value;
    }

    //GameCenter
    void NativeInterface::leaderBoard()
    {
        static_LeaderBoard();
    }

    //GameCenter
    void NativeInterface::reportScore(long score, const char *category)
    {
        static_ReportScore(score, category);
    }


    void NativeInterface::launchUrl(const char* pszUrl)
    {
        static_LaunchUrl(pszUrl);
    }

    void NativeInterface::launchTwitter(char const* tweet)
    {
        static_LaunchTwitter(tweet);
    }


    void NativeInterface::showAd(bool adFlag)
    {
        static_showAd(adFlag);
    }

    void NativeInterface::showHouseAd(bool adFlag)
    {
        static_showHouseAd(adFlag);
    }
    
    void NativeInterface::showGenuineAd(bool adFlag)
    {
        static_showGenuineAd(adFlag);
    }
    
    void NativeInterface::genuineAdPreparation()
    {
        static_genuineAdPreparation();
    }
    
    void NativeInterface::showIconAd(int scene, bool adFlag)
    {
        static_showIconAd(scene, adFlag);
    }
    
    void NativeInterface::vibrate()
    {
        static_vibrate();
    }

    //GameFeat
    void NativeInterface::showGameFeat()
    {
        static_showGameFeat();
    }

    //AppliPromotion
    void NativeInterface::showAppliPromotion()
    {
        static_showAppliPromotion();
    }
    
    bool NativeInterface::isFirstTimeInToday()
    {
        return static_isFirstTimeInToday();
    }

    void NativeInterface::initAppliPromotionSplash()
    {
        static_initAppliPromotionSplash();
    }

    void NativeInterface::showAppliPromotionSplash()
    {
        static_showAppliPromotionSplash();
    }


    //Bead (広告ダイアログ表示)
    void NativeInterface::showBeadOptionalAd()
    {
        static_showBeadOptionalAd();
    }

	//i-mobileスプラッシュ広告
	void NativeInterface::showImobileSplashAd()
	{
        static_showImobileSplashAd();
	}

    //ZOON - EQSSDK (おすすめアプリ)
    void NativeInterface::showZoonStart()
    {
        static_showZoonStart();
    }
    
    void NativeInterface::showAdSplash(int adCount)
    {
        static_showAdSplash(adCount);
    }
    void NativeInterface::showAdSplashAddRanking()    {
        static_showAdSplashAddRanking();
    }
    
    //Review
    void NativeInterface::showReviewPopup()
    {
        static_showReviewPopup();
    }
    
    //Tapjoy
    void NativeInterface::getVideoReward()
    {
        static_getVideoReward();
    }
    
    void NativeInterface::showVideoReward()
    {
        static_showVideoReward();
    }
    
    bool NativeInterface::getIsVideoRewardEnabled()
    {
        return static_getIsVideoRewardEnabled();
    }
    
    //FLURRY
    void NativeInterface::reportGameCountToFlurry(int count)
    {
        static_reportGameCountToFlurry(count);
    }
    void NativeInterface::reportFlurryWithEvent(const char *eventName)
    {
        static_reportFlurryWithEvent(eventName);
    }
}
