//
//  AppInfoViewController.m
//  Oshiri
//
//  Created by Masato Fukano on 12/05/28.
//  Copyright (c) 2012年 Goodia Inc. All rights reserved.
//

#import "AppInfoViewController.h"

@implementation AppInfoViewController

@synthesize url = _url;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        _url = [[NSString alloc] initWithString:@"http://goodiaappcan.appspot.com/appinfo/appinfo.html"];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
}

- (void)viewDidUnload
{
    [myWebView release];
    myWebView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations

    //縦向き
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);

    //横向き
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) ||
            (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
}

- (IBAction)closeView:(id)sender {
    [[UIApplication sharedApplication].keyWindow.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)dealloc {
    [myWebView release];
    [_url release], _url = nil;
    [super dealloc];
}
@end
