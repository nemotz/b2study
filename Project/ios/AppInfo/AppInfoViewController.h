//
//  AppInfoViewController.h
//  Oshiri
//
//  Created by Masato Fukano on 12/05/28.
//  Copyright (c) 2012年 Goodia Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppInfoViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *myWebView;
}

- (IBAction)closeView:(id)sender;

@property (nonatomic, retain) NSString * url;

@end
