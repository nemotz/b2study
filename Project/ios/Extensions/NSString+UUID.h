//
//  NSString+UUID.h
//  colors
//
//  Created by masaya takaki on 1/24/13.
//  Copyright (c) 2013 littlenstar.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UUID)

+ (NSString*) UUID;

@end
