//
//  NSString+Encryotion.h
//  colors
//
//  Created by masaya takaki on 1/24/13.
//  Copyright (c) 2013 littlenstar.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encryotion)
+ (NSString *) stringWidthEncryptedData:(NSData *) data;
- (NSData *) encryptedString;
@end
