//
//  NativeInterface_objc.m
//
//  Created by Tomoaki Shimizu on 12/04/04.
//  Copyright (c) 2012 TKS2. All rights reserved.
//

#import "NativeInterface_objc.h"
#import "AppInfoViewController.h"
#import "EAGLView.h"
#import <Twitter/TWTweetComposeViewController.h>
#import "HouseAdViewController.h"

#import "GameCenter.h"

#import "HouseAdConfig.h"
#import "Bead.h"
#import "EQSSDK.h"
#import <GameFeatKit/GFView.h>
#import <GameFeatKit/GFController.h>
#import <AidAd/AidAd.h>
#import "AMoAdSDK.h"  //Applipromotion
#import "NativeInterface.h"
#import "Flurry.h"
#import "ImobileSdkAds/ImobileSdkAds.h"

@implementation NativeInterface


#pragma mark - CreateUUID
+ (char *) stringWithUUID
{
    CFUUIDRef uuidObj = CFUUIDCreate(nil);//create a new UUID

    //get the string representation of the UUID
    NSString * uuidString = (NSString *)CFUUIDCreateString(nil, uuidObj);
    CFRelease(uuidObj);

    [uuidString autorelease];


    char * value = (char *)[uuidString UTF8String];

#ifdef DEBUG
    NSLog(@"%s UUID:%s", __func__, value);
#endif

    return value;
}


//static UIWebView *webview = nil;

#pragma mark - GameCenter
+ (void)leaderBoard
{
    [[GameCenter sharedGameCenter] showLeaderboard];
}

+ (void)reportScore:(long)score Category:(NSString *)category
{
    [[GameCenter sharedGameCenter] reportScore:score Category:category];
}


#pragma mark - Goodia More App
+ (void)launchUrl:(NSString *)pszUrl
{
    //Goodia WebView
    {
        AppInfoViewController *controller = [[AppInfoViewController alloc] initWithNibName:@"AppInfoViewController" bundle:[NSBundle mainBundle]];

		[[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:controller animated:YES completion:nil];
        [controller release];
    }
}


#pragma mark - Twitter
+ (void)launchTwitter:(NSString*)tweet
{
    UIDevice *device = [UIDevice currentDevice];
    float version_ = [device.systemVersion floatValue];

    if (version_ < 5.0)
    {
        [self showAlert:@"" text:NSLocalizedString(@"You can not tweet.", @"")];
        return;
    }

    //投稿文 フォーマット
    TWTweetComposeViewController * viewController = [[TWTweetComposeViewController alloc] init];

    [viewController setInitialText:tweet];

    viewController.completionHandler = ^(TWTweetComposeViewControllerResult res) {
        if (res == TWTweetComposeViewControllerResultCancelled) {
            NSLog(@"キャンセル");
        }
        else if (res == TWTweetComposeViewControllerResultDone) {
            NSLog(@"成功");
        }
        [[UIApplication sharedApplication].keyWindow.rootViewController dismissViewControllerAnimated:YES completion:nil];
    };

    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:viewController animated:YES completion:nil];

    [viewController release];
}

//アラートの表示
+ (void)showAlert:(NSString*)title text:(NSString*)text
{
    UIAlertView* alert=[[[UIAlertView alloc]
                         initWithTitle:title message:text delegate:nil
                         cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    [alert show];
}


#pragma mark - Ad
+ (void)showHouseAd:(BOOL)adFlag
{
    HouseAdViewController * view = (HouseAdViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if ([view respondsToSelector:@selector(showHouseAd:)])
    {
        [view showHouseAd:adFlag];
    }
}

+ (void)showGenuineAd:(BOOL)adFlag
{
    HouseAdViewController * view = (HouseAdViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if ([view respondsToSelector:@selector(showGenuineAd:)])
    {
        [view showGenuineAd:adFlag];
    }
}

+ (void)genuineAdPreparation
{
    HouseAdViewController * view = (HouseAdViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if ([view respondsToSelector:@selector(genuineAdPreparation)])
    {
        [view genuineAdPreparation];
    }
}
+ (void)showAd:(BOOL)adFlag
{
    HouseAdViewController * view = (HouseAdViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if ([view respondsToSelector:@selector(showAd:)])
    {
        [view showAd:adFlag];
    }
}

+ (void)showIconAd:(int)scene flag:(BOOL)adFlag
{
    HouseAdViewController * view = (HouseAdViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if ([view respondsToSelector:@selector(showIconAd:flag:)])
    {
        [view showIconAd:scene flag:adFlag];
    }
}

#pragma mark - バイブレータ
+ (void)vibrate
{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

#pragma mark - GameFeat
+ (void)showGameFeat
{
    HouseAdViewController * view = (HouseAdViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if ([view respondsToSelector:@selector(showGameFeat)])
    {
        [view showGameFeat];
    }
	
	// splash重ねてだす
	[self showAdSplashAddWall];
}


#pragma mark - AppliPromotion
+ (void)showAppliPromotion
{
    [AMoAdSDK sendUUID];
    
	if ([APP_ORIENTATION isEqualToString:@"LANDSCAPE"]) {
		[AMoAdSDK
		 showAppliPromotionWall:[UIApplication sharedApplication].keyWindow.rootViewController
		 orientation:UIInterfaceOrientationLandscapeRight];
	} else {
		[AMoAdSDK
		 showAppliPromotionWall:[UIApplication sharedApplication].keyWindow.rootViewController
		 orientation:UIInterfaceOrientationPortrait];
	}
	
	// splash重ねてだす
	[self showAdSplashAddWall];

}

+ (bool)isFirstTimeInToday
{
//    NSLog(@"AppliPromotion:%@",([AppliPromotionSDK isFirstTimeInToday]?@"YES":@"NO"));
    return [AMoAdSDK isFirstTimeInToday];
}

+ (void)initAppliPromotionSplash
{
    [AMoAdSDK loadInterstitial:[UIApplication sharedApplication].keyWindow.rootViewController];
    NSLog(@"initAppliPromotionSplash:");
}

+ (void)showAppliPromotionSplash
{
    [AMoAdSDK showInterstitial:[UIApplication sharedApplication].keyWindow.rootViewController forced:YES];
    NSLog(@"showAppliPromotionSplash:");
}


#pragma mark - Bead
+ (void)showBeadOptionalAd
{
    if ([APP_ORIENTATION isEqualToString:@"LANDSCAPE"]) {
        [[Bead sharedInstance] showWithSIDandOrientation:BEAD_SID orientation:1000] ;
    }
    else{
        [[Bead sharedInstance] showWithSIDandOrientation:BEAD_SID orientation:1];
    }
}

#pragma mark - i-mobile
+ (void)showImobileSplashAd
{
	[ImobileSdkAds showBySpotID:kAdIMoblieSpotIDSplash];
}

#pragma mark - ZOON
+ (void)showZoonStart
{
    UIViewController * rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [EQSSDK start:rootViewController mid:ZOON_MEDIA_ID];
	
    // splash重ねてだす
	[self showAdSplashAddWall];
}


#pragma mark - Aid
+ (void) showAid
{
    [[AidAd agentForMedia:AID_MEDIA_CODE]showDialog];
}

#pragma mark - Review
//Review
+ (void)showReviewPopup
{
    HouseAdViewController * view = (HouseAdViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if ([view respondsToSelector:@selector(showReviewPopup)])
    {
        [view showReviewPopup];
    }
}

#pragma mark Tapjoy
+ (void)getVideoReward
{
    HouseAdViewController * view = (HouseAdViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if ([view respondsToSelector:@selector(getVideoReward)])
    {
        return [view getVideoReward];
    }
}

+ (void)showVideoReward
{
    HouseAdViewController * view = (HouseAdViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if ([view respondsToSelector:@selector(showVideoReward)])
    {
        return [view showVideoReward];
    }
}

+ (bool)getIsVideoRewardEnabled
{
    BOOL isVideoRewardEnabled = NO;
    HouseAdViewController * view = (HouseAdViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if ([view respondsToSelector:@selector(getIsVideoRewardEnabled)])
    {
        
        isVideoRewardEnabled = [view getIsVideoRewardEnabled];
    }
    return isVideoRewardEnabled;
}

//FLURRY
+ (void)reportGameCountToFlurry:(int)count{
    if (count <= 10) {
        [Flurry logEvent:[NSString stringWithFormat:@"PLAY_COUNT:%d",count]];
    }
    else if(count <= 100 && count % 10 == 0){
        [Flurry logEvent:[NSString stringWithFormat:@"PLAY_COUNT:%d",count]];
    }
    else if(count <= 1000 && count % 100 == 0){
        [Flurry logEvent:[NSString stringWithFormat:@"PLAY_COUNT:%d",count]];
    }
    else if(count <= 10000 && count % 1000 == 0){
        [Flurry logEvent:[NSString stringWithFormat:@"PLAY_COUNT:%d",count]];
    }
    else if(count <= 100000 && count % 10000 == 0){
        [Flurry logEvent:[NSString stringWithFormat:@"PLAY_COUNT:%d",count]];
    }
}

+(void)reportFlurryWithEvent:(NSString *)eventName{
    [Flurry logEvent:eventName];
}

#pragma mark - AdSplash
+ (void)showAdSplash:(int)adCount//どこかで強制的に表示させたい場合はadCount = 0
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    //スプラッシュ広告を表示するかどうかをGAEの設定を元に判定する
    int splashAdFrequency = [userDefaults integerForKey:@"AdSplashFrequency"];
    if (splashAdFrequency < 1 && adCount != 0) {    //GAEの設定で未設定だったら3が入っているはず。0だったら表示しない
        return;
    }
    if (adCount > 0) {
        if (adCount % splashAdFrequency != 0) {     //GAEの設定で割った余りが0じゃなかったら表示しない
            return;
        }
    }
    
    //int adIndex = [userDefaults integerForKey:@"AdSplashIndex"];
    //int answerNum;
    NSString *splashType = [userDefaults stringForKey:@"AdSplashType"];
    
    NSLog(@"splashType:%@", splashType);
    
    if ([splashType isEqualToString:@"A"]) {
        NSLog(@"showAdSplash:A");
        [self showAppliPromotionSplash];
    }
    else if ([splashType isEqualToString:@"B"]) {
        NSLog(@"showAdSplash:B");
		//BEAD 100%
		[self showBeadOptionalAd];
    }
    else if ([splashType isEqualToString:@"I"]){
        NSLog(@"showAdSplash:I");
        //iMobile
        [self showImobileSplashAd];
    }
    else{
        return;
    }
    
//    
//    else if ([splashType isEqualToString:@"BAI"]) {
//        NSLog(@"showAdSplash:BAI");
//        
//        int index = (int)arc4random_uniform(2);
//        NSLog(@"index:%d",index);
//        
//        switch (index)
//        {
//            case 0:
//                //BEAD
//                [self showBeadOptionalAd];
//                break;
//            default:
//                //Aid
//                [self showAid];
//                break;
//        }
//        
//    } else if ([splashType isEqualToString:@"AB"]) {
//        
//        if (adIndex > 0) {
//            switch (adIndex) {
//                case 1:
//                    answerNum = 2;
//                    [self showAppliPromotionSplash];
//                    break;
//                default:
//                    answerNum = 1;
//                    [self showBeadOptionalAd];
//                    break;
//            }
//        } else {
//            answerNum = 1;
//        }
//    } else if ([splashType isEqualToString:@"AI"]) {
//        NSLog(@"showAdSplash:AI");
//        //Aid 100%
//        [self showAid];
//    } else {
//        
//    }
//    
//    [userDefaults setInteger:answerNum forKey:@"AdSplashIndex"];
//    [userDefaults synchronize];
//    
//    NSLog(@"showAdSplash:");
}

#pragma mark - AdSplash
+ (void)showAdSplashAddWall//どこかで強制的に表示させたい場合はadCount = 0
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    //スプラッシュ広告を表示するかどうかをGAEの設定を元に判定する
    int splashAdFrequency = [userDefaults integerForKey:@"AdSplashFrequency"];
    if (splashAdFrequency == 0) {    //GAEの設定で未設定だったら3が入っているはず。0だったら表示しない
        return;
    }
    
    NSString *splashType = [userDefaults stringForKey:@"AdSplashTypeWall"];
    
    NSLog(@"AdSplashTypeWall:%@", splashType);
    
    if ([splashType isEqualToString:@"A"]) {
        NSLog(@"AdSplashTypeWall:A");
        [self showAppliPromotionSplash];
    }
    else if ([splashType isEqualToString:@"B"]) {
        NSLog(@"AdSplashTypeWall:B");
		//BEAD 100%
		[self showBeadOptionalAd];
    }
    else if ([splashType isEqualToString:@"I"]){
        NSLog(@"AdSplashTypeWall:I");
        //iMobile
        [self showImobileSplashAd];
    }
    else{
        return;
    }
}

+ (void)showAdSplashAddRanking{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    //スプラッシュ広告を表示するかどうかをGAEの設定を元に判定する
    int splashAdFrequency = [userDefaults integerForKey:@"AdSplashFrequency"];
    if (splashAdFrequency == 0) {    //GAEの設定で未設定だったら3が入っているはず。0だったら表示しない
        return;
    }
    
    NSString *splashType = [userDefaults stringForKey:@"AdSplashTypeRank"];
    
    NSLog(@"AdSplashTypeRank:%@", splashType);
    
    if ([splashType isEqualToString:@"A"]) {
        NSLog(@"AdSplashTypeRank:A");
        [self showAppliPromotionSplash];
    }
    else if ([splashType isEqualToString:@"B"]) {
        NSLog(@"AdSplashTypeRank:B");
		//BEAD 100%
		[self showBeadOptionalAd];
    }
    else if ([splashType isEqualToString:@"I"]){
        NSLog(@"AdSplashTypeRank:I");
        //iMobile
        [self showImobileSplashAd];
    }
    else{
        return;
    }
}

@end
