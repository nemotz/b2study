//
//  GoodiaUserInfo.m
//  9COLORS
//
//  Created by masaya takaki on 1/25/13.
//  Copyright (c) 2013 littlenstar.com. All rights reserved.
//

#import "GoodiaUserInfo.h"

#import "NSString+UUID.h"
#import "NSString+Encryotion.h"

typedef void (^GoodiaUserInfoeConfirmNumberOfItemPaymentsBlock)(NSUInteger numberOfItemPayments, NSError *error);
typedef void (^GoodiaUserInfoeConfirmWhichAppDriverAvailableBlock)(BOOL isAvailable, NSError *error);
typedef void (^GoodiaUserInfoeConfirmWhichNewAppAvailableBlock)(NSDictionary *dict, NSError *error);


@interface GoodiaUserInfo ()
@property (nonatomic, copy) NSString *userUUID;
@property (nonatomic, copy) NSDictionary *itemStocks;

- (void) confirmNumberOfItemPaymentsInBackgroundForItemIdentifier:(NSDictionary *) dict;

@end

@implementation GoodiaUserInfo

@synthesize goodiaAPIReachability = _goodiaAPIReachability;
@synthesize goodiaFmsReachability = _goodiaFmsReachability;


- (Reachability *) goodiaAPIReachability
{
    if (_goodiaAPIReachability == nil) {
        _goodiaAPIReachability = [[Reachability reachabilityWithHostName:self.goodiaAPIHost] retain];
    }

    return _goodiaAPIReachability;
}


- (Reachability *) goodiaFmsReachability
{
    if (_goodiaFmsReachability == nil) {
        _goodiaFmsReachability = [[Reachability reachabilityWithHostName:self.goodiaFmsHost] retain];
    }

    return _goodiaFmsReachability;
}


- (void)encodeWithCoder:(NSCoder*)coder
{
    NSData *encryptedUserIDData = [self.userUUID encryptedString];
    [coder encodeObject:encryptedUserIDData forKey:USER_UUID_KEY];

    NSMutableDictionary *encodedItems = [NSMutableDictionary dictionary];
    for (NSString *key in self.itemStocks) {
        NSNumber *value = [self.itemStocks objectForKey:key];;

        if ([value boolValue]) {
            NSData *encodedValue = [key encryptedString];
            [encodedItems setObject:encodedValue forKey:key];
        }
    }
    [coder encodeObject:encodedItems forKey:ITEMS_KEY];
}


- (id)initWithCoder:(NSCoder*)decoder
{
    self = [super init];

    NSData *encryptedUserIDData = [decoder decodeObjectForKey:USER_UUID_KEY];
    NSString *userID = [NSString stringWidthEncryptedData:encryptedUserIDData];
    self.userUUID = userID;

    // アイテム
    NSDictionary *encodedItems = [decoder decodeObjectForKey:ITEMS_KEY];
    NSMutableDictionary *itemStocks = [NSMutableDictionary dictionary];
    for (NSString *key in encodedItems) {
        NSData *encodedValue = [encodedItems objectForKey:key];
        NSString *valueString = [NSString stringWidthEncryptedData:encodedValue];
        NSNumber *value = nil;
        if ([key isEqualToString:valueString]) {
            value = [NSNumber numberWithBool:YES];
        }
        else {
            value = [NSNumber numberWithBool:NO];
        }

        [itemStocks setObject:value forKey:key];
    }
    self.itemStocks = itemStocks;

    return self;
}


- (NSString *) goodiaAPIHost
{
#ifdef DEBUG
    NSString *goodiaAPIHost = @"1.goodiaaddev.appspot.com";
#else
    NSString *goodiaAPIHost = @"1.goodiaappdriver.appspot.com";
#endif

    return goodiaAPIHost;
}


- (NSString *) goodiaFmsHost
{
    return @"goodiafms.appspot.com";
}


+ (NSString *) applicationPrefix
{
    return APP_ID;
}


+ (GoodiaUserInfo *) goodiaUserInfo
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *userInfoData = [userDefaults objectForKey:USER_INFO_KEY];

    GoodiaUserInfo *goodiaUserInfo = nil;
    if (userInfoData != nil) {
        goodiaUserInfo = [NSKeyedUnarchiver unarchiveObjectWithData:userInfoData];
    }
    else {
        goodiaUserInfo = [[[GoodiaUserInfo alloc] init] autorelease];

        // UUIDの決定
        NSString *userUUID = [NSString UUID];
        // アプリケーションのPrefix
        NSString *appPrefix = [[self class] applicationPrefix];
        if (appPrefix != nil) {
            userUUID = [NSString stringWithFormat:@"%@%@", appPrefix, userUUID];
        }
        goodiaUserInfo.userUUID = userUUID;


        // アイテムはカラの状態なので何もしない

        // 保存
        [goodiaUserInfo save];
    }

    return goodiaUserInfo;
}


- (void) save
{
    NSData *userInfoData = [NSKeyedArchiver archivedDataWithRootObject:self];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:userInfoData forKey:USER_INFO_KEY];

    [userDefaults synchronize];
}


- (NSUInteger) numberOfItemPaymentsForItemIdentifier:(NSString *) itemIdentifier isConfirmed:(BOOL *)isConfirmed
{
    NSNumber *itemStockNumber = [self.itemStocks objectForKey:itemIdentifier];

    if (itemStockNumber == nil && isConfirmed != NULL) {
        *isConfirmed = NO;
    }

    return [itemStockNumber unsignedIntegerValue];
}


- (void) confirmNumberOfItemPaymentsForItemIdentifier:(NSString *) itemIdentifier
                                              handler:(void (^)(NSUInteger numberOfItemPayments, NSError *error))handler;
{
    // Reachabilityのチェック
    NetworkStatus status = [self.goodiaAPIReachability currentReachabilityStatus];
    if (status == NotReachable) {
        // ネットワークに繋がっていない
        NSError *error = [NSError errorWithDomain:@"local" code:0 userInfo:@{@"message" : @"APIにアクセスできません。"}];
        handler(0, error);
        return;
    }

    NSDictionary *dict = @{
    @"itemID" : itemIdentifier,
    @"handler" : [[handler copy] autorelease]
    };
    
    [self performSelectorInBackground:@selector(confirmNumberOfItemPaymentsInBackgroundForItemIdentifier:)
                           withObject:dict];
}


- (void) confirmNumberOfItemPaymentsInBackgroundForItemIdentifier:(NSDictionary *) dict
{
    NSString *itemIdentifier = [dict objectForKey:@"itemID"];
    GoodiaUserInfoeConfirmNumberOfItemPaymentsBlock handler = [dict objectForKey:@"handler"];

    NSString *host = self.goodiaAPIHost;
    NSString *userID = self.userUUID;
    //NSString *itemID = itemIdentifier;
    NSString *itemID = @"XXX";

//    NSString *urlString = [NSString stringWithFormat:@"http://%@/v1/is_active/%@/%@", host, userID, itemID];
//    NSString *urlString = [NSString stringWithFormat:@"http://%@/v1/point_count/%@", host, userID];
    NSString *urlString = [NSString stringWithFormat:@"http://%@/v1/list_of_item/%@", host, userID];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

    NSNumber *numberOfItems = nil;
    if (error == nil) {
    //    // 「is_active」or「point_count」の場合
    //    NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //    NSNumber *numberOfItems = [NSNumber numberWithInt:[text intValue]];
        // 「list_of_item」の場合
        NSError *jsonError = nil;
        NSArray *listOfItems = [NSJSONSerialization JSONObjectWithData:data
                                                               options:0
                                                                 error:&jsonError];
        NSUInteger count = 0;
        for (NSString *identifier in listOfItems) {
            if ([identifier isEqualToString:itemID]) {
                ++count;
            }
        }
        numberOfItems = [NSNumber numberWithInteger:count];
    }

    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result setObject:itemIdentifier forKey:@"itemID"];
    [result setObject:[[handler copy] autorelease] forKey:@"handler"];
    if (numberOfItems != nil) {
        [result setObject:numberOfItems forKey:@"numberOfItems"];
    }
    if (error != nil) {
        [result setObject:error forKey:@"error"];

    };

    [self performSelectorOnMainThread:@selector(confirmNumberOfItemPaymentsCompletedWithResult:)
                           withObject:result
                        waitUntilDone:NO];
}


- (void) confirmNumberOfItemPaymentsCompletedWithResult:(NSDictionary *) result
{
    NSString *itemIdentifier = [result objectForKey:@"itemID"];
    GoodiaUserInfoeConfirmNumberOfItemPaymentsBlock handler = [result objectForKey:@"handler"];
    NSNumber *numberOfItems = [result objectForKey:@"numberOfItems"];
    NSError *error = [result objectForKey:@"error"];

    // 自身の数字を更新する
    if (numberOfItems != nil) {
        NSMutableDictionary *newItemsStocks = [NSMutableDictionary dictionaryWithDictionary:self.itemStocks];
        [newItemsStocks setObject:numberOfItems forKey:itemIdentifier];
        self.itemStocks = newItemsStocks;
        [self save];
    }

    // 通知
    NSUInteger value = numberOfItems != nil ? [numberOfItems unsignedIntegerValue] : 0;
    handler(value, error);
}


// Rewardが有効かどうか確認
- (void) confirmWhichAppDriverAvailableWithHandler:(void (^)(BOOL isAvailable, NSError *error))handler
{
    // Reachabilityのチェック
    NetworkStatus status = [self.goodiaFmsReachability currentReachabilityStatus];
    if (status == NotReachable) {
        // ネットワークに繋がっていない
        NSError *error = [NSError errorWithDomain:@"local" code:0 userInfo:@{@"message" : @"APIにアクセスできません。"}];
        handler(0, error);
        return;
    }

    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(aQueue, ^{
        // 確認URL
        NSString *host = self.goodiaFmsHost;
        NSString *appID = APP_ID;
        NSString *flagID = RWD_ID;

        NSString *urlString = [NSString stringWithFormat:@"http://%@/v1/value_of/%@/%@", host, appID, flagID];

        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];

        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

        if (error) {
            return;
        }

        NSString *text = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        __block NSInteger value = [text integerValue];
        NSLog(@"valeu : %d", value);

        // メインスレッドで通知
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^{
            if (handler != nil) {
                handler((value == 1), error);  // 返り値から値を取得
            }
        });
    });
}


- (void) confirmWhichNewAppAvailableWithHandler:(void (^)(NSDictionary *dict, NSError *error))handler
{
    // Reachabilityのチェック
    NetworkStatus status = [self.goodiaFmsReachability currentReachabilityStatus];
    if (status == NotReachable) {
        // ネットワークに繋がっていない
        NSError *error = [NSError errorWithDomain:@"local" code:0 userInfo:@{@"message" : @"APIにアクセスできません。"}];
        handler(0, error);
        return;
    }

    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(aQueue, ^{
        // 確認URL
        NSString *host = self.goodiaFmsHost;
        NSString *appID = APP_ID;
        NSString *flagID = POP_ID;

        NSString *urlString = [NSString stringWithFormat:@"http://%@/v1/value_of/%@/%@", host, appID, flagID];

        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];

        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

        if (error) {
            return;
        }

        NSError *json_error;
        __block NSDictionary *json = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&json_error] retain];
        
        // メインスレッドで通知
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^{
            if (handler != nil) {
                handler(json, error);  // 返り値から値を取得
            }
            [json release], json = nil;
        });
    });
}


- (void) confirmWhichGenuineAdAvailableWithHandler:(void (^)(NSDictionary *, NSError *))handler
{
    // Reachabilityのチェック
    NetworkStatus status = [self.goodiaFmsReachability currentReachabilityStatus];
    if (status == NotReachable) {
        // ネットワークに繋がっていない
        NSError *error = [NSError errorWithDomain:@"local" code:0 userInfo:@{@"message" : @"APIにアクセスできません。"}];
        handler(0, error);
        return;
    }

    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(aQueue, ^{
        // 確認URL
        NSString *host = self.goodiaFmsHost;
        NSString *appID = APP_ID;
        NSString *flagID = END_ID;

        NSString *urlString = [NSString stringWithFormat:@"http://%@/v1/value_of/%@/%@", host, appID, flagID];

        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];

        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

        if (error) {
            return;
        }

        NSError *json_error;
        __block NSDictionary *json = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&json_error] retain];

        // メインスレッドで通知
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^{
            if (handler != nil) {
                handler(json, error);  // 返り値から値を取得
            }
            [json release], json = nil;
        });
    });
}


- (void) confirmWhichAdSplashWithHandler:(void (^)(NSDictionary *dict, NSError *error))handler
{
    // Reachabilityのチェック
    NetworkStatus status = [self.goodiaFmsReachability currentReachabilityStatus];
    if (status == NotReachable) {
        // ネットワークに繋がっていない
        NSError *error = [NSError errorWithDomain:@"local" code:0 userInfo:@{@"message" : @"APIにアクセスできません。"}];
        handler(0, error);
        return;
    }

    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(aQueue, ^{
        // 確認URL
        NSString *host = self.goodiaFmsHost;
        NSString *appID = APP_ID;
        NSString *flagID = SPL_ID;

        NSString *urlString = [NSString stringWithFormat:@"http://%@/v1/value_of/%@/%@", host, appID, flagID];

        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];

        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

        if (error) {
            return;
        }

        NSError *json_error;
        __block NSDictionary *json = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&json_error] retain];

        // メインスレッドで通知
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^{
            if (handler != nil) {
                handler(json, error);  // 返り値から値を取得
            }
            [json release], json = nil;
        });
    });
}

- (void) confirmWhichAdSplashFrequencyWithHandler:(void (^)(NSDictionary *, NSError *))handler
{
    // Reachabilityのチェック
    NetworkStatus status = [self.goodiaFmsReachability currentReachabilityStatus];
    if (status == NotReachable) {
        // ネットワークに繋がっていない
        NSError *error = [NSError errorWithDomain:@"local" code:0 userInfo:@{@"message" : @"APIにアクセスできません。"}];
        handler(0, error);
        return;
    }
    
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(aQueue, ^{
        // 確認URL
        NSString *host = self.goodiaFmsHost;
        NSString *appID = APP_ID;
        NSString *flagID = SPF_ID;
        
        NSString *urlString = [NSString stringWithFormat:@"http://%@/v1/value_of/%@/%@", host, appID, flagID];
        
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        if (error) {
            return;
        }
        
        NSError *json_error;
        __block NSDictionary *json = [[NSJSONSerialization JSONObjectWithData:data options:0 error:&json_error] retain];
        
        // メインスレッドで通知
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^{
            if (handler != nil) {
                handler(json, error);  // 返り値から値を取得
            }
            [json release], json = nil;
        });
    });
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"%@\n  UUID : %@\nitems : %@",[super description],self.userUUID,self.itemStocks];
}
@end
