//
//  EQSSDK.h
//  EQSSDK
//
//  Copyright (c) 2012 EQS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Security/Security.h>

@interface EQSSDK : UIViewController <UIWebViewDelegate>

+ (void)start: (UIViewController*)delegate mid:(NSString*)mid;
+ (void)start: (UIViewController*)delegate mid:(NSString*)mid transitionStyle:(int)transitionStyle;
+ (void)start: (UIViewController*)delegate mid:(NSString*)mid appid:(NSString*)appid;
+ (void)start: (UIViewController*)delegate mid:(NSString*)mid appid:(NSString*)appid transitionStyle:(int)transitionStyle;

@end
