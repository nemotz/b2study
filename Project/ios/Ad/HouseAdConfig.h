//
//  HouseAdConfig.h
//  HundredPunch
//
//  Created by kuzawa yuusuke on 12/02/21.
//  Copyright (c) 2012年 cocolo-bit.com All rights reserved.
//

#ifndef HouseAdConfig_h
#define HouseAdConfig_h

#define kGenuineAdURL	@"http://goodiaappcan.appspot.com/ad.html"

#define kHouseAdSize CGSizeMake(320, 50)

#define SHOW_PERFORM_CUSTOMEVENT	1
//#define HOUSEAD_LANDSCAPE_TOPRIGHT

#define NOALAUDIO
#define NCOCOS2D

#define APP_ORIENTATION         @"PORTRAIT"
//#define APP_ORIENTATION         @"LANDSCAPE"

#define ICON_AD_MAIN            @"IMOB"    //i-mobile多めの場合
//#define ICON_AD_MAIN            @"ASTA"    //Asta多めの場合

//ポップアップ用アプリID
#define APP_ID @"PRI"
#define RWD_ID @"RWD01"
#define POP_ID @"POPNEW01"
#define END_ID @"END01"
#define SPL_ID @"SPL01"
#define SPF_ID @"SPF01"         //SPlashAd Frequency スプラッシュ広告の表示頻度(4だったら4で割ったときの余りが0のときsplash広告を表示)

//レビュー用アプリID
#define REVIEW_POPUP_IOS_APP_ID (689077899)
#define REVIEW_POPUP_ALERTVIEW_TAG (123)
#define REVIEW_POPUP_COUNT (3)

//*** 以下、各社広告ID設定 *********************************************

//*** asterisk ID ***
#define ASTERISK_ID             @"ast00284i0d1iq8jua8y"

//*** Aid ***
#define AID_MEDIA_CODE          @"id719287412mt8"

//*** GameFeat ID ***
#define GAMEFEAT_ID             @"2802"

//*** Adstir ***
#define ADSTIR_MEDIA_ID         @"MEDIA-a1b9855e"
#define ADSTIR_SPOT_NO          (1)

//*** AdStir が表示されなかった場合 ***
#define ADSTIR_FAIL_TO_RECIEVED_ADTYPE  (1)    // 1:nend - iMobile //2:iMobile - nend

//*** BEAD ID ***
#define BEAD_SID                @"94ed3cfaf69459517d88c0e5da0e9b0a5096a931d60410d4"

//*** AmoAd ***
#define kAdCloudSID				@"62056d310111552c3c53e85a25cfda3d2d3350353ff6b95d26af71e530951d2b"

//*** i-mobile icon ***
#define kAdIMobliePushlisherID			24099  // 固定 OLD:10529 NEW:24776 (2):24099
#define kAdIMoblieMediaIDIcon			83884
#define kAdIMoblieSpotIDIcon			176912
//*** i-mobile bannar ***
#define kAdIMoblieMediaID				83884
#define kAdIMoblieSpotID				176913
//*** i-mobile splash ***
#define kAdIMobliePushlisherIDSplash	@"24099"    
#define kAdIMoblieMediaIDSplash			@"84571"
#define kAdIMoblieSpotIDSplash			@"183485"

//*** Nend ***
#define kAdNendApiKey			@"5731884389a05f3bed8780aa059938db08d039f5"
#define kAdNendSpotID			@"95380"

//*** AdLantis *** SDK未実装 ***
#define kAdLantisPublisherID	@""

//*** InMobi ***
#define INMOBI_ID               @"6059409913144e768fcf22b0e000f1da"

//*** ZOON ***
#define ZOON_MEDIA_ID           @"gda051"

//*** Flurry ***
#define FLURRY_API_KEY          @"JMP5D3SDSXN9KR33FQH9"

//*** Tapjoy ***
#define TAP_JOY_MEDIA_ID           @"f8c661c7-71a6-4251-bcf5-a0631f654758"
#define TAP_JOY_SECRET_KEY         @"cXM4qNYhhpjaup1WoN8D"
#define TAP_JOY_CURRENCY_NAME      @"continue"

//*** AMoAd(AppliPromotion) ***
// AMoAdSDK/AMoAdSettings.txt appKeyに設定

// Framework
//
// AMoAd(Applipromotion)
//  AdSupport.framework
//
// i-mobile
//  CFNetwork.framework
//  libz.dylib
//  MobileCoreServices.framework
//  SystemConfiguraion.framework
//  AdSupport.framework
//  Target[Other Linker Flags] "-ObjC"
//
// BEAD
//  UIKit.framework
//  Foundation.framework
//  CoreGraphics.framework
//  CFNetwork.framework
//  SystemConfiguration.framework
//  CoreMotion.framework
//  ImageIO.framework
//  QuartzCore.framework
//  Target[Other Linker Flags] "-ObjC" "–all_load"
//
// Adstir
//  MediaPlayer.framework
//  QuartzCore.framework
//  CoreLocation.framework
//  SystemConfiguration.framework
//  AdSupport.framework
//
// TapJoy
//  EvenKitUI
//  EventKit
//  libsqlite3.0
//  CoreMotion
//  Social (Optional)
//  libxml2


#endif
