//
//  AMoAdSDK
//
//  Copyright © AMoAd, Inc. All Rights Reserved.
//

#import <UIKit/UIKit.h>


@interface AMoAdSDK : NSObject {
}

/*
 * Wallの表示.
 */
+ (UIViewController *) showAppliPromotionWall:(UIViewController *)owner;
+ (UIViewController *) showAppliPromotionWall:(UIViewController *)owner
                                  orientation:(UIInterfaceOrientation)orientation;


/*
 * Wall表示時、同時にスプラッシュ表示
 */
+ (UIViewController *) showAppliPromotionWallWithInserstitial:(UIViewController *)owner;
+ (UIViewController *) showAppliPromotionWallWithInserstitial:(UIViewController *)owner
                                    orientation:(UIInterfaceOrientation)orientation;
+ (UIViewController *) showAppliPromotionWallWithInserstitial:(UIViewController *)owner
                                                       forced:(BOOL)forced;
+ (UIViewController *) showAppliPromotionWallWithInserstitial:(UIViewController *)owner
                                                  orientation:(UIInterfaceOrientation)orientation
                                                       forced:(BOOL)forced;
+ (UIViewController *) showAppliPromotionWallWithInserstitial:(UIViewController *)owner
                                                      loading:(BOOL)loading;
+ (UIViewController *) showAppliPromotionWallWithInserstitial:(UIViewController *)owner
                                                  orientation:(UIInterfaceOrientation)orientation
                                                      loading:(BOOL)loading;
+ (UIViewController *) showAppliPromotionWallWithInserstitial:(UIViewController *)owner
                                                       forced:(BOOL)forced
                                                      loading:(BOOL)loading;
+ (UIViewController *) showAppliPromotionWallWithInserstitial:(UIViewController *)owner
                                                  orientation:(UIInterfaceOrientation)orientation
                                                       forced:(BOOL)forced
                                                      loading:(BOOL)loading;


/*
 * Wallの表示が初めてかどうかのチェック.
 */
+ (BOOL)isFirstTimeInToday;

/*
 * UUIDの送信.
 */
+ (void)sendUUID;


/*
 * インタースティシャル広告設定の取得.
 */
+ (void)loadInterstitial:(UIViewController *)owner;

/*
 * インタースティシャル広告の表示.
 */
+ (void)showInterstitial:(UIViewController *)owner;
+ (void)showInterstitial:(UIViewController *)owner forced:(BOOL)forced;
+ (void)showInterstitial:(UIViewController *)owner callback:(SEL)callback;
+ (void)showInterstitial:(UIViewController *)owner forced:(BOOL)forced callback:(SEL)callback;

/*
 * インタースティシャル広告の表示.
 * ローディング表示を行い、スタックせずに直接読み込む.
 */
+ (void)showInterstitialLoading:(UIViewController *)owner;
+ (void)showInterstitialLoading:(UIViewController *)owner forced:(BOOL)forced;
+ (void)showInterstitialLoading:(UIViewController *)owner callback:(SEL)callback;
+ (void)showInterstitialLoading:(UIViewController *)owner forced:(BOOL)forced callback:(SEL)callback;
+ (void)showInterstitialLoading:(UIViewController *)owner callback:(SEL)callback
                      screen:(NSString *)screen
                       flame:(int)flame
                   crossDisp:(BOOL)crossDisp
             crossLoadingBar:(BOOL)crossLoadingBar
            crossNextSeconds:(int)crossNextSeconds
              paidLoadingBar:(BOOL)paidLoadingBar
             paidNextSeconds:(int)paidNextSeconds
         paidCloseBtnSeconds:(int)paidCloseBtnSeconds
               crossMyAdRate:(int)crossMyAdRate
                paidMyAdRate:(int)paidMyAdRate
                     wallBtn:(BOOL)wallBtn
                   swipeIcon:(BOOL)swipeIcon;
+ (void)showInterstitialLoading:(UIViewController *)owner forced:(BOOL)forced callback:(SEL)callback
                         screen:(NSString *)screen
                          flame:(int)flame
                      crossDisp:(BOOL)crossDisp
                crossLoadingBar:(BOOL)crossLoadingBar
               crossNextSeconds:(int)crossNextSeconds
                 paidLoadingBar:(BOOL)paidLoadingBar
                paidNextSeconds:(int)paidNextSeconds
            paidCloseBtnSeconds:(int)paidCloseBtnSeconds
                  crossMyAdRate:(int)crossMyAdRate
                   paidMyAdRate:(int)paidMyAdRate
                        wallBtn:(BOOL)wallBtn
                      swipeIcon:(BOOL)swipeIcon;

/*
 * インタースティシャル広告の表示調整.
 * 画面ローテーション時などに実行して下さい
 */
+ (void)resizeIntersitial;

@end
