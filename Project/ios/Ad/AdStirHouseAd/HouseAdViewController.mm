//
//  HouseAdViewController.m
//
//
//  Created by kuzawa yuusuke on 12/02/14.
//  Copyright (c) 2012年 cocolo-bit.com All rights reserved.
//

#import "HouseAdViewController.h"
#import "HouseAdConfig.h"
#import "GoodiaUserInfo.h"
#import "imobileAds/IMobileAdIconView.h"
#import "imobileAds/IMAdView.h"
#import "imobileAds/IMobileAdIconViewParams.h"
#ifndef NOALAUDIO
#import "OALAudio.h"
#endif
#ifndef NCOCOS2D
#import "cocos2d.h"
#endif
#include "HelloWorldScene.h"
#include "PrivateConfig.h"
#include "Flurry.h"

#define HouseAdViewTag (1)
#define GenuineAdViewTag (2)

#define kAdViewWidth 320
#define kAdViewHeight 50

//TapJoy
#define kTapJoyServiceTimer (10.0f)
#define TAPJOY_POPUP_ALERTVIEW_TAG (1206)

//FLURRY
#define FLURRY_EVENT_HEADER_TAPPED  (@"POPUP_TAP_YES")
#define FLURRY_EVENT_HEADER_SHOW    (@"POPUP_SHOW")

#define ADSPLASH_BASE_FREQUENCY (3) //GAE未設定時の表示頻度 ADSPLASH_BASE_FREQUENCY回に1回


@interface HouseAdViewController ()
@property (nonatomic, retain) GoodiaUserInfo *userInfo;
@property (assign) BOOL houseAdLoading;
@property (assign) BOOL genuineAdLoading;

@property (nonatomic, retain) NSDate* lastConfigLoadDate;
@property (nonatomic, retain) NSDate* lastConfigRequestDate;
@property (nonatomic, copy) NSString* bundleName;
- (void)commonInit;
- (void)displayHouseAdView:(BOOL)show;
- (CGRect)frameRectForAdView;
- (void)applicationDidReceiveMemoryWarning:(NSNotification *)notification;
- (void)applicationDidBecomeActive:(NSNotification *)notification;
- (void)applicationWillResignActive:(NSNotification *)notification;

- (NSString *)getAppName:(NSString *)title;
@end

@implementation HouseAdViewController
@synthesize houseAdView = houseAdView_;
@synthesize houseAdLoading;

@synthesize genuineAdView = genuineAdView_;
@synthesize genuineAdLoading;

@synthesize lastConfigLoadDate;
@synthesize lastConfigRequestDate;
@synthesize bundleName;
@synthesize popAppUrl = _popAppUrl;
@synthesize popAppName = _popAppName;

@synthesize adSplashType = adSplashType_;

//Flurry追加メソッド//GAEから取ってきたtitleからアプリ名を抜き取る
- (NSString *)getAppName:(NSString *)title{
    NSRange hoge1 = [title rangeOfString:@"「"];
    NSRange hoge2 = [title rangeOfString:@"」"];
    return [title substringWithRange:NSMakeRange(hoge1.location + 1, hoge2.location - hoge1.location - 1)];
}

- (void) confirmTapjoy{
    self.userInfo = [GoodiaUserInfo goodiaUserInfo];
    
    [self.userInfo confirmWhichAppDriverAvailableWithHandler:^(BOOL isAvailable, NSError *error) {
        if (!error)
        {
            isVideoRewardAvailable_ = isAvailable;
        }
    }];
}

- (void) confirmNewApp
{
    needToDisplayIconAd = NO;

    self.userInfo = [GoodiaUserInfo goodiaUserInfo];
    
    [self.userInfo confirmWhichNewAppAvailableWithHandler:^(NSDictionary *dict, NSError *error) {
        
        if (!error)
        {
            
            NSString *flg = [NSString stringWithFormat:@"%@", [dict objectForKey:@"flg"]];
            NSString *scheme1 = [NSString stringWithFormat:@"%@://", [dict objectForKey:@"scheme1"]];
            NSString *scheme2 = [NSString stringWithFormat:@"%@://", [dict objectForKey:@"scheme2"]];
            NSString *scheme3 = [NSString stringWithFormat:@"%@://", [dict objectForKey:@"scheme3"]];
            
            //タイトルが登録されていたら、アイコンも表示OKとする
            if ([dict objectForKey:@"flg"]){
                needToDisplayIconAd = YES;
            }
            
            if (![flg isEqualToString:@"1"]) {
                return;
            }
            
            BOOL canOpen1 = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme1]];
            BOOL canOpen2 = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme2]];
            BOOL canOpen3 = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme3]];
            if (showHouseAd_) {
                if (!canOpen1) {
                    
                    self.popAppUrl = [NSString stringWithFormat:@"%@", [dict objectForKey:@"url1"]];
                    self.popAppName = [self getAppName:[dict objectForKey:@"title1"]];//アプリ名を抜き出す
                    UIAlertView *alert =
                    [[UIAlertView alloc] initWithTitle:@"お知らせ" message:[dict objectForKey:@"title1"]
                                              delegate:self cancelButtonTitle:@"いいえ" otherButtonTitles:@"はい", nil];
                    
                    [alert show];
                    [Flurry logEvent:[NSString stringWithFormat:@"%@:%@",FLURRY_EVENT_HEADER_SHOW,self.popAppName]];
                    
                } else if (!canOpen2) {
                    
                    self.popAppUrl = [NSString stringWithFormat:@"%@", [dict objectForKey:@"url2"]];
                    self.popAppName = [self getAppName:[dict objectForKey:@"title2"]];//アプリ名を抜き出す
                    
                    UIAlertView *alert =
                    [[UIAlertView alloc] initWithTitle:@"お知らせ" message:[dict objectForKey:@"title2"]
                                              delegate:self cancelButtonTitle:@"いいえ" otherButtonTitles:@"はい", nil];
                    [alert show];
                    [Flurry logEvent:[NSString stringWithFormat:@"%@:%@",FLURRY_EVENT_HEADER_SHOW,self.popAppName]];
                    
                } else if (!canOpen3) {
                    
                    self.popAppUrl = [NSString stringWithFormat:@"%@", [dict objectForKey:@"url3"]];
                    self.popAppName = [self getAppName:[dict objectForKey:@"title3"]];//アプリ名を抜き出す
                    UIAlertView *alert =
                    [[UIAlertView alloc] initWithTitle:@"お知らせ" message:[dict objectForKey:@"title3"]
                                              delegate:self cancelButtonTitle:@"いいえ" otherButtonTitles:@"はい", nil];
                    [alert show];
                    [Flurry logEvent:[NSString stringWithFormat:@"%@:%@",FLURRY_EVENT_HEADER_SHOW,self.popAppName]];
                }
            }
        }
        //#ifdef DEBUG
        //        else
        //        {
        //            NSLog(@"%s error:%@", __func__, [error description]);
        //        }
        //#endif
    }];
}

- (void) confirmGenuineAd
{
    needToDisplayGenuineAd = NO;
    needToDisplayIconAd = NO;
    self.userInfo = [GoodiaUserInfo goodiaUserInfo];
    
    [self.userInfo confirmWhichGenuineAdAvailableWithHandler:^(NSDictionary *dict, NSError *error) {
        
        if (!error)
        {
            
            NSString *scheme = [NSString stringWithFormat:@"%@://", [dict objectForKey:@"scheme"]];
            NSString *flg = [NSString stringWithFormat:@"%@", [dict objectForKey:@"flg"]];
            
            NSString *strUrl = [NSString stringWithFormat:@"%@" , [dict objectForKey:@"url"]];
            NSURL* url = [NSURL URLWithString:strUrl];
            
            //エンディングが登録されていたら、アイコンも表示OKとする(END_IDが一致していれば、flg = 0 でもアイコンは表示)
            if ([dict objectForKey:@"flg"]){
                needToDisplayIconAd = YES;
            }
            
            if (![flg isEqualToString:@"1"]) {
                return;
            }
            
            BOOL canOpen = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme]];

            if (!canOpen) {
                needToDisplayGenuineAd = YES;

                //純広告
                CGRect cr = [[UIScreen mainScreen] bounds];
                CGRect ret = CGRectZero;
                if ([APP_ORIENTATION isEqualToString:@"LANDSCAPE"]) {
                    ret = CGRectMake(cr.size.height/2 - kAdViewWidth/2,
                                     0,
                                     kAdViewWidth,
                                     100.0f);
                } else {
                    ret = CGRectMake(cr.size.width/2 - kAdViewWidth/2,
                                     0,
                                     kAdViewWidth,
                                     100.0f);
                }

                if ( !self.genuineAdView ) {
                    self.genuineAdView = [[[UIWebView alloc] initWithFrame:ret] autorelease];
                    [self.genuineAdView setTag:GenuineAdViewTag];
                    [self.genuineAdView setBackgroundColor:[UIColor clearColor]];
                    [self.genuineAdView setOpaque:NO];
                    self.genuineAdView.delegate = self;
                    [self displayGenuineAdView:NO];
                    showGenuineAd_ = NO;
                    //閉じるボタン
                    {
                        UIImage *btnImage = [UIImage imageNamed:@"closeButton"];
                        UIButton *closeButton = [[[UIButton alloc] init] autorelease];// A
                        closeButton.frame = CGRectMake(0, 0, 25, 25); // B
                        [closeButton setBackgroundImage:btnImage forState:UIControlStateNormal];//C
                        [closeButton addTarget:self action:@selector(closeButtonTapped) forControlEvents:UIControlEventTouchUpInside];//D
                        [self.genuineAdView addSubview:closeButton]; //E
                    }
                    [self.view addSubview:self.genuineAdView];
                    
                    if ( !self.genuineAdLoading ) {
                        self.genuineAdLoading = YES;
                        NSURLRequest* request = [NSURLRequest requestWithURL:url];
                        assert(self.genuineAdView);
                        [self.genuineAdView loadRequest:request];
                    }
                    
                    // サポートされているiOSバージョンなら、webViewのスクロールを無効にする
                    NSString* reqSysVer = @"5.0";
                    NSString* currSysVer = [[UIDevice currentDevice] systemVersion];
                    BOOL osVersionSupported = ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
                    if ( osVersionSupported )
                        [[self.genuineAdView scrollView] setScrollEnabled:NO];
                }
                if ( self.genuineAdView.superview != self.view ) {
                    [self.view addSubview:self.genuineAdView];
                    self.genuineAdView.frame = ret;
                }
            }
            else{
                
            }
        }
    }];
}

- (void) confirmAdSplashType
{
    self.userInfo = [GoodiaUserInfo goodiaUserInfo];
    
    
    //Splash広告のタイプをGAEから取得してUserDefaultに保存
    [self.userInfo confirmWhichAdSplashWithHandler:^(NSDictionary *dict, NSError *error) {
        NSString * adSplashTypeWall = NULL;
        NSString * adSplashTypeRank = NULL;
        if (!error)
        {
            self.adSplashType = [NSString stringWithFormat:@"%@", [dict objectForKey:@"splashType"]];
            adSplashTypeWall = [NSString stringWithFormat:@"%@", [dict objectForKey:@"splashTypeWall"]];
            adSplashTypeRank = [NSString stringWithFormat:@"%@", [dict objectForKey:@"splashTypeRank"]];
#ifdef DEBUG
            NSLog(@"splashType:%@ , adSplashTypeWall:%@ , adSplashTypeRank:%@", self.adSplashType,adSplashTypeWall,adSplashTypeRank);
#endif
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            //Ending
            [userDefaults setObject:self.adSplashType forKey:@"AdSplashType"];
            //Wall
            [userDefaults setObject:adSplashTypeWall forKey:@"AdSplashTypeWall"];
            //Ranking
            [userDefaults setObject:adSplashTypeRank forKey:@"AdSplashTypeRank"];
            
            [userDefaults synchronize];
        }
    }];
    //Splash広告の表示頻度をGAEから取得してUserDefaultに保存
    [self.userInfo confirmWhichAdSplashFrequencyWithHandler:^(NSDictionary *dict, NSError *error) {
        int adSplashFrequency = ADSPLASH_BASE_FREQUENCY;
        if (!error && dict != nil)
        {
            adSplashFrequency = [[dict objectForKey:@"AdSplashFrequency"] intValue];
#ifdef DEBUG
            NSLog(@"success:adSplashFrequency:%d", adSplashFrequency);
#endif
        }
        else{
#ifdef DEBUG
            NSLog(@"fail:adSplashFrequency:%d", adSplashFrequency);
#endif
        }
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setInteger:adSplashFrequency forKey:@"AdSplashFrequency"];
        [userDefaults synchronize];
    }];
}

#pragma mark - alertView
// アラートのボタンが押された時に呼ばれるデリゲート例文
-(void)alertView:(UIAlertView*)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == REVIEW_POPUP_ALERTVIEW_TAG) {
        BOOL isNever = NO;
        BOOL isReset = NO;
        switch (buttonIndex) {
            case 0:
                //cancel
                isReset = YES;
                break;
            case 1:
                //書くのとき
                isNever = YES;
                [self review];
                break;
            case 2:
                //二度と表示しない
                isNever = YES;
                break;
            default:
                break;
        }

        if (isNever == YES) {
            [self setCountReviewPopupUserDefault:1111];
        }
        if (isReset == YES) {
            [self setCountReviewPopupUserDefault:0];
        }
    }
	else if (alertView.tag == TAPJOY_POPUP_ALERTVIEW_TAG) {
		[self tapjoyAlertViewButtonTapped];
	}
    else{
        switch (buttonIndex) {
            case 0:
                //１番目のボタンが押されたときの処理を記述する
                break;
            case 1:
                //はいのときの処理
                [Flurry logEvent:[NSString stringWithFormat:@"%@:%@",FLURRY_EVENT_HEADER_TAPPED,self.popAppName]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.popAppUrl]];
                break;
        }
   }
}

#pragma mark - Inherit from UIViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if ( self ) {
		[self commonInit];
	}
	return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if ( self ) {
		[self commonInit];
	}
	return self;
}

- (void)commonInit
{
    _icons = [[NSMutableArray alloc] initWithCapacity:4];
    _iconsIM = [[NSMutableArray alloc] initWithCapacity:4];
	_iconsAstaTitle = [[NSMutableArray alloc] initWithCapacity:4];
    
    needToDisplayGenuineAd = NO;
    needToDisplayIconAd = NO;
	
    //純広告の確認
    [self confirmGenuineAd];
    
    //最新作の確認
    [self confirmNewApp];
    
    //スプラッシュ広告のタイプの確認
    [self confirmAdSplashType];
    
	displayHouseAdState_ = -1;
#if GOOGLEANALYTICS
	[[GANTracker sharedTracker] startTrackerWithAccountID:kGANAccountID
										   dispatchPeriod:60*60*4
												 delegate:nil];
#ifdef DEBUG
	[GANTracker sharedTracker].debug = YES;
    //	[GANTracker sharedTracker].dryRun = YES;
#endif
#endif
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(applicationDidReceiveMemoryWarning:)
												 name:UIApplicationDidReceiveMemoryWarningNotification
											   object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(applicationDidBecomeActive:)
												 name:UIApplicationDidBecomeActiveNotification
											   object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(applicationWillResignActive:)
												 name:UIApplicationWillResignActiveNotification
											   object:nil];
	
	//tapjoy出すかどうかのフラグ確認
	[self confirmTapjoy];
	//tapjoy Notification
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(fullScreenAdEnabled:)
												 name:TJC_FULL_SCREEN_AD_RESPONSE_NOTIFICATION
											   object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(fullScreenAdError:)
												 name:TJC_FULL_SCREEN_AD_RESPONSE_NOTIFICATION_ERROR
											   object:nil];
	isVideoRewardEnabled_ = NO;//動画広告があるかないか、tapjoyへのリクエストの結果
	isVideoRewardAvailable_ = NO;//動画広告を配信するか、goodiaへのリクエストの結果
	isVideoRewardServiceEnabled_ = NO;//動画広告をサービスで見たことにしてあげる。。。
    isTapjoyViewAppear_ = NO;//動画広告画面を表示しているかどうか。。。
	[Tapjoy setViewDelegate:self];

	
	NSString* bundleIdentifier = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleIdentifierKey];
	self.bundleName = [[bundleIdentifier componentsSeparatedByString:@"."] lastObject];
}

- (void)dealloc
{
    [_icons release], _icons = nil;
    [_iconsIM release], _iconsIM = nil;
	[_iconsAstaTitle release], _iconsAstaTitle = nil;
    
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIApplicationDidReceiveMemoryWarningNotification
												  object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIApplicationWillResignActiveNotification
												  object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIApplicationDidBecomeActiveNotification
												  object:nil];
	
	//TapJoy
	[Tapjoy setViewDelegate:NULL];
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:TJC_FULL_SCREEN_AD_RESPONSE_NOTIFICATION
												  object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:TJC_FULL_SCREEN_AD_RESPONSE_NOTIFICATION_ERROR
												  object:nil];

#if GOOGLEANALYTICS
	[[GANTracker sharedTracker] stopTracker];
#endif
	[houseAdView_ setDelegate:nil];
    [genuineAdView_ setDelegate:nil];
#ifndef NADMAKER
	[adMaker_ setDelegate:nil];
	[adMediba_ setDelegate:nil];
#endif
	[adNad_ setDelegate:nil];
	[adCloud_ setDelegate:nil];
#ifndef NADVISION
	[adVision_ setDelegate:nil];
#endif
    
	[houseAdView_ release];
    [genuineAdView_ release];
#ifndef NADMAKER
	[adMaker_ release];
	[adMediba_ release];
#endif
	[adNad_ release];
	[adCloud_ release];
#ifndef NADVISION
	[adVision_ release];
#endif
    [_adstirView setDelegate:nil];
    [_adstirView release];
    
	[super dealloc];
    
}

- (void)applicationDidReceiveMemoryWarning:(NSNotification *)notification
{
#ifndef NADMAKER
	if ( adMaker_ && adMaker_.view.superview == nil ) {
		[adMaker_ setDelegate:nil];
		[adMaker_ release];
		adMaker_ = nil;
	}
	if ( adMediba_ && adMediba_.view.superview == nil ) {
		[adMediba_ setDelegate:nil];
		[adMediba_ release];
		adMediba_ = nil;
	}
#endif
	if ( adNad_ && adNad_.superview == nil ) {
		[adNad_ setDelegate:nil];
		[adNad_ release];
		adNad_ = nil;
	}
	if ( adCloud_ && adCloud_.superview == nil ) {
		[adCloud_ setDelegate:nil];
		[adCloud_ release];
		adCloud_ = nil;
	}
#ifndef NADVISION
	if ( adVision_ && adVision_.superview == nil ) {
		[adVision_ setDelegate:nil];
		[adVision_ release];
		adVision_ = nil;
	}
#endif
}

- (void)applicationDidBecomeActive:(NSNotification *)notification
{
#if GOOGLEANALYTICS
	NSError* error;
	if ( ![[GANTracker sharedTracker] trackEvent:@"lunch-app"
										  action:@"lunch-app"
										   label:self.bundleName
										   value:-1
									   withError:&error]) {
		NSLog(@"%@", error);
	}
#endif
}

- (void)applicationWillResignActive:(NSNotification *)notification
{
#if GOOGLEANALYTICS
    [[GANTracker sharedTracker] dispatchSynchronous:2.0];
#endif
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    
	[self attachAd];
}

- (void)viewDidUnload
{
	self.houseAdView.delegate = nil;
	self.houseAdView = nil;
    
    self.genuineAdView.delegate = nil;
	self.genuineAdView = nil;
    
	houseAdLoadSuccess_ = NO;
    genuineAdLoadSuccess_ = NO;
    
    [super viewDidUnload];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	[super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //	NSLog(@"URL = %@", [[request URL] absoluteString]);
	if ( navigationType == UIWebViewNavigationTypeLinkClicked ) {
#if GOOGLEANALYTICS
		NSError* error;
        //		NSString* js = [NSString stringWithFormat:@"$(\"a[href='%@']\").attr(\"src\");", [[request URL] absoluteString]];
        //		NSString* js = @"$(\"#linksImgDiv\");";
        //		NSString* src = [webView stringByEvaluatingJavaScriptFromString:js];
		if ( ![[GANTracker sharedTracker] trackEvent:@"banner-click"
											  action:[[request URL] absoluteString]
											   label:self.bundleName
											   value:-1
										   withError:&error]) {
			NSLog(@"%@", error);
		}
#endif
		[[UIApplication sharedApplication] openURL:[request URL]];
		return NO;
	}
	return YES;
}

//NSURL *skypeURL = [NSURL URLWithString:@"http://phobos.apple.com/WebObjects/MZSearch.woa/wa/search?entity=software&media=software&submit=seeAllLockups&term=skype"];
//[[UIApplication sharedApplication] openURL:skypeURL];

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (webView.tag == HouseAdViewTag) {
        houseAdLoadSuccess_ = YES;
        [self displayHouseAdView:showHouseAd_];
        self.houseAdLoading = NO;
    }
    else if(webView.tag == GenuineAdViewTag){
        genuineAdLoadSuccess_ = YES;
        [self displayGenuineAdView:showGenuineAd_];
        self.genuineAdLoading = NO;
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if (webView.tag == HouseAdViewTag) {
        houseAdLoadSuccess_ = NO;
        [self displayHouseAdView:NO];
        self.houseAdLoading = NO;
    }
    else if(webView.tag == GenuineAdViewTag){
        genuineAdLoadSuccess_ = NO;
        [self displayGenuineAdView:NO];
        self.genuineAdLoading = NO;
    }
}

-(int)getRandInt:(int)min max:(int)max {
	static int randInitFlag;
	if (randInitFlag == 0) {
		srand(time(NULL));
		randInitFlag = 1;
	}
	return min + (int)(rand()*(max-min+1.0)/(1.0+RAND_MAX));
}

- (void)showHouseAd:(BOOL)enable
{
	showHouseAd_ = enable;
	if ( showHouseAd_ ) {
    forceHouseAd:
        
		if ( houseAdLoadSuccess_ ) {
			[self displayHouseAdView:YES];
		}
		else {
			if ( !self.houseAdLoading ) {
				self.houseAdLoading = YES;
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                int adIndex = [userDefaults integerForKey:@"AdIndex"];
                int answerNum;
                if (adIndex > 0) {
                    switch (adIndex) {
                        case 1:
                            answerNum = 2;
                            break;
                        case 2:
                            answerNum = 3;
                            break;
                        default:
                            answerNum = 1;
                            break;
                    }
                } else {
                    answerNum = 1;
                }
                [userDefaults setInteger:answerNum forKey:@"AdIndex"];
                [userDefaults synchronize];
                
                NSString *strUrl = [NSString stringWithFormat:@"http://goodiaappcan.appspot.com/ad_newnew_%i.html" , answerNum];
                NSURL* url = [NSURL URLWithString:strUrl];
				NSURLRequest* request = [NSURLRequest requestWithURL:url];
				assert(self.houseAdView);
				[self.houseAdView loadRequest:request];

				// i-mobileのアイコンサイズ
				IMobileAdIconViewParams *params = [[IMobileAdIconViewParams alloc] init];
				params.iconSize = 57;
				
				// Astaのアイコンサイズとサイドの間隔
                int iconSize = 75;
                int sideMargin = 3;
				
				// iPadのアイコンサイズを大きくする
                int adJustRate = 1.0f;
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                    adJustRate = 2.0f;
					params.iconSize = 114;
					iconSize = 150;
					sideMargin = 55;
                }


				int bottomMargin;
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
					// iPadのときはアイコン下のスペースを広くする
                    bottomMargin = 20 * adJustRate;
                } else {
					// 3.5インチのときはアイコン下のスペースを狭くする
					CGRect frame = [[UIScreen mainScreen] applicationFrame];
					if (frame.size.height>480.0) {
						bottomMargin = -5 * adJustRate;
					} else {
						bottomMargin = 10 * adJustRate;
					}
				}
				
                int count_icon = [_icons count];
                if (count_icon < 1) {
                    MrdIconLoader* iconLoader = [[MrdIconLoader alloc]init]; // (1)
                    CGRect cr = [[UIScreen mainScreen] bounds];

                    if ([APP_ORIENTATION isEqualToString:@"LANDSCAPE"])
                    {
                        //横向き

                        //Asta
                        {
                            CGRect frame = CGRectMake(sideMargin,cr.size.width/4,iconSize,iconSize);
                            MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                            [iconLoader addIconCell:iconCell];  // (3)
                            [_icons addObject:iconCell];
                            [self.view addSubview:iconCell];  // (4)
                        }
                        {
                            CGRect frame = CGRectMake(sideMargin + iconSize + sideMargin,cr.size.width/4,iconSize,iconSize);
                            MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                            [iconLoader addIconCell:iconCell];  // (3)
                            [_icons addObject:iconCell];
                            [self.view addSubview:iconCell];  // (4)
                        }
                        
                        if ([ICON_AD_MAIN isEqualToString:@"IMOB"])
                        {
                            //iMobile
                            {
                                CGRect frame = CGRectMake(cr.size.height - iconSize - sideMargin,cr.size.width/4,iconSize,iconSize);
                                IMobileAdIconView *imAdIconView = [[IMobileAdIconView alloc]
                                                                   initWithFrame:frame
                                                                   publisherId:kAdIMobliePushlisherID
                                                                   mediaId:kAdIMoblieMediaIDIcon
                                                                   spotId:kAdIMoblieSpotIDIcon
                                                                   iconNumber:1
																   params:params
																   testMode:NO];
                                [_iconsIM addObject:imAdIconView];
                                [self.view addSubview:imAdIconView];
                            }
                            {
                                CGRect frame = CGRectMake(cr.size.height - iconSize - sideMargin - iconSize,cr.size.width/4,iconSize,iconSize);
                                IMobileAdIconView *imAdIconView = [[IMobileAdIconView alloc]
                                                                   initWithFrame:frame
                                                                   publisherId:kAdIMobliePushlisherID
                                                                   mediaId:kAdIMoblieMediaIDIcon
                                                                   spotId:kAdIMoblieSpotIDIcon
                                                                   iconNumber:1
																   params:params
																   testMode:NO];
                                [_iconsIM addObject:imAdIconView];
                                [self.view addSubview:imAdIconView];
                            }
                        }
                        else
                        {
                            //Asta
                            {
                                CGRect frame = CGRectMake(cr.size.height - iconSize - sideMargin,cr.size.width/4,iconSize,iconSize);
                                MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                                [iconLoader addIconCell:iconCell];  // (3)
                                [_icons addObject:iconCell];
                                [self.view addSubview:iconCell];  // (4)
                            }
                            {
                                CGRect frame = CGRectMake(cr.size.height - iconSize - sideMargin - iconSize,cr.size.width/4,iconSize,iconSize);
                                MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                                [iconLoader addIconCell:iconCell];  // (3)
                                [_icons addObject:iconCell];
                                [self.view addSubview:iconCell];  // (4)
                            }
                        }
                    }
                    else
                    {
                        //縦向き
                        if ([ICON_AD_MAIN isEqualToString:@"IMOB"])
                        {
                            //iMobile
                            {
                                CGRect frame = CGRectMake(cr.size.width/2 - (320*adJustRate)/2,cr.size.height/2 - (70*adJustRate) - (10*adJustRate),(320*adJustRate),(80*adJustRate));
                                IMobileAdIconView *imAdIconView = [[IMobileAdIconView alloc]
                                                                   initWithFrame:frame
                                                                   publisherId:kAdIMobliePushlisherID
                                                                   mediaId:kAdIMoblieMediaIDIcon
                                                                   spotId:kAdIMoblieSpotIDIcon
																   iconNumber:4
																   params:params
																   testMode:NO];
                                [imAdIconView setHidden:YES];
                                [_iconsIM addObject:imAdIconView];
                                [self.view addSubview:imAdIconView];
                            }
                            //Asta
                            {
                                CGRect frame = CGRectMake(sideMargin,cr.size.height/2 + (43*adJustRate) - (10*adJustRate),iconSize,iconSize);
                                MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                                [iconLoader addIconCell:iconCell];  // (3)
                                [_icons addObject:iconCell];
                                [self.view addSubview:iconCell];  // (4)
                            }
                            {
                                CGRect frame = CGRectMake(cr.size.width - iconSize - sideMargin,cr.size.height/2 + (43*adJustRate) - (10*adJustRate),iconSize,iconSize);
                                MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                                [iconLoader addIconCell:iconCell];  // (3)
                                [_icons addObject:iconCell];
                                [self.view addSubview:iconCell];  // (4)
                            }
							//Asta Title
							int sideMargin2 = (cr.size.width/4 - iconSize)/2;
							{
								CGRect frame = CGRectMake((cr.size.width/4 - iconSize)/2,
														  cr.size.height/2 - bottomMargin,
														  iconSize,iconSize);
								MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
								[iconLoader addIconCell:iconCell];  // (3)
								[_iconsAstaTitle addObject:iconCell];
								[self.view addSubview:iconCell];  // (4)
							}
							{
								CGRect frame = CGRectMake(cr.size.width/4 + sideMargin2,
														  cr.size.height/2 - bottomMargin,
														  iconSize,iconSize);
								MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
								[iconLoader addIconCell:iconCell];  // (3)
								[_iconsAstaTitle addObject:iconCell];
								[self.view addSubview:iconCell];  // (4)
							}
							{
								CGRect frame = CGRectMake((cr.size.width/4)*2 + sideMargin2,
														  cr.size.height/2 - bottomMargin,
														  iconSize,iconSize);
								MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
								[iconLoader addIconCell:iconCell];  // (3)
								[_iconsAstaTitle addObject:iconCell];
								[self.view addSubview:iconCell];  // (4)
							}
							{
								CGRect frame = CGRectMake((cr.size.width/4)*3 + sideMargin2,
														  cr.size.height/2 - bottomMargin,
														  iconSize,iconSize);
								MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
								[iconLoader addIconCell:iconCell];  // (3)
								[_iconsAstaTitle addObject:iconCell];
								[self.view addSubview:iconCell];  // (4)
							}
                        }
                        else
                        {
                            //Asta
                            {
                                CGRect frame = CGRectMake(sideMargin,cr.size.height/2 - (35*adJustRate) - (80*adJustRate) - (10*adJustRate),iconSize,iconSize);
                                MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                                [iconLoader addIconCell:iconCell];  // (3)
                                [_icons addObject:iconCell];
                                [self.view addSubview:iconCell];  // (4)
                            }
                            {
                                CGRect frame = CGRectMake(sideMargin,cr.size.height/2 - (35*adJustRate) - (10*adJustRate),iconSize,iconSize);
                                MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                                [iconLoader addIconCell:iconCell];  // (3)
                                [_icons addObject:iconCell];
                                [self.view addSubview:iconCell];  // (4)
                            }
                            {
                                CGRect frame = CGRectMake(sideMargin,cr.size.height/2 + (43*adJustRate) - (10*adJustRate),iconSize,iconSize);
                                MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                                [iconLoader addIconCell:iconCell];  // (3)
                                [_icons addObject:iconCell];
                                [self.view addSubview:iconCell];  // (4)
                            }
                            {
                                CGRect frame = CGRectMake(cr.size.width - iconSize - sideMargin,cr.size.height/2 - (35*adJustRate) - (80*adJustRate) - (10*adJustRate),iconSize,iconSize);
                                MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                                [iconLoader addIconCell:iconCell];  // (3)
                                [_icons addObject:iconCell];
                                [self.view addSubview:iconCell];  // (4)
                            }
                            {
                                CGRect frame = CGRectMake(cr.size.width - iconSize - sideMargin,cr.size.height/2 - (35*adJustRate) - (10*adJustRate),iconSize,iconSize);
                                MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                                [iconLoader addIconCell:iconCell];  // (3)
                                [_icons addObject:iconCell];
                                [self.view addSubview:iconCell];  // (4)
                            }
                            {
                                CGRect frame = CGRectMake(cr.size.width - iconSize - sideMargin,cr.size.height/2 + (43*adJustRate) - (10*adJustRate),iconSize,iconSize);
                                MrdIconCell* iconCell = [[[MrdIconCell alloc]initWithFrame:frame]autorelease]; // (2)
                                [iconLoader addIconCell:iconCell];  // (3)
                                [_icons addObject:iconCell];
                                [self.view addSubview:iconCell];  // (4)
                            }
                        }
                    }

                    [iconLoader startLoadWithMediaCode:ASTERISK_ID]; // (5)
                    _iconLoader = iconLoader; // (6)
                    
                    [self changeVisibleIconAdwithAlpha:0.0f];
                }
                
			}
		}
	}
	else {
		[self displayHouseAdView:NO];
	}
    
}
- (void)genuineAdPreparation{
    [self confirmGenuineAd];
}
- (void)closeButtonTapped{
    [self genuineAdHideAnimation];
}
- (void)showGenuineAd:(BOOL)enable
{
    //	assert(adWhirl_);
    if (needToDisplayGenuineAd == NO) {
        return;
    }
	showGenuineAd_ = enable;
	if ( showGenuineAd_ ) {
    forceGenuineAd:
		if ( genuineAdLoadSuccess_ ) {
            [self displayGenuineAdView:YES];
		}
		else {
			if ( !self.genuineAdLoading ) {
				[self confirmGenuineAd];
                [self displayGenuineAdView:NO];
            }
        }
    }
	else {
		[self displayGenuineAdView:NO];
	}
}
// 自社バナーを表示しないときは常にADを表示
// self.houseAdView.hiddenを直接読んでは駄目
- (void)displayHouseAdView:(BOOL)show;
{
    if ( show ) {
        if ( houseAdLoadSuccess_ ) {
            displayHouseAdState_ = YES;
            [self.houseAdView setHidden:NO];
        }
        else {
            [self.houseAdView setHidden:YES];
        }
    }
    else {
        [self.houseAdView setHidden:YES];
    }
}

- (void)displayGenuineAdView:(BOOL)show;
{
    CGRect cr = [[UIScreen mainScreen] bounds];

    CGRect hideRect = CGRectZero;
    if ([APP_ORIENTATION isEqualToString:@"LANDSCAPE"]) {
        hideRect = CGRectMake(cr.size.height/2 - kAdViewWidth/2,
                              -100,
                              kAdViewWidth,
                              100);
    } else {
        hideRect = CGRectMake(cr.size.width/2 - kAdViewWidth/2,
                              -100,
                              kAdViewWidth,
                              100);
    }

    if ( show ) {
        if ( houseAdLoadSuccess_ ) {
            displayHouseAdState_ = YES;
            
            [self.genuineAdView setHidden:NO];
            [self performSelector:@selector(genuineAdShowAnimation) withObject:nil afterDelay:1.0f];
        }
        else {
            [self.genuineAdView setHidden:YES];
            [self.genuineAdView setFrame:hideRect];
        }
    }
    else {
        [self.genuineAdView setHidden:YES];
        [self.genuineAdView setFrame:hideRect];
    }
}

- (void)genuineAdShowAnimation
{
    CGRect cr = [[UIScreen mainScreen] bounds];

    CGRect showRect = CGRectZero;
    if ([APP_ORIENTATION isEqualToString:@"LANDSCAPE"]) {
        showRect = CGRectMake(cr.size.height/2 - kAdViewWidth/2,
                              0,
                              kAdViewWidth,
                              100);
    } else {
        showRect = CGRectMake(cr.size.width/2 - kAdViewWidth/2,
                              0,
                              kAdViewWidth,
                              100);
    }

    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.genuineAdView setFrame:showRect];
                     }];
}

- (void)genuineAdHideAnimation
{
    CGRect cr = [[UIScreen mainScreen] bounds];

    CGRect hideRect = CGRectZero;
    if ([APP_ORIENTATION isEqualToString:@"LANDSCAPE"]) {
        hideRect = CGRectMake(cr.size.height/2 - kAdViewWidth/2,
                              -100,
                              kAdViewWidth,
                              100);
    } else {
        hideRect = CGRectMake(cr.size.width/2 - kAdViewWidth/2,
                              -100,
                              kAdViewWidth,
                              100);
    }

    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.genuineAdView setFrame:hideRect];
                     }];
}

- (void)showAd:(BOOL)enable
{
	
}

//アイコン広告修正 2014_1_20---ここから
- (void)showIconAd:(int)scene flag:(BOOL)enable
{
	switch (scene) {
		case 0:  //ゲーム中
			[self changeVisibleIconAdwithAlpha:0.0f];
			[self changeVisibleIconAdwithAlphaImobile:0.0f];
			[self changeVisibleIconAdwithAlphaAstaTitle:0.0f];
			
			break;
			
		case 1:  //タイトル
			if (enable == YES && needToDisplayIconAd == YES)
			{
				[self changeVisibleIconAdwithAlpha:0.0f];
				[self changeVisibleIconAdwithAlphaImobile:0.0f];
				[self changeVisibleIconAdwithAlphaAstaTitle:1.0f];
			}
			else if (enable == YES && needToDisplayIconAd == NO) {
				[self changeVisibleIconAdwithAlpha:0.0f];
				[self changeVisibleIconAdwithAlphaImobile:0.0f];
				[self changeVisibleIconAdwithAlphaAstaTitle:0.0f];
			}
			else
			{
				[self changeVisibleIconAdwithAlpha:0.0f];
				[self changeVisibleIconAdwithAlphaImobile:0.0f];
				[self changeVisibleIconAdwithAlphaAstaTitle:0.0f];
			}
			
			break;
			
		case 2: //エンディング
			if (enable == YES && needToDisplayIconAd == YES)
			{
				[self changeVisibleIconAdwithAlpha:1.0f];
				[self changeVisibleIconAdwithAlphaImobile:1.0f];
				[self changeVisibleIconAdwithAlphaAstaTitle:0.0f];
			}
			else if (enable == YES && needToDisplayIconAd == NO) {
				[self changeVisibleIconAdwithAlpha:1.0f];
				[self changeVisibleIconAdwithAlphaImobile:0.0f];
				[self changeVisibleIconAdwithAlphaAstaTitle:0.0f];
			}
			else
			{
				[self changeVisibleIconAdwithAlpha:0.0f];
				[self changeVisibleIconAdwithAlphaImobile:0.0f];
				[self changeVisibleIconAdwithAlphaAstaTitle:0.0f];
			}
			break;
			
		default:
			break;
	}
}

- (void)changeVisibleIconAdwithAlphaImobile:(float)aAlpha
{
    for (IMobileAdIconView *imobile in _iconsIM) {
        if (aAlpha==0) {
            imobile.hidden = YES;
        } else {
            imobile.hidden = NO;
        }
    }
}

- (void)changeVisibleIconAdwithAlpha:(float)aAlpha
{
    for (MrdIconCell *work in _icons)
    {
        work.alpha = aAlpha;
    }
}

- (void)changeVisibleIconAdwithAlphaAstaTitle:(float)aAlpha
{
    for (MrdIconCell *work in _iconsAstaTitle)
    {
        work.alpha = aAlpha;
    }
}

//アイコン広告修正 2014_1_20---ここまで

#pragma mark - attachAd

- (void)attachAd
{
	[self attachAdForView:self.view];
}

- (void)attachAdForView:(UIView *)targetView
{
    
	assert(targetView);
    //	AWLogSetLogLevel(AWLogLevelDebug);
	CGRect frame = [self frameRectForAdView];
    
    //AdStir
	// MEDIA-ID,SPOT-NOには、管理画面で発行されたメディアID, 枠ナンバーを埋め込んでください。
	// 詳しくはhttp://wiki.ad-stir.com/%E3%83%A1%E3%83%87%E3%82%A3%E3%82%A2ID%E5%8F%96%E5%BE%97をご覧ください。
	if (!self.adstirView) {
        self.adstirView = [[[AdstirView alloc]initWithOrigin:frame.origin]autorelease];
        self.adstirView.media = ADSTIR_MEDIA_ID;
        self.adstirView.spot = ADSTIR_SPOT_NO;
        self.adstirView.rootViewController = self;
        [self.adstirView start];
        [targetView addSubview:self.adstirView];
        
        self.adstirView.delegate = self;
    }
    
    if ( !self.houseAdView ) {
        
        CGRect cr = [[UIScreen mainScreen] bounds];
        CGRect ret = CGRectZero;
        
        if ([APP_ORIENTATION isEqualToString:@"LANDSCAPE"]) {
            ret = CGRectMake(cr.size.height/2 - kAdViewWidth/2 ,
                             0,
                             kAdViewWidth,
                             kAdViewHeight);
            
        }
        else{
            ret = CGRectMake(cr.size.width/2 - kAdViewWidth/2,
                             0,
                             kAdViewWidth,
                             kAdViewHeight);
        }
        
        self.houseAdView = [[[UIWebView alloc] initWithFrame:ret] autorelease];
        [self.houseAdView setTag:HouseAdViewTag];
		self.houseAdView.delegate = self;
		[self displayHouseAdView:NO];
		showHouseAd_ = NO;
		[targetView addSubview:self.houseAdView];
        [self showHouseAd:YES];
		// サポートされているiOSバージョンなら、webViewのスクロールを無効にする
		NSString* reqSysVer = @"5.0";
		NSString* currSysVer = [[UIDevice currentDevice] systemVersion];
		BOOL osVersionSupported = ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
		if ( osVersionSupported )
			[[self.houseAdView scrollView] setScrollEnabled:NO];
	}
	if ( self.houseAdView.superview != targetView ) {
		[targetView addSubview:self.houseAdView];
		self.houseAdView.frame = frame;
	}
}

//========================================================================
// AdVision
//========================================================================

#pragma mark - AdVision

#ifndef NADVISION
- (void)performEventAdVision:(AdWhirlView *)adWhirlView
{
#if SHOW_PERFORM_CUSTOMEVENT
	NSLog(@"performEventAdVision");
#endif
	if ( !adVision_ ) {
		adVision_ = [[AdVisionView alloc] initWithFrame:CGRectMake(0.0f,0.0f,320.0f,48.0f)
										   withDelegate:self
										 withController:self];
		[adVision_ setSiteId:kAdVisionSiteID];
		[adVision_ start];
	}
	[adWhirlView replaceBannerViewWith:adMediba_.view];
	[self displayHouseAdView:showHouseAd_];
}

- (void)didChangeNoAdStatus:(BOOL)_noAdF
{
	if ( _noAdF ) {
		NSLog(@"failed load AdVision");
		[adWhirl_ rollOver];
	}
	else {
		[adWhirl_ replaceBannerViewWith:adVision_];
		[self displayHouseAdView:showHouseAd_];
	}
}
#endif

//========================================================================
// ADIMoblie (i-moblie)
//========================================================================

#pragma mark - imoblie (ADIMoblie)

#ifndef NADIMOBLIE
- (void)performEventAdIMoblie:(CGRect)rect
{
#if SHOW_PERFORM_CUSTOMEVENT
	NSLog(@"performEventAdIMoblie");
#endif
    IMobileAdView * banner = [[IMobileAdView alloc] initWithFrame:rect publisherId:kAdIMobliePushlisherID
                                                          mediaId:kAdIMoblieMediaID
                                                           spotId:kAdIMoblieSpotID
                                                         testMode:NO];
    [self.view addSubview:banner];
}

- (void)imAdViewDidFinishReceiveAd:(IMAdView *)imAdView
{
    [self.view addSubview:imAdView];
    NSLog(@"IMoblieを表示しています");
}

- (void)imAdViewDidFailToReceiveAd:(IMAdView *)imAdView
{
    [self performEventAdNend:imAdView.frame];
    NSLog(@"failed load AdIMoblie");
}
#endif

//========================================================================
// AdMediba
//========================================================================

#pragma mark - AdMediba

#ifndef NADMAKER
- (void)performEventAdMediba:(AdWhirlView *)adWhirlView
{
	assert(0);	// 未テストの関数
#if SHOW_PERFORM_CUSTOMEVENT
	NSLog(@"performEventAdMediba");
#endif
	if ( !adMediba_ ) {
		adMediba_ = [[MasManagerViewController alloc] init];
		// replaceBannerViewWithでaddSubviewされるので
        //		[self.view addSubview:adMediba_.view];
		[adMediba_ setPosition:kMasMVPosition_bottom];
		adMediba_.auID = kAdMedibaCode;
		adMediba_.delegate = self;
		[adMediba_ loadRequest];
	}
	[adWhirlView replaceBannerViewWith:adMediba_.view];
	[self displayHouseAdView:showHouseAd_];
}

- (void)masManagerViewControllerReceiveAd:(MasManagerViewController *)masManagerViewController
{
}

- (void)masManagerViewControllerFailedToReceiveAd:(MasManagerViewController *)masManagerViewController
{
	NSLog(@"failed load MedibaAd");
	[adWhirl_ rollOver];
}
#endif


//========================================================================
// AdNend
//========================================================================

#pragma mark - AdNend

- (void)performEventAdNend:(CGRect)rect
{
#if SHOW_PERFORM_CUSTOMEVENT
	NSLog(@"performEventAdNend");
#endif
	if ( !adNad_ ) {
		adNad_ = [[NADView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y, NAD_ADVIEW_SIZE_320x50.width, NAD_ADVIEW_SIZE_320x50.height)];
		[adNad_ setNendID:kAdNendApiKey spotID:kAdNendSpotID];
		adNad_.backgroundColor = [UIColor clearColor];
		[adNad_ setDelegate:self];
		[adNad_ load:nil];
	}
}

- (void)nadViewDidFinishLoad:(NADView *)adView{
    [self.view addSubview:adView];
    NSLog(@"nendを表示しています");
}

- (void)nadViewDidReceiveAd:(NADView *)adView{
    
}

- (void)nadViewDidFailToReceiveAd:(NADView *)adView{
    [adView pause];
    [self performEventAdIMoblie:adView.frame];
}

//========================================================================
// AdStir
//========================================================================

#pragma mark -
#pragma mark AdStirDelegate
//AdStirDelegate
- (void)adstirDidReceiveAd:(AdstirView*)adstirview{
	//広告取得に成功した時
    NSLog(@"adstirを表示しています。");
}
- (void)adstirDidFailToReceiveAd:(AdstirView*)adstirview{
	//広告取得に失敗した時
    NSLog(@"adstirDidFailToReceiveAd");
    if (ADSTIR_FAIL_TO_RECIEVED_ADTYPE == 1) {
        [self performEventAdNend:adstirview.frame];
    }
    else if (ADSTIR_FAIL_TO_RECIEVED_ADTYPE == 2){
        [self performEventAdIMoblie:adstirview.frame];
    }
    [self.adstirView stop];
}

- (CGRect)frameRectForAdView
{
    CGRect cr = [[UIScreen mainScreen] bounds];
	CGRect ret = CGRectZero;
    
    if ([APP_ORIENTATION isEqualToString:@"LANDSCAPE"]) {
        NSLog(@"よこなが");
        ret = CGRectMake(cr.size.height/2 - kAdViewWidth/2,
                         cr.size.width - kAdViewHeight,
                         kAdViewWidth,
                         kAdViewHeight);
    }
    else{
        NSLog(@"たてなが");
		ret = CGRectMake(cr.size.width/2 - kAdViewWidth/2,
                         cr.size.height - kAdViewHeight,
                         kAdViewWidth,
                         kAdViewHeight);
    }
	return ret;
}


- (void)showGameFeat
{
    [GFController showGF:self site_id:GAMEFEAT_ID delegate:self];
}

#pragma mark -
#pragma mark GFViewDelegate

//=======================================================
// GFViewDelegate
//=======================================================
- (void)didShowGameFeat{
    // GameFeatが表示されたタイミングで呼び出されるdelegateメソッド
    NSLog(@"didShowGameFeat");
}
- (void)didCloseGameFeat{
    // GameFeatが閉じられたタイミングで呼び出されるdelegateメソッド
    NSLog(@"didCloseGameFeat");
}

-(void)review {

    //iOS 7でアプリのレビュー画面を開く際の対策
    // http://lab.dolice.net/blog/2013/09/25/objc-ios7-review-url/

    NSString * app_id = [NSString stringWithFormat:@"%d",REVIEW_POPUP_IOS_APP_ID];
    NSString * url = nil;

    // iOSのバージョンを判別
    BOOL isIOS7 = FALSE;
    {
        NSString *osversion = [UIDevice currentDevice].systemVersion;
        NSArray *a = [osversion componentsSeparatedByString:@"."];
        isIOS7 = [(NSString *)[a objectAtIndex:0] intValue] >= 7;
    }

    if (isIOS7) {
        // iOS 7以降
        url = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@", app_id];
    } else {
        // iOS 7未満
        url = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software", app_id];
    }

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)showReviewPopup{
    //回数カウンターを入れる
    NSString * versionString = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString * keyString = [NSString stringWithFormat:@"reviewPopupCount_%@",versionString];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    int count = [userDefaults integerForKey:keyString];
    count++;
    if (REVIEW_POPUP_COUNT <= count && count < 1000) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"新記録おめでとうございます！"
                                                         message:@"\nもしよかったら\nレビューを書いてもらえますか？"
                                                        delegate:self
                                               cancelButtonTitle:@"キャンセル"
                                               otherButtonTitles:@"レビューを書く",@"二度と表示しない", nil];
        [alert setTag:REVIEW_POPUP_ALERTVIEW_TAG];
        [alert show];
        [alert release];
    }
    [self setCountReviewPopupUserDefault:count];
}

- (void)setCountReviewPopupUserDefault:(int)count{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * versionString = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString * keyString = [NSString stringWithFormat:@"reviewPopupCount_%@",versionString];
    [userDefaults setInteger:count forKey:keyString];
    [userDefaults synchronize];
}

#pragma mark -
#pragma mark TapJoy

//=======================================================
// TapJoy
//=======================================================
//tapjoyのビデオ広告があるかどうかの確認
-(void)getVideoReward{
    isVideoRewardEnabled_ = NO;
    if (isVideoRewardAvailable_ == YES) {
        [Tapjoy getFullScreenAd];
    }
}
//tapjoyのビデオ広告があった。
-(void)fullScreenAdEnabled:(NSNotification*)notification{
    isVideoRewardEnabled_ = YES;
	//    [Tapjoy showFullScreenAdWithViewController:self];
}
//tapjoyのビデオ広告がなかった。
-(void)fullScreenAdError:(NSNotification*)notification{
    isVideoRewardEnabled_ = NO;
	//    [[[UIAlertView alloc] initWithTitle:@"Error"
	//                                message:@"No Full Screen Ad available"
	//                               delegate:self
	//                      cancelButtonTitle:@"OK"
	//					  otherButtonTitles:nil] show];
}

- (BOOL)getIsVideoRewardEnabled{
    BOOL result = NO;
    if (isVideoRewardAvailable_ && isVideoRewardEnabled_) {
        result = YES;
    }
    return result;
}

- (void)showVideoReward{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil
                                                     message:@"動画を最後まで視聴すると\nコンテニューできるよ！"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
    [alert setTag:TAPJOY_POPUP_ALERTVIEW_TAG];
    [alert show];
    [alert release];
}

-(void)tapjoyAlertViewButtonTapped{
    isVideoRewardServiceEnabled_ = NO;
    [Tapjoy showFullScreenAdWithViewController:self];
    [self performSelector:@selector(setIsVideoRewardServiceEnabled) withObject:nil afterDelay:kTapJoyServiceTimer];
}

- (void)setIsVideoRewardServiceEnabled{
    isVideoRewardServiceEnabled_ = YES;
}
#pragma mark +++ tapjoy delegate +++
// This method is called right before the video has appeared.
- (void)viewWillAppearWithType:(int)viewType{
    NSLog(@"Video is about to be shown");
}
// This method is called right after the video has appeared.
- (void)viewDidAppearWithType:(int)viewType{
    isTapjoyViewAppear_ = YES;
    NSLog(@"Video has been shown");
}
// This method is called right before the video has ended.
- (void)viewWillDisappearWithType:(int)viewType {
    NSLog(@"Video is about to go away");
}
// This method is called right after the ad has closed.
- (void)viewDidDisappearWithType:(int)viewType{
    NSLog(@"Video has closed");
    // Get updated balance after the video view is closed.
    //[Tapjoy getTapPoints];
    if (isTapjoyViewAppear_ == NO) {
        return;
    }
    isTapjoyViewAppear_ = NO;
    [Tapjoy getTapPointsWithCompletion:^(NSDictionary *parameters, NSError *error) {
		if (error){
            [self judgeTapJoyContinue:NO];
        }
        
		else
		{
            if ([[NSString stringWithString:[parameters objectForKey:@"currencyName"]] isEqualToString:TAP_JOY_CURRENCY_NAME]) {
                if ([[parameters objectForKey:@"amount"] intValue] != 0) {
                    //ここでコンテニューを実装する
                    //そして使う。
                    [Tapjoy spendTapPoints:[[parameters objectForKey:@"amount"] intValue] completion:^(NSDictionary *parameters, NSError *error) {
                        if (error){
                            [self judgeTapJoyContinue:NO];
                        }
                        else{
                            [self judgeTapJoyContinue:YES];
                            //                            [_statusLabel setText:[NSString stringWithFormat:@"spendTapPoints returned %@: %d", parameters[@"currencyName"], [parameters[@"amount"] intValue]]];
                        }
                    }];
                }
                else{
                    [self judgeTapJoyContinue:NO];
                }
            }
            else{
                //失敗
                [self judgeTapJoyContinue:NO];
            }
        }
    }];
}

- (void)judgeTapJoyContinue:(BOOL)availeble{
#ifdef USE_COIN_CONTINUE
    if (availeble == YES || isVideoRewardServiceEnabled_ == YES) {
        HelloWorld::sharedHelloWorldLayer()->rewardMovieEnded(YES);
    }
    else{
        HelloWorld::sharedHelloWorldLayer()->rewardMovieEnded(NO);
    }
#endif
}


@end
