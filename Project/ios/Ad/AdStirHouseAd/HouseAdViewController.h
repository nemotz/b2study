//
//  HouseAdViewController.h
// 
//
//  Created by kuzawa yuusuke on 12/02/14.
//  Copyright (c) 2012年 cocolo-bit.com All rights reserved.
//

#define NADVISION
#define NADMAKER

#import <Foundation/Foundation.h>
#ifndef NADMAKER
#import "AdMakerView.h"
#import "MasManagerViewController.h"
#endif
#import "NADView.h"
#ifndef NADLANTIS
#import "AdlantisView.h"
#endif
#import "AMoAdView.h"

#ifndef NADIMOBLIE
#import "imobileAds/IMAdView.h"
#endif
#ifndef NADVISION
#import "AdVisionView.h"
#endif

#import <MrdIconSDK/MrdIconSDK.h>

#import <GameFeatKit/GFView.h>
#import <GameFeatKit/GFController.h>
#import <JSONKIT/JSONKit.h>

//AdStir
#import "AdstirView.h"

#import <Tapjoy/Tapjoy.h>



// https://www.adwhirl.com/doc/ios/AdWhirliOSSDKSetup.html
// from AdWhirl
/*
	- Add the system frameworks required by the supported ad networks:

	AddressBook.framework
	AudioToolbox.framework
	AVFoundation.framework
	CoreLocation.framework
	MapKit.framework
		weak-linked for OS 2.X support
	MediaPlayer.framework
	MessageUI.framework
		weak-linked for OS 2.X support
	QuartzCore.framework
	SystemConfiguration.framework
	iAd.framework
		required for iAd,
		weak-linked for OS 3.X support
	libsqlite3.dylib
	libz.dylib

	- The following additional frameworks are required by the iAd adapter:

	iAd
	QuartzCore
	SystemConfiguration

	- AdMob

	AudioToolbox
	MessageUI
	SystemConfiguration
	CoreGraphics

	- nend

	Security Framework

	- AdLantis

	MobileCoreService.framework
	CFNetwork.framework
	SystemConfiguration.framework
	libz.dylib

	- imoblie

	CFNetwork.framework
	libz.1.2.3.dylib
	MobileCoreServices.framework
	SystemConfiguration.framework
	imobileAds.frameworks

	linker flag -ObjC
*/

@interface HouseAdViewController :
#ifdef BIRTHDAYGAME
		UITabBarController
#else
		UIViewController
#endif
		<
#ifndef NADVISION
			AdVisionDelegate,
#endif
#ifndef NADLANTIS
#endif
			AMoAdViewDelegate, NADViewDelegate, UIWebViewDelegate, UIAlertViewDelegate, GFViewDelegate,AdstirViewDelegate,TJCViewDelegate>
{
@private

	UIWebView*		houseAdView_;
    UIWebView*		genuineAdView_;

	NADView*		adNad_;
#ifndef NADLANTIS
#endif
	AMoAdView*      adCloud_;
#ifndef NADIMOBLIE
#endif
#ifndef NADVISION
	AdVisionView*	adVision_;
#endif
	BOOL			showHouseAd_;
	BOOL			houseAdLoadSuccess_;
	int				displayHouseAdState_;

    BOOL			showGenuineAd_;
	BOOL			genuineAdLoadSuccess_;
	int				displayGenuineAdState_;
    
    BOOL            needToDisplayGenuineAd;
    
    //GoogleAppEngineのENDがない場合にアイコン広告を表示させないフラグ
    BOOL            needToDisplayIconAd;
    
	// @@@@
//	BOOL	debugRollOver_;
    
    MrdIconLoader* _iconLoader;
    NSMutableArray *_icons;
    NSMutableArray *_iconsIM;
    NSMutableArray *_iconsAstaTitle;
    
    BOOL isLoadAdlantis;
	
	//TapJoy
	BOOL isVideoRewardEnabled_;
    BOOL isVideoRewardAvailable_;
    BOOL isVideoRewardServiceEnabled_;
    BOOL isTapjoyViewAppear_;
}

@property (nonatomic, retain) UIWebView* houseAdView;
@property (nonatomic, retain) UIWebView* genuineAdView;
@property (nonatomic, readonly) UIView* adNetworkView;
@property (nonatomic, retain) NSString *popAppUrl;
@property (nonatomic, retain) NSString *popAppName;
@property (nonatomic, retain) NSString *adSplashType;
@property (nonatomic, retain) AdstirView* adstirView;

// nibファイルを使用していない場合は、インスタンスを作成したあとに呼び出す
//（その場合はそのインスタンスを作り直すごとにattatchAdを呼ぶ）
//（nibを使用している場合は、viewDidLoadで自動で呼び出される）
- (void)attachAd;
- (void)attachAdForView:(UIView*)targetView;

- (void)showHouseAd:(BOOL)enable;

- (void)showGenuineAd:(BOOL)enable;
- (void)genuineAdPreparation;
- (void)genuineAdShowAnimation;

- (void)showAd:(BOOL)enable;
- (void)showIconAd:(int)scene flag:(BOOL)enable;
- (void)showGameFeat;

- (void)closeButtonTapped;

- (void)showReviewPopup;
- (void)review;
- (void)setCountReviewPopupUserDefault:(int)count;

//TapJoy
- (void)getVideoReward;
- (void)showVideoReward;
- (BOOL)getIsVideoRewardEnabled;

@end
