//
//  ImobileSdkAds.h
//
//  Copyright (c) 2013年 i-mobile. All rights reserved.
//

#pragma mark - ImobileSdkAds

@protocol IMobileSdkAdsDelegate;

// 広告の取得に関するステータス
typedef enum {
    IMOBILESDKADS_STATUS_READY,
    IMOBILESDKADS_STATUS_NOT_READY,
    IMOBILESDKADS_STATUS_NOT_FOUND,
    IMOBILESDKADS_STATUS_RETRY_WAIT,
    IMOBILESDKADS_STATUS_OTHERS
} ImobileSdkAdsStatus;

// 広告の向き
typedef enum {
    IMOBILESDKADS_AD_ORIENTATION_AUTO,
    IMOBILESDKADS_AD_ORIENTATION_PORTRAIT,
    IMOBILESDKADS_AD_ORIENTATION_LANDSCAPE,
} ImobileSdkAdsAdOrientation;

// SDK本体
@interface ImobileSdkAds : NSObject

// 広告を受け取る広告枠の情報を登録します
// 戻り値：広告枠が登録された場合はYES
+ (BOOL)registerWithPublisherID:(NSString *)publisherid MediaID:(NSString *)mediaid SpotID:(NSString *)spotid;

// 登録済みのすべての広告枠の広告取得を開始します
+ (void)start;

// 登録済みのすべての広告枠の広告取得を停止します
+ (void)stop;

// 登録済みの指定された広告枠の広告取得を開始します
// 戻り値：スポットが登録済みの場合、YES
+ (BOOL)startBySpotID:(NSString *)spotid;

// 登録済みの指定された広告枠の広告取得を停止します
// 戻り値：スポットが登録済みの場合、YES
+ (BOOL)stopBySpotID:(NSString *)spotid;

// 登録済みの指定された広告枠が表示可能な場合、広告を表示します
// 戻り値：スポットが登録済みの場合、YES
+ (BOOL)showBySpotID:(NSString *)spotid;

// 登録済みの指定された広告枠の広告が表示されている場合、広告を閉じます
// 戻り値：スポットが登録済みの場合、YES
+ (BOOL)closeBySpotID:(NSString *)spotid;

#pragma mark @setter

// SDKのメッセージを受け取るデリゲートを設定します
// 戻り値：スポットが登録済みの場合、YES
+ (BOOL)setSpotDelegate:(NSString *)spotid delegate:(id<IMobileSdkAdsDelegate>)delegate;

// この値に設定された値の分だけ、"showBySpotID"を呼んでもスキップします。
// その次に"showBySpotID"を呼んだときに広告が表示されます。
// 初期値：0
// 戻り値：スポットが登録済みの場合、YES
+ (BOOL)setSpotSkipCount:(NSString *)spotid skipCount:(NSUInteger)skipCount  __attribute__((deprecated("管理画面より設定ください。")));

// テストモードの設定をします
// isTestModeが、YES：テスト広告を配信します, NO：通常広告を配信します
// 初期値：NO
+ (void)setTestMode:(BOOL)isTestMode;

// 広告が表示される向きの設定
// 初期値：AUTO
+ (void)setAdOrientation:(ImobileSdkAdsAdOrientation)orientation;


// ルートコントロールビューを設定します
+ (void)setRootViewController:(UIViewController *)rootViewController;

#pragma mark @getter

// 登録済みの指定された広告枠の状態を取得します
// スポットが登録されていない場合は、nilが返ります。
+ (ImobileSdkAdsStatus)getStatusBySpotID:(NSString *)spotid;

// skipCountに達するまで、Showメソッドが呼ばれた回数
// スポットが登録されていない場合は、nilが返ります。
+ (NSNumber *)getCountAttemptsToShowBySpotID:(NSString *)spotid;

// Showメソッドが呼ばれた回数の合計
// スポットが登録されていない場合は、nilが返ります。
+ (NSNumber *)getCountAttemptsToShowTotalBySpotID:(NSString *)spotid;

@end


#pragma mark - IMobileSdkAdsDelegate

// 広告表示準備完了時の広告の種類
typedef enum {
    IMOBILESDKADS_READY_AD,
    IMOBILESDKADS_READY_HOUSE_AD
} ImobileSdkAdsReadyResult;

// 広告取得失敗時のエラーの種類
typedef enum {
    IMOBILESDKADS_ERROR_PARAM,
    IMOBILESDKADS_ERROR_AUTHORITY,
    IMOBILESDKADS_ERROR_RESPONSE,
    IMOBILESDKADS_ERROR_NETWORK_NOT_READY,
    IMOBILESDKADS_ERROR_NETWORK,
    IMOBILESDKADS_ERROR_UNKNOWN,
    IMOBILESDKADS_ERROR_AD_NOT_READY
} ImobileSdkAdsFailResult;


// SDKのメッセージを受け取るデリゲート(アプリ単位)
@protocol IMobileSdkAdsDelegate <NSObject>

//広告の表示が準備完了した際に呼ばれます
- (void)imobileSdkAdsSpot:(NSString *)spotid didReadyWithValue:(ImobileSdkAdsReadyResult)value;

//広告の取得を失敗した際に呼ばれます
- (void)imobileSdkAdsSpot:(NSString *)spotid didFailWithValue:(ImobileSdkAdsFailResult)value;

//広告の表示要求があった際に、準備が完了していない場合に呼ばれます
- (void)imobileSdkAdsSpotIsNotReady:(NSString *)spotid;

//広告クリックした際に呼ばれます
- (void)imobileSdkAdsSpotDidClick:(NSString *)spotid;

//広告を閉じた際に呼ばれます(広告の表示がスキップされた場合も呼ばれます)
- (void)imobileSdkAdsSpotDidClose:(NSString *)spotid;

@end

