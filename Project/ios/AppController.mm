//
//  AppController.mm
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//

#import <UIKit/UIKit.h>
#import "AppController.h"
#import "cocos2d.h"
#import "EAGLView.h"

#import "AppDelegate.h"
#import "RootViewController.h"
#import "HelloWorldScene.h"

#import "GameCenter.h"
#import "SimpleAudioEngine.h"

#import "PrivateUserDefault.h"
#import "GoodiaUserInfo.h"
#import "HouseAdConfig.h"

#import "Bead.h"
#import <AidAd/AidAd.h>
#import "ImobileSdkAds/ImobileSdkAds.h"

#import "Flurry.h"
#import <Tapjoy/Tapjoy.h>

#include "CDAudioManager.h"

@interface AppController ()
@property (nonatomic, retain) GoodiaUserInfo *userInfo;
@end

@implementation AppController

@synthesize window;
@synthesize viewController;
@synthesize appUrlNew = _appUrlNew;

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;


//iOS6.0 LandScape時にGameCenterが起動できない不具合対応
//URL: http://kghitokoto.blog113.fc2.com/blog-entry-79.html
-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return UIInterfaceOrientationMaskAll;
    }
    else //iphone
    {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
}


#pragma mark - alertView
// アラートのボタンが押された時に呼ばれるデリゲート例文
-(void)alertView:(UIAlertView*)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            //１番目のボタンが押されたときの処理を記述する
            break;
        case 1:
            //はいのときの処理
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.appUrlNew]];
            break;
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Flurry
    {
        [Flurry startSession:FLURRY_API_KEY];
    }
    
    //Aid
    {
        // 広告の取得を開始します
        [[AidAd agentForMedia:AID_MEDIA_CODE]startLoading];
    }
    
    //Bead
    {
        [Bead initializeAd];

        //[[Bead sharedInstance] addSID:BEAD_SID];
        [[Bead sharedInstance] addSID:BEAD_SID interval:0];
    }

	//i-mobile
	{
		[ImobileSdkAds registerWithPublisherID:kAdIMobliePushlisherIDSplash
									   MediaID:kAdIMoblieMediaIDSplash
										SpotID:kAdIMoblieSpotIDSplash];
		
		if ([APP_ORIENTATION isEqualToString:@"LANDSCAPE"]) {
			[ImobileSdkAds setAdOrientation:IMOBILESDKADS_AD_ORIENTATION_LANDSCAPE];
		} else {
			[ImobileSdkAds setAdOrientation:IMOBILESDKADS_AD_ORIENTATION_PORTRAIT];
		}
		[ImobileSdkAds startBySpotID:kAdIMoblieSpotIDSplash];
	}

    // Override point for customization after application launch.

    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    EAGLView *__glView = [EAGLView viewWithFrame: [window bounds]
                                     pixelFormat: kEAGLColorFormatRGBA8
                                     depthFormat: GL_DEPTH_COMPONENT16
                              preserveBackbuffer: NO
                                      sharegroup: nil
                                   multiSampling: NO
                                 numberOfSamples:0 ];

//    // マルチタップを有効にする
//    [__glView setMultipleTouchEnabled:YES];

    // Use RootViewController manage EAGLView
    viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    viewController.wantsFullScreenLayout = YES;
    viewController.view = __glView;

    // ここに追加
    [viewController attachAd];

    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: viewController.view];
        window.rootViewController = viewController;
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:viewController];
    }

    [window makeKeyAndVisible];

    [[UIApplication sharedApplication] setStatusBarHidden: YES];

    cocos2d::CCApplication::sharedApplication()->run();

    //GameCenter
    [[GameCenter sharedGameCenter] startGameCenter];

    //AudioManager
    [[CDAudioManager sharedManager] setResignBehavior: kAMRBStopPlay  autoHandle:YES];

	//TapJoy
	{
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(tjcConnectSuccess:)
                                                     name:TJC_CONNECT_SUCCESS
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(tjcConnectFail:)
                                                     name:TJC_CONNECT_FAILED
                                                   object:nil];
        
        // NOTE: This is the only step required if you're an advertiser.
        // NOTE: This must be replaced by your App ID. It is retrieved from the Tapjoy website, in your account.
        [Tapjoy requestTapjoyConnect:TAP_JOY_MEDIA_ID
                           secretKey:TAP_JOY_SECRET_KEY
                             options:@{ TJC_OPTION_ENABLE_LOGGING : @(YES) }
         // If you are not using Tapjoy Managed currency, you would set your own user ID here.
         //TJC_OPTION_USER_ID : @"A_UNIQUE_USER_ID"
         ];
    }

	return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    
    // GameFeat コンバージョン確認
    UIDevice *device = [UIDevice currentDevice];
    BOOL backgroundSupported = NO;
    if ([device respondsToSelector:@selector(isMultitaskingSupported)]) {
        backgroundSupported = device.multitaskingSupported;
    }
    if (backgroundSupported) {
        [GFController backgroundTask];
    }

	//TapJoy
	[Tapjoy dismissContent];

    
    cocos2d::CCApplication::sharedApplication()->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    
    // GameFeat コンバージョン確認タスクの停止
     [GFController conversionCheckStop];
    
    cocos2d::CCApplication::sharedApplication()->applicationWillEnterForeground();
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
     cocos2d::CCDirector::sharedDirector()->purgeCachedData();
}


- (void)dealloc {
    [super dealloc];
}

#pragma mark -
#pragma mark TapJoy
-(void)tjcConnectSuccess:(NSNotification*)notifyObj
{
	NSLog(@"Tapjoy connect Succeeded");
}


- (void)tjcConnectFail:(NSNotification*)notifyObj
{
	NSLog(@"Tapjoy connect Failed");
}

@end

