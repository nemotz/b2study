//
//  GameCenter.m
//  TemplateProject
//
//  Created by Goodia.Inc on 2013/09/09.
//
//

#import "GameCenter.h"

@interface GameCenter () <GKLeaderboardViewControllerDelegate>
+ (GameCenter *) sharedGameCenter;
@end


@implementation GameCenter

#pragma mark - Singleton
static GameCenter* _sharedGameCenter = nil;

+ (GameCenter *)sharedGameCenter
{
    @synchronized(self) {
        if (!_sharedGameCenter) {
            [[self alloc] init];
        }
    }
    return _sharedGameCenter;
}

+ (id)allocWithZone:(NSZone*)zone
{
    @synchronized(self) {
        if (!_sharedGameCenter) {
            _sharedGameCenter = [super allocWithZone:zone];
            
            return _sharedGameCenter;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone*)zone
{
    return self;
}

- (id)retain
{
    return self;
}

- (unsigned)retainCount
{
    return UINT_MAX;
}

- (oneway void)release
{
}

- (id)autorelease
{
    return self;
}


- (BOOL) startGameCenter
{
    if( [self isGameCenterAPIAvailable] == NO )
    {
        NSLog(@"startGameCenter Faile");
        return NO;
    }
    [self authenticateLocalPlayer];
    
    NSLog(@"startGameCenter OK");
    return YES;
}

- (BOOL) isGameCenterAPIAvailable
{
    // Check for presence of GKLocalPlayer class.
    BOOL localPlayerClassAvailable = (NSClassFromString(@"GKLocalPlayer")) != nil;
    
    // The device must be running iOS 4.1 or later.
    NSString *reqSysVer = @"4.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    BOOL osVersionSupported = ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
    
    return (localPlayerClassAvailable && osVersionSupported);
}

- (void) authenticateLocalPlayer
{
    GKLocalPlayer* localPlayer = [GKLocalPlayer localPlayer];
    [localPlayer authenticateWithCompletionHandler:^(NSError *error) {
        if( localPlayer.isAuthenticated )
        {
            /*
             ///< NSStringをchar*に変換...
             CCLOG("Alias : %s", localPlayer.alias);
             CCLOG("Player ID : %s", localPlayer.playerID);
             */
            NSLog(@"Alias : %@", localPlayer.alias);
            NSLog(@"Player ID : %@", localPlayer.playerID);
        }
    }];
}

- (void) reportScore:(int64_t)score Category:(NSString *)category
{
    GKScore * scoreReporter = [[[GKScore alloc] initWithCategory:category] autorelease];
    scoreReporter.value = score;
    [scoreReporter reportScoreWithCompletionHandler:^(NSError *error)
     {
         // スコア送信失敗時
         if( error != nil )
         {
//             CCMessageBox("reportScore Error", "Game Center");
         }
     }];
}

- (void) showLeaderboard
{
    GKLeaderboardViewController* pController = [[[GKLeaderboardViewController alloc] init] autorelease];
    
    if( pController != nil )
    {
        pController.leaderboardDelegate = self;
        
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        [rootViewController presentViewController:pController animated:YES completion:nil];
    }
}

#pragma mark GKLeaderboardViewControllerDelegate
- (void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark 以下Deprecated
+ (BOOL) startGameCenter
{
    return [[self sharedGameCenter] startGameCenter];
}

+ (BOOL) isGameCenterAPIAvailable
{
    return [[self sharedGameCenter] isGameCenterAPIAvailable];
}

+ (void) authenticateLocalPlayer
{
    [[self sharedGameCenter] authenticateLocalPlayer];
}

+ (void) reportScore:(int64_t)score Category:(NSString *)category
{
    [[self sharedGameCenter] reportScore:score Category:category];
}

+ (void) showLeaderboard
{
    // Singletonのインスタンスに投げる
    [[self sharedGameCenter] showLeaderboard];
}

@end
