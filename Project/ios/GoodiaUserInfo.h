//
//  GoodiaUserInfo.h
//  9COLORS
//
//  Created by masaya takaki on 1/25/13.
//  Copyright (c) 2013 littlenstar.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ASIHTTPRequest/Reachability.h>
#import "HouseAdConfig.h"

#define USER_INFO_KEY @"user_info"
#define USER_UUID_KEY @"user_uuid"
#define ITEMS_KEY @"items"

@interface GoodiaUserInfo : NSObject <NSCoding>

+ (NSString *) applicationPrefix;
+ (GoodiaUserInfo *) goodiaUserInfo;
- (void) save;

@property (nonatomic, readonly) NSString *goodiaAPIHost;
@property (nonatomic, readonly) NSString *goodiaFmsHost;

@property (nonatomic, copy, readonly) NSString *userUUID;
@property (nonatomic, copy, readonly) NSDictionary *itemStocks;
- (NSUInteger) numberOfItemPaymentsForItemIdentifier:(NSString *) itemIdentifier
                                         isConfirmed:(BOOL *)isConfirmed;

// 指定アイテムの個数確認
- (void) confirmNumberOfItemPaymentsForItemIdentifier:(NSString *) itemIdentifier
                                              handler:(void (^)(NSUInteger numberOfItemPayments, NSError *error))handler;

// Rewardが有効かどうか確認
- (void) confirmWhichAppDriverAvailableWithHandler:(void (^)(BOOL isAvailable, NSError *error))handler;
- (void) confirmWhichNewAppAvailableWithHandler:(void (^)(NSDictionary *dict, NSError *error))handler;
- (void) confirmWhichAdSplashWithHandler:(void (^)(NSDictionary *dict, NSError *error))handler;
- (void) confirmWhichAdSplashFrequencyWithHandler:(void (^)(NSDictionary *dict, NSError *error))handler;

//純広告
- (void) confirmWhichGenuineAdAvailableWithHandler:(void (^)(NSDictionary *dict, NSError *error))handler;

// Reachability
@property (nonatomic, retain, readonly) Reachability *goodiaAPIReachability;
@property (nonatomic, retain, readonly) Reachability *goodiaFmsReachability;

@end
